﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHTAPI.Models.Cosmo;

namespace VHTAPI.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class BrokerageMultipleIpsController : ControllerBase
    {
        private readonly COSMOContext _context;

        public BrokerageMultipleIpsController(COSMOContext context)
        {
            _context = context;
        }

        // GET: api/BrokerageMultipleIps
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BrokerageMultipleIp>>> GetBrokerageMultipleIps()
        {
          if (_context.BrokerageMultipleIps == null)
          {
              return NotFound();
          }
            return await _context.BrokerageMultipleIps.ToListAsync();
        }

        // GET: api/BrokerageMultipleIps/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BrokerageMultipleIp>> GetBrokerageMultipleIp(string id)
        {
          if (_context.BrokerageMultipleIps == null)
          {
              return NotFound();
          }
            var brokerageMultipleIp = await _context.BrokerageMultipleIps.FindAsync(id);

            if (brokerageMultipleIp == null)
            {
                return NotFound();
            }

            return brokerageMultipleIp;
        }

        // PUT: api/BrokerageMultipleIps/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrokerageMultipleIp(string id, BrokerageMultipleIp brokerageMultipleIp)
        {
            if (id != brokerageMultipleIp.Ipaddress)
            {
                return BadRequest();
            }

            _context.Entry(brokerageMultipleIp).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrokerageMultipleIpExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BrokerageMultipleIps
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BrokerageMultipleIp>> PostBrokerageMultipleIp(BrokerageMultipleIp brokerageMultipleIp)
        {
          if (_context.BrokerageMultipleIps == null)
          {
              return Problem("Entity set 'COSMOContext.BrokerageMultipleIps'  is null.");
          }
            _context.BrokerageMultipleIps.Add(brokerageMultipleIp);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BrokerageMultipleIpExists(brokerageMultipleIp.Ipaddress))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetBrokerageMultipleIp", new { id = brokerageMultipleIp.Ipaddress }, brokerageMultipleIp);
        }

        // DELETE: api/BrokerageMultipleIps/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrokerageMultipleIp(string id)
        {
            if (_context.BrokerageMultipleIps == null)
            {
                return NotFound();
            }
            var brokerageMultipleIp = await _context.BrokerageMultipleIps.FindAsync(id);
            if (brokerageMultipleIp == null)
            {
                return NotFound();
            }

            _context.BrokerageMultipleIps.Remove(brokerageMultipleIp);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BrokerageMultipleIpExists(string id)
        {
            return (_context.BrokerageMultipleIps?.Any(e => e.Ipaddress == id)).GetValueOrDefault();
        }
    }
}
