﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHTAPI.Models.Cosmo;

namespace VHTAPI.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class LoginTokensController : ControllerBase
    {
        private readonly COSMOContext _context;

        public LoginTokensController(COSMOContext context)
        {
            _context = context;
        }

        // GET: api/LoginTokens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LoginToken>>> GetLoginTokens()
        {
          if (_context.LoginTokens == null)
          {
              return NotFound();
          }
            return await _context.LoginTokens.ToListAsync();
        }

        // GET: api/LoginTokens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LoginToken>> GetLoginToken(Guid id)
        {
          if (_context.LoginTokens == null)
          {
              return NotFound();
          }
            var loginToken = await _context.LoginTokens.FindAsync(id);

            if (loginToken == null)
            {
                return NotFound();
            }

            return loginToken;
        }

        // PUT: api/LoginTokens/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoginToken(Guid id, LoginToken loginToken)
        {
            if (id != loginToken.LoginTokenId)
            {
                return BadRequest();
            }

            _context.Entry(loginToken).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoginTokenExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LoginTokens
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LoginToken>> PostLoginToken(LoginToken loginToken)
        {
          if (_context.LoginTokens == null)
          {
              return Problem("Entity set 'COSMOContext.LoginTokens'  is null.");
          }
            _context.LoginTokens.Add(loginToken);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LoginTokenExists(loginToken.LoginTokenId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLoginToken", new { id = loginToken.LoginTokenId }, loginToken);
        }

        // DELETE: api/LoginTokens/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLoginToken(Guid id)
        {
            if (_context.LoginTokens == null)
            {
                return NotFound();
            }
            var loginToken = await _context.LoginTokens.FindAsync(id);
            if (loginToken == null)
            {
                return NotFound();
            }

            _context.LoginTokens.Remove(loginToken);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LoginTokenExists(Guid id)
        {
            return (_context.LoginTokens?.Any(e => e.LoginTokenId == id)).GetValueOrDefault();
        }
    }
}
