﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyZipCodeKeyword
    {
        public int CompanyKeywordZipCodeId { get; set; }
        public int ParentCompanyId { get; set; }
        public string ZipCode { get; set; } = null!;
        public string Keyword { get; set; } = null!;
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
