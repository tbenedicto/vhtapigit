﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DvdList2MissingSpreadsheet
    {
        public string? Dvd { get; set; }
        public int? Countme { get; set; }
        public string? Spreadsheet { get; set; }
    }
}
