﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Mred
    {
        public int RowId { get; set; }
        public string AgentId { get; set; } = null!;
        public string PropertyId { get; set; } = null!;
        public int Vhtid { get; set; }
        public int? ContactId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
