﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CoverageCompany2
    {
        public string? CoverageCompany { get; set; }
        public string? State { get; set; }
        public string? City { get; set; }
        public double? ZipCode { get; set; }
    }
}
