﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class _9131GoogleMatchesQ12014On
    {
        public double? ListingId { get; set; }
        public double? SearchedImageId { get; set; }
        public string? SourceImageUrl { get; set; }
        public string? FoundImageUrl { get; set; }
        public string? FoundImageRefUrl { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public double? PropertyZip { get; set; }
        public double? AgentId { get; set; }
        public DateTime? AddedDate { get; set; }
        public double? G3codeId { get; set; }
        public string? Displayas { get; set; }
    }
}
