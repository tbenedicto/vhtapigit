﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessManual
    {
        public ProcessManual()
        {
            ProcessManualZzDistPoints = new HashSet<ProcessManualZzDistPoint>();
        }

        public int ProcessManualId { get; set; }
        public int ProcessManualTypeId { get; set; }
        public int SortId { get; set; }
        public string Name { get; set; } = null!;
        public string Descr { get; set; } = null!;
        public string? GeneralNotes { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ProcessManualType ProcessManualType { get; set; } = null!;
        public virtual ICollection<ProcessManualZzDistPoint> ProcessManualZzDistPoints { get; set; }
    }
}
