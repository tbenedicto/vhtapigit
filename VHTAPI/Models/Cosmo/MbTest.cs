﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MbTest
    {
        public int? Myrow { get; set; }
        public byte[] TimeStampField { get; set; } = null!;
    }
}
