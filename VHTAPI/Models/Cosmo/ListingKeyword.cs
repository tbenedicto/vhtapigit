﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingKeyword
    {
        public int ListingId { get; set; }
        public string Keyword { get; set; } = null!;
        public string? KeywordInUrlformat { get; set; }
    }
}
