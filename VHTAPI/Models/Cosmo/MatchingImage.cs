﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MatchingImage
    {
        public int MatchingImageId { get; set; }
        public int SearchedImageId { get; set; }
        public string ImageUrl { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
