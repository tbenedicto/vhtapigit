﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllCompanySalesAndAde
    {
        public int CompanyId { get; set; }
        public string? AdeSalesNational { get; set; }
        public string? RepSalesRegionalLong { get; set; }
        public string? RepCustomerService { get; set; }
        public string? RepSalesRegional { get; set; }
        public string CommercialOrResidential { get; set; } = null!;
        public string? AdeOrNone { get; set; }
    }
}
