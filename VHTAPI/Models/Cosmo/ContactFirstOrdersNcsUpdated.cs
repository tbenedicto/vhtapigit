﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactFirstOrdersNcsUpdated
    {
        public int Contactid { get; set; }
        public string? Addedby { get; set; }
        public int SalespersonId { get; set; }
    }
}
