﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactContactGroup
    {
        public int ContactId { get; set; }
        public int ContactGroupId { get; set; }
        public Guid Rowguid { get; set; }
        public DateTime? Addeddate { get; set; }
        public string? Addedby { get; set; }
    }
}
