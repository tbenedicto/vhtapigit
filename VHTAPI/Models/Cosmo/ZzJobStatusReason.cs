﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzJobStatusReason
    {
        public int JobStatusReasonId { get; set; }
        public string? Descr { get; set; }
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
        public bool IsForRent { get; set; }
        public bool IsField { get; set; }
        public bool IsActive { get; set; }
    }
}
