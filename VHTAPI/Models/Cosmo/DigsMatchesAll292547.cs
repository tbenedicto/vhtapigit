﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DigsMatchesAll292547
    {
        public string? GroupId { get; set; }
        public string? VhtPath { get; set; }
        public string? ZilPath { get; set; }
        public string? RoomCode { get; set; }
        public string? ListingId { get; set; }
        public string? Match { get; set; }
        public string? Host { get; set; }
        public string? OurId { get; set; }
        public string? VhtImageUrl { get; set; }
    }
}
