﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AccountUpdateFinal
    {
        public decimal? GeoLocationLatitudeS { get; set; }
        public decimal? GeoLocationLongitudeS { get; set; }
        public string Id { get; set; } = null!;
        public decimal? MtdNewRevC { get; set; }
        public decimal? MtdNewRevPreviousYearC { get; set; }
        public decimal? MtdPcRevC { get; set; }
        public decimal? MtdPcRevPreviousYearC { get; set; }
        public decimal? MtdRevC { get; set; }
        public decimal? MtdRevPreviousYearC { get; set; }
        public decimal? MtdTrailingRevC { get; set; }
        public decimal? MtdTrailingRevPreviousYearC { get; set; }
        public decimal? YtdNewRevC { get; set; }
        public decimal? YtdNewRevPreviousYearC { get; set; }
        public decimal? YtdPcRevC { get; set; }
        public decimal? YtdPcRevPreviousYearC { get; set; }
        public decimal? YtdRevC { get; set; }
        public decimal? YtdRevPreviousYearC { get; set; }
        public decimal? YtdTrailingRevC { get; set; }
        public decimal? YtdTrailingRevPreviousYearC { get; set; }
        public decimal? YtdListingsPreviousYearC { get; set; }
        public decimal? YtdListingsC { get; set; }
        public decimal? LifetimeListingsC { get; set; }
        public decimal? LifetimeRevC { get; set; }
        public decimal? Rolling12MonthRevC { get; set; }
        public decimal? Rollings12MonthListingsC { get; set; }
        public string? AccountDevelC { get; set; }
        public string? MappedCompanyCosmoIdC { get; set; }
        public string? SalesTerritoryC { get; set; }
        public decimal? OfOfficesC { get; set; }
        public decimal? OfAgentsC { get; set; }
        public DateTime? SalesAccountStartDateC { get; set; }
        public decimal? OfAgentsCustomersPast12MonthsC { get; set; }
        public decimal? OfOfficesWithOrdersInPastMonthC { get; set; }
        public decimal? MtdNewAgentsC { get; set; }
        public decimal? YtdNewAgentsC { get; set; }
        public decimal? ListingsInStudioC { get; set; }
        public decimal? ListingsInfieldC { get; set; }
        public decimal? RevInfieldC { get; set; }
        public decimal? RevInStudioC { get; set; }
        public string? Error { get; set; }
    }
}
