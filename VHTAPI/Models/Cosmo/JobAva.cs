﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobAva
    {
        public int Rowid { get; set; }
        public int JobId { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public int AgentId { get; set; }
        public string AgentName { get; set; } = null!;
        public string? AgentSchedulingPhone { get; set; }
        public int? AgentPreferredCvId { get; set; }
        public string? AgentPreferredCvName { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public Guid? ConversationId { get; set; }
        public DateTime? ConversationStartedDate { get; set; }
        public DateTime? LastMessagetoAgentDate { get; set; }
        public string? LastMessage { get; set; }
        public DateTime? ConversationEndedDate { get; set; }
        public string? Status { get; set; }
        public string? Outcome { get; set; }
    }
}
