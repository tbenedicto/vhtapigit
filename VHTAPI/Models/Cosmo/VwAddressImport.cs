﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAddressImport
    {
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public bool IsActiveInd { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public int? AddressStatusId { get; set; }
        public int? AddressStatusReasonId { get; set; }
        public DateTime? AddressStatusReasonDate { get; set; }
        public string? AddressStatusReasonNote { get; set; }
        public bool? PropogateToContactsInd { get; set; }
        public int RelatedAddressId { get; set; }
    }
}
