﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RoofStockLoader
    {
        public string? ClientId { get; set; }
        public string? ListingType { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZipCode { get; set; }
        public string? Bedrooms { get; set; }
        public string? Baths { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? ListingAgentEmail { get; set; }
        public string? ListingAgentCellPhone { get; set; }
        public string? LockBoxCode { get; set; }
        public string? BrokerageName { get; set; }
        public string? BrokerageAgentId { get; set; }
        public string? BrokerageOfficeId { get; set; }
        public string? BrokerageOfficeAddress1 { get; set; }
        public string? BrokerageOfficeAddress2 { get; set; }
        public string? BrokerageOfficeCity { get; set; }
        public string? BrokerageOfficeState { get; set; }
        public string? BrokerageOfficeZipCode { get; set; }
        public string? MlsId { get; set; }
        public string? Package1 { get; set; }
        public string? Package2 { get; set; }
        public string? Package3 { get; set; }
        public string? Package4 { get; set; }
        public string? Package5 { get; set; }
        public string? Package6 { get; set; }
        public string? Package7 { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? ListingAgentSecondaryPhone { get; set; }
    }
}
