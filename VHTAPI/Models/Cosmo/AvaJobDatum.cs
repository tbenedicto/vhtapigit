﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AvaJobDatum
    {
        public int AvaJobDataId { get; set; }
        public int? JobId { get; set; }
        public int? AgentId { get; set; }
        public string? AgentName { get; set; }
        public string? AgentPhone { get; set; }
        public string? ConversationStatus { get; set; }
        public DateTime? ConversationStartDate { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public int? AgentPreferredCv { get; set; }
        public string? AgentPreferredCvname { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string? IdleNotificationStatus { get; set; }
        public Guid Rowguid { get; set; }
        public int? AreYouThere { get; set; }
    }
}
