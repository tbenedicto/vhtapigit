﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvpreferredAuditDetail
    {
        public int CvpreferredAuditDetailId { get; set; }
        public int CvpreferredAuditHeaderId { get; set; }
        public int VideographerContactId { get; set; }
        public int OrderIndex { get; set; }
        public Guid RowId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
