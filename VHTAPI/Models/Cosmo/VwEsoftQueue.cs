﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwEsoftQueue
    {
        public int EsoftQueueId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int JobId { get; set; }
        public string VhtJobId { get; set; } = null!;
        public int ListingId { get; set; }
        public string PropertyState { get; set; } = null!;
        public string ProcessingState { get; set; } = null!;
        public string? EsoftPremiumBatchId { get; set; }
        public string? EsoftSignatureBatchId { get; set; }
        public bool Result { get; set; }
        public int PremiumPhotosCount { get; set; }
        public int SignaturePhotosCount { get; set; }
        public string? DigitalEditorDisplayAs { get; set; }
        public string Staging { get; set; } = null!;
    }
}
