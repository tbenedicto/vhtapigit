﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DistributionAlertNote
    {
        public int DistributionAlertNoteId { get; set; }
        public int DistributionAlertId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public string Note { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual DistributionAlert DistributionAlert { get; set; } = null!;
    }
}
