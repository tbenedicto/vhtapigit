﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VedderMissingDvdInfoCompletedate
    {
        public DateTime? FieldComplete { get; set; }
        public int ListingId { get; set; }
    }
}
