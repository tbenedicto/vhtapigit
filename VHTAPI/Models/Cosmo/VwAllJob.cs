﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllJob
    {
        public int JobId { get; set; }
        public string? VhtjobId { get; set; }
        public int ListingId { get; set; }
        public string? JobStatus { get; set; }
        public string? JobDetailedStatus { get; set; }
        public string? JobStage { get; set; }
        public DateTime JobStatusDate { get; set; }
        public string? JobStatusReason { get; set; }
        public string? JobCancellationReason { get; set; }
        public string? JobSource { get; set; }
        public DateTime? JobAddedDate { get; set; }
        public DateTime? JobUpdatedDate { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public DateTime? MediaReceivedDate { get; set; }
        public DateTime? ProductionCompletedDate { get; set; }
        public DateTime? JobCompletedDate { get; set; }
        public DateTime? MonthCompleted { get; set; }
        public int AgentId { get; set; }
        public int? PhotographerId { get; set; }
        public int CompanyId { get; set; }
        public int? ParentCompanyId { get; set; }
        public int? OrigContactId { get; set; }
        public int? OrigCompanyId { get; set; }
        public string? JobPriority { get; set; }
        public string? JobUpdatedBy { get; set; }
        public int? VideditorId { get; set; }
        public int? DigeditorId { get; set; }
        public string? ImageSpecialist { get; set; }
        public bool FlagCustomerFirstOrder { get; set; }
        public int? JobStatusId { get; set; }
        public int JobSourceId { get; set; }
        public int? ContactCreditCardId { get; set; }
    }
}
