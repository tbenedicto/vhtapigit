﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllTopLevelCompany
    {
        public int? TopLevelCompanyId { get; set; }
        public string? TopLevelCompanyName { get; set; }
        public string? TopLevelCompanyFullName { get; set; }
    }
}
