﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class WebAdminAuthList
    {
        public int ContactId { get; set; }
        public int ContactIdtoManage { get; set; }
        public Guid Rowguid { get; set; }
    }
}
