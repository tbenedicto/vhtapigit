﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BonusEmail
    {
        public string? AgentEmail { get; set; }
    }
}
