﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyPivot
    {
        public int CompanyId { get; set; }
        public string CompanyDisplayAs { get; set; } = null!;
        public int TopParentId { get; set; }
        public string Address1 { get; set; } = null!;
        public string Address2 { get; set; } = null!;
        public string City { get; set; } = null!;
        public string StateAbbr { get; set; } = null!;
        public string Zip { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string PhoneExt { get; set; } = null!;
        public string Website { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
