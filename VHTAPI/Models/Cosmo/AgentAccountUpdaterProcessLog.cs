﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AgentAccountUpdaterProcessLog
    {
        public int RunId { get; set; }
        public DateTime? LastRunTime { get; set; }
        public string? LogMsg { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
    }
}
