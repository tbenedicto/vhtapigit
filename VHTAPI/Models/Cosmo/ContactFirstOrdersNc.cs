﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactFirstOrdersNc
    {
        public int ContactId { get; set; }
        public int? Listingid { get; set; }
        public DateTime _1stStartdate { get; set; }
        public int? CompletedOnJobid { get; set; }
        public string? ContactHistoryType { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public string? JobSource { get; set; }
        public decimal? TotalRevListing1stJob { get; set; }
        public decimal? TotalRev { get; set; }
        public int SalespersonId { get; set; }
    }
}
