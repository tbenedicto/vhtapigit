﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ViewerLink
    {
        public int ViewerLinkId { get; set; }
        public string? BrokerLogo { get; set; }
        public string? Address { get; set; }
        public string? AgentPhoto { get; set; }
        public string? MyWebsite { get; set; }
        public string? BrokerWebsite { get; set; }
        public string? MyListings { get; set; }
        public string? PropertyDetails { get; set; }
        public string? AreaMap { get; set; }
        public string? SchoolInformation { get; set; }
        public string? ScheduleAshowing { get; set; }
        public string? MortgageCalculator { get; set; }
        public string? Neighborhoods { get; set; }
        public string? Community { get; set; }
        public string? Others { get; set; }
        public string? WalkScore { get; set; }
        public string? EstimatedValue { get; set; }
        public string? OtherListingsLikeThis { get; set; }
        public string? EmaillAllNewListings { get; set; }
        public string? Register { get; set; }
        public string? BrokerSocialMedia { get; set; }
        public string? AgentSocialMedia { get; set; }
        public string? SaveListing { get; set; }
        public string? TaxInfo { get; set; }
        public string? WeatherInfo { get; set; }
        public string? HomezadaInventoryFile { get; set; }
        public string? QuickSearches { get; set; }
        public string? ChatWithRep { get; set; }
        public int CompanyId { get; set; }
    }
}
