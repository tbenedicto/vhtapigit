﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Cvaddress
    {
        public int CvaddressId { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
    }
}
