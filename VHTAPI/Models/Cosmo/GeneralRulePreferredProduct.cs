﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GeneralRulePreferredProduct
    {
        public int GeneralRuleId { get; set; }
        public int ProductId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual GeneralRule GeneralRule { get; set; } = null!;
        public virtual Product Product { get; set; } = null!;
    }
}
