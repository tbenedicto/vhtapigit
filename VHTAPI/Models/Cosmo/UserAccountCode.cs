﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class UserAccountCode
    {
        public int UserAccountCodeId { get; set; }
        public int UserAccountId { get; set; }
        public string Code { get; set; } = null!;
        public DateTime ExpiryDate { get; set; }
        public int UserAccountCodeTypeId { get; set; }
        public bool IsUsed { get; set; }
        public Guid Rowguid { get; set; }
    }
}
