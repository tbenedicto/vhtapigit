﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CreditCard1
    {
        public int CreditCardId { get; set; }
        public int ContactId { get; set; }
        public int CreditCardStatusId { get; set; }
        public int ExpirationMonthId { get; set; }
        public int ExpirationYearId { get; set; }
    }
}
