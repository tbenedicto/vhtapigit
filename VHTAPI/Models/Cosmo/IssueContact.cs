﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class IssueContact
    {
        public int IssueContactId { get; set; }
        public int IssueId { get; set; }
        public int ContactId { get; set; }
        public int IssueContactTypeId { get; set; }
        public bool IsUnread { get; set; }
        public DateTime FollowUpDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public Guid Rowguid { get; set; }
        public Guid Rowguid12 { get; set; }
    }
}
