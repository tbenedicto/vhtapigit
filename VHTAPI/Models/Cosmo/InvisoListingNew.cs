﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoListingNew
    {
        public string? OrderContainerId { get; set; }
        public string? OrderContainerState { get; set; }
        public string? CustomerReference { get; set; }
        public string? OrderContainerType { get; set; }
        public string? OrderContainerCreatedAt { get; set; }
        public string? FirstBundleOfOrdersDeliveredAt { get; set; }
        public string? CustomerId { get; set; }
        public string? BrokerId { get; set; }
        public string? OrderContainerAddressDoorbell { get; set; }
        public string? OrderContainerAddressFloor { get; set; }
        public string? OrderContainerAddressStreet { get; set; }
        public string? OrderContainerAddressPostalArea { get; set; }
        public string? OrderContainerAddressZipCode { get; set; }
        public string? OrderContainerAddressRegion { get; set; }
        public string? OrderContainerAddressAdditionalInfo { get; set; }
        public string? LocationPoint { get; set; }
        public string? LocationZoom { get; set; }
        public string? LocationSource { get; set; }
        public string? EstateType1 { get; set; }
        public string? EstateAreasizeRange { get; set; }
        public string? EstateFloornoRange { get; set; }
        public string? EstateValueRange { get; set; }
        public string? SellerName { get; set; }
        public string? SellerTelephone { get; set; }
        public string? SellerMobile { get; set; }
        public string? SellerEmail { get; set; }
        public string? Id { get; set; }
        public string? M2 { get; set; }
        public string? EstateInformationContainerId { get; set; }
        public string? EstateInformationContainerType { get; set; }
        public string? CreatedAt { get; set; }
        public string? UpdatedAt { get; set; }
        public string? EstateType { get; set; }
        public string? Floors { get; set; }
        public string? Value { get; set; }
        public string? Classification { get; set; }
        public string? EstateM2 { get; set; }
        public string? EstateFloors { get; set; }
    }
}
