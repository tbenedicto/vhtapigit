﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Contact
    {
        public Contact()
        {
            ContactIdentifiers = new HashSet<ContactIdentifier>();
            ContactPrepaidJobProductTransactions = new HashSet<ContactPrepaidJobProductTransaction>();
            ContactPrepaidJobProducts = new HashSet<ContactPrepaidJobProduct>();
            ContactPrepaidProductBalances = new HashSet<ContactPrepaidProductBalance>();
            CreditCards = new HashSet<CreditCard>();
        }

        public int ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int ContactStatusId { get; set; }
        public DateTime ContactStatusDate { get; set; }
        public int ContactStatusReasonId { get; set; }
        public string? ContactStatusReasonNote { get; set; }
        public int? SalespersonId { get; set; }
        public string Title { get; set; } = null!;
        public string? FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string? MiddleInitial { get; set; }
        public string? LastName { get; set; }
        public string Suffix { get; set; } = null!;
        public string? DisplayAs { get; set; }
        public string? Salutation { get; set; }
        public int? ContactDisplayAsFormatId { get; set; }
        public int? ContactSalutationFormatId { get; set; }
        public string? ContactCode { get; set; }
        public int WebAccountTypeId { get; set; }
        public bool AccountDisabledInd { get; set; }
        public int NumBadLoginAttempts { get; set; }
        public bool AccountLockedOutInd { get; set; }
        public DateTime? AccountLockedOutDate { get; set; }
        public string? PasswordHash { get; set; }
        public DateTime? LastLogin { get; set; }
        public int ContactCategoryId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public bool PortraitInd { get; set; }
        public string? TourDisplayWebsite { get; set; }
        public string? TourDisplayEmail { get; set; }
        public string? TourDisplayPhone { get; set; }
        public Guid Rowguid { get; set; }
        public int ContactSourceId { get; set; }
        public decimal DollarsInLast180Days { get; set; }
        public bool IsStyleInherited { get; set; }
        public int ViewerStyleId { get; set; }
        public int BrochureStyleId { get; set; }
        public string? LoginEmail { get; set; }
        public string ImageAccessCode { get; set; } = null!;
        public bool HasUploadedToMm { get; set; }
        public string? CorporateContactId { get; set; }
        public bool CanPremiumPhotography { get; set; }
        public bool Can360 { get; set; }
        public bool CanFloorPlan { get; set; }
        public int? ContactVipflagId { get; set; }
        public int? ContactTlcflagId { get; set; }
        public int? ContactClassificationId { get; set; }
        public int? ContactBusinessTypeId { get; set; }
        public int? ContactClientStatusId { get; set; }
        public int? RepPhotographer { get; set; }
        public int? RepSalesLocal { get; set; }
        public int? RepSalesRegional { get; set; }
        public int? RepSalesNational { get; set; }
        public int? RepCustomerService { get; set; }
        public bool IsResidentialTrained { get; set; }
        public bool IsHouzzQualified { get; set; }
        public bool DisableVideoSyndication { get; set; }
        public byte[] TimeStampField { get; set; } = null!;
        public int? CapacityThreshold { get; set; }
        public bool? UseWaterMark1 { get; set; }
        public bool? WaterMarkPhotosFromFeed1 { get; set; }
        public bool? WaterMarkPhotosFromMyVht1 { get; set; }
        public bool? WaterMark3rdPartyPhotos1 { get; set; }
        public byte? WaterMarkSizePercentage1 { get; set; }
        public bool? PositionLowerLeft1 { get; set; }
        public bool? PositionLowerRight1 { get; set; }
        public bool? PositionUpperRight1 { get; set; }
        public bool? PositionUpperLeft1 { get; set; }
        public string? WaterMarkUrl2 { get; set; }
        public bool? UseWaterMark2 { get; set; }
        public bool? WaterMarkPhotosFromFeed2 { get; set; }
        public bool? WaterMarkPhotosFromMyVht2 { get; set; }
        public bool? WaterMark3rdPartyPhotos2 { get; set; }
        public byte? WaterMarkSizePercentage2 { get; set; }
        public bool? PositionLowerLeft2 { get; set; }
        public bool? PositionLowerRight2 { get; set; }
        public bool? PositionUpperRight2 { get; set; }
        public bool? PositionUpperLeft2 { get; set; }
        public string? WaterMarkUrl1Oasis { get; set; }
        public bool? UseWaterMark1Oasis { get; set; }
        public bool? WaterMarkPhotosFromFeed1Oasis { get; set; }
        public bool? WaterMarkPhotosFromMyVht1Oasis { get; set; }
        public bool? WaterMark3rdPartyPhotos1Oasis { get; set; }
        public byte? WaterMarkSizePercentage1Oasis { get; set; }
        public bool? PositionLowerLeft1Oasis { get; set; }
        public bool? PositionLowerRight1Oasis { get; set; }
        public bool? PositionUpperRight1Oasis { get; set; }
        public string? WaterMarkUrl2Oasis { get; set; }
        public bool? UseWaterMark2Oasis { get; set; }
        public bool? WaterMarkPhotosFromFeed2Oasis { get; set; }
        public bool? WaterMarkPhotosFromMyVht2Oasis { get; set; }
        public bool? WaterMark3rdPartyPhotos2Oasis { get; set; }
        public byte? WaterMarkSizePercentage2Oasis { get; set; }
        public bool? PositionLowerLeft2Oasis { get; set; }
        public bool? PositionLowerRight2Oasis { get; set; }
        public bool? PositionUpperRight2Oasis { get; set; }
        public bool? PositionUpperLeft1Oasis { get; set; }
        public bool? PositionUpperLeft2Oasis { get; set; }
        public bool? WatermarkNonBrandedMyVht1 { get; set; }
        public bool? WatermarkNonBrandedMyVht2 { get; set; }
        public bool? WatermarkNonBrandedOasis1 { get; set; }
        public bool? WatermarkNonBrandedOasis2 { get; set; }
        public bool? IsMyVhtFullServicePlus { get; set; }
        public bool? HideNonbrandedLinksMyVht { get; set; }
        public bool? HideNonbrandedLinksOasis { get; set; }
        public bool? HideNonbrandedLinksMyVhtLinks { get; set; }
        public bool? OptOutPrintVht1 { get; set; }
        public bool? OptOutPrintVht2 { get; set; }
        public bool? OptOutPrintOasis1 { get; set; }
        public bool? OptOutPrintOasis2 { get; set; }
        public bool? DisconnectFromCompanySettings { get; set; }
        public string? WaterMarkUrl1 { get; set; }
        public int? CampaignSourceId { get; set; }
        public int? CampaignId { get; set; }
        public bool IsCvovqualified { get; set; }
        public bool IsEcpqualified { get; set; }
        public bool CanSignature { get; set; }
        public bool IsEventQualified { get; set; }
        public bool CanAerial { get; set; }
        public bool CanSilver { get; set; }
        public bool CanGold { get; set; }
        public bool CanPlatinum { get; set; }
        public bool Can3D { get; set; }
        public int? MmsloginId { get; set; }
        public bool IsAltisourceQualified { get; set; }
        public DateTime? PcAddedDate { get; set; }
        public bool DetachCompanyReceiptEmailSettings { get; set; }
        public bool EnableReceiptEmailAutomation { get; set; }
        public string? NrtEmail { get; set; }
        public string? FullTitleDesignation { get; set; }
        public DateTime? FirstContactDate { get; set; }
        public string? FirstContactDateAddedBy { get; set; }
        public string? PcAddedBy { get; set; }
        public DateTime? PcModifiedDate { get; set; }
        public string? PcModifiedby { get; set; }
        public bool IsVhtnationQualified { get; set; }
        public bool IsVhtselectQualified { get; set; }
        public bool CanCalscheduling { get; set; }
        public bool IsPixQualified { get; set; }
        public bool CanFullMotion { get; set; }
        public bool CanElevated { get; set; }
        public bool CanZillow { get; set; }
        public int? RepSalesNcs { get; set; }
        public bool? IsAvaEnabled { get; set; }
        public bool CanAva { get; set; }
        public bool CanFloorplanCc { get; set; }
        public string? CubicasaEmail { get; set; }
        public bool CanAerialVideo { get; set; }
        public bool CanDroneElitePhotos { get; set; }
        public string? PortraitUrl { get; set; }
        public bool CanFmv2 { get; set; }
        public bool ManageOwnCalendar { get; set; }

        public virtual BrokerageIdmatch BrokerageIdmatch { get; set; } = null!;
        public virtual ICollection<ContactIdentifier> ContactIdentifiers { get; set; }
        public virtual ICollection<ContactPrepaidJobProductTransaction> ContactPrepaidJobProductTransactions { get; set; }
        public virtual ICollection<ContactPrepaidJobProduct> ContactPrepaidJobProducts { get; set; }
        public virtual ICollection<ContactPrepaidProductBalance> ContactPrepaidProductBalances { get; set; }
        public virtual ICollection<CreditCard> CreditCards { get; set; }
    }
}
