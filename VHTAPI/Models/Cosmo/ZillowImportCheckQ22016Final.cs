﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZillowImportCheckQ22016Final
    {
        public int ZillowImportId { get; set; }
        public int ListingId { get; set; }
        public int ZillowId { get; set; }
        public string Url { get; set; } = null!;
        public string Address { get; set; } = null!;
        public DateTime? SoldDate { get; set; }
        public decimal? SquareFootage { get; set; }
        public string? Status { get; set; }
    }
}
