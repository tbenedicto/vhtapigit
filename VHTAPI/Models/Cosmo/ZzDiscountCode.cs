﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzDiscountCode
    {
        public int DiscountCodeId { get; set; }
        public string DiscountCode { get; set; } = null!;
        public string Descr { get; set; } = null!;
        public bool IsActive { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
