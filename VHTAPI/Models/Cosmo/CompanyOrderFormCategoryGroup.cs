﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyOrderFormCategoryGroup
    {
        public int CompanyOrderFormCategoryGroupId { get; set; }
        public int CompanyId { get; set; }
        public int CompanyOrderFormId { get; set; }
        public int GroupId { get; set; }
        public string CategoryGroupLabel { get; set; } = null!;
        public bool GroupMoreInfoEnable { get; set; }
        public string GroupMoreInfoLabel { get; set; } = null!;
        public string GroupMoreInfoLink { get; set; } = null!;
    }
}
