﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Dvd4q2015Edited
    {
        public string? VhtphotoUrl { get; set; }
        public string? Descr { get; set; }
        public double? ListingId { get; set; }
        public double? RoomCodeId { get; set; }
        public string? Basepath { get; set; }
        public double? ContentServerId { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public double? PropertyZip { get; set; }
        public double? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public double? G3codeId { get; set; }
        public DateTime? TrcAddeddate { get; set; }
        public string? CvLast { get; set; }
        public string? CvFirst { get; set; }
        public DateTime? FieldComplete { get; set; }
    }
}
