﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCpixNewCustomerProspect
    {
        public DateTime _1stStartdate { get; set; }
        public string VhtrelationshipId { get; set; } = null!;
        public int? New { get; set; }
    }
}
