﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class EllimanCheck5182016All
    {
        public int ContactId { get; set; }
        public string? ContactCode { get; set; }
        public string? Contactname { get; set; }
        public int ContactStatusId { get; set; }
        public int? MmsloginId { get; set; }
        public decimal? Revenuelifetime { get; set; }
        public string? Companyname { get; set; }
        public int? Companystatusid { get; set; }
        public int? Parentcompanyid { get; set; }
        public int? MmsofficeId { get; set; }
        public int? MmscompanyId { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public int? AddressStatusId { get; set; }
    }
}
