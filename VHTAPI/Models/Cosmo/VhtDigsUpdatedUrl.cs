﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VhtDigsUpdatedUrl
    {
        public string? Id { get; set; }
        public string? Dvd { get; set; }
        public string? Filename { get; set; }
        public string? VhtImage { get; set; }
        public string? ZillowImageActualImage { get; set; }
        public string? ZilPath { get; set; }
        public string? ListingId { get; set; }
        public string? RoomCodeId { get; set; }
        public string? UpdatedVhtUrl { get; set; }
    }
}
