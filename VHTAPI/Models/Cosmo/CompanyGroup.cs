﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyGroup
    {
        public CompanyGroup()
        {
            CompanyGroupCompanies = new HashSet<CompanyGroupCompany>();
        }

        public int CompanyGroupId { get; set; }
        public string Descr { get; set; } = null!;
        public string? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<CompanyGroupCompany> CompanyGroupCompanies { get; set; }
    }
}
