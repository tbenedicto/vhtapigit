﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbContactGroupOrderFormCategory
    {
        public int ContactGroupOrderFormCategoryId { get; set; }
        public int CategoryId { get; set; }
        public int ContactGroupId { get; set; }
        public string CategoryLabel { get; set; } = null!;
        public string OptionLabel { get; set; } = null!;
        public string AfterLabelContent { get; set; } = null!;
        public string BeforeLabelContent { get; set; } = null!;
        public string? OverrideSelection { get; set; }
        public Guid Rowguid { get; set; }
    }
}
