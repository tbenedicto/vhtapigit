﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DistributionAssignment
    {
        public DistributionAssignment()
        {
            TimeslotAllocations = new HashSet<TimeslotAllocation>();
        }

        public int DistributionAssignmentId { get; set; }
        public int ListingId { get; set; }
        public int StatusId { get; set; }
        public int DistributionTargetId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; } = null!;
        public byte[] Tsvalue { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual DistributionTarget DistributionTarget { get; set; } = null!;
        public virtual DistributionAssignmentStatus Status { get; set; } = null!;
        public virtual ICollection<TimeslotAllocation> TimeslotAllocations { get; set; }
    }
}
