﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobCreditCard
    {
        public int JobCreditCardId { get; set; }
        public int? JobId { get; set; }
        public int? ContactId { get; set; }
        public int? CreditCardStatusId { get; set; }
        public string? NameOnCard { get; set; }
        public int? CreditCardTypeId { get; set; }
        public string? CreditCardNumber { get; set; }
        public int? ExpirationMonthId { get; set; }
        public int? ExpirationYearId { get; set; }
        public string? BillingAddress1 { get; set; }
        public string? BillingAddress2 { get; set; }
        public string? BillingCity { get; set; }
        public string? BillingState { get; set; }
        public string? BillingZip { get; set; }
        public bool IsDefault { get; set; }
        public string? Last4Nums { get; set; }
        public string? Cvncode { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? Company { get; set; }
        public string? EmailAddress { get; set; }
        public string? TransId { get; set; }
        public string? SafeId { get; set; }
        public DateTime? Addeddate { get; set; }
    }
}
