﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvdoNotUse
    {
        public int ContactId { get; set; }
        public int VideographerContactId { get; set; }
        public Guid RowId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
