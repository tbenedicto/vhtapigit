﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbGeneralRuleAvailableProduct
    {
        public int GeneralRuleId { get; set; }
        public int ProductId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
