﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvdoNotUseByCompany
    {
        public int CompanyId { get; set; }
        public int VideographerContactId { get; set; }
        public Guid RowId { get; set; }
    }
}
