﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactGroupOrderFormProduct
    {
        public int ContactGroupOrderFormId { get; set; }
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public int? SortId { get; set; }
        public string? MarketingProdName { get; set; }
        public int? ContactGroupOrderFormPackageId { get; set; }
        public bool UseRateCustomText { get; set; }
        public string? RateCustomText { get; set; }
        public int ContactGroupOrderFormProductId { get; set; }
        public bool UseAsAdditionalInfo { get; set; }
        public bool UseWithQuantity { get; set; }
        public int ContactGroupOrderFormShowRateTypeId { get; set; }
        public string? ProductInfoContent { get; set; }
    }
}
