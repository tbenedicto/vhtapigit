﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TblDbmaintVhtopSupportC21imageworksBillingReportOld
    {
        public DateTime LoadDate { get; set; }
        public Guid? Guid { get; set; }
        public string? IwlListingid { get; set; }
        public string IwlMlsname { get; set; } = null!;
        public string IwlMlsnumber { get; set; } = null!;
        public string IwlImportstatus { get; set; } = null!;
        public DateTime IwlImportstatusdate { get; set; }
        public int? CosmoLiListingid { get; set; }
        public int? Tourid { get; set; }
        public int? SynVhtlistingid { get; set; }
        public int? SynInternalid { get; set; }
        public string? Url { get; set; }
        public DateTime? ExternalVideoLastTouched { get; set; }
        public DateTime? InternalVideoLastTouched { get; set; }
    }
}
