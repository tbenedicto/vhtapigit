﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageExchangePartnerCompany
    {
        public int ImageExchangePartnerCompanyId { get; set; }
        public Guid PartnerId { get; set; }
        public string BrokerId { get; set; } = null!;
        public int ParentCompanyId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
