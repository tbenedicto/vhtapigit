﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CpixPrepaidBalanceLoad
    {
        public int? CosmoContactid { get; set; }
        public int? Balance { get; set; }
    }
}
