﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportMarketFlips20190128Na
    {
        public string? ProductRegion { get; set; }
        public double? Zip { get; set; }
        public string? StateId { get; set; }
        public double? RatesheetId { get; set; }
        public string? F5 { get; set; }
    }
}
