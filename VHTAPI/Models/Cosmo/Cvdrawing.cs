﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Cvdrawing
    {
        public int CvdrawingId { get; set; }
        public int JobId { get; set; }
        public string DrawingsFolder { get; set; } = null!;
        public DateTime DateSubmitted { get; set; }
        public Guid Rowguid { get; set; }
    }
}
