﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LogJobTaskMediaSent
    {
        public int LogJobTaskMediaSentId { get; set; }
        public int? JobId { get; set; }
        public DateTime? TaskMediaReceivedCompletedOn { get; set; }
        public DateTime? TaskMediaReceivedCompletedOnOld { get; set; }
        public string? TaskMediaReceivedCompletedBy { get; set; }
        public string? TaskMediaReceivedCompletedByOld { get; set; }
        public string? CurrentUser { get; set; }
        public string? AppName { get; set; }
        public string? HostName { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
    }
}
