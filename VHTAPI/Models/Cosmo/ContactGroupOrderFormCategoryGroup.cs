﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactGroupOrderFormCategoryGroup
    {
        public int ContactGroupOrderFormCategoryGroupId { get; set; }
        public int ContactGroupId { get; set; }
        public int GroupId { get; set; }
        public string CategoryGroupLabel { get; set; } = null!;
        public bool GroupMoreInfoEnable { get; set; }
        public string GroupMoreInfoLabel { get; set; } = null!;
        public string GroupMoreInfoLink { get; set; } = null!;
    }
}
