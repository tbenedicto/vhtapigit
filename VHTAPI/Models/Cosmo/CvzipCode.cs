﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvzipCode
    {
        public int CvzipcodeId { get; set; }
        public int Cvid { get; set; }
        public string Zipcode { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
