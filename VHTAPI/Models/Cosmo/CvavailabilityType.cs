﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvavailabilityType
    {
        public int CvavailabilityTypeId { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
        public Guid Rowguid5 { get; set; }
    }
}
