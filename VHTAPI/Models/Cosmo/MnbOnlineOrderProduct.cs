﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbOnlineOrderProduct
    {
        public int OnlineOrderProductId { get; set; }
        public int OnlineOrderId { get; set; }
        public int ProductId { get; set; }
        public decimal? AgentPay { get; set; }
        public int? Quantity { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? Cvpay { get; set; }
        public decimal? Cvupsell { get; set; }
        public Guid Rowguid { get; set; }
    }
}
