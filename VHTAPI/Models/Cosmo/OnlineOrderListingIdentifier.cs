﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OnlineOrderListingIdentifier
    {
        public int OnlineOrderId { get; set; }
        public string Mlsid { get; set; } = null!;
        public int ListingTypeId { get; set; }
        public int OnlineOrderListingIdentifierId { get; set; }
    }
}
