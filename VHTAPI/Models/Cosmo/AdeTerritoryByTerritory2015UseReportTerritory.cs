﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdeTerritoryByTerritory2015UseReportTerritory
    {
        public string? State { get; set; }
        public string? State1 { get; set; }
        public string? SalesTerritory { get; set; }
        public string? Ade { get; set; }
        public string? Regional { get; set; }
        public string? AseMl { get; set; }
        public string? Complete { get; set; }
        public string? F8 { get; set; }
        public string? F9 { get; set; }
        public string? F10 { get; set; }
        public int? RepSalesNational { get; set; }
    }
}
