﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NwIdTerritoryZipCoverage
    {
        public double? NwIndianaZipCodes { get; set; }
    }
}
