﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LeadUpdate
    {
        public decimal? AmountOfOrderC { get; set; }
        public string? ApplicationRatingC { get; set; }
        public string? ApplicationStatusC { get; set; }
        public string? AreYouComfortableTravelingUpTo20C { get; set; }
        public string? AreYouCurrentlyEmployedC { get; set; }
        public string? AuthorizeNetTransactionIdC { get; set; }
        public string? CameraTypeSC { get; set; }
        public string? CampaignGuidC { get; set; }
        public string? CampaignNameC { get; set; }
        public string? CampaignSourceC { get; set; }
        public string? CidC { get; set; }
        public string? City { get; set; }
        public string? CommentC { get; set; }
        public string? CommercialTypeLC { get; set; }
        public string Company { get; set; } = null!;
        public string? CompanyTypeC { get; set; }
        public string? ConnectionReceivedId { get; set; }
        public string? ConnectionSentId { get; set; }
        public string? ConvertedAccountId { get; set; }
        public string? ConvertedContactId { get; set; }
        public DateTime? ConvertedDate { get; set; }
        public string? ConvertedOpportunityId { get; set; }
        public string? CopyPasteResumeWorkHistoryC { get; set; }
        public string? Country { get; set; }
        public string? CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public string? Ctct21OptedOutByCtctC { get; set; }
        public string? CurrentAvailabilityC { get; set; }
        public string? CurrentAvailabilityDaysNightsWeekC { get; set; }
        public string? CurrentAvailabilityDaysNightsWeekenC { get; set; }
        public DateTime? DateInterviewWasCompletedC { get; set; }
        public string? DiscountCodeC { get; set; }
        public string? DoNotMoveForwardReasonC { get; set; }
        public string? DoYouHaveAComputerInternetAtHomeC { get; set; }
        public string? DoYouHaveAReliableCarC { get; set; }
        public string? DoYouHaveAScannerAtHomeC { get; set; }
        public string? DoYouHaveASmartPhoneC { get; set; }
        public string? DoYouHaveBuildingHomeMeasureExpienC { get; set; }
        public string? DoYouHavePriorRealEstatePhotographC { get; set; }
        public string? DoYouHavePriorSalesExperienceC { get; set; }
        public string? DoYouHaveREPhotographyExperienceC { get; set; }
        public string? DoYouOwnALaserMeasureC { get; set; }
        public string? Email { get; set; }
        public DateTime? EmailBouncedDate { get; set; }
        public string? EmailBouncedReason { get; set; }
        public string? EquipmentApprovedC { get; set; }
        public string? EquipmentListC { get; set; }
        public string? FieldOpsEquipmentQuestionsC { get; set; }
        public string? FirstName { get; set; }
        public string HasOptedOutOfEmail { get; set; } = null!;
        public string? HowDidYouAboutVhtStudios2C { get; set; }
        public string? HowDidYouHearAboutVhtStudiosC { get; set; }
        public string Id { get; set; } = null!;
        public string? IfTeamMemberOrOtherPleaseDescribeC { get; set; }
        public string? IfYesPleaseProvideDetailsC { get; set; }
        public string? IfYesProvideDetailsC { get; set; }
        public string? IfYesProvideDetailsPhotographyC { get; set; }
        public string? InterviewNotesC { get; set; }
        public string? InterviewOutcomeC { get; set; }
        public string? InterviewRatingC { get; set; }
        public string? InterviewStatusC { get; set; }
        public string IsConverted { get; set; } = null!;
        public string IsDeleted { get; set; } = null!;
        public string IsUnreadByOwner { get; set; } = null!;
        public string? JigsawContactId { get; set; }
        public string? KeepInMindForHighEndProjectsC { get; set; }
        public DateTime? LastActivityDate { get; set; }
        public string? LastModifiedById { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastName { get; set; } = null!;
        public DateTime? LastReferencedDate { get; set; }
        public DateTime? LastViewedDate { get; set; }
        public decimal? Latitude { get; set; }
        public DateTime? LaunchedC { get; set; }
        public string? LeadFormIdC { get; set; }
        public string? LeadSource { get; set; }
        public string? LensTypeSC { get; set; }
        public string? LinkToYourREPhotographyPortfolioC { get; set; }
        public string? LinkToYourRealEstatePhotographyPorC { get; set; }
        public decimal? Longitude { get; set; }
        public string? MappedCompanyCosmoIdC { get; set; }
        public string? MappedContactCosmoIdC { get; set; }
        public string? MarketC { get; set; }
        public string? MarketOrderPlacedC { get; set; }
        public string? MasterRecordId { get; set; }
        public DateTime? Mkto2AcquisitionDateC { get; set; }
        public string? Mkto2AcquisitionProgramC { get; set; }
        public decimal? Mkto2AcquisitionProgramIdC { get; set; }
        public string? Mkto2InferredCityC { get; set; }
        public string? Mkto2InferredCompanyC { get; set; }
        public string? Mkto2InferredCountryC { get; set; }
        public string? Mkto2InferredMetropolitanAreaC { get; set; }
        public string? Mkto2InferredPhoneAreaCodeC { get; set; }
        public string? Mkto2InferredPostalCodeC { get; set; }
        public string? Mkto2InferredStateRegionC { get; set; }
        public decimal? Mkto2LeadScoreC { get; set; }
        public string? Mkto2OriginalReferrerC { get; set; }
        public string? Mkto2OriginalSearchEngineC { get; set; }
        public string? Mkto2OriginalSearchPhraseC { get; set; }
        public string? Mkto2OriginalSourceInfoC { get; set; }
        public string? Mkto2OriginalSourceTypeC { get; set; }
        public string? MlCvAppStatusC { get; set; }
        public string? MobilePhone { get; set; }
        public string Name { get; set; } = null!;
        public string? NotesFrom2ndScreeningC { get; set; }
        public string? OtherGearC { get; set; }
        public string OwnerId { get; set; } = null!;
        public string? PaperworkStatusC { get; set; }
        public string? Phone { get; set; }
        public string? PhotoUrl { get; set; }
        public string? PostalCode { get; set; }
        public string? PropertyTypeC { get; set; }
        public string? ProspectConversationNotesC { get; set; }
        public string? ReferredByC { get; set; }
        public string? ResumeWorkHistoryC { get; set; }
        public string? ResumeWorkHistoryPhotographyC { get; set; }
        public string? Salutation { get; set; }
        public string? ServiceInterestedInC { get; set; }
        public string? SipsourceC { get; set; }
        public string? State { get; set; }
        public string Status { get; set; } = null!;
        public string? Street { get; set; }
        public DateTime SystemModstamp { get; set; }
        public string? Title { get; set; }
        public string? VhtProductToOrderC { get; set; }
        public string? VhtRelationshipLC { get; set; }
        public string? Website { get; set; }
        public string? WebsiteUrlC { get; set; }
        public string? X2ndScreeningRatingC { get; set; }
        public string? X2ndScreeningStatusC { get; set; }
        public string? ZipMarketC { get; set; }
        public string? Error { get; set; }
    }
}
