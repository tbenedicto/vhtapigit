﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablSdrRevComparison
    {
        public int? Id { get; set; }
        public string? Title { get; set; }
        public double? ValuesOfTitle { get; set; }
    }
}
