﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CommReportNewMonthlyHistoryTotalsMissing2
    {
        public int? Companyid { get; set; }
        public string? DisplayAs { get; set; }
        public string? MissTerritory { get; set; }
        public int? Companystatusid { get; set; }
        public int? RepSalesRegional { get; set; }
        public string? Regional { get; set; }
        public int? SalesTerritoryId { get; set; }
        public int? Rep { get; set; }
        public string? Ade { get; set; }
        public string? CompanyState { get; set; }
        public int? RepSalesNational { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public string? Territory { get; set; }
        public string? StateId { get; set; }
        public decimal? TotalRev { get; set; }
        public int? TotalOrders { get; set; }
        public int? TotalClients { get; set; }
        public decimal? Pcrev { get; set; }
        public decimal? TrailingYtdNewRevenue { get; set; }
        public decimal? NewRev { get; set; }
        public int? NewOrders { get; set; }
        public int? NewClients { get; set; }
        public decimal? RenewRev { get; set; }
        public int? ReNewOrders { get; set; }
        public int? ReNewClients { get; set; }
        public decimal? TotNewRenewRev { get; set; }
        public int? TotNewRenewOrders { get; set; }
        public int? TotNewRenewClients { get; set; }
        public decimal? TrailingYtdNewRenewRevenue { get; set; }
        public decimal? Trailing11monthRevNewRenew { get; set; }
        public decimal? CarryOverTrailingRevNewRenew { get; set; }
        public int? UniqAgents { get; set; }
    }
}
