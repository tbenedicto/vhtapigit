﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Timeslot
    {
        public Timeslot()
        {
            TimeslotAllocations = new HashSet<TimeslotAllocation>();
        }

        public int TimeslotId { get; set; }
        public int DistributionTargetId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime SubmittalDeadline { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public byte[] Tsvalue { get; set; } = null!;

        public virtual DistributionTarget DistributionTarget { get; set; } = null!;
        public virtual ICollection<TimeslotAllocation> TimeslotAllocations { get; set; }
    }
}
