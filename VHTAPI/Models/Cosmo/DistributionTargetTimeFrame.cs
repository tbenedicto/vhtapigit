﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DistributionTargetTimeFrame
    {
        public DistributionTargetTimeFrame()
        {
            DistributionTargets = new HashSet<DistributionTarget>();
        }

        public int TimeFrameId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public byte[] Tsvalue { get; set; } = null!;

        public virtual ICollection<DistributionTarget> DistributionTargets { get; set; }
    }
}
