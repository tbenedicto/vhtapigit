﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablCancellationReport
    {
        public int Jobid { get; set; }
        public int ListingId { get; set; }
        public string? JobStatus { get; set; }
        public DateTime Jobstatusdate { get; set; }
        public int Jobstatusreasonid { get; set; }
        public int Jobpriorityid { get; set; }
        public string? VhtjobId { get; set; }
        public int Jobsourceid { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? JobCancellationReasonId { get; set; }
        public string? JobCancellationReason { get; set; }
        public int? SortId { get; set; }
        public string? Descr { get; set; }
        public string? Propertyaddressline1 { get; set; }
        public string? Propertyaddressline2 { get; set; }
        public string? Propertycity { get; set; }
        public string? Propertystate { get; set; }
        public string? Propertyzip { get; set; }
        public string? Development { get; set; }
        public int? Agentid { get; set; }
        public string? DisplayAs { get; set; }
        public int? Listingtypeid { get; set; }
        public int? Addressstyleid { get; set; }
        public int? Contactid { get; set; }
        public int? Companyid { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompanyName { get; set; }
        public int? Contactstatusid { get; set; }
        public DateTime? Contactstatusdate { get; set; }
        public string? Zip { get; set; }
    }
}
