﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvpreferredByCompanyAuditHeader
    {
        public int CvpreferredByCompanyAuditHeaderId { get; set; }
        public int CompanyId { get; set; }
        public DateTime AuditDate { get; set; }
        public string AuditBy { get; set; } = null!;
        public string Reason { get; set; } = null!;
        public Guid RowId { get; set; }
    }
}
