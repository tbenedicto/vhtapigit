﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContactDollarUpdate
    {
        public int ContactId { get; set; }
        public decimal DollarsInLast180Days { get; set; }
        public decimal DollarTotal { get; set; }
    }
}
