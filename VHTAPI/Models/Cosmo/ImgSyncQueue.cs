﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImgSyncQueue
    {
        public int MessageId { get; set; }
        public string MessageData { get; set; } = null!;
        public DateTime MessageTimeStamp { get; set; }
        public Guid Rowguid { get; set; }
    }
}
