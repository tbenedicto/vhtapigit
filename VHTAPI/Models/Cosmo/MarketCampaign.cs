﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MarketCampaign
    {
        public MarketCampaign()
        {
            MarketMailings = new HashSet<MarketMailing>();
        }

        public int MarketCampaignId { get; set; }
        public string CampaignName { get; set; } = null!;
        public int DaysInCycle { get; set; }
        public int CustomerFilter { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public bool FilterOnZip { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<MarketMailing> MarketMailings { get; set; }
    }
}
