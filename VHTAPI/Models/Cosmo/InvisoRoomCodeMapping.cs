﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoRoomCodeMapping
    {
        public int Id { get; set; }
        public string InvisoRoomCode { get; set; } = null!;
        public string VhtRoomGroup { get; set; } = null!;
    }
}
