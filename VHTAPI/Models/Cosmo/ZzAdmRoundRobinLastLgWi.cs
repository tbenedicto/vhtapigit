﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzAdmRoundRobinLastLgWi
    {
        public int RoundRobinId { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
