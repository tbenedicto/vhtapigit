﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PrismOfficesOld
    {
        public string? CompanyName { get; set; }
        public string? StreetAddress { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public int? CosmoCompanyId { get; set; }
    }
}
