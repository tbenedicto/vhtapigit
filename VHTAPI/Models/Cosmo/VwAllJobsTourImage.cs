﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllJobsTourImage
    {
        public int ListingId { get; set; }
        public DateTime? ListingAddedDate { get; set; }
        public int TourId { get; set; }
        public DateTime? TourAddedDate { get; set; }
        public int? Jobid { get; set; }
        public int? ProofJobId { get; set; }
        public DateTime? ProofJobAddedDate { get; set; }
        public bool ShouldShowTour { get; set; }
        public bool ShouldShowImageOnTour { get; set; }
        public DateTime? TrcmodifiedDate { get; set; }
        public string? ExternalUrl { get; set; }
        public string? VideoUrlmp4 { get; set; }
        public string TourStatusDescr { get; set; } = null!;
        public string? RoomName { get; set; }
        public int? SortId { get; set; }
        public string? RoomGroup { get; set; }
        public bool? IsProofPurchase { get; set; }
        public bool? IsVirtualStaged { get; set; }
        public bool IsThirdPartyImage { get; set; }
        public bool? IsExterior { get; set; }
        public int? PhotoTypeId { get; set; }
        public string? PhotoType { get; set; }
        public int RoomCodeId { get; set; }
    }
}
