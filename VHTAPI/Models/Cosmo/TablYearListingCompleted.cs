﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablYearListingCompleted
    {
        public int Listingid { get; set; }
        public string? ChildCompany { get; set; }
        public string? ParentCompany { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public DateTime? CompletedDate { get; set; }
        public string ListingType { get; set; } = null!;
        public string? CompanySubsidiary { get; set; }
    }
}
