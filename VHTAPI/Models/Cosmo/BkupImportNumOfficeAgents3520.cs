﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkupImportNumOfficeAgents3520
    {
        public string? AccountName { get; set; }
        public string? SfAccountId { get; set; }
        public double? OfAgents { get; set; }
        public double? OfOffices { get; set; }
        public string? SalesAccountStartDate { get; set; }
    }
}
