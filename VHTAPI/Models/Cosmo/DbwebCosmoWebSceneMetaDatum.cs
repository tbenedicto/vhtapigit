﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DbwebCosmoWebSceneMetaDatum
    {
        public int SceneMetadataId { get; set; }
        public int TourRoomCodeId { get; set; }
        public string DataKey { get; set; } = null!;
        public string DataVal { get; set; } = null!;
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; } = null!;
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
