﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PackageRuleCondRateAdj
    {
        public int PackageRuleCondRateAdjId { get; set; }
        public int PackageRuleId { get; set; }
        public int RateAdjTypeId { get; set; }
        public decimal RateAdj { get; set; }
        public int PackageProductId { get; set; }
        public int SortId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
