﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OnlineOrder
    {
        public Guid? ClientKey { get; set; }
        public int? ContactId { get; set; }
        public int? CompanyId { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public int ListingTypeId { get; set; }
        public string? Comments { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentEmail { get; set; }
        public string? AgentCompanyName { get; set; }
        public string? AgentOfficeAddress { get; set; }
        public string? AgentOfficeCity { get; set; }
        public string? AgentOfficeState { get; set; }
        public string? AgentOfficeZip { get; set; }
        public string? AgentPhone { get; set; }
        public int? PixAgentId { get; set; }
        public string? CampaignName { get; set; }
        public string? CampaignSource { get; set; }
        public string? CampaignMedium { get; set; }
        public string? CampaignTerm { get; set; }
        public string? CampaignContent { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public int? JobId { get; set; }
        public int? CustomerContactId { get; set; }
        public string Source { get; set; } = null!;
        public int? ListingId { get; set; }
        public bool? CbdfwcorpOrder { get; set; }
        public bool? CorporateOrder { get; set; }
        public bool? AtPropertiesOrder { get; set; }
        public string? PropertyDevelopment { get; set; }
        public decimal? PropertyBeds { get; set; }
        public decimal? PropertyBaths { get; set; }
        public string? Distribution { get; set; }
        public string? MiscellaneousComment { get; set; }
        public int? AgentPhoneType { get; set; }
        public string? SchedulingAgentChoice { get; set; }
        public DateTime? SchedulingPreferredDate1 { get; set; }
        public string? SchedulingPreferredTime1 { get; set; }
        public DateTime? SchedulingPreferredDate2 { get; set; }
        public string? SchedulingPreferredTime2 { get; set; }
        public int? ListingCategoryId { get; set; }
        public decimal? ListPrice { get; set; }
        public bool? HideDuplicateOrderFormComments { get; set; }
        public int? VideographerContactId { get; set; }
        public int? ContactCreditCardId { get; set; }
        public int? JobSourceId { get; set; }
        public int? EmailRetryCount { get; set; }
        public int OnlineOrderId { get; set; }
        public string? OrderFormUrl { get; set; }
        public string? SchedulingContactName { get; set; }
        public string? SchedulingContactPhone { get; set; }
        public string? SchedulingContactEmail { get; set; }
        public string? AgentOfficeAddress2 { get; set; }
        public string? PromoCode { get; set; }
        public string? JobSource { get; set; }
        public bool? VideographerPreferred { get; set; }
        public bool? OrderEmailPending { get; set; }
    }
}
