﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbOnlineOrderPackage
    {
        public int OnlineOrderPackageId { get; set; }
        public int OnlineOrderId { get; set; }
        public int PackageId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
