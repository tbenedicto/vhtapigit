﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwJobTotalPrice
    {
        public int JobId { get; set; }
        public decimal JobTotalPhotographerMiscPay { get; set; }
        public int JobTotalPhotographerMilesTraveled { get; set; }
        public decimal JobTotalPhotographerMileagePayRate { get; set; }
        public decimal? JobTotalPhotographerMileageCompensation { get; set; }
        public decimal? JobTotalPhotographerBasePay { get; set; }
        public decimal? JobTotalPhotographerUpsellPay { get; set; }
        public decimal? JobTotalPhotographerPay { get; set; }
        public decimal? JobTotalAgentPrice { get; set; }
        public decimal? JobTotalCorporatePrice { get; set; }
        public decimal? JobTotalPrice { get; set; }
        public decimal? JobTotalRevenue { get; set; }
        public int? NumberOfProducts { get; set; }
    }
}
