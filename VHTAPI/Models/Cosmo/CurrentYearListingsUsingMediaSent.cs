﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CurrentYearListingsUsingMediaSent
    {
        public string ListingType { get; set; } = null!;
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? ListingsWithMediaSent { get; set; }
    }
}
