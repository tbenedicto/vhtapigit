﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VedderMissingDvdInfo
    {
        public int ListingId { get; set; }
        public int RoomCodeId { get; set; }
        public string? Basepath { get; set; }
        public int ContentServerId { get; set; }
        public string G3 { get; set; } = null!;
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public int G3codeId { get; set; }
        public DateTime? TrcAddeddate { get; set; }
        public string? VhtphotoUrl { get; set; }
    }
}
