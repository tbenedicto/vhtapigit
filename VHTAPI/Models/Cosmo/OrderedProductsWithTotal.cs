﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderedProductsWithTotal
    {
        public string? OrderComponentId { get; set; }
        public string? OrderContainerId { get; set; }
        public string? OrderId { get; set; }
        public string? Type { get; set; }
        public string? Quantity { get; set; }
        public string? OrderComponentCreatedAt { get; set; }
        public string? OrderComponentDeliveredAt { get; set; }
        public string? Options { get; set; }
        public string? Data { get; set; }
        public string? BookedResourceType { get; set; }
        public string? BookedResourceId { get; set; }
        public string? Total { get; set; }
    }
}
