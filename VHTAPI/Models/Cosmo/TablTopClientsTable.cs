﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablTopClientsTable
    {
        public int AgentId { get; set; }
        public DateTime? JobCompletedDate { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentEmail { get; set; }
        public string? CompanyFullName { get; set; }
        public string? TopCompanyFullName { get; set; }
        public string? Territory { get; set; }
        public string? TerritoryState { get; set; }
        public int? TotalJobs { get; set; }
        public decimal? TotalPhotographerPay { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? AvgAmountPerListing { get; set; }
        public int? TotalJobProductQuantity { get; set; }
        public int? TotalListings { get; set; }
        public int? TotalPhotographers { get; set; }
        public int? Num3D { get; set; }
        public int? Aerial { get; set; }
        public int? Albums { get; set; }
        public int? Clear360 { get; set; }
        public int? Duplicates { get; set; }
        public int? FloorPlans { get; set; }
        public int? GoldSeries { get; set; }
        public int? PlatinumSeries { get; set; }
        public int? PremiumStills { get; set; }
        public int? Prints { get; set; }
        public int? SilverSeries { get; set; }
        public int? Studio360 { get; set; }
        public int? StandardStills { get; set; }
        public int? VhtcustomTours { get; set; }
        public int? Videos { get; set; }
    }
}
