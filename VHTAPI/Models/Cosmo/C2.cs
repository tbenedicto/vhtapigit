﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class C2
    {
        public string Subject { get; set; } = null!;
        public DateTime? CommentAddeddate { get; set; }
        public int JobId { get; set; }
        public DateTime? StatusNewCompletedOn { get; set; }
        public string? StatusNewCompletedBy { get; set; }
        public DateTime? StatusInFieldCompletedOn { get; set; }
        public string? StatusInFieldCompletedBy { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public string? StatusProductionCompletedBy { get; set; }
        public DateTime? StatusAccountingCompletedOn { get; set; }
        public string? StatusAccountingCompletedBy { get; set; }
        public DateTime? StatusCompleteCompletedOn { get; set; }
        public string? StatusCompleteCompletedBy { get; set; }
        public DateTime? TaskCvassignedCompletedOn { get; set; }
        public string? TaskCvassignedCompletedBy { get; set; }
        public DateTime? TaskAgentPymtInfoVerifiedCompletedOn { get; set; }
        public string? TaskAgentPymtInfoVerifiedCompletedBy { get; set; }
        public DateTime? TaskApptEmailSentCompletedOn { get; set; }
        public string? TaskApptEmailSentCompletedBy { get; set; }
        public DateTime? TaskConfEmailSentCompletedOn { get; set; }
        public string? TaskConfEmailSentCompletedBy { get; set; }
        public DateTime? TaskViewedByCvCompletedOn { get; set; }
        public string? TaskViewedByCvCompletedBy { get; set; }
        public DateTime? TaskInitialPhoneCallMadeCompletedOn { get; set; }
        public string? TaskInitialPhoneCallMadeCompletedBy { get; set; }
        public DateTime? TaskMediaReceivedCompletedOn { get; set; }
        public string? TaskMediaReceivedCompletedBy { get; set; }
        public DateTime? TaskJobCheckedInCompletedOn { get; set; }
        public string? TaskJobCheckedInCompletedBy { get; set; }
        public DateTime? TaskCvcompensationVerifiedCompletedOn { get; set; }
        public string? TaskCvcompensationVerifiedCompletedBy { get; set; }
        public DateTime? TaskDigproductsCompletedCompletedOn { get; set; }
        public string? TaskDigproductsCompletedCompletedBy { get; set; }
        public DateTime? TaskVirtproductsCompletedCompletedOn { get; set; }
        public string? TaskVirtproductsCompletedCompletedBy { get; set; }
        public DateTime? TaskCdromcompletedCompletedOn { get; set; }
        public string? TaskCdromcompletedCompletedBy { get; set; }
        public DateTime? TaskProdCompleteEmailSentCompletedOn { get; set; }
        public string? TaskProdCompleteEmailSentCompletedBy { get; set; }
        public DateTime? TaskVidproductsCompletedCompletedOn { get; set; }
        public string? TaskVidproductsCompletedCompletedBy { get; set; }
        public DateTime? TaskPrintsCompletedCompletedOn { get; set; }
        public string? TaskPrintsCompletedCompletedBy { get; set; }
        public DateTime? TaskPostingSitesCompleteCompletedOn { get; set; }
        public string? TaskPostingSitesCompleteCompletedBy { get; set; }
        public DateTime? TaskAcctReviewCompleteCompletedOn { get; set; }
        public string? TaskAcctReviewCompleteCompletedBy { get; set; }
        public DateTime? TaskInvoiceGeneratedCompletedOn { get; set; }
        public string? TaskInvoiceGeneratedCompletedBy { get; set; }
        public DateTime? TaskAgentPaymentProcessedCompletedOn { get; set; }
        public string? TaskAgentPaymentProcessedCompletedBy { get; set; }
        public Guid Rowguid { get; set; }
        public DateTime? TaskVhttourPropertyInfoReceivedCompletedOn { get; set; }
        public string? TaskVhttourPropertyInfoReceivedCompletedBy { get; set; }
        public DateTime? TaskVhttourScriptWrittenCompletedOn { get; set; }
        public string? TaskVhttourScriptWrittenCompletedBy { get; set; }
        public DateTime? TaskVhttourScriptApprovedCompletedOn { get; set; }
        public string? TaskVhttourScriptApprovedCompletedBy { get; set; }
        public DateTime? TaskVhttourVoiceoverSentCompletedOn { get; set; }
        public string? TaskVhttourVoiceoverSentCompletedBy { get; set; }
        public DateTime? TaskVhttourVoiceoverCompletedCompletedOn { get; set; }
        public string? TaskVhttourVoiceoverCompletedCompletedBy { get; set; }
        public DateTime? TaskVhttourSentToScriptWriterCompletedOn { get; set; }
        public string? TaskVhttourSentToScriptWriterCompletedBy { get; set; }
        public DateTime? TaskVhttourSentForApprovalCompletedOn { get; set; }
        public string? TaskVhttourSentForApprovalCompletedBy { get; set; }
        public Guid? OriginDatasourceId { get; set; }
        public int? OrigContactId { get; set; }
        public int? OrigCompanyId { get; set; }
        public int? OrigContactStatusId { get; set; }
        public int? OrigContactSourceId { get; set; }
        public int? OrigContactVipflagId { get; set; }
        public int? OrigContactTlcflagId { get; set; }
        public int? OrigContactClassificationId { get; set; }
        public int? OrigContactBusinessTypeId { get; set; }
        public int? OrigContactClientStatusId { get; set; }
        public int? OrigRepPhotographer { get; set; }
        public int? OrigRepSalesLocal { get; set; }
        public int? OrigRepSalesRegional { get; set; }
        public int? OrigRepSalesNational { get; set; }
        public int? OrigRepCustomerService { get; set; }
    }
}
