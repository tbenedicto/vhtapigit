﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class FmordersCompleted
    {
        public string? VhtJobId { get; set; }
        public string? JobStatus { get; set; }
        public DateTime? OrderCancelDate { get; set; }
        public string? JobCancellationReason { get; set; }
        public DateTime? OrderDate { get; set; }
        public string? ProductionCompletedDate { get; set; }
        public string? PropertyAddress { get; set; }
        public string? PropertyCity { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? ReoId { get; set; }
        public string? Url { get; set; }
        public string? ProductName { get; set; }
        public decimal? Quantity { get; set; }
    }
}
