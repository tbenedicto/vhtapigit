﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PackageRuleProductDatum
    {
        public int PackageRuleProductDataId { get; set; }
        public int PackageRuleId { get; set; }
        public int PackageProductId { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? CustomerPay { get; set; }
        public decimal? CustomerRateAdj { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? CorporateRateAdj { get; set; }
        public decimal? Cvbase { get; set; }
        public decimal? CvrateAdj { get; set; }
        public decimal? Cvbonus { get; set; }
        public string? ScreenshotUrl { get; set; }
        public string? SampleUrl { get; set; }
        public int? ProofPhotoCredit { get; set; }
        public decimal? ProofAmountCredit { get; set; }
        public decimal? ProofPercentCredit { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
