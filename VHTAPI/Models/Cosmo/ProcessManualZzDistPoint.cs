﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessManualZzDistPoint
    {
        public int ProcessManualId { get; set; }
        public int DistributionPointId { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateRemoved { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ZzDistributionPoint DistributionPoint { get; set; } = null!;
        public virtual ProcessManual ProcessManual { get; set; } = null!;
    }
}
