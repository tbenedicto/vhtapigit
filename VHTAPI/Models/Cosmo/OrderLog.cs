﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderLog
    {
        public long OrderLogId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string? Customer { get; set; }
        public string? Afn { get; set; }
        public string? Aln { get; set; }
        public string? Cct { get; set; }
        public string? Ccn { get; set; }
        public string? Exm { get; set; }
        public string? Exy { get; set; }
        public string? Ccm { get; set; }
        public string? Ba1 { get; set; }
        public string? Ba2 { get; set; }
        public string? Bcy { get; set; }
        public string? Bst { get; set; }
        public string? Bzp { get; set; }
        public string? Cn { get; set; }
        public string? Cs { get; set; }
    }
}
