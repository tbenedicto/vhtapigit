﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkupCosmoCompanyOfficeLoaderMb
    {
        public string? ParentCompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? Street { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? NumOfOffices { get; set; }
        public string? NumOfagents { get; set; }
        public string? Companyphone { get; set; }
        public int? CompanyId { get; set; }
    }
}
