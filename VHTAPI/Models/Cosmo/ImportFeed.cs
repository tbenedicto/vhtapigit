﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportFeed
    {
        public int FeedId { get; set; }
        public string FeedName { get; set; } = null!;
        public string Nickname { get; set; } = null!;
        public string SourceDescrForUser { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
