﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzOrderFormProductCategory
    {
        public int OrderFormProductCategoryId { get; set; }
        public string Description { get; set; } = null!;
    }
}
