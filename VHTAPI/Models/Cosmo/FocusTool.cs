﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class FocusTool
    {
        public int FocusToolId { get; set; }
        public string Title { get; set; } = null!;
        public int SourceType { get; set; }
        public int? LinkType { get; set; }
        public string? Link { get; set; }
        public string Description { get; set; } = null!;
        public string? SourceData { get; set; }
        public Guid Rowguid { get; set; }
    }
}
