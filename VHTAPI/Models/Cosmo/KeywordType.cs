﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class KeywordType
    {
        public int KeywordTypeId { get; set; }
        public int KeywordCategoryId { get; set; }
        public string Description { get; set; } = null!;
        public string? ParentColumnId { get; set; }
        public string? ChildColumnId { get; set; }
        public string? ChildDataColumn { get; set; }
        public Guid Rowguid { get; set; }
    }
}
