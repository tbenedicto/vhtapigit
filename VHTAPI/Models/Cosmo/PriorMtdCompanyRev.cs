﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PriorMtdCompanyRev
    {
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? DisplayAsCompany { get; set; }
        public string? StateId { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public decimal? TotalRev { get; set; }
        public int? TotalOrders { get; set; }
        public int? TotalClients { get; set; }
        public decimal? YtdRev { get; set; }
        public int? Ytdorders { get; set; }
        public int? YtdClients { get; set; }
        public decimal? YtdRevLessFees { get; set; }
        public decimal? Pcrev { get; set; }
        public int? Pcorders { get; set; }
        public int? Pcclients { get; set; }
        public decimal? TotNewRenewRev { get; set; }
        public int? TotNewRenewOrders { get; set; }
        public int? TotNewRenewClients { get; set; }
        public int? ComhistYear { get; set; }
        public int? ComhistMonth { get; set; }
        public decimal? Trailing11monthRevNewRenew { get; set; }
        public decimal? TrailingYtdRenewRevenue { get; set; }
        public decimal? ComhistTotalRev { get; set; }
        public decimal? ComhistNewRev { get; set; }
        public decimal? ComhistYtdRev { get; set; }
        public decimal? ComhistPcrev { get; set; }
        public decimal? ComhistTrailingYtdRenewRevenue { get; set; }
        public decimal? ComhistTotNewRenewRev { get; set; }
        public decimal? ComhistTrailing11monthRevNewRenew { get; set; }
        public decimal? ComhistTrailingYtdNewRenewRevenue { get; set; }
    }
}
