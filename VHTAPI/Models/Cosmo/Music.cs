﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Music
    {
        public int MusicId { get; set; }
        public int MusicGenreId { get; set; }
        public int MusicSubtypeId { get; set; }
        public string Title { get; set; } = null!;
        public string Descr { get; set; } = null!;
        public string NetworkPath { get; set; } = null!;
        public string Artist { get; set; } = null!;
        public string Album { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
