﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class YoyThisMonth
    {
        public DateTime? DateLastYear { get; set; }
        public DateTime? DateThisYear { get; set; }
        public int? AllListingsLastYear { get; set; }
        public int? AllListingsThisYear { get; set; }
        public int? ResListingsLastYear { get; set; }
        public int? ResListingsThisYear { get; set; }
        public int? EcplistingsLastYear { get; set; }
        public int? EcplistingsThisYear { get; set; }
        public int? FloorPlansLastYear { get; set; }
        public int? FloorPlansThisYear { get; set; }
        public int? PhoneThisYear { get; set; }
        public int? OnlineThisYear { get; set; }
        public int? ImportThisYear { get; set; }
        public int? MmsimportThisYear { get; set; }
        public int? EmailThisYear { get; set; }
        public int? FannieMaeThisYear { get; set; }
    }
}
