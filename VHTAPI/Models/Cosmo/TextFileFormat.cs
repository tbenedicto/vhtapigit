﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TextFileFormat
    {
        public int G3codeId { get; set; }
        public string? G3code { get; set; }
        public int? TextFileFormatId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
