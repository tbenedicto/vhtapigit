﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzRunStatus
    {
        public ZzRunStatus()
        {
            MrstatusDaemons = new HashSet<MrstatusDaemon>();
        }

        public int RunStatusId { get; set; }
        public int SortId { get; set; }
        public string? Descr { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<MrstatusDaemon> MrstatusDaemons { get; set; }
    }
}
