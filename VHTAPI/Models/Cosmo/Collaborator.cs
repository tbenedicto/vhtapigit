﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Collaborator
    {
        public int CollaboratorId { get; set; }
        public string Email { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string? Password { get; set; }
        public string? Lastname { get; set; }
        public string? Firstname { get; set; }
        public string? Company { get; set; }
        public Guid Rowguid { get; set; }
        public DateTime? LastLogin { get; set; }
    }
}
