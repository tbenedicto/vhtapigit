﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Mb2ProductRegionZipcode
    {
        public int ProductRegionId { get; set; }
        public string Zip { get; set; } = null!;
        public string? State { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
