﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvcontactAvailability
    {
        public int CvcontactAvailabilityId { get; set; }
        public int ContactId { get; set; }
        public int CvavailabilityTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string? Note { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public Guid Rowguid11 { get; set; }
    }
}
