﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessScheduleDateOptionType
    {
        public ProcessScheduleDateOptionType()
        {
            ProcessSchedules = new HashSet<ProcessSchedule>();
        }

        public int ProcessScheduleDateOptionTypeId { get; set; }
        public string DateOptionSwitch { get; set; } = null!;
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<ProcessSchedule> ProcessSchedules { get; set; }
    }
}
