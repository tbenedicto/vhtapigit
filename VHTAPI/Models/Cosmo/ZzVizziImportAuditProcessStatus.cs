﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzVizziImportAuditProcessStatus
    {
        public int Id { get; set; }
        public string? Description { get; set; }
    }
}
