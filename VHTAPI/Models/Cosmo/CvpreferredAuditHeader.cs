﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvpreferredAuditHeader
    {
        public int CvpreferredAuditHeaderId { get; set; }
        public int ContactId { get; set; }
        public DateTime AuditDate { get; set; }
        public string AuditBy { get; set; } = null!;
        public string Reason { get; set; } = null!;
        public Guid RowId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
