﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TimeslotAllocation
    {
        public TimeslotAllocation()
        {
            Voddeliveries = new HashSet<Voddelivery>();
        }

        public int TimeslotAllocationId { get; set; }
        public int DistributionAssignmentId { get; set; }
        public int TimeslotId { get; set; }
        public int StatusId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public byte[] Tsvalue { get; set; } = null!;

        public virtual DistributionAssignment DistributionAssignment { get; set; } = null!;
        public virtual TimeslotAllocationStatus Status { get; set; } = null!;
        public virtual Timeslot Timeslot { get; set; } = null!;
        public virtual ICollection<Voddelivery> Voddeliveries { get; set; }
    }
}
