﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TempToDropObject
    {
        public int ObjectId { get; set; }
        public string Name { get; set; } = null!;
        public string? Type { get; set; }
    }
}
