﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MatchingImageRef
    {
        public int MatchingImageRefId { get; set; }
        public int MatchingImageId { get; set; }
        public string RefUrl { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
