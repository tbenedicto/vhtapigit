﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SingleSignOnKey
    {
        public Guid SignOnKey { get; set; }
        public string BrokerageIpaddress { get; set; } = null!;
        public DateTime CreationDate { get; set; }
    }
}
