﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TourRoomCodeRoomCodeCategory
    {
        public int TourRoomCodeId { get; set; }
        public int RoomcodeCategoryId { get; set; }
    }
}
