﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DocCategory
    {
        public int DocCategoryId { get; set; }
        public int SortId { get; set; }
        public string CategoryName { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
