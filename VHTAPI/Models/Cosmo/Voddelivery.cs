﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Voddelivery
    {
        public int VoddeliveryId { get; set; }
        public string ProcessTag { get; set; } = null!;
        public int ListingId { get; set; }
        public int TimeslotAllocationId { get; set; }
        public int ProcessStatus { get; set; }
        public Guid Rowguid { get; set; }

        public virtual TimeslotAllocation TimeslotAllocation { get; set; } = null!;
    }
}
