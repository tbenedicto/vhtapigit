﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ReportInfo
    {
        public int ReportId { get; set; }
        public string ReportObject { get; set; } = null!;
        public string ReportName { get; set; } = null!;
        public string ReportDesc { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public int SortOrder { get; set; }
        public Guid Rowguid { get; set; }
    }
}
