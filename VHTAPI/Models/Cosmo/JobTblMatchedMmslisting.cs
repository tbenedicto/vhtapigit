﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobTblMatchedMmslisting
    {
        public int Rowid { get; set; }
        public int? CosmoListingId { get; set; }
        public string? MlsIds { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? ListingAgent { get; set; }
        public int? MmslistingId { get; set; }
        public string? MmswebId { get; set; }
        public string? MmsdisplayAddress { get; set; }
        public string? MmslistingAddressLine1 { get; set; }
        public string? MmslistingAddressLine2 { get; set; }
        public string? MmslistingCity { get; set; }
        public string? MmslistingState { get; set; }
        public string? MmslistingZip { get; set; }
        public string? MmslistingAgents { get; set; }
        public bool? Matched { get; set; }
    }
}
