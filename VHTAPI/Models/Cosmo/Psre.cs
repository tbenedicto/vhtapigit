﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Psre
    {
        public string Mlsid { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string Website { get; set; } = null!;
        public int ContactId { get; set; }
    }
}
