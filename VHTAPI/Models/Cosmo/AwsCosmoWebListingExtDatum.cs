﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebListingExtDatum
    {
        public int ListingId { get; set; }
        public DateTime? ListingDate { get; set; }
        public string? Area { get; set; }
        public string? Township { get; set; }
        public string? Waterfront { get; set; }
        public int? YearBuilt { get; set; }
        public string? AgeofProperty { get; set; }
        public string? LotDimensions { get; set; }
        public string? TotalNumofRooms { get; set; }
        public string? Basement { get; set; }
        public string? Parking { get; set; }
        public string? NumberofCars { get; set; }
        public string? ParcelId { get; set; }
        public string? Taxes { get; set; }
        public string? SpecialAssessments { get; set; }
        public string? AssociationDues { get; set; }
        public string? FrequencyofDues { get; set; }
        public string? RoomSizeLivingRoom { get; set; }
        public string? RoomSizeDiningRoom { get; set; }
        public string? RoomSizeKitchen { get; set; }
        public string? RoomSizeFamilyRoom { get; set; }
        public string? RoomSizeMasterBedroom { get; set; }
        public string? RoomSizeBedroom2 { get; set; }
        public string? RoomSizeBedroom3 { get; set; }
        public string? RoomSizeBedroom4 { get; set; }
        public string? RoomSizeBedroom5 { get; set; }
        public string? RoomSizeOther1 { get; set; }
        public string? RoomSizeOther2 { get; set; }
        public string? RoomSizeOther3 { get; set; }
        public string? SchoolDistrict { get; set; }
        public string? Remarks { get; set; }
        public string? OwnersPhone { get; set; }
        public int? MaintCc { get; set; }
        public double? FinancePerccAllowed { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public byte[] RowTimeStamp { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
