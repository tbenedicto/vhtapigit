﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvpreferredByCompanyAuditDetail
    {
        public int CvpreferredByCompanyAuditDetailId { get; set; }
        public int CvpreferredByCompanyAuditHeaderId { get; set; }
        public int VideographerContactId { get; set; }
        public int OrderIndex { get; set; }
        public Guid RowId { get; set; }
    }
}
