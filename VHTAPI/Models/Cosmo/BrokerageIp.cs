﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BrokerageIp
    {
        public string Ipaddress { get; set; } = null!;
        public int BrokerageCompanyId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual Company BrokerageCompany { get; set; } = null!;
    }
}
