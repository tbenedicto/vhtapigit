﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GalleryQueue
    {
        public int GalleryQueueId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int JobId { get; set; }
        public string VhtjobId { get; set; } = null!;
        public int ListingId { get; set; }
        public string PropertyState { get; set; } = null!;
        public bool Result { get; set; }
    }
}
