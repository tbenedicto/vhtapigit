﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkupProductRulesImportLog20210726
    {
        public int ProductRulesImportLogId { get; set; }
        public int? RowId { get; set; }
        public string? Text { get; set; }
    }
}
