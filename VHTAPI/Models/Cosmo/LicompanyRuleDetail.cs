﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LicompanyRuleDetail
    {
        public int ParentCompanyId { get; set; }
        public int ListingIdentifierTypeId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ZzListingIdentifierType ListingIdentifierType { get; set; } = null!;
        public virtual LicompanyRule ParentCompany { get; set; } = null!;
    }
}
