﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PhotographerRepCommission
    {
        public int PhotographerCompanyType { get; set; }
        public string Descr { get; set; } = null!;
        public double? Month1 { get; set; }
        public double? Month212 { get; set; }
    }
}
