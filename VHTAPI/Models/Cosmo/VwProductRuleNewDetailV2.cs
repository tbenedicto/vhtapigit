﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwProductRuleNewDetailV2
    {
        public string? VhtReleationship { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public string? Ecp { get; set; }
        public string? Available { get; set; }
        public string? Default { get; set; }
        public string? Preferred { get; set; }
        public string? MarketRateCompanyOverride { get; set; }
        public int ProductRuleId { get; set; }
        public int ProductId { get; set; }
        public string? InternalName { get; set; }
        public int RuleConditionTypeId { get; set; }
        public int? ContactId { get; set; }
        public string? AgentName { get; set; }
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompanyName { get; set; }
        public int? Cvid { get; set; }
        public string? CvName { get; set; }
        public int? ProductRegionId { get; set; }
        public string? ProductRegion { get; set; }
        public string? StateId { get; set; }
        public int? ContactGroupId { get; set; }
        public string? ContactGroup { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? CustomerPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? Cvbase { get; set; }
        public decimal? Cvbonus { get; set; }
        public string? ScreenshotUrl { get; set; }
        public string? SampleUrl { get; set; }
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
