﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Appointment
    {
        public int AppointmentId { get; set; }
        public int ContactId { get; set; }
        public int? JobId { get; set; }
        public int AppointmentTypeId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime? ExpiresDateTime { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public int? RecurrenceFrequencyId { get; set; }
        public int? RecurrenceMaxCount { get; set; }
        public DateTime? RecurrenceMaxDate { get; set; }
        public int? StartAddressId { get; set; }
        public int? EndAddressId { get; set; }
        public string? Subject { get; set; }
        public string? Descr { get; set; }
        public bool? WeeklyAvailability { get; set; }
    }
}
