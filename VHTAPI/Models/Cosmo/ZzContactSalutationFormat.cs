﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzContactSalutationFormat
    {
        public int ContactSalutationFormatId { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
