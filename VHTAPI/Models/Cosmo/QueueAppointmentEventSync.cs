﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class QueueAppointmentEventSync
    {
        public int QueueId { get; set; }
        public int AppointmentId { get; set; }
        public string? CalendarEventAction { get; set; }
        public string? CalendarId { get; set; }
        public string? EventId { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Product { get; set; }
        public string? CompanyName { get; set; }
        public int? ContactId { get; set; }
        public string? Artist { get; set; }
        public string? NotificationEmail { get; set; }
        public string? Agent { get; set; }
        public string? AgentMobilePhone { get; set; }
        public string? AgentOfficePhone { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public DateTime DateAdded { get; set; }
        public bool Processing { get; set; }
        public bool Processed { get; set; }
        public DateTime? DateLastProcessed { get; set; }
        public string? ProcessMessage { get; set; }
        public Guid Rowguid { get; set; }
        public int JobId { get; set; }
        public int AppointmentTypeId { get; set; }
        public int? RecurrenceFreqId { get; set; }
        public int? RecurrenceMaxCount { get; set; }
        public DateTime? RecurrenceMaxDate { get; set; }
    }
}
