﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CpixPrepaid1152018After2015LoadSummed
    {
        public int? Contactid { get; set; }
        public int? Productid { get; set; }
        public int? Totalbal { get; set; }
    }
}
