﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Process
    {
        public Process()
        {
            DistributionAlerts = new HashSet<DistributionAlert>();
            ProcessRuns = new HashSet<ProcessRun>();
            ProcessSchedules = new HashSet<ProcessSchedule>();
        }

        public int ProcessId { get; set; }
        public int SortId { get; set; }
        public string ProcessName { get; set; } = null!;
        public string Descr { get; set; } = null!;
        public DateTime? LastVerified { get; set; }
        public string? GeneralNotes { get; set; }
        public string? OperatorNotes { get; set; }
        public string? FeedContacts { get; set; }
        public string? AutoContactEmail { get; set; }
        public bool IsActive { get; set; }
        public int PartnerProcessingModeId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<DistributionAlert> DistributionAlerts { get; set; }
        public virtual ICollection<ProcessRun> ProcessRuns { get; set; }
        public virtual ICollection<ProcessSchedule> ProcessSchedules { get; set; }
    }
}
