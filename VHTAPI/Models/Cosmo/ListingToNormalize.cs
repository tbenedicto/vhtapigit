﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingToNormalize
    {
        public int ListingToNormalizeId { get; set; }
        public int ListingId { get; set; }
    }
}
