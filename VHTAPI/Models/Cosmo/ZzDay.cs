﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzDay
    {
        public string? No { get; set; }
        public string? DayDescr { get; set; }
    }
}
