﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllGeneralrule
    {
        public int GeneralRuleId { get; set; }
        public int RuleConditionTypeId { get; set; }
        public string Type { get; set; } = null!;
        public int? ContactId { get; set; }
        public string? ContactName { get; set; }
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public int? Cvid { get; set; }
        public string? Cvname { get; set; }
        public int? ProductRegionId { get; set; }
        public string? Market { get; set; }
        public string? StateId { get; set; }
        public string? StateName { get; set; }
        public int? ContactGroupId { get; set; }
        public string? ContactGroupName { get; set; }
        public int CvmileageTypeId { get; set; }
        public decimal? CvmileageAmount { get; set; }
        public int AvailableProductsOpId { get; set; }
        public int PreferredProductsOpId { get; set; }
        public int DefaultProductsOpId { get; set; }
        public int DistributionSitesOpId { get; set; }
        public int AvailablePackagesOpId { get; set; }
        public int PreferredPackagesOpId { get; set; }
        public int DefaultPackagesOpId { get; set; }
    }
}
