﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllAgent
    {
        public int AgentId { get; set; }
        public string? AgentContactCode { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentName { get; set; }
        public DateTime? AgentAddedDate { get; set; }
        public string? AgentCategory { get; set; }
        public string? AgentStatus { get; set; }
        public string? AgentStatusReason { get; set; }
        public string? AgentSource { get; set; }
        public string? Tlc { get; set; }
        public string? Vip { get; set; }
        public decimal? Agent { get; set; }
        public int? AgentLifetimeListings { get; set; }
        public int? AgentLifetimeJobsOver50 { get; set; }
        public string? AgentEmail { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? AgentStreetAddress { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? AgentCompanyId { get; set; }
        public string? AgentPhone { get; set; }
        public decimal? AgentLifetimeRevenue { get; set; }
        public int? ListingLifetime { get; set; }
        public int? PreferredPhotographerId { get; set; }
        public DateTime? AgentFirstStartDate { get; set; }
        public string? ContactHistoryType { get; set; }
        public string? AgentMlsid { get; set; }
    }
}
