﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCompanyContactSearch6
    {
        public int CompanyId { get; set; }
        public string? CompanyDisplayAs { get; set; }
        public int? CompanyCategoryId { get; set; }
        public string? CompanyName { get; set; }
        public int? CompanyStatusId { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public string? ContactDisplayAs { get; set; }
        public string? CompanyCommunicationText { get; set; }
        public bool TopLevelReportingInd { get; set; }
        public int? ParentCompanyId { get; set; }
        public int? RepSalesLocal { get; set; }
        public int? RepSalesRegional { get; set; }
        public int? RepSalesNational { get; set; }
        public int? RepCustomerService { get; set; }
        public int? CompanyTypeId { get; set; }
        public int? OfficeTypeId { get; set; }
        public int? RepSalesNcs { get; set; }
        public int? RepSalesSe { get; set; }
    }
}
