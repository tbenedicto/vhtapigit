﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdeReassignment2015Updated
    {
        public double? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? Ade { get; set; }
        public double? Agents { get; set; }
        public double? Offices { get; set; }
        public string? CompanyNameFromMikeBrown { get; set; }
        public string? NotInSf { get; set; }
        public string? StateId { get; set; }
        public string? Market { get; set; }
        public double? NewAgents { get; set; }
        public decimal? TotalRev { get; set; }
        public decimal? Pcrev { get; set; }
        public decimal? TotNewRenewRev { get; set; }
        public string? F14 { get; set; }
        public string? F15 { get; set; }
        public string? F16 { get; set; }
        public string? F17 { get; set; }
        public string? F18 { get; set; }
        public int? SalespersonId { get; set; }
    }
}
