﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NewCommissionReportMonthly
    {
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompany { get; set; }
        public string? RegionalSalesRep { get; set; }
        public string? AreaSalesRep { get; set; }
        public string? Ade { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public decimal? TotalRev { get; set; }
        public int? TotalOrders { get; set; }
        public int? TotalClients { get; set; }
        public decimal ImageWorksRev { get; set; }
        public int ImageWorksOrders { get; set; }
        public int ImageWorksClients { get; set; }
        public decimal NewTrailingRev { get; set; }
        public int NewTrailingOrders { get; set; }
        public int NewTrailingClients { get; set; }
        public decimal NewRev { get; set; }
        public int NewOrders { get; set; }
        public int NewClients { get; set; }
        public decimal YtdRev { get; set; }
        public int Ytdorders { get; set; }
        public int YtdClients { get; set; }
        public decimal YtdRevLessFees { get; set; }
        public decimal FeesRev { get; set; }
        public int FeesOrders { get; set; }
        public int FeesClients { get; set; }
        public decimal Pcrev { get; set; }
        public int Pcorders { get; set; }
        public int Pcclients { get; set; }
        public decimal RenewTrailingRev { get; set; }
        public int RenewTrailingOrders { get; set; }
        public int RenewTrailingClients { get; set; }
        public decimal RenewRev { get; set; }
        public int RenewOrders { get; set; }
        public int RenewClients { get; set; }
        public decimal MktDomintorRev { get; set; }
        public int MktDomintorOrders { get; set; }
        public int MktDomintorClients { get; set; }
        public decimal CommercialRev { get; set; }
        public int CommercialOrders { get; set; }
        public int CommercialClients { get; set; }
        public decimal NoFeeOver50Rev { get; set; }
        public int NoFeeOver50Orders { get; set; }
        public int NoFeeOver50Clients { get; set; }
        public decimal? ExistingResidentalRev { get; set; }
        public int? ExistingResidentalOrders { get; set; }
        public int? ExistingResidentalClients { get; set; }
        public string? StateId { get; set; }
        public decimal? TotNewRenewRev { get; set; }
        public int? TotNewRenewOrders { get; set; }
        public int? TotNewRenewClients { get; set; }
        public decimal? TrailingRevNewRenew { get; set; }
        public int? TrailingOrdersNewRenew { get; set; }
        public int? TrailingClientsNewRenew { get; set; }
        public decimal YtdNewRenewRevenue { get; set; }
        public decimal? CarryOverTrailingRevNewRenew { get; set; }
        public int YtdNewRenewOrders { get; set; }
        public int YtdNewRenewClients { get; set; }
    }
}
