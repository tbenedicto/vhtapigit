﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class QueueMediaExportToMm
    {
        public int QueueId { get; set; }
        public int ListingId { get; set; }
        public int ContactId { get; set; }
        public bool DeleteExistingImgs { get; set; }
        public bool AddScreenPlay { get; set; }
        public bool CreateListingIfNotExists { get; set; }
        public DateTime DateAdded { get; set; }
        public bool Processed { get; set; }
        public DateTime? DateLastProcessed { get; set; }
        public string? ProcessMessage { get; set; }
        public DateTime? DateListingCompleted { get; set; }
        public bool? AddVirtualTourUrl { get; set; }
    }
}
