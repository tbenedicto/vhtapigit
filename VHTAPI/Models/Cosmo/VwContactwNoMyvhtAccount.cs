﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContactwNoMyvhtAccount
    {
        public string? Loginemail { get; set; }
        public int Contactid { get; set; }
        public string? Contactcode { get; set; }
        public string? Agentname { get; set; }
        public string? Officename { get; set; }
        public int Companyid { get; set; }
    }
}
