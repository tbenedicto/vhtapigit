﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwSummedPackageproduct
    {
        public int PackageId { get; set; }
        public int PackageRuleId { get; set; }
        public string PackageName { get; set; } = null!;
        public string? InternalName { get; set; }
        public string? InternalDescription { get; set; }
        public int? ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int? Cvid { get; set; }
        public int? ProductRegionId { get; set; }
        public string? StateId { get; set; }
        public int? ContactGroupId { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? Customerpay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? Cvbase { get; set; }
        public decimal? Cvbonus { get; set; }
        public int? ProofPhotoCredit { get; set; }
        public decimal? ProofAmountCredit { get; set; }
        public decimal? ProofPercentCredit { get; set; }
        public decimal? CustomerRateAdj { get; set; }
        public decimal? CorporateRateAdj { get; set; }
        public decimal? CvrateAdj { get; set; }
    }
}
