﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Package
    {
        public int PackageId { get; set; }
        public string PackageName { get; set; } = null!;
        public string? InternalName { get; set; }
        public string? InternalDescription { get; set; }
        public string? ExternalName { get; set; }
        public string? ExternalDescription { get; set; }
        public int PackageStatusId { get; set; }
        public int SortId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
    }
}
