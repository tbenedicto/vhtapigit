﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoJob
    {
        public string? Id { get; set; }
        public string? OrderContainerId { get; set; }
        public string? Reference { get; set; }
        public string? CreatedAt { get; set; }
        public string? UpdatedAt { get; set; }
        public string? State { get; set; }
        public string? InvoiceRecipientId { get; set; }
        public string? InvoiceRecipientType { get; set; }
        public string? CustomerComment { get; set; }
        public string? AccountingRef { get; set; }
        public string? CreatorId { get; set; }
        public string? ExportId { get; set; }
        public string? PackageId { get; set; }
        public string? FirstOrderComponentDeliveredAt { get; set; }
        public string? LastOrderComponentDeliveredAt { get; set; }
        public string? IdMa { get; set; }
        public string? ComingFromWebOrder { get; set; }
        public string? ForcedToInvoice { get; set; }
        public string? HasExpressDelivery { get; set; }
    }
}
