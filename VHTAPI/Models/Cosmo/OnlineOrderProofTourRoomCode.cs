﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OnlineOrderProofTourRoomCode
    {
        public int OnlineOrderId { get; set; }
        public int TourRoomCodeId { get; set; }
        public int OnlineOrderProofTourRoomCodeId { get; set; }
    }
}
