﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzG3code
    {
        /// <summary>
        /// Unique value that comes from the G3 system
        /// </summary>
        public int G3codeId { get; set; }
        /// <summary>
        /// Value to sort list by
        /// </summary>
        public int SortId { get; set; }
        /// <summary>
        /// Description of the data
        /// </summary>
        public string Descr { get; set; } = null!;
        public int ContentServerId { get; set; }
        /// <summary>
        /// Date the row was inserted
        /// </summary>
        public DateTime? AddedDate { get; set; }
        /// <summary>
        /// Who inserted the row
        /// </summary>
        public string? AddedBy { get; set; }
        /// <summary>
        /// Date the row was updated last
        /// </summary>
        public DateTime? ModifiedDate { get; set; }
        /// <summary>
        /// Who updated the row last
        /// </summary>
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
        public string OrderFormUrl { get; set; } = null!;
        public bool? IsNewViewer { get; set; }
        public bool? IsImageWork { get; set; }
        public bool? EnableNewViewer { get; set; }
        public string? RateSheetUrl { get; set; }
        public bool? ViewerCreate { get; set; }
        public bool? IsViewerCreated { get; set; }
        public string? ViewerStatusFlag { get; set; }
        public string CorpPayOrderFormUrl { get; set; } = null!;
        public string? BrokerLogoUrl { get; set; }
    }
}
