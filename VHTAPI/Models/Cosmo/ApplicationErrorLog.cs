﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ApplicationErrorLog
    {
        public int ApplicationErrorLogId { get; set; }
        public string ApplicationName { get; set; } = null!;
        public string ApplicationUser { get; set; } = null!;
        public string ApplicationModule { get; set; } = null!;
        public DateTime ErrorTimestamp { get; set; }
        public string ErrorData1 { get; set; } = null!;
        public string ErrorData2 { get; set; } = null!;
        public string ErrorData3 { get; set; } = null!;
        public string ErrorMessage { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
