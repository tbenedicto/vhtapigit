﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class YouTubeChannel
    {
        public YouTubeChannel()
        {
            YouTubeAudits = new HashSet<YouTubeAudit>();
        }

        public int YouTubeChannelId { get; set; }
        public string ChannelName { get; set; } = null!;
        public string ChannelLogin { get; set; } = null!;
        public string ChannelPassword { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public byte[] ObjectVersion { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public Guid Rowguid11 { get; set; }

        public virtual ICollection<YouTubeAudit> YouTubeAudits { get; set; }
    }
}
