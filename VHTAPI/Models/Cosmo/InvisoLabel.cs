﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoLabel
    {
        public string? Id { get; set; }
        public string? Name { get; set; }
        public string? Scope { get; set; }
        public string? InstanceId { get; set; }
    }
}
