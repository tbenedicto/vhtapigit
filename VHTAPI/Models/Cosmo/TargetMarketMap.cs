﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TargetMarketMap
    {
        public string? Market { get; set; }
        public string? TargetMarket { get; set; }
    }
}
