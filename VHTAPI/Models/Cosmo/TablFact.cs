﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablFact
    {
        public DateTime? MonthJobCompleted { get; set; }
        public string? PropertyState { get; set; }
        public string? OfficeName { get; set; }
        public string? TopLevelCompanyName { get; set; }
        public int? VirtualStagingListings { get; set; }
        public int? VirtualStagingAgents { get; set; }
        public int? VirtualStagingOffices { get; set; }
        public decimal? VirtualStagingRevenue { get; set; }
        public int? AllResidentialListings { get; set; }
        public int? AllResidentialAgents { get; set; }
        public int? AllResidentialOffices { get; set; }
        public decimal? AllResidentialRevenue { get; set; }
    }
}
