﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkupCheckDupsParentContact
    {
        public string? Lastname { get; set; }
        public string? Firstname { get; set; }
        public string? TourdisplayEmail { get; set; }
        public int? ParentCompanyId { get; set; }
        public int? ContactCount { get; set; }
    }
}
