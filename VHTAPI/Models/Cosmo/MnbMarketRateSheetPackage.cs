﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbMarketRateSheetPackage
    {
        public int MarketRateSheetPackageId { get; set; }
        public int PackageId { get; set; }
        public int CategoryId { get; set; }
        public int RateSheet { get; set; }
        public int SortId { get; set; }
        public string MarketingName { get; set; } = null!;
        public bool Collapsed { get; set; }
        public bool UseRateCustomText { get; set; }
        public string? RateCustomText { get; set; }
        public Guid Rowguid { get; set; }
    }
}
