﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoBilling5172018FixContactid
    {
        public int? Agentid { get; set; }
        public int Listingid { get; set; }
        public int? CosmoContactid { get; set; }
    }
}
