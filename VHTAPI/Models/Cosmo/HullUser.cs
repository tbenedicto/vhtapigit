﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class HullUser
    {
        public string Id { get; set; } = null!;
        public string? ExternalId { get; set; }
        public string? Email { get; set; }
        public string? AnonymousIds { get; set; }
        public string? Name { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Domain { get; set; }
        public DateTimeOffset? LastSeenAt { get; set; }
        public string? LastKnownIp { get; set; }
        public string? Segments { get; set; }
        public string? AccountId { get; set; }
        public string? AddressStreet { get; set; }
        public string? AddressCity { get; set; }
        public string? AddressPostalCode { get; set; }
        public string? AddressState { get; set; }
        public string? SfMobile { get; set; }
        public DateTimeOffset SyncCreatedAt { get; set; }
        public DateTimeOffset SyncUpdatedAt { get; set; }
        public DateTimeOffset? SyncDeletedAt { get; set; }
    }
}
