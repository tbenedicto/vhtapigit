﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwRateregionzip
    {
        public int ProductRegionId { get; set; }
        public string Region { get; set; } = null!;
        public string Zip { get; set; } = null!;
    }
}
