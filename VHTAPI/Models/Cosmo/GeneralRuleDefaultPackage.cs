﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GeneralRuleDefaultPackage
    {
        public int GeneralRuleId { get; set; }
        public int PackageId { get; set; }
    }
}
