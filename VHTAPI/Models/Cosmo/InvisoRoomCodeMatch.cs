﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoRoomCodeMatch
    {
        public double? Id { get; set; }
        public string? Name { get; set; }
        public string? Scope { get; set; }
        public string? InstanceId { get; set; }
        public double? CosmoRoomCodeId { get; set; }
        public string? F6 { get; set; }
        public string? F7 { get; set; }
        public string? F8 { get; set; }
        public string? F9 { get; set; }
        public string? F10 { get; set; }
        public string? F11 { get; set; }
        public string? F12 { get; set; }
        public string? F13 { get; set; }
        public string? F14 { get; set; }
        public string? F15 { get; set; }
        public string? F16 { get; set; }
        public string? F17 { get; set; }
        public string? F18 { get; set; }
        public string? F19 { get; set; }
        public string? F20 { get; set; }
        public string? F21 { get; set; }
    }
}
