﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablLateTicketByDepartment
    {
        public int IssueId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? EmailSubject { get; set; }
        public string FkparentSource { get; set; } = null!;
        public string? SubCategory { get; set; }
        public string? Category { get; set; }
        public string? ClientName { get; set; }
        public string? CompanyName { get; set; }
        public string? Pc { get; set; }
        public string? Owner { get; set; }
        public string? Department { get; set; }
        public string? TicketStatus { get; set; }
        public string? VhtjobId { get; set; }
    }
}
