﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class KeywordConditionValueDataType
    {
        public int KeywordConditionValueDataTypeId { get; set; }
        public string Description { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
