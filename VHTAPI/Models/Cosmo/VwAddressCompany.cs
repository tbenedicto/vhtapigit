﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAddressCompany
    {
        public int AddressId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public bool IsActiveInd { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public int? AddressStatusId { get; set; }
        public int? AddressStatusReasonId { get; set; }
        public DateTime? AddressStatusReasonDate { get; set; }
        public string? AddressStatusReasonNote { get; set; }
        public bool? PropogateToContactsInd { get; set; }
        public int? RelatedAddressId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public Guid Rowguid { get; set; }
        public bool? CatPremStill { get; set; }
        public bool? CatStd360 { get; set; }
        public string? CustomerDescr { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? FullAddress { get; set; }
    }
}
