﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzTerritory
    {
        public int TerritoryId { get; set; }
        public string? Territory { get; set; }
        public string? TerritoryDescr { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid? Rowguid { get; set; }
        public string? RegionName { get; set; }
        public int? RepSalesRegion { get; set; }
        public string? RepSalesRegionTitle { get; set; }
    }
}
