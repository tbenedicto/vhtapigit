﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablCvJobsAndReassigment
    {
        public DateTime? AddedDate { get; set; }
        public int? JobId { get; set; }
        public string? VhtjobId { get; set; }
        public int? VideographerContactId { get; set; }
        public string? DisplayAs { get; set; }
        public string? JobStatus { get; set; }
        public string? Zip { get; set; }
        public string? StateId { get; set; }
        public string? CvcompanyName { get; set; }
        public string? PropertyZip { get; set; }
        public string? JobCompanyName { get; set; }
        public string? JobParentCompany { get; set; }
    }
}
