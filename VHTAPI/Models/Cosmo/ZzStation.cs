﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzStation
    {
        public int StationId { get; set; }
        public string? Descr { get; set; }
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
