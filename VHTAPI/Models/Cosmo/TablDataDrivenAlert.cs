﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablDataDrivenAlert
    {
        public string? ModifiedBy { get; set; }
        public int Messages { get; set; }
    }
}
