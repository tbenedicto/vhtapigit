﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OnlineOrderPackage
    {
        public int OnlineOrderId { get; set; }
        public int PackageId { get; set; }
        public int OnlineOrderPackageId { get; set; }
    }
}
