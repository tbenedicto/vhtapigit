﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobCpixdoneTrigger
    {
        public int JobCpixdoneTriggerId { get; set; }
        public int JobId { get; set; }
        public int ListingId { get; set; }
        public int AgentId { get; set; }
        public int VideditorId { get; set; }
        public int? OldVideditorId { get; set; }
        public int HasCpixproductId { get; set; }
        public string? ProductIds { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
