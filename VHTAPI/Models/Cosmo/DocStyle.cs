﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DocStyle
    {
        public DocStyle()
        {
            CompanyDocStylePrefs = new HashSet<CompanyDocStylePref>();
        }

        public int DocStyleId { get; set; }
        public int DocId { get; set; }
        public string StyleName { get; set; } = null!;
        public bool IsDefault { get; set; }
        public int DocWriterId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<CompanyDocStylePref> CompanyDocStylePrefs { get; set; }
    }
}
