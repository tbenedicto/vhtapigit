﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablSdrAgentListing
    {
        public int? Month { get; set; }
        public int? Year { get; set; }
        public int? UniqAgent12MonthRolling { get; set; }
        public int? UniqListingsYtd { get; set; }
    }
}
