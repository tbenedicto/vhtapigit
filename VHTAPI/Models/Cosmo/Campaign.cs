﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Campaign
    {
        public int CampaignId { get; set; }
        public string Description { get; set; } = null!;
    }
}
