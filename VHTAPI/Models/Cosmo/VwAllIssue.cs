﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllIssue
    {
        public int IssueId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public string IssueType { get; set; } = null!;
        public int FkparentId { get; set; }
        public string? IssueSource { get; set; }
        public DateTime? IssueAddedDate { get; set; }
        public DateTime? DayIssueAdded { get; set; }
        public DateTime? DayIssueResolved { get; set; }
        public DateTime IssueStatusDate { get; set; }
        public DateTime? IssueModifiedDate { get; set; }
        public string? IssueStatus { get; set; }
        public int IssueCategoryId { get; set; }
        public string? IssueCategory { get; set; }
        public int IssueSubCategoryId { get; set; }
        public string? IssueSubCategory { get; set; }
        public string IssueSubCategoryType { get; set; } = null!;
        public string? IssueSubCategoryShort { get; set; }
        public string? EmailAddress { get; set; }
        public string? EmailSubject { get; set; }
        public int? ContactId { get; set; }
        public string? ContactName { get; set; }
        public int? IssuePhotographer { get; set; }
        public string? IssueAddedBy { get; set; }
    }
}
