﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobToAssign
    {
        public int JobToAssignId { get; set; }
        public int JobId { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public int? OnlineOrderId { get; set; }
    }
}
