﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactImportSetting
    {
        public int ContactId { get; set; }
        public int? FeedId { get; set; }
        public DateTime? FeedLastUpdatedDate { get; set; }
        public DateTime? FeedPortraitLastUpdatedDate { get; set; }
        public bool IsFeedLinkActiveContactData { get; set; }
        public bool IsFeedLinkActivePortrait { get; set; }
        public bool IsFeedLinkActiveCreateOrders { get; set; }
        public Guid Rowguid { get; set; }
    }
}
