﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderImportBatch
    {
        public int OrderImportBatchId { get; set; }
        public int OrderImportBatchStatusId { get; set; }
        public int OrderImportFilterId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual OrderImportBatchStatus OrderImportBatchStatus { get; set; } = null!;
        public virtual OrderImportFilter OrderImportFilter { get; set; } = null!;
    }
}
