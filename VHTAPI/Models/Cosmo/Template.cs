﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Template
    {
        public int TemplateId { get; set; }
        public int RevisionId { get; set; }
        public int TemplateTypeId { get; set; }
        public string Descr { get; set; } = null!;
        public string Subject { get; set; } = null!;
        public byte[]? Html { get; set; }
        public bool IsDefault { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
