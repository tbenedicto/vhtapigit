﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NewClientReportTarget
    {
        public string? CompanyType { get; set; }
        public int? Month { get; set; }
        public int? Target { get; set; }
        public int? Year { get; set; }
    }
}
