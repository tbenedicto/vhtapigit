﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzviewerMenuId
    {
        public int ViewerMenuId { get; set; }
        public string Descr { get; set; } = null!;
    }
}
