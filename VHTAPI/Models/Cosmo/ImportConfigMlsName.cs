﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportConfigMlsName
    {
        public int ImportConfigId { get; set; }
        public string MlsName { get; set; } = null!;
    }
}
