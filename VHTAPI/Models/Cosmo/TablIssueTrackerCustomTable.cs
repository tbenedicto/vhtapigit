﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablIssueTrackerCustomTable
    {
        public int IssueId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public int IssueCategoryId { get; set; }
        public DateTime IssueStatusDate { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? IssueAssignedCv { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? Companyname { get; set; }
        public string? SubCategoryDescr { get; set; }
        public string? StatusDescr { get; set; }
        public string? SourceDescr { get; set; }
        public string? TopLevelCompany { get; set; }
        public bool? TopLevelIndicator { get; set; }
        public string TopLevelParentCompany { get; set; } = null!;
        public string? TourDisplayEmail { get; set; }
        public string? Cvlastname { get; set; }
        public int? Listingid { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Descr { get; set; }
        public string FirstNote { get; set; } = null!;
    }
}
