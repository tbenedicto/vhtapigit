﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class WatermarkSetting
    {
        public int WatermarkSettingsId { get; set; }
        public string? WaterMarkUrl1Oasis { get; set; }
        public bool? UseWaterMark1Oasis { get; set; }
        public bool? WaterMarkPhotosFromFeed1Oasis { get; set; }
        public bool? WaterMarkPhotosFromMyVht1Oasis { get; set; }
        public bool? WaterMark3rdPartyPhotos1Oasis { get; set; }
        public byte? WaterMarkSizePercentage1Oasis { get; set; }
        public bool? PositionLowerLeft1Oasis { get; set; }
        public bool? PositionLowerRight1Oasis { get; set; }
        public bool? PositionUpperRight1Oasis { get; set; }
        public bool? PositionUpperLeft1Oasis { get; set; }
        public string? WaterMarkUrl2Oasis { get; set; }
        public bool? UseWaterMark2Oasis { get; set; }
        public bool? WaterMarkPhotosFromFeed2Oasis { get; set; }
        public bool? WaterMarkPhotosFromMyVht2Oasis { get; set; }
        public bool? WaterMark3rdPartyPhotos2Oasis { get; set; }
        public byte? WaterMarkSizePercentage2Oasis { get; set; }
        public bool? PositionLowerLeft2Oasis { get; set; }
        public bool? PositionLowerRight2Oasis { get; set; }
        public bool? PositionUpperRight2Oasis { get; set; }
        public bool? PositionUpperLeft2Oasis { get; set; }
        public string? WaterMarkUrl2 { get; set; }
        public bool? UseWaterMark2 { get; set; }
        public bool? WaterMarkPhotosFromFeed2 { get; set; }
        public bool? WaterMarkPhotosFromMyVht2 { get; set; }
        public bool? WaterMark3rdPartyPhotos2 { get; set; }
        public byte? WaterMarkSizePercentage2 { get; set; }
        public bool? PositionLowerLeft2 { get; set; }
        public bool? PositionLowerRight2 { get; set; }
        public bool? PositionUpperRight2 { get; set; }
        public bool? PositionUpperLeft2 { get; set; }
        public bool? HideNonbrandedLinksMyVht { get; set; }
        public bool? HideNonbrandedLinksOasis { get; set; }
        public bool? HideNonbrandedLinksMyVhtLinks { get; set; }
        public bool? OptOutLegDistWaterMark { get; set; }
        public bool? WatermarkNonBrandedMyVht1 { get; set; }
        public bool? WatermarkNonBrandedMyVht2 { get; set; }
        public bool? WatermarkNonBrandedOasis1 { get; set; }
        public bool? WatermarkNonBrandedOasis2 { get; set; }
        public string? WaterMarkUrl1 { get; set; }
        public bool? UseWaterMark1 { get; set; }
        public bool? WaterMarkPhotosFromFeed1 { get; set; }
        public bool? WaterMarkPhotosFromMyVht1 { get; set; }
        public bool? WaterMark3rdPartyPhotos1 { get; set; }
        public byte? WaterMarkSizePercentage1 { get; set; }
        public bool? PositionLowerLeft1 { get; set; }
        public bool? PositionLowerRight1 { get; set; }
        public bool? PositionUpperRight1 { get; set; }
        public bool? PositionUpperLeft1 { get; set; }
        public bool? OptOutPrintVht1 { get; set; }
        public bool? OptOutPrintVht2 { get; set; }
        public bool? OptOutPrintOasis1 { get; set; }
        public bool? OptOutPrintOasis2 { get; set; }
    }
}
