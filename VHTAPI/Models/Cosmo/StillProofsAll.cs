﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class StillProofsAll
    {
        public double? Vhtid { get; set; }
        public DateTime? ListingCreated { get; set; }
        public double? Year { get; set; }
        public double? Still { get; set; }
        public string? Proof { get; set; }
        public string? ClientLastName { get; set; }
        public string? ClientFirstName { get; set; }
        public string? Company { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public double? PropertyZip { get; set; }
        public string? Market { get; set; }
    }
}
