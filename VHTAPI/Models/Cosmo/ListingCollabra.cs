﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingCollabra
    {
        public int ListingCollabraId { get; set; }
        public int ListingId { get; set; }
        public string Id { get; set; } = null!;
        public string CollabraApiId { get; set; } = null!;
        public string Result { get; set; } = null!;
    }
}
