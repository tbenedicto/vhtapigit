﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyExtraImageSize
    {
        public int CompanyExtraImageSizesId { get; set; }
        public int ParentCompanyId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Description { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public bool? HideLinkBranded { get; set; }
        public bool? HideLinkNonbranded { get; set; }
        public int? ResizeMode { get; set; }
    }
}
