﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class FirstContact
    {
        public int FirstContactId { get; set; }
        public int? ContactId { get; set; }
        public DateTime? FirstContactDate { get; set; }
        public string? FirstContactDateAddedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
