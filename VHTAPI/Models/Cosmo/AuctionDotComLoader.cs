﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AuctionDotComLoader
    {
        public string? ClientId { get; set; }
        public double? ListingType { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public double? PropertyZipCode { get; set; }
        public double? Bedrooms { get; set; }
        public double? Baths { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? ListingAgentEmail { get; set; }
        public string? ListingAgentCellPhone { get; set; }
        public double? LockBoxCode { get; set; }
        public string? BrokerageName { get; set; }
        public string? BrokerageAgentId { get; set; }
        public string? BrokerageOfficeId { get; set; }
        public string? BrokerageOfficeAddress1 { get; set; }
        public string? BrokerageOfficeAddress2 { get; set; }
        public string? BrokerageOfficeCity { get; set; }
        public string? BrokerageOfficeState { get; set; }
        public double? BrokerageOfficeZipCode { get; set; }
        public string? MlsId { get; set; }
        public double? Package1 { get; set; }
        public double? Package2 { get; set; }
        public double? Package3 { get; set; }
        public double? Package4 { get; set; }
        public double? Package5 { get; set; }
        public double? Package6 { get; set; }
        public double? Package7 { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? ListingAgentSecondaryPhone { get; set; }
    }
}
