﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactIdentifierType
    {
        public ContactIdentifierType()
        {
            ContactIdentifiers = new HashSet<ContactIdentifier>();
        }

        public int ContactIdentifierTypeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public string ValidationRegex { get; set; } = null!;
        public string ValidationMessage { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<ContactIdentifier> ContactIdentifiers { get; set; }
    }
}
