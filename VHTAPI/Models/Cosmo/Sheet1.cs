﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Sheet1
    {
        public double? JobId { get; set; }
        public double? Flag { get; set; }
        public string? VhtjobId { get; set; }
        public string? CommentCategory { get; set; }
        public DateTime? CommentDate { get; set; }
        public string? Subject { get; set; }
        public string? CommentText { get; set; }
        public string? FirstOrderAlert { get; set; }
        public string? CommentRecipient { get; set; }
    }
}
