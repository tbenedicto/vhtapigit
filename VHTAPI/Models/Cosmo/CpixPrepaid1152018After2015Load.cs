﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CpixPrepaid1152018After2015Load
    {
        public string? RealtorId { get; set; }
        public string? State { get; set; }
        public string? CoupId { get; set; }
        public string? ProdId { get; set; }
        public string? CoupName { get; set; }
        public string? ProdName { get; set; }
        public string? ProdMask { get; set; }
        public string? Remaining { get; set; }
        public string? Unitvalue { get; set; }
        public string? Totalvalue { get; set; }
        public string? PurchaseDate { get; set; }
        public string? LastUsedDate { get; set; }
        public int? Cosmoproductid { get; set; }
        public int? CosmoContactid { get; set; }
        public int? PrepaidProductId { get; set; }
        public string? CosmoContactName { get; set; }
        public string? CosmoCpixemail { get; set; }
        public int? Plus1 { get; set; }
        public int? TotalBalance { get; set; }
        public string? PrepaidProductIdname { get; set; }
    }
}
