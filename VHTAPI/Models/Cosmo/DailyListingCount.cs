﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DailyListingCount
    {
        public string? CompName { get; set; }
        public string? ParentCompanyName { get; set; }
        public int CompanyId { get; set; }
        public int? ParentId { get; set; }
        public string? State { get; set; }
        public string? Territory { get; set; }
        public int? Year { get; set; }
        public int? Months { get; set; }
        public string? Addeddate { get; set; }
        public int Listingid { get; set; }
        public int Contactid { get; set; }
    }
}
