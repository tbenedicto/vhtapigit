﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VirtualStagingFormOrder
    {
        public Guid OrderId { get; set; }
        public string PropertyAddressLine1 { get; set; } = null!;
        public string? PropertyAddressLine2 { get; set; }
        public string PropertyCity { get; set; } = null!;
        public string PropertyState { get; set; } = null!;
        public string PropertyZip { get; set; } = null!;
        public int? ListingId { get; set; }
        public string? Tour { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string ProductsOrdered { get; set; } = null!;
        public string RoomType { get; set; } = null!;
        public string WhichPhoto { get; set; } = null!;
        public string? WhichPhotoRoomCode { get; set; }
        public string? ProductDetails { get; set; }
        public string? Notes { get; set; }
        public DateTime DateAdded { get; set; }
        public bool? OrderPlaced { get; set; }
        public string? PromoCode { get; set; }
    }
}
