﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyDocStylePref
    {
        public int CompanyId { get; set; }
        public int DocId { get; set; }
        public int DocStyleId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual Doc Doc { get; set; } = null!;
        public virtual DocStyle DocStyle { get; set; } = null!;
    }
}
