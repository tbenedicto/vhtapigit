﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzDistributionPoint
    {
        public ZzDistributionPoint()
        {
            GeneralRuleDistributionSites = new HashSet<GeneralRuleDistributionSite>();
            ProcessManualZzDistPoints = new HashSet<ProcessManualZzDistPoint>();
        }

        public int DistributionPointId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public string Explaination { get; set; } = null!;
        public byte[]? Tsvalue { get; set; }

        public virtual ICollection<GeneralRuleDistributionSite> GeneralRuleDistributionSites { get; set; }
        public virtual ICollection<ProcessManualZzDistPoint> ProcessManualZzDistPoints { get; set; }
    }
}
