﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContactAddress
    {
        public int FkparentId { get; set; }
        public int? AddressId { get; set; }
    }
}
