﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class IssueTracking
    {
        public int TrackingId { get; set; }
        public int IssueId { get; set; }
        public DateTime? StatusNew { get; set; }
        public DateTime? StatusInProgress { get; set; }
        public DateTime? StatusEscalated { get; set; }
        public DateTime? StatusLate { get; set; }
        public DateTime? StatusNoTicketNeeded { get; set; }
        public DateTime? StatusForSupervisorApproval { get; set; }
        public DateTime? StatusClosed { get; set; }
        public DateTime? StatusCancelled { get; set; }
        public Guid Rowguid { get; set; }
    }
}
