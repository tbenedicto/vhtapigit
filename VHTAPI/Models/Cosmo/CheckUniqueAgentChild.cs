﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CheckUniqueAgentChild
    {
        public string? CompanyName { get; set; }
        public int? CompleteMonth { get; set; }
        public int? CompleteYear { get; set; }
        public int Jobs { get; set; }
        public int Listings { get; set; }
        public decimal TotalAgentPay { get; set; }
        public decimal TotalCorporatePay { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal? AvgAmountListing { get; set; }
        public int UniqueAgentCount { get; set; }
        public int UniqueAgentYtdCount { get; set; }
        public string? Territory { get; set; }
        public string? RegionName { get; set; }
    }
}
