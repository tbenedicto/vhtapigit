﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageFileArrayLog
    {
        public int ImageFileArrayLogId { get; set; }
        public string FileName { get; set; } = null!;
        public string FilePath { get; set; } = null!;
        public DateTime FileDate { get; set; }
        public DateTime DeletionDate { get; set; }
        public decimal FileSize { get; set; }
        public string? Reason { get; set; }
        public Guid Rowguid { get; set; }
    }
}
