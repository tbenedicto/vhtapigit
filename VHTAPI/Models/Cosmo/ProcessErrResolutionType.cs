﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessErrResolutionType
    {
        public int ProcessErrResolutionId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public Guid Rowguid5 { get; set; }
    }
}
