﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebListingRet
    {
        public int ListingId { get; set; }
        public decimal? NumFireplaces { get; set; }
        public decimal? NumStories { get; set; }
        public decimal? NumGarageSpaces { get; set; }
        public bool? HasBasement { get; set; }
        public decimal? BasementFinishedPercent { get; set; }
        public int? YearBuilt { get; set; }
        public decimal? Area { get; set; }
        public decimal? LotSize { get; set; }
        public decimal? NumBeds { get; set; }
        public decimal? NumBaths { get; set; }
        public decimal? ListPrice { get; set; }
        public string? Remarks { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public byte[] RowVersion { get; set; } = null!;
        public Guid RowId { get; set; }
        public int? ListPriceRangeId { get; set; }
        public decimal? InteriorSqft { get; set; }
        public int? NumRooms { get; set; }
    }
}
