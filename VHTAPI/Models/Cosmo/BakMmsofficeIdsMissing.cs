﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BakMmsofficeIdsMissing
    {
        public int? OfficeId { get; set; }
    }
}
