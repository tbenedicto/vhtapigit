﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RestBapiPrediction
    {
        public int Id { get; set; }
        public int ListingId { get; set; }
        public int TourId { get; set; }
        public int TourRoomCodeId { get; set; }
        public int RoomCodeId { get; set; }
        public string ImageUrl { get; set; } = null!;
        public bool IsApiCallSucceeded { get; set; }
        public double? ApiProcessTime { get; set; }
        public string? Prediction1 { get; set; }
        public double? Prediction1Confidence { get; set; }
        public string? Prediction2 { get; set; }
        public double? Prediction2Confidence { get; set; }
        public string? Prediction3 { get; set; }
        public double? Prediction3Confidence { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
