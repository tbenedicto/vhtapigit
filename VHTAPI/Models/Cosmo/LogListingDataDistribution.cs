﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LogListingDataDistribution
    {
        public int LogId { get; set; }
        public decimal BatchId { get; set; }
        public int ListingId { get; set; }
        public int ParentCompanyId { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Processed { get; set; }
        public DateTime? DateLastProcessed { get; set; }
        public string? ProcessMessage { get; set; }
    }
}
