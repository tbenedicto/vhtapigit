﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ViewThisHome
    {
        public int ListingId { get; set; }
        public string VideoId { get; set; } = null!;
        public DateTime LastUpdated { get; set; }
    }
}
