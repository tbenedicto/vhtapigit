﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VirtualStagingOrderFile
    {
        public int Id { get; set; }
        public int VirtualStagingOrderId { get; set; }
        public bool IsAttachment { get; set; }
        public string? AttachmentFileName { get; set; }
        public int ServiceType { get; set; }
        public int? TourRoomCodeId { get; set; }
        public int? RoomCodeId { get; set; }
        public int? ContentType { get; set; }
        public bool? IsThirdParty { get; set; }
        public string? Url { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; } = null!;
    }
}
