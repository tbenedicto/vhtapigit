﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobAppointmentDateBackup
    {
        public int JobId { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
