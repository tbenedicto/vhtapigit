﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Commissionreport
    {
        public DateTime Importdate { get; set; }
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompany { get; set; }
        public string? RegionalSalesRep { get; set; }
        public string? AreaSalesRep { get; set; }
        public string? AccountManager { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public decimal? TotalRev { get; set; }
        public int? TotalOrders { get; set; }
        public int? TotalClients { get; set; }
        public decimal? OldRev { get; set; }
        public int? OldOrders { get; set; }
        public int? OldClients { get; set; }
        public decimal NewTrailingRev { get; set; }
        public int NewTrailingOrders { get; set; }
        public int NewTrailingClients { get; set; }
        public decimal NewRev { get; set; }
        public int NewOrders { get; set; }
        public int NewClients { get; set; }
        public decimal YtdRev { get; set; }
        public decimal YtdRevLessFees { get; set; }
        public decimal FeesRev { get; set; }
        public int FeesOrders { get; set; }
        public int FeesClients { get; set; }
        public decimal Viprev { get; set; }
        public int Viporders { get; set; }
        public int Vipclients { get; set; }
        public decimal ReNewTrailingRev { get; set; }
        public int ReNewTrailingOrders { get; set; }
        public int ReNewTrailingClients { get; set; }
        public decimal ReNewRev { get; set; }
        public int ReNewOrders { get; set; }
        public int ReNewClients { get; set; }
    }
}
