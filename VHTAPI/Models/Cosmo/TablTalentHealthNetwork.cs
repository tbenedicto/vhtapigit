﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablTalentHealthNetwork
    {
        public string? DisplayAs { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ContactId { get; set; }
        public string? TerritoryDescr { get; set; }
        public int? NumberOfJobs { get; set; }
        public int? Scheduled { get; set; }
        public int? NotScheduled { get; set; }
        public int? AppoinmentNext4Days { get; set; }
        public int? AvgCapacity { get; set; }
        public int? MaxCapacity { get; set; }
    }
}
