﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Mrcomment
    {
        public int CommentId { get; set; }
        public int MrstatusDaemonId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual MrstatusDaemon MrstatusDaemon { get; set; } = null!;
    }
}
