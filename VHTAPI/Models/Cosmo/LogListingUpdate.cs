﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LogListingUpdate
    {
        public int LogId { get; set; }
        public int ListingId { get; set; }
        public string? ColumnName { get; set; }
        public string? OldValue { get; set; }
        public string? NewValue { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
    }
}
