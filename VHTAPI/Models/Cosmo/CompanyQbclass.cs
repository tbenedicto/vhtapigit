﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyQbclass
    {
        public int CompanyId { get; set; }
        public string Qbclass { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
