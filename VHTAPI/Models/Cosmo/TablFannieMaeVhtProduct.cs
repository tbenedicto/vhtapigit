﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablFannieMaeVhtProduct
    {
        public int Jobid { get; set; }
        public string? Vhtjobid { get; set; }
        public DateTime Jobstatusdate { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public DateTime? VidmediaArrivalDate { get; set; }
        public string? AcceptDecline { get; set; }
        public string? ExternalName { get; set; }
        public string? ExternalDescription { get; set; }
    }
}
