﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SqlrptIssueTrackerDailyTrends7Pm
    {
        public DateTime? Date { get; set; }
        public int? New { get; set; }
        public int? InProgress { get; set; }
        public int? WaitingForInfoInternalVht { get; set; }
        public int? WaitingForInfoClient { get; set; }
        public int? Escalated { get; set; }
        public int? Late { get; set; }
    }
}
