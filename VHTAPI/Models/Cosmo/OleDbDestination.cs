﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OleDbDestination
    {
        public string? OleDbSourceVhtjobId { get; set; }
        public string? OleDbSourceCommentText { get; set; }
        public string? OleDbSourceAddedDate { get; set; }
        public string? OleDbSourcePropertyAddressLine1 { get; set; }
        public string? Propertyaddressline2 { get; set; }
        public string? Propertycity { get; set; }
        public string? Propertystate { get; set; }
        public string? Propertyzip { get; set; }
    }
}
