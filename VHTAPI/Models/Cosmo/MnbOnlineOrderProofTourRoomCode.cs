﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbOnlineOrderProofTourRoomCode
    {
        public int OnlineOrderProofTourRoomCodeId { get; set; }
        public int OnlineOrderId { get; set; }
        public int TourRoomCodeId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
