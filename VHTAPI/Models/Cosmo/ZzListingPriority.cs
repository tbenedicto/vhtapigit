﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzListingPriority
    {
        public int ListingPriorityId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
