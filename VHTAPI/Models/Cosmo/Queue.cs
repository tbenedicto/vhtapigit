﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Queue
    {
        public int ListingId { get; set; }
        public int QueueTypeId { get; set; }
        public Guid? JobId { get; set; }
        public string? Status { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid Rowguid { get; set; }
        public int QueueId { get; set; }
    }
}
