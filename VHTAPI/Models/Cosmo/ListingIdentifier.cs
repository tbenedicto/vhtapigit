﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingIdentifier
    {
        public int ListingIdentifierId { get; set; }
        public int ListingId { get; set; }
        public string Id { get; set; } = null!;
        public int ListingIdentifierTypeId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
        public byte[] RowTimeStamp { get; set; } = null!;
    }
}
