﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzRuleConditionType
    {
        public ZzRuleConditionType()
        {
            GeneralRules = new HashSet<GeneralRule>();
            ProductRuleNews = new HashSet<ProductRuleNew>();
        }

        public int RuleConditionTypeId { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<GeneralRule> GeneralRules { get; set; }
        public virtual ICollection<ProductRuleNew> ProductRuleNews { get; set; }
    }
}
