﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCompanyQuirk
    {
        public ZzCompanyQuirk()
        {
            CompanyCompanyQuirks = new HashSet<CompanyCompanyQuirk>();
        }

        public int CompanyQuirkId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public bool? IsObsoleteFlag { get; set; }

        public virtual ICollection<CompanyCompanyQuirk> CompanyCompanyQuirks { get; set; }
    }
}
