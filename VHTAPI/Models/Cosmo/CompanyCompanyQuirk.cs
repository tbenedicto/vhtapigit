﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyCompanyQuirk
    {
        public int CompanyId { get; set; }
        public int CompanyQuirkId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual Company Company { get; set; } = null!;
        public virtual ZzCompanyQuirk CompanyQuirk { get; set; } = null!;
    }
}
