﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablTalentJobCompletedLast30
    {
        public int? ContactId { get; set; }
        public string? Cvname { get; set; }
        public string? PhotographerNetwork { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string? Cvinstructions { get; set; }
        public decimal Cvmiscellaneous { get; set; }
        public DateTime? FollowupDate { get; set; }
        public string? FollowupNote { get; set; }
        public decimal? InfieldTotalRevenue { get; set; }
        public DateTime JobStatusDate { get; set; }
        public int MileageQuantity { get; set; }
        public decimal MileageRate { get; set; }
        public DateTime? ReassignedVideographerDate { get; set; }
        public string? VhtjobId { get; set; }
        public DateTime? StatusCompleteCompletedOn { get; set; }
        public DateTime? StatusInFieldCompletedOn { get; set; }
        public DateTime? TaskCvassignedCompletedOn { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
    }
}
