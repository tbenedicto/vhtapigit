﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Dvd2yrResult
    {
        public int JobId { get; set; }
        public string? VhtjobId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompanyName { get; set; }
        public int Listingid { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Development { get; set; }
        public decimal? ListPrice { get; set; }
        public string Desc1 { get; set; } = null!;
        public string Desc2 { get; set; } = null!;
        public string? Desc3 { get; set; }
        public string? CvLast { get; set; }
        public string? CvFirst { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public DateTime? TaskMediaReceivedCompletedOn { get; set; }
        public bool FlagCustomerFirstOrder { get; set; }
        public DateTime? FollowupDate { get; set; }
        public string? FollowupNote { get; set; }
        public DateTime? MinPhotoAddedDate { get; set; }
        public DateTime? MaxPhotoModifiedDate { get; set; }
    }
}
