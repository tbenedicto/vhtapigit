﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VirtualStagingFormOrderHistoryDetail
    {
        public Guid OrderHistoryId { get; set; }
        public string ItemDescription { get; set; } = null!;
        public string ItemDetail { get; set; } = null!;
        public DateTime DateAdded { get; set; }
    }
}
