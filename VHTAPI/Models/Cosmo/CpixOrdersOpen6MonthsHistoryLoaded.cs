﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CpixOrdersOpen6MonthsHistoryLoaded
    {
        public double? TourId { get; set; }
        public double? ProductId { get; set; }
        public string? MlsId { get; set; }
        public string? MlsName { get; set; }
        public string? ListingDate { get; set; }
        public double? ListingPrice { get; set; }
        public string? ListingType { get; set; }
        public string? Line1 { get; set; }
        public string? Line2 { get; set; }
        public string? City { get; set; }
        public string? StateCode { get; set; }
        public double? Zip { get; set; }
        public double? Bedrooms { get; set; }
        public double? FullBaths { get; set; }
        public double? HalfBaths { get; set; }
        public double? ListingAgent { get; set; }
        public string? Description { get; set; }
        public string? NotesToPhotographer { get; set; }
        public double? PhotographerId { get; set; }
        public string? CompletedOrderDate { get; set; }
        public double? PhotographerPay { get; set; }
        public double? AgentPay { get; set; }
        public double? CorporatePay { get; set; }
        public string? ContactName { get; set; }
        public string? ContactPhone { get; set; }
        public string? ContactNotes { get; set; }
        public string? SchduleTime { get; set; }
        public string? ContactLog { get; set; }
        public int? CosmoProductid { get; set; }
        public int? CosmoPhotogId { get; set; }
        public int? CosmoContactid { get; set; }
        public int? Loadstatusid { get; set; }
        public int? MissingAgentId { get; set; }
        public int? MissingPhotographerId { get; set; }
        public string? Postponed { get; set; }
        public int? MatchedListingid { get; set; }
    }
}
