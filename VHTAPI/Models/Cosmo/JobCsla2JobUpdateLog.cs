﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobCsla2JobUpdateLog
    {
        public int Jobid { get; set; }
        public DateTime JobDatechange { get; set; }
        public string? Jobchangeby { get; set; }
        public int? VideographerContactId { get; set; }
        public int JobCsla2JobUpdateLogId { get; set; }
        public DateTime? DbofficeGetdate { get; set; }
    }
}
