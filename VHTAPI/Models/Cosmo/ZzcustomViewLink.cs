﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzcustomViewLink
    {
        public int ZzcustomViewLinkId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public string DefaultValue { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
