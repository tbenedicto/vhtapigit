﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PackageRuleCondRateAdjCondition
    {
        public int PackageRuleCondRateAdjConditionId { get; set; }
        public int PackageRuleCondRateAdjId { get; set; }
        public int ProductCategoryId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
