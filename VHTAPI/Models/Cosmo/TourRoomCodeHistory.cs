﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TourRoomCodeHistory
    {
        public int TourRoomCodeHistoryId { get; set; }
        public int? TourRoomCodeId { get; set; }
        public int TourId { get; set; }
        public int RoomCodeId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public byte[]? TimpStampField { get; set; }
        public Guid? Rowguid { get; set; }
        public bool ShouldShowOnTour { get; set; }
        public bool ShouldDistribute { get; set; }
        public int CustomSortId { get; set; }
        public bool UseCustomSceneName { get; set; }
        public string CustomSceneName { get; set; } = null!;
        public string SceneDescription { get; set; } = null!;
        public string FileNameBase { get; set; } = null!;
        public bool IsThirdPartyImage { get; set; }
        public int UploadedByContactId { get; set; }
        public int FeedId { get; set; }
        public bool? IsPrimaryExteriorImage { get; set; }
        public DateTime? EventDate { get; set; }
        public string? EventType { get; set; }
        public int? CvContactId { get; set; }
    }
}
