﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProductPackage
    {
        public int ProductPackageId { get; set; }
        public int PackageId { get; set; }
        public int ProductId { get; set; }
        public int? SortOrder { get; set; }
        public decimal? ProductPackageCredit { get; set; }
        public decimal? ProductPackageDiscount { get; set; }
        public int? ProductPackageQty { get; set; }
    }
}
