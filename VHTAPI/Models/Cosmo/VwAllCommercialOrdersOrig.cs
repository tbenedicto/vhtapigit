﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllCommercialOrdersOrig
    {
        public int? CompanyId { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? Companyname { get; set; }
        public string? Parentcompanyname { get; set; }
        public string? AgentName { get; set; }
        public int Jobid { get; set; }
        public int? CompanysubsidiaryId { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string? Contactcode { get; set; }
        public string? Vhtjobid { get; set; }
        public int Jobstatusid { get; set; }
        public Guid? CampaignGuid { get; set; }
        public string? ContactBusinessType { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? JobAddedBy { get; set; }
        public DateTime? JobAddedDate { get; set; }
        public DateTime? _1stStartdate { get; set; }
        public string? ContactHistoryType { get; set; }
        public string? Ade { get; set; }
        public int NumOffices { get; set; }
        public int NumAgents { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public string? Territory { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public decimal? Totalrev { get; set; }
        public int ListingId { get; set; }
        public int Contactid { get; set; }
    }
}
