﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwBwilistingAdj
    {
        public int ListingId { get; set; }
        public int G3codeId { get; set; }
        public int ContentServerId { get; set; }
    }
}
