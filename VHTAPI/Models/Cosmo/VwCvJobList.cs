﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCvJobList
    {
        public string? AgentName { get; set; }
        public string? CompanyName { get; set; }
        public int ListingId { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? VhtjobId { get; set; }
        public DateTime? OrderDate { get; set; }
        public int VideographerContactId { get; set; }
        public DateTime? TaskViewedByCvCompletedOn { get; set; }
        public DateTime? TaskInitialPhoneCallMadeCompletedOn { get; set; }
        public int JobStatusId { get; set; }
        public DateTime JobStatusDate { get; set; }
        public string JobStatus { get; set; } = null!;
        public DateTime? FollowupDate { get; set; }
        public int JobId { get; set; }
        public int JobStatusCode { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public bool FlagCustomerFirstOrder { get; set; }
        public DateTime? MediaArrivalDate { get; set; }
        public string? JobStatusCodeText { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Cvchange { get; set; }
        public DateTime? TentativeAppointmentDateTime { get; set; }
        public int JobSourceId { get; set; }
        public int? PremiumProductsCount { get; set; }
        public DateTime? ReassignedVideographerDate { get; set; }
    }
}
