﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CustomViewer
    {
        public int CustomViewerId { get; set; }
        public int G3codeId { get; set; }
        public int CustomViewerLinkId { get; set; }
        public string CustomViewerValue { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public string MlsName { get; set; } = null!;
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string MlsNumber { get; set; } = null!;
    }
}
