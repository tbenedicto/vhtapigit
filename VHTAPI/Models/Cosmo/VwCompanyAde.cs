﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCompanyAde
    {
        public int? RepSalesNational { get; set; }
        public string? Descr { get; set; }
        public string? Territory { get; set; }
        public int CompanyId { get; set; }
        public int? CompanyCategoryId { get; set; }
        public string? CompanyName { get; set; }
        public string? DisplayAs { get; set; }
        public int? CompanyStatusId { get; set; }
        public DateTime? CompanyStatusDate { get; set; }
        public int? CompanyStatusReasonId { get; set; }
        public string? CompanyStatusReasonNote { get; set; }
        public int? SalespersonId { get; set; }
        public int? G3codeId { get; set; }
        public int? ParentCompanyId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public bool CoreCustomerInd { get; set; }
        public bool TopLevelReportingInd { get; set; }
        public string? CompanyRefId { get; set; }
        public string? CorporateOfficeId { get; set; }
        public int TemplateIdVhtgatewayWelcomeEmail { get; set; }
        public int TemplateIdVhtgatewayPasswordResetEmail { get; set; }
        public int TemplateIdCvappointmentEmail { get; set; }
        public int TemplateIdConfirmationEmail { get; set; }
        public int TemplateIdStudioCompleteEmail { get; set; }
        public int TemplateIdUnpaidBalanceEmail { get; set; }
        public int TemplateIdIncorrectCreditCardEmail { get; set; }
        public int TemplateIdVhtsummaryEmail { get; set; }
        public int TemplateIdBasicInfoEmail { get; set; }
        public Guid Rowguid { get; set; }
        public bool IsStyleInherited { get; set; }
        public int ViewerStyleId { get; set; }
        public int BrochureStyleId { get; set; }
        public int? RepSalesLocal { get; set; }
        public int? RepSalesRegional { get; set; }
        public int? RepCustomerService { get; set; }
        public int NumOffices { get; set; }
        public DateTime NumOfficesLastUpdatedDate { get; set; }
        public string NumOfficesLastUpdatedBy { get; set; } = null!;
        public int NumAgents { get; set; }
        public DateTime NumAgentsLastUpdatedDate { get; set; }
        public string NumAgentsLastUpdatedBy { get; set; } = null!;
        public string LrebrokerId { get; set; } = null!;
        public int NumListingsPerYear { get; set; }
        public DateTime NumListingsPerYearLastUpdatedDate { get; set; }
        public string NumListingsPerYearLastUpdatedBy { get; set; } = null!;
        public bool HidePricingInCvinterface { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public int? CompanyTypeId { get; set; }
    }
}
