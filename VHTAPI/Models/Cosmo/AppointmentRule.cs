﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AppointmentRule
    {
        public int AppointmentRuleId { get; set; }
        public int AppointmentRuleTypeId { get; set; }
        public string FkparentId { get; set; } = null!;
        public int? AppointmentLengthSilver { get; set; }
        public int? AppointmentLengthGold { get; set; }
        public int? AppointmentLengthPlatinum { get; set; }
        public int? AppointmentLengthFloorplan { get; set; }
        public int? DriveTimeBufferInMin { get; set; }
        public int SortId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public int? AppointmentLengthAerial { get; set; }
        public int? AppointmentLengthFullMotionVideo { get; set; }
        public int? AppointmentLength3D { get; set; }
        public int? AppointmentLengthFpcc { get; set; }
    }
}
