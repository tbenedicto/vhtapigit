﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CslaViewOrderImportBatchHeader
    {
        public int OrderImportBatchId { get; set; }
        public int OrderImportFilterId { get; set; }
        public string FilterName { get; set; } = null!;
        public int OrderImportBatchStatusId { get; set; }
        public string Status { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
    }
}
