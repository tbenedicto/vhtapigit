﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PackageRule
    {
        public int PackageRuleId { get; set; }
        public int PackageId { get; set; }
        public int RuleConditionTypeId { get; set; }
        public int? ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int? Cvid { get; set; }
        public int? ProductRegionId { get; set; }
        public string? StateId { get; set; }
        public int? ContactGroupId { get; set; }
        public int SortId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
