﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactSecondOrder
    {
        public int Contactid { get; set; }
        public int? Listingid { get; set; }
        public DateTime _1stStartdate { get; set; }
        public int? _2ndOrderJobid { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? _2ndOrderDate { get; set; }
        public string? JobSource { get; set; }
        public decimal? TotalRev { get; set; }
    }
}
