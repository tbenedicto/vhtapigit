﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SummaryNewCommissionReportMonthlyHistoryRollingUniq
    {
        public int? Myyear { get; set; }
        public int? YtdMonth { get; set; }
        public int? RollingcountYtddistinctAgents { get; set; }
    }
}
