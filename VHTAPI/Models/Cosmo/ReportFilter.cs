﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ReportFilter
    {
        public int RowId { get; set; }
        public int ReportSessionId { get; set; }
        public string? Marker { get; set; }
        public int? KeyValue { get; set; }
    }
}
