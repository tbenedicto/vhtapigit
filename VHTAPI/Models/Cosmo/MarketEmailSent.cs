﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MarketEmailSent
    {
        public int MarketMailingId { get; set; }
        public string EmailAddr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual MarketMailing MarketMailing { get; set; } = null!;
    }
}
