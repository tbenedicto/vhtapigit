﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class UsplusZipcode72319New
    {
        public string? Zipcode { get; set; }
        public string? State { get; set; }
        public string? City { get; set; }
        public string? County { get; set; }
        public string? Sequence { get; set; }
        public string? Pancode { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? AreaCode { get; set; }
        public string? StateFips { get; set; }
        public string? CountyFips { get; set; }
        public string? PlaceFips { get; set; }
        public string? Msacode { get; set; }
        public string? TimeZone { get; set; }
        public string? Dst { get; set; }
        public string? Ziptype { get; set; }
    }
}
