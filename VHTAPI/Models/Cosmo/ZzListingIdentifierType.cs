﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzListingIdentifierType
    {
        public ZzListingIdentifierType()
        {
            LicompanyRuleDetails = new HashSet<LicompanyRuleDetail>();
            ListateRuleDetails = new HashSet<ListateRuleDetail>();
        }

        public int ListingIdentifierTypeId { get; set; }
        public int SortId { get; set; }
        public string? Descr { get; set; }
        public Guid Rowguid { get; set; }
        public string? ValidationRegex { get; set; }
        public string ValidationMessage { get; set; } = null!;
        public string ClientDescr { get; set; } = null!;
        public string ClientValidationMessage { get; set; } = null!;

        public virtual ICollection<LicompanyRuleDetail> LicompanyRuleDetails { get; set; }
        public virtual ICollection<ListateRuleDetail> ListateRuleDetails { get; set; }
    }
}
