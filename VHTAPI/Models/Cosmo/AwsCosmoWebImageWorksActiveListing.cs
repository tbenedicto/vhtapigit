﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebImageWorksActiveListing
    {
        public int ImageWorksActiveListingId { get; set; }
        public int ParentCompanyId { get; set; }
        public int CompanyId { get; set; }
        public int ContactId { get; set; }
        public int ListingId { get; set; }
        public DateTime ActiveDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
