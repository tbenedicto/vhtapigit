﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Dvd4q2015EditedFinalCount
    {
        public int? CompanyId { get; set; }
        public int? PhotoCount { get; set; }
    }
}
