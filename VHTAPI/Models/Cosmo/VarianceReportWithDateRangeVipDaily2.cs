﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VarianceReportWithDateRangeVipDaily2
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? TourDisplayEmail { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? ParentCompany { get; set; }
        public string? AccountType { get; set; }
        public string? LocalSalesRep { get; set; }
        public string? AreaSalesRep { get; set; }
        public string? AccountDevelopement { get; set; }
        public string? Csrep { get; set; }
        public double? TotalAgentPay { get; set; }
        public double? TotalCorporatePay { get; set; }
        public double? TotalRevenue { get; set; }
        public int? NumJobs { get; set; }
        public double? AvgAmountPerListing { get; set; }
        public int? NumListings { get; set; }
        public string? Market { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public DateTime? CompletedDate { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
