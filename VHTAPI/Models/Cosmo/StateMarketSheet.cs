﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class StateMarketSheet
    {
        public string? State { get; set; }
        public string? StateId { get; set; }
        public int? RateSheetId { get; set; }
    }
}
