﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GeneralRulesAddWt
    {
        public int? RuleConditionTypeId { get; set; }
        public string? RuleType { get; set; }
        public int? CompanyId { get; set; }
        public int? ProductId { get; set; }
    }
}
