﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyGroupCompany
    {
        public int CompanyGroupId { get; set; }
        public int CompanyId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual Company Company { get; set; } = null!;
        public virtual CompanyGroup CompanyGroup { get; set; } = null!;
    }
}
