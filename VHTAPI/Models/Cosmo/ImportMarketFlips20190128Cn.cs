﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportMarketFlips20190128Cn
    {
        public string? ProductRegion { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public double? RatesheetId { get; set; }
    }
}
