﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListateRuleDetail
    {
        public string StateAbbr { get; set; } = null!;
        public int ListingIdentifierTypeId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ZzListingIdentifierType ListingIdentifierType { get; set; } = null!;
        public virtual ListateRule StateAbbrNavigation { get; set; } = null!;
    }
}
