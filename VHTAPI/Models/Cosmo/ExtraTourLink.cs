﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ExtraTourLink
    {
        public int? Listing { get; set; }
        public int? Room { get; set; }
        public string? CurrentUrl { get; set; }
    }
}
