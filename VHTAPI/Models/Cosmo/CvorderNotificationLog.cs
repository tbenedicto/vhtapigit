﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvorderNotificationLog
    {
        public int CvorderNotificationLogId { get; set; }
        public int JobId { get; set; }
        public int Cvid { get; set; }
        public string? EmailAddress { get; set; }
        public string? EmailSubject { get; set; }
        public string? EmailBody { get; set; }
        public string? ErrorMessage { get; set; }
        public bool? SentStatus { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public Guid Rowguid { get; set; }
        public Guid Rowguid14 { get; set; }
    }
}
