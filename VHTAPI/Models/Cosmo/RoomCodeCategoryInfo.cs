﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RoomCodeCategoryInfo
    {
        public int RoomCodeId { get; set; }
        public string RoomName { get; set; } = null!;
        public int Sortid { get; set; }
        public string? RoomGroup { get; set; }
        public string? RoomcodeCategoryName { get; set; }
        public bool? Isexterior { get; set; }
    }
}
