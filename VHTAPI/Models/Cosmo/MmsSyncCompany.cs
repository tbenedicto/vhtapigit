﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MmsSyncCompany
    {
        public double? ParentCompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? City { get; set; }
        public string? Street { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public int? MmsofficeId { get; set; }
        public int? CompanyStatusId { get; set; }
        public int? MmscompanyId { get; set; }
        public int? Companyid { get; set; }
    }
}
