﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCdromlabelStyle
    {
        public int CdromlabelStyleId { get; set; }
        public string? Descr { get; set; }
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
