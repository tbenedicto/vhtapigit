﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwG3codeUpdate
    {
        public int G3codeIdListing { get; set; }
        public int? G3codeIdCompany { get; set; }
    }
}
