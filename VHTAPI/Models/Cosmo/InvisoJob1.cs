﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoJob1
    {
        public double? Id { get; set; }
        public double? OrderContainerId { get; set; }
        public string? Reference { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string? State { get; set; }
        public double? InvoiceRecipientId { get; set; }
        public string? InvoiceRecipientType { get; set; }
        public string? CustomerComment { get; set; }
        public string? AccountingRef { get; set; }
        public double? CreatorId { get; set; }
        public string? ExportId { get; set; }
        public string? PackageId { get; set; }
        public string? FirstOrderComponentDeliveredAt { get; set; }
        public string? LastOrderComponentDeliveredAt { get; set; }
        public string? IdMa { get; set; }
        public double? ComingFromWebOrder { get; set; }
        public double? ForcedToInvoice { get; set; }
        public string? HasExpressDelivery { get; set; }
    }
}
