﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VhtmerrillProduct
    {
        public int RowId { get; set; }
        public int VhtproductId { get; set; }
        public string MerrillProductId { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
