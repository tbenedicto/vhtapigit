﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Cvmessage
    {
        public int CvmessageId { get; set; }
        public string MessageText { get; set; } = null!;
        public string Color { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool PrivateSw { get; set; }
        public Guid Rowguid { get; set; }
    }
}
