﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VizziimportAudit
    {
        public int VizziinternalTourId { get; set; }
        public int VizzitourId { get; set; }
        public int JobId { get; set; }
        public int ListingId { get; set; }
        public int AgentId { get; set; }
        public DateTime AddedDate { get; set; }
        public int? ProcessStatusId { get; set; }
    }
}
