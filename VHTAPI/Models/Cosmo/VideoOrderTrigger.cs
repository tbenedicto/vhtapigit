﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VideoOrderTrigger
    {
        public int VideoOrderTriggerId { get; set; }
        public int JobId { get; set; }
        public string VhtjobId { get; set; } = null!;
        public DateTime TriggerEventDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
