﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoListingHistory
    {
        public string? TourId { get; set; }
        public int? ProductId { get; set; }
        public int? MlsId { get; set; }
        public int? MlsName { get; set; }
        public DateTime? ListingDate { get; set; }
        public int? ListingPrice { get; set; }
        public int? ListingType { get; set; }
        public string? Line1 { get; set; }
        public string? Line2 { get; set; }
        public string? City { get; set; }
        public string? StateCode { get; set; }
        public string? InvisoZip { get; set; }
        public int? Bedrooms { get; set; }
        public int? FullBaths { get; set; }
        public int? HalfBaths { get; set; }
        public int? ListingAgent { get; set; }
        public int? Description { get; set; }
        public int? NotesToPhotographer { get; set; }
        public int? PhotographerId { get; set; }
        public DateTime? CompletedOrderDate { get; set; }
        public int? PhotographerPay { get; set; }
        public int? AgentPay { get; set; }
        public int? CorporatePay { get; set; }
        public int? CosmoProductId { get; set; }
        public int? CosmoPhotogId { get; set; }
        public int? CosmoContactid { get; set; }
        public int Loadstatusid { get; set; }
        public int? MissingAgentId { get; set; }
        public int? MissingPhotographerId { get; set; }
        public string? ListingAddedDate { get; set; }
        public string? InvisoCustomerId { get; set; }
        public string? InvisoPersonId { get; set; }
    }
}
