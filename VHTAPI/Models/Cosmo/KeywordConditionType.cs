﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class KeywordConditionType
    {
        public int KeywordConditionTypeId { get; set; }
        public string? Description { get; set; }
        public Guid Rowguid { get; set; }
    }
}
