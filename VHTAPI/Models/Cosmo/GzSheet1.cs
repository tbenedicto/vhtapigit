﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GzSheet1
    {
        public double? RowId { get; set; }
        public string? SourceImageUrl { get; set; }
        public string? F3 { get; set; }
        public string? F4 { get; set; }
        public string? F5 { get; set; }
        public string? F6 { get; set; }
        public string? F7 { get; set; }
        public double? ListingId { get; set; }
        public string? RoomCode { get; set; }
    }
}
