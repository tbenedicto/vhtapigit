﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProofingEmailQueue
    {
        public int ProofingEmailQueueId { get; set; }
        public int JobId { get; set; }
    }
}
