﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NevinInvisoClientswcompany
    {
        public DateTime? AddedDate { get; set; }
        public double? ChainId { get; set; }
        public string? ChainName { get; set; }
        public double? RegionId { get; set; }
        public string? RegionName { get; set; }
        public double? CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public string? LegalName { get; set; }
        public string? CustomerAddressStreet { get; set; }
        public string? CustomerAddressPostalArea { get; set; }
        public double? CustomerAddressZipCode { get; set; }
        public double? PersonId { get; set; }
        public string? PersonName { get; set; }
        public string? Email { get; set; }
        public string? CosmoParentId { get; set; }
        public double? CosmoCompanyId { get; set; }
        public double? CosmoContactid { get; set; }
        public string? Ignore { get; set; }
        public double? FirstOrderId { get; set; }
        public DateTime? FirstOrderCreatedAt { get; set; }
    }
}
