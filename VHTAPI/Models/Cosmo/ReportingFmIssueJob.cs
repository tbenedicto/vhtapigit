﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ReportingFmIssueJob
    {
        public int Jobid { get; set; }
        public string Note { get; set; } = null!;
    }
}
