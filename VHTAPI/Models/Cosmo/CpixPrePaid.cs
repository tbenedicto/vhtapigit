﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CpixPrePaid
    {
        public double? RealtorId { get; set; }
        public double? CoupId { get; set; }
        public string? CoupName { get; set; }
        public double? Remaining { get; set; }
        public double? Plus1 { get; set; }
        public double? TotalBalance { get; set; }
        public double? UnitPaid { get; set; }
        public double? TotalPaid { get; set; }
        public string? Package { get; set; }
        public double? AvgPaid { get; set; }
        public string? _ { get; set; }
        public double? Stills { get; set; }
        public double? _360S { get; set; }
        public double? Hdr { get; set; }
        public string? F15 { get; set; }
        public double? ValueUnit { get; set; }
        public double? ValueTotal { get; set; }
        public string? F18 { get; set; }
        public double? RealtorId2 { get; set; }
        public string? State { get; set; }
        public double? ProdId { get; set; }
        public string? CoupName1 { get; set; }
        public string? ProdName { get; set; }
        public double? ProdMask { get; set; }
        public double? Remaining1 { get; set; }
        public double? Year { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? LastUsedDate { get; set; }
        public double? YearUsed { get; set; }
        public string? Recent { get; set; }
        public double? StateLaw { get; set; }
        public string? ExpireAfterClose { get; set; }
        public double? MonthUsed { get; set; }
        public int? PrepaidProductId { get; set; }
    }
}
