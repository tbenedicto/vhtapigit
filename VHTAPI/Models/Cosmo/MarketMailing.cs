﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MarketMailing
    {
        public int MarketMailingId { get; set; }
        public int MarketCampaignId { get; set; }
        public DateTime DateSent { get; set; }
        public string MailedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual MarketCampaign MarketCampaign { get; set; } = null!;
    }
}
