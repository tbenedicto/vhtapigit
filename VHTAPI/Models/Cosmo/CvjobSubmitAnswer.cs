﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvjobSubmitAnswer
    {
        public int CvjobSubmitAnswerId { get; set; }
        public int JobId { get; set; }
        public bool AreProductsAndPayCorrect { get; set; }
        public bool WasClientPresent { get; set; }
        public bool WasPropertyOwnerPresent { get; set; }
        public bool WasConfirmedAppointment { get; set; }
        public bool WereImagesReviewedWithClient { get; set; }
        public bool WasFrontImageTaken { get; set; }
        public bool OtherExpensesIncurred { get; set; }
        public int CvSignatureCount { get; set; }
        public int CvPremiumCount { get; set; }
        public int Cv360SignatureCount { get; set; }
        public int Cv360PremiumCount { get; set; }
        public Guid Rowguid { get; set; }
        public int? Cv360TotalCount { get; set; }
        public int CvSilverCount { get; set; }
        public int CvGoldCount { get; set; }
        public int CvPlatinumCount { get; set; }
        public bool? IsCpix { get; set; }
        public bool? IsPotentialEasyOrder { get; set; }
        public int? CvSgpUpgrade { get; set; }
    }
}
