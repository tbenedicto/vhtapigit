﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageHashXxx
    {
        public int ImageHashId { get; set; }
        public string HashValue { get; set; } = null!;
        public string Url { get; set; } = null!;
        public int TourRoomCodeId { get; set; }
        public int ListingId { get; set; }
    }
}
