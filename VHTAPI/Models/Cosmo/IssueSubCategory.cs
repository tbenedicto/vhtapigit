﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class IssueSubCategory
    {
        public int IssueSubCategoryId { get; set; }
        public int IssueCategoryId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public Guid Rowguid { get; set; }
        public int VhtdepartmentId { get; set; }
        public bool IsActive { get; set; }
    }
}
