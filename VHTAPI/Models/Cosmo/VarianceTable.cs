﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VarianceTable
    {
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompanyName { get; set; }
        public string? AccountType { get; set; }
        public string? LocalSalesRep { get; set; }
        public string? AreaSalesRep { get; set; }
        public string? AccountManager { get; set; }
        public string? Csrep { get; set; }
        public string? Market { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public int? TheYear { get; set; }
        public int? TheMonth { get; set; }
        public int? NumAgents { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public double? TotalRevenue { get; set; }
        public string? Territory { get; set; }
    }
}
