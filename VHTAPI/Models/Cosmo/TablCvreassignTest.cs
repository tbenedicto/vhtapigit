﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablCvreassignTest
    {
        public int? Jobid { get; set; }
        public int? VideographerContactId { get; set; }
        public string? Ddltype { get; set; }
        public string? DisplayAs { get; set; }
    }
}
