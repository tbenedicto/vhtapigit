﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllProduct
    {
        public int ProductId { get; set; }
        public string? ExternalProductName { get; set; }
        public string? CorporateBillingName { get; set; }
        public bool? IsNonCommissionFee { get; set; }
        public string? ProductStatus { get; set; }
        public int? ProductQuantity { get; set; }
        public int? PremiumProductQuantity { get; set; }
        public int? TotalProductQuantity { get; set; }
        public string? ProductGroup { get; set; }
        public bool? CatStdStill { get; set; }
        public bool? CatPremStill { get; set; }
        public bool? CatStd360 { get; set; }
        public bool? CatClear360 { get; set; }
        public bool? CatDup { get; set; }
        public bool? CatPrints { get; set; }
        public bool? CatVideo { get; set; }
        public bool CatVodservices { get; set; }
        public bool CatVhttourCustom { get; set; }
        public bool CatVhttourMn { get; set; }
        public bool CatAlbum { get; set; }
        public bool CatFloorplan { get; set; }
        public bool CatSilver { get; set; }
        public bool CatGold { get; set; }
        public bool CatPlatinum { get; set; }
        public bool CatAerial { get; set; }
        public bool Cat3d { get; set; }
        public string Category { get; set; } = null!;
        public string CategoryGrouping { get; set; } = null!;
        public bool CatPix { get; set; }
        public bool CatPrepaid { get; set; }
        public bool CatBlueSky { get; set; }
        public bool CatDigitalEnhancement { get; set; }
        public int? SessionImagesCount { get; set; }
        public bool CatFullMotion { get; set; }
        public bool CatElevated { get; set; }
        public bool CatZillow { get; set; }
    }
}
