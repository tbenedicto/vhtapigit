﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbCompanyOrderFormPackage
    {
        public int CompanyOrderFormPackageId { get; set; }
        public int CompanyOrderFormId { get; set; }
        public int PackageId { get; set; }
        public int OrderFormProductCategoryId { get; set; }
        public int SortId { get; set; }
        public string MarketingName { get; set; } = null!;
        public bool Collapsed { get; set; }
        public int CompanyOrderFormShowRateTypeId { get; set; }
        public bool UseRateCustomText { get; set; }
        public string? RateCustomText { get; set; }
        public Guid Rowguid { get; set; }
    }
}
