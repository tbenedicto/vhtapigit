﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactLifttimeRevenueDailySummary
    {
        public int CompanyId { get; set; }
        public string? DisplayAs { get; set; }
        public decimal? Revenue12month { get; set; }
        public int? Listing12Month { get; set; }
        public decimal? RevenueLifetime { get; set; }
        public int? ListingLifetime { get; set; }
        public decimal? RevenuePriorYear { get; set; }
        public int? ListingPriorYear { get; set; }
        public decimal? RevenueYeartoDate { get; set; }
        public int? ListingYeartoDate { get; set; }
        public int? ListingMtd { get; set; }
        public Guid Rowguid { get; set; }
    }
}
