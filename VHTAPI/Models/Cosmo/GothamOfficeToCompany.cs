﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GothamOfficeToCompany
    {
        public int GothamOfficeid { get; set; }
        public int Companyid { get; set; }
        public int? NewCompanyId { get; set; }
    }
}
