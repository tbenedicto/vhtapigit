﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessLog
    {
        public int ProcessLogId { get; set; }
        public int ProcessRunId { get; set; }
        public int ProcessLogTypeId { get; set; }
        public DateTime LogDate { get; set; }
        public string Message { get; set; } = null!;
        public string? MessageExtended { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ProcessLogType ProcessLogType { get; set; } = null!;
        public virtual ProcessRun ProcessRun { get; set; } = null!;
    }
}
