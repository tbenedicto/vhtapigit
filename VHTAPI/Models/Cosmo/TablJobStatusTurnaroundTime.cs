﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablJobStatusTurnaroundTime
    {
        public int JobId { get; set; }
        public string JobStatus { get; set; } = null!;
        public DateTime? Date { get; set; }
        public int? MyHour { get; set; }
        public int? Timespent { get; set; }
    }
}
