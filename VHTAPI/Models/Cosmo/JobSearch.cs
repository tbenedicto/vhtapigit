﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobSearch
    {
        public int RowId { get; set; }
        public string? SearchText { get; set; }
        public Guid Rowguid { get; set; }
    }
}
