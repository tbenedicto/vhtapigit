﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DistributionAlertStatus
    {
        public DistributionAlertStatus()
        {
            DistributionAlerts = new HashSet<DistributionAlert>();
        }

        public int DistributionAlertStatusId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<DistributionAlert> DistributionAlerts { get; set; }
    }
}
