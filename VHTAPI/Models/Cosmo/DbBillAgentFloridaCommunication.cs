﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DbBillAgentFloridaCommunication
    {
        public int? FkparentId { get; set; }
        public string? TextValue { get; set; }
        public double? Id { get; set; }
        public string? Email { get; set; }
        public string? FullName { get; set; }
        public string? FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
        public string? Suffix { get; set; }
        public string? OfficeName { get; set; }
        public string? OfficeAddress1 { get; set; }
        public string? OfficeAddress2 { get; set; }
        public string? OfficeCity { get; set; }
        public string? OfficeState { get; set; }
        public string? OfficeZip { get; set; }
        public string? OfficeCounty { get; set; }
        public string? OfficePhone { get; set; }
        public string? OfficeFax { get; set; }
        public string? CellPhone { get; set; }
        public string? LicenseType { get; set; }
        public string? LicenseNumber { get; set; }
        public string? OriginalIssueDate { get; set; }
        public string? ExpirationDate { get; set; }
        public string? HomeAddress1 { get; set; }
        public string? HomeAddress2 { get; set; }
        public string? HomeCity { get; set; }
        public string? HomeState { get; set; }
        public string? HomeZip { get; set; }
        public string? HomeCounty { get; set; }
        public string? Association { get; set; }
    }
}
