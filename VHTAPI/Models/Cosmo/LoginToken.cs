﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LoginToken
    {
        public Guid LoginTokenId { get; set; }
        public string AspsessionId { get; set; } = null!;
        public string UserAgent { get; set; } = null!;
        public int ContactId { get; set; }
        public DateTime LoginDateTime { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime RowModifiedDate { get; set; }
        public string RowModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
