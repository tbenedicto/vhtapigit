﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablRollingNewClient
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? DisplayAs { get; set; }
        public string? TourDisplayEmail { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public string? RegionName { get; set; }
        public DateTime? LastOrderDate { get; set; }
        public string? Ade { get; set; }
        public string? Territory { get; set; }
        public string? RegionalSales { get; set; }
        public DateTime? CosmoAddedDate { get; set; }
        public string? ContactHistoryType { get; set; }
        public decimal? Revenuelifetime { get; set; }
        public string? Contactcode { get; set; }
        public int Contactid { get; set; }
        public string? AgentName { get; set; }
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? Tourlink { get; set; }
        public string? ParentCompanyName { get; set; }
        public string? AgentStatusDescr { get; set; }
        public int? CompanyCountNew { get; set; }
        public int? CompanyCount2ndOrderDate { get; set; }
        public int? CompanyCountRenew { get; set; }
        public DateTime _1stStartdate { get; set; }
        public int? Months { get; set; }
        public string? Year { get; set; }
        public DateTime? NewAgentAddedDate { get; set; }
        public string? CompanyType { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public string? LocalSales { get; set; }
        public int? Jobover5012Month { get; set; }
        public DateTime? _2ndOrderDate { get; set; }
        public int? Listingid { get; set; }
        public string HowContactus { get; set; } = null!;
        public string? State { get; set; }
        public decimal? AgentPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? CvPay { get; set; }
        public string? CertificateNumber { get; set; }
        public string? AddedBy { get; set; }
        public string? AddedbyJob { get; set; }
        public string? AddedbyListing { get; set; }
        public string AccountType { get; set; } = null!;
        public DateTime? StatusProductionCompletedOn { get; set; }
        public string? CompanyStatus { get; set; }
        public string? Region { get; set; }
        public string? Adm { get; set; }
        public string? NewAde { get; set; }
        public int? CompletedOnJobid { get; set; }
        public string? Vhtrelationship { get; set; }
        public string? OfficeType { get; set; }
        public string? JobSource { get; set; }
    }
}
