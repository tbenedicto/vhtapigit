﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwG3SearchResult
    {
        public int ListingId { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? PropertyId { get; set; }
        public string CustomerCode { get; set; } = null!;
        public int? TourCount { get; set; }
        public string? Address { get; set; }
        public string? State { get; set; }
        public string? VhtjobId { get; set; }
        public string? Url { get; set; }
        public string BaseUrl { get; set; } = null!;
    }
}
