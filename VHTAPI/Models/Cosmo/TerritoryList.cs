﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TerritoryList
    {
        public string? Territory { get; set; }
        public string? TerritoryDescr { get; set; }
        public int TerritoryId { get; set; }
    }
}
