﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SalesforceContact
    {
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? Salutation { get; set; }
        public string Title { get; set; } = null!;
        public string? MyVhtEmail { get; set; }
        public string? MailingStreet { get; set; }
        public string? Phone { get; set; }
        public string? MobilePhone { get; set; }
        public string CvStatus { get; set; } = null!;
    }
}
