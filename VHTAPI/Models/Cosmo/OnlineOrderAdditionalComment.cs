﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OnlineOrderAdditionalComment
    {
        public int OnlineOrderId { get; set; }
        public int CommentCategoryId { get; set; }
        public string? CommentText { get; set; }
        public int? CommentRecipientId { get; set; }
        public bool? CommentUnread { get; set; }
        public string? CommentDescr { get; set; }
        public string? CommentSubject { get; set; }
        public int OnlineOrderAdditionalCommentId { get; set; }
    }
}
