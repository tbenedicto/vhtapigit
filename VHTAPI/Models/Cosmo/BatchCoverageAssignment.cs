﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BatchCoverageAssignment
    {
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZipCode { get; set; }
        public int? JobId { get; set; }
    }
}
