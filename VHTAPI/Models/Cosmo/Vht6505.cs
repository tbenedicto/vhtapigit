﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Vht6505
    {
        public string? AgentHeadshot { get; set; }
        public string? Url { get; set; }
        public string? AgentEmail { get; set; }
        public double? AgentId { get; set; }
        public string? AgentName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public double? BreLicense { get; set; }
        public string? Office { get; set; }
        public string? OfficeMlsId { get; set; }
    }
}
