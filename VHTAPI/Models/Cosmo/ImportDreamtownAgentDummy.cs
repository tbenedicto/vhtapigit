﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportDreamtownAgentDummy
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? ExchangeEmail { get; set; }
        public string? CellPhone { get; set; }
        public double? MlsAgentId { get; set; }
        public string? DreamTownOffice { get; set; }
        public double? VhtOfficeCode { get; set; }
        public string? EmailToCc { get; set; }
        public double? EmailMatch { get; set; }
        public double? LastMatch { get; set; }
        public string? F11 { get; set; }
        public string? F12 { get; set; }
        public string? F13 { get; set; }
        public string? Cosmo { get; set; }
        public double? ContactId { get; set; }
        public string? Status { get; set; }
        public string? ContactSource { get; set; }
        public string? Last { get; set; }
        public string? First { get; set; }
        public string? Company { get; set; }
        public string? Code { get; set; }
        public string? MainPhonePrimary { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public string? EmailPrimary { get; set; }
        public string? Email1 { get; set; }
        public string? Email2 { get; set; }
        public string? Email3 { get; set; }
        public string? TextPager { get; set; }
        public string? MainPhone { get; set; }
        public string? Fax { get; set; }
        public string? CellularPhone { get; set; }
        public string? Pager { get; set; }
        public string? HomePhone { get; set; }
        public string? DirectLine { get; set; }
        public string? Voicemail { get; set; }
        public string? Website { get; set; }
    }
}
