﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingOpenHouse
    {
        public int ListingOpenHouseId { get; set; }
        public int ListingId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public Guid Rowguid { get; set; }
    }
}
