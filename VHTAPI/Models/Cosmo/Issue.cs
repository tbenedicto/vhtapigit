﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Issue
    {
        public int IssueId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public int IssueCategoryId { get; set; }
        public int IssueSubCategoryId { get; set; }
        public int IssueStatusId { get; set; }
        public int IssueSourceId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public Guid Rowguid { get; set; }
        public Guid Rowguid13 { get; set; }
        public DateTime IssueStatusDate { get; set; }
        public string? EmailAddress { get; set; }
        public string? EmailSubject { get; set; }
        public int? IssueAssignedCvid { get; set; }
        public bool ClientServicesReview { get; set; }
    }
}
