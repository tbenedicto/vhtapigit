﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class StudioWorkTrace
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public string TraceInfo { get; set; } = null!;
        public int CommentId { get; set; }
        public int StudioWorkTraceStatusId { get; set; }
        public string? ProcessDetail { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
    }
}
