﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvzipcodePin
    {
        public double? Companyid { get; set; }
        public double? Cvid { get; set; }
        public string? Displayas { get; set; }
        public string? Firstname { get; set; }
        public string? Lastname { get; set; }
        public double? Contactstatusid { get; set; }
        public string? ZipCode { get; set; }
        public string? Addressline1 { get; set; }
        public string? City { get; set; }
        public string? Stateid { get; set; }
    }
}
