﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SecurityGroup
    {
        public SecurityGroup()
        {
            SecurityGroupMembers = new HashSet<SecurityGroupMember>();
        }

        public int SecurityGroupId { get; set; }
        public bool SystemInd { get; set; }
        public string GroupName { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public int SecurityGroupTypeId { get; set; }

        public virtual ICollection<SecurityGroupMember> SecurityGroupMembers { get; set; }
    }
}
