﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdeReassignment2015ChildOffice
    {
        public int NumOffices { get; set; }
        public int NumAgents { get; set; }
        public int CompanyId { get; set; }
        public double? Agents { get; set; }
        public double? Offices { get; set; }
        public DateTime NumOfficesLastUpdatedDate { get; set; }
        public DateTime NumAgentsLastUpdatedDate { get; set; }
    }
}
