﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebListingDistributionPoint
    {
        public int ListingDistributionPointId { get; set; }
        public int ListingId { get; set; }
        public int DistributionPointId { get; set; }
        public string Descr { get; set; } = null!;
        public bool PostedInd { get; set; }
        public DateTime? PostedOn { get; set; }
        public string? PostedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
