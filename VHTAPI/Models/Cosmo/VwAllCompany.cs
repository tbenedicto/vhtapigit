﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllCompany
    {
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? CompanyFullName { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? ParentCompany { get; set; }
        public int? TopLevelCompanyId { get; set; }
        public string? TopLevelCompanyName { get; set; }
        public DateTime? TopLevelAddedDate { get; set; }
        public string? TopLevelCompanyFullName { get; set; }
        public DateTime? CompanyAddedDate { get; set; }
        public string? Territory { get; set; }
        public string? TerritoryState { get; set; }
        public string? CompanyType { get; set; }
        public string? CompanyCategory { get; set; }
    }
}
