﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class EsoftPhotoType
    {
        public int EsoftPhotoTypeId { get; set; }
        public int JobId { get; set; }
        public int RoomCodeId { get; set; }
        public DateTime AddedTime { get; set; }
        public bool Signature { get; set; }
        public bool Premium { get; set; }
    }
}
