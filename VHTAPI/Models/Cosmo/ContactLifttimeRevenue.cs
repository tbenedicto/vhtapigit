﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactLifttimeRevenue
    {
        public int ContactId { get; set; }
        public decimal? Revenue12month { get; set; }
        public int? Listing12Month { get; set; }
        public int? Jobover5012Month { get; set; }
        public decimal? RevenueLifetime { get; set; }
        public int? ListingLifetime { get; set; }
        public int? Jobover50Lifetime { get; set; }
        public decimal? RevenuePriorYear { get; set; }
        public int? ListingPriorYear { get; set; }
        public int? Jobover50PriorYear { get; set; }
        public decimal? RevenueYeartoDate { get; set; }
        public int? ListingYeartoDate { get; set; }
        public int? Jobover50YeartoDate { get; set; }
        public int? Jobover5012MonthYesterday { get; set; }
        public Guid Rowguid { get; set; }
        public decimal? RevenueMtd { get; set; }
        public int? ListingMtd { get; set; }
        public int? Jobover50Mtd { get; set; }
        public int? Listing6Month { get; set; }
        public decimal? Revenue6month { get; set; }
        public decimal? Revenue2PriorYears { get; set; }
        public int? Listing2PriorYears { get; set; }
        public int? Jobover502PriorYears { get; set; }
    }
}
