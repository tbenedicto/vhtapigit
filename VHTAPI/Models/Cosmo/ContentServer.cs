﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContentServer
    {
        public int ContentServerId { get; set; }
        public string BaseUrl { get; set; } = null!;
        public string? BasePath { get; set; }
        public string? Descr { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public byte[]? TimpStampField { get; set; }
        public int? Xref01 { get; set; }
        public string? Xref02 { get; set; }
        public string? Xref03 { get; set; }
        public Guid Rowguid { get; set; }
        public string? BaseUrlimages { get; set; }
        public string? DriveLetter { get; set; }
    }
}
