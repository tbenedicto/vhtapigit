﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvReassignmentConnection
    {
        public string? Cvreassign { get; set; }
        public int? ContactConnection { get; set; }
    }
}
