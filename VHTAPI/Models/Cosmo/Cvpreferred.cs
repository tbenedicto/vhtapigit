﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Cvpreferred
    {
        public int ContactId { get; set; }
        public int VideographerContactId { get; set; }
        public int OrderIndex { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid RowId { get; set; }
        public byte[] RowVersion { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
