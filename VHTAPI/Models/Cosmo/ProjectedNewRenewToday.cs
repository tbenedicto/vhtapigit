﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProjectedNewRenewToday
    {
        public int ContactId { get; set; }
        public string? _1stStartdate { get; set; }
        public int? ContactClientStatusId { get; set; }
        public string? ContactName { get; set; }
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public int? RepSalesNational { get; set; }
        public string? Ade { get; set; }
        public DateTime? SalesStartDate { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? ParentCompany { get; set; }
        public int? StatusProductionCompletedOn { get; set; }
        public int Jobover5012Month { get; set; }
        public string ProjectedNewOrRenew { get; set; } = null!;
        public int? IsMms { get; set; }
        public string? MinStage { get; set; }
        public DateTime? MinProjProductionComplete { get; set; }
        public int? JobStatusid { get; set; }
        public string? Territory { get; set; }
        public int? Jobid { get; set; }
        public DateTime? MinAppointmentDateTime { get; set; }
        public int Listingid { get; set; }
    }
}
