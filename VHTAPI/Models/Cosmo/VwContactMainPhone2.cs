﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContactMainPhone2
    {
        public int? ContactId { get; set; }
        public int? CommunicationId { get; set; }
    }
}
