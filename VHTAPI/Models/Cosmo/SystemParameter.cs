﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SystemParameter
    {
        public int SystemParameterId { get; set; }
        public string XmlproductTree { get; set; } = null!;
        public string? ProfileName { get; set; }
        public bool IsProfileActive { get; set; }
        public string? G3partnerBaseUrl { get; set; }
        public string? G3tempUploadDir { get; set; }
        public string? G3viewerFileDir { get; set; }
        public string? G3partnerTextFileDir { get; set; }
        public string? G3agentPortraitDir { get; set; }
        public string? G3agentPortraitUrl { get; set; }
        public string? G3smtpserver { get; set; }
        public Guid Rowguid { get; set; }
    }
}
