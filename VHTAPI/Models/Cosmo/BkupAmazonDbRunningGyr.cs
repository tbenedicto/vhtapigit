﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkupAmazonDbRunningGyr
    {
        public string? Instance { get; set; }
        public string? InstanceName { get; set; }
        public string? AwsAccountId { get; set; }
        public string? Status { get; set; }
        public string? InstanceType { get; set; }
        public string? Rgy { get; set; }
        public string? Notes { get; set; }
    }
}
