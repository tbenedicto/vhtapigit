﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Vw13monthProductCount
    {
        public int? CompleteYear { get; set; }
        public int? CompleteMonth { get; set; }
        public int Productid { get; set; }
        public string Product { get; set; } = null!;
        public int? YearMonthCount { get; set; }
    }
}
