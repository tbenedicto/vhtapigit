﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZipcodeU
    {
        public double? Zip { get; set; }
        public string? State { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string? Coverage { get; set; }
        public string? WithTravel { get; set; }
        public long? RegionId { get; set; }
        public string? Market { get; set; }
    }
}
