﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VneContactGroupsNevin
    {
        public string? Email { get; set; }
        public string? Contactgroup { get; set; }
        public int? Contactgroupid { get; set; }
    }
}
