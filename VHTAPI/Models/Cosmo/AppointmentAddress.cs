﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AppointmentAddress
    {
        public int AppointmentId { get; set; }
        public string PropertyAddressLine1 { get; set; } = null!;
        public string? PropertyAddressLine2 { get; set; }
        public string PropertyCity { get; set; } = null!;
        public string PropertyState { get; set; } = null!;
        public string PropertyZip { get; set; } = null!;
        public int? ServiceCategoryId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
