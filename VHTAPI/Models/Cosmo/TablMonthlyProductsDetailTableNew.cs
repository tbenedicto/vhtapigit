﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablMonthlyProductsDetailTableNew
    {
        public DateTime? MonthCompleted { get; set; }
        public string? VhtjobId { get; set; }
        public int MonthInShortRange { get; set; }
        public string? ExternalProductName { get; set; }
        public string? ProductGroup { get; set; }
        public string Category { get; set; } = null!;
        public string CategoryGrouping { get; set; } = null!;
        public int CompanyId { get; set; }
        public string? CompanyFullName { get; set; }
        public int TopLevelCompanyId { get; set; }
        public string? TopLevelCompanyFullName { get; set; }
        public string? Territory { get; set; }
        public string? TerritoryState { get; set; }
        public string? CompanyCategory { get; set; }
        public string ListingType { get; set; } = null!;
        public int JobId { get; set; }
        public int ListingId { get; set; }
        public int? AgentId { get; set; }
        public decimal AgentPay { get; set; }
        public decimal CorporatePay { get; set; }
        public decimal? ProductTotalPrice { get; set; }
        public string? AccountType { get; set; }
    }
}
