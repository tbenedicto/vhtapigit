﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Vhtnation
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? ApplicationRating { get; set; }
        public string? ApplicationStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? LastActivity { get; set; }
        public DateTime? LastModified { get; set; }
        public string? Email { get; set; }
        public string? City { get; set; }
        public string? StateProvince { get; set; }
        public string? ZipPostalCode { get; set; }
    }
}
