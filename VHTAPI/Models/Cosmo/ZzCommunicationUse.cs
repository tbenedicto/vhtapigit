﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCommunicationUse
    {
        /// <summary>
        /// Unique identifier assigned by the system
        /// </summary>
        public int CommunicationUseId { get; set; }
        /// <summary>
        /// Communication Category Group this Use belongs to
        /// </summary>
        public int? CommunicationCategoryGroupId { get; set; }
        /// <summary>
        /// Value to sort list by
        /// </summary>
        public int? SortId { get; set; }
        /// <summary>
        /// Description of the data
        /// </summary>
        public string? Descr { get; set; }
        /// <summary>
        /// Date the row was inserted
        /// </summary>
        public DateTime? AddedDate { get; set; }
        /// <summary>
        /// Who inserted the row
        /// </summary>
        public string? AddedBy { get; set; }
        /// <summary>
        /// Date the row was updated last
        /// </summary>
        public DateTime? ModifiedDate { get; set; }
        /// <summary>
        /// Who updated the row last
        /// </summary>
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
