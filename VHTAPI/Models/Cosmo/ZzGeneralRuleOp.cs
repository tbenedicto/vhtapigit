﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzGeneralRuleOp
    {
        public ZzGeneralRuleOp()
        {
            GeneralRuleAvailableProductsOps = new HashSet<GeneralRule>();
            GeneralRuleDefaultProductsOps = new HashSet<GeneralRule>();
            GeneralRuleDistributionSitesOps = new HashSet<GeneralRule>();
            GeneralRulePreferredProductsOps = new HashSet<GeneralRule>();
        }

        public int GeneralRuleOpId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<GeneralRule> GeneralRuleAvailableProductsOps { get; set; }
        public virtual ICollection<GeneralRule> GeneralRuleDefaultProductsOps { get; set; }
        public virtual ICollection<GeneralRule> GeneralRuleDistributionSitesOps { get; set; }
        public virtual ICollection<GeneralRule> GeneralRulePreferredProductsOps { get; set; }
    }
}
