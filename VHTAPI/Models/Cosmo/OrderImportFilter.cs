﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderImportFilter
    {
        public OrderImportFilter()
        {
            OrderImportBatches = new HashSet<OrderImportBatch>();
        }

        public int OrderImportFilterId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<OrderImportBatch> OrderImportBatches { get; set; }
    }
}
