﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GeneralRulePreferredWt
    {
        public int? GeneralRuleId { get; set; }
        public int? ProductId { get; set; }
    }
}
