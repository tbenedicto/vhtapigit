﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablRollingUniqueClient
    {
        public string? DisplayAs { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? TourDisplayEmail { get; set; }
        public string? CompanyName { get; set; }
        public string? CompanyName1 { get; set; }
        public string? ParentCompanyName { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public string? Region { get; set; }
        public DateTime? LastOrderDate { get; set; }
        public int? AgentId { get; set; }
        public string? Vhtrelationship { get; set; }
        public string? OfficeType { get; set; }
        public string? JobSource { get; set; }
        public string? HowContactus { get; set; }
        public string? CompanySubsidiary { get; set; }
    }
}
