﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CubicasaPackageGroup
    {
        public int CubicasaPackageGroupId { get; set; }
        public int PackageId { get; set; }
        public int PackageGroupId { get; set; }
        public int? SizeDownTo { get; set; }
        public int? SizeUpTo { get; set; }
    }
}
