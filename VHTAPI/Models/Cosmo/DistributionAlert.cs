﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DistributionAlert
    {
        public DistributionAlert()
        {
            DistributionAlertNotes = new HashSet<DistributionAlertNote>();
        }

        public int DistributionAlertId { get; set; }
        public int? ProcessId { get; set; }
        public string Title { get; set; } = null!;
        public int DistributionAlertStatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual DistributionAlertStatus DistributionAlertStatus { get; set; } = null!;
        public virtual Process? Process { get; set; }
        public virtual ICollection<DistributionAlertNote> DistributionAlertNotes { get; set; }
    }
}
