﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AusteKeye
    {
        public string? Office { get; set; }
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Classification { get; set; }
        public string? F7 { get; set; }
        public string? F8 { get; set; }
    }
}
