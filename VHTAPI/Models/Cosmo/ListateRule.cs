﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListateRule
    {
        public ListateRule()
        {
            ListateRuleDetails = new HashSet<ListateRuleDetail>();
        }

        public string StateAbbr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<ListateRuleDetail> ListateRuleDetails { get; set; }
    }
}
