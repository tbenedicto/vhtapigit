﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SupportTicketTag
    {
        public int SupportTicketTagId { get; set; }
        public int SupportTicketId { get; set; }
        public int TicketTagId { get; set; }
    }
}
