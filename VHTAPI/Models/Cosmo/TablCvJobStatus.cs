﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablCvJobStatus
    {
        public DateTime? AddedDate { get; set; }
        public string? DisplayAs { get; set; }
        public string? JobStatus { get; set; }
        public int JobId { get; set; }
        public string? Name { get; set; }
        public int ContactId { get; set; }
        public int? CommentCategoryId { get; set; }
        public int? CommentId { get; set; }
        public string? CommentText { get; set; }
        public string? Cvreassign { get; set; }
        public bool? IsAutomated { get; set; }
        public string? Reassign { get; set; }
        public string? VhtjobId { get; set; }
    }
}
