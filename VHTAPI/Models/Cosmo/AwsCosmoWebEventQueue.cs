﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebEventQueue
    {
        public Guid EventId { get; set; }
        public DateTime EventDate { get; set; }
        public string Topic { get; set; } = null!;
        public string Data { get; set; } = null!;
    }
}
