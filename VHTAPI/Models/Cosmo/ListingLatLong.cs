﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingLatLong
    {
        public int ListingLatLongId { get; set; }
        public int ListingId { get; set; }
        public string LatitudeCoordinate { get; set; } = null!;
        public string LongitudeCoordinate { get; set; } = null!;
    }
}
