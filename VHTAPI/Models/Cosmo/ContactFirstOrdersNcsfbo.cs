﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactFirstOrdersNcsfbo
    {
        public int Contactid { get; set; }
        public int Listingid { get; set; }
        public DateTime? _1ststartdate { get; set; }
        public int CompletedOnJobid { get; set; }
        public string ContactHistoryType { get; set; } = null!;
        public DateTime? AppointmentDateTime { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public string? JobSource { get; set; }
        public decimal? TotalRevListing1stJob { get; set; }
        public decimal? TotalRev { get; set; }
        public int SalespersonId { get; set; }
    }
}
