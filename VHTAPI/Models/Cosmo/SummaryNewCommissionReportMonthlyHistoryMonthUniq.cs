﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SummaryNewCommissionReportMonthlyHistoryMonthUniq
    {
        public int? Myyear { get; set; }
        public int? Mymonth { get; set; }
        public int? UniqAgentPerMonth { get; set; }
    }
}
