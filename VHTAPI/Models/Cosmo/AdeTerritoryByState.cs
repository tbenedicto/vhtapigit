﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdeTerritoryByState
    {
        public string? State { get; set; }
        public string? StateId { get; set; }
        public string? Ade { get; set; }
        public int? RepSalesNational { get; set; }
    }
}
