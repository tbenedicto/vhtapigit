﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MobileApiClient
    {
        public Guid ClientId { get; set; }
        public string ClientSecret { get; set; } = null!;
        public string ClientName { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
