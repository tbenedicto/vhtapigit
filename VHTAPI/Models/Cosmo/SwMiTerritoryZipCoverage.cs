﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SwMiTerritoryZipCoverage
    {
        public double? SwMichiganZipCodes { get; set; }
        public string? F2 { get; set; }
        public string? F3 { get; set; }
    }
}
