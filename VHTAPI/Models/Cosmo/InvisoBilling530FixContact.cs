﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoBilling530FixContact
    {
        public int? FkparentId { get; set; }
        public int? CosmoContactid { get; set; }
        public string? VhtjobId { get; set; }
        public int MasterJobid { get; set; }
        public int? InvisoTourId { get; set; }
        public int Masterlistingid { get; set; }
    }
}
