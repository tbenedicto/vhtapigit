﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListPriceRange
    {
        public int ListPriceRangeId { get; set; }
        public decimal PriceGreaterEqual { get; set; }
        public decimal PriceLessThan { get; set; }
        public string DisplayName { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public byte[] Tsvalue { get; set; } = null!;
    }
}
