﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablYearEndProjection
    {
        public string? TargetType { get; set; }
        public double? YearTarget { get; set; }
        public double Ytdtotal { get; set; }
        public double? EofyearProjection { get; set; }
    }
}
