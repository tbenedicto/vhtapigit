﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class WebAccountAudit
    {
        public int RowId { get; set; }
        public Guid? LoginTokenId { get; set; }
        public string? AspsessionId { get; set; }
        public int? ContactId { get; set; }
        public string? UserId { get; set; }
        public DateTime EventDate { get; set; }
        public int EventId { get; set; }
        public Guid Rowguid { get; set; }
        public string? LoginEmail { get; set; }
    }
}
