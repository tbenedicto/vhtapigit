﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllJobsCount
    {
        public int JobId { get; set; }
        public int? TotalJobProductQuantity { get; set; }
        public int? TotalJobProductsInCategory { get; set; }
        public int? Num3D { get; set; }
        public int? Aerial { get; set; }
        public int? Albums { get; set; }
        public int? Clear360 { get; set; }
        public int? Duplicates { get; set; }
        public int? FloorPlans { get; set; }
        public int? GoldSeries { get; set; }
        public int? PlatinumSeries { get; set; }
        public int? PremiumStills { get; set; }
        public int? Prints { get; set; }
        public int? SilverSeries { get; set; }
        public int? Studio360 { get; set; }
        public int? StandardStills { get; set; }
        public int? VhtcustomTours { get; set; }
        public int? Videos { get; set; }
        public int? VirtualStaging { get; set; }
        public int? BlueSky { get; set; }
        public int? DigitalEnhance { get; set; }
    }
}
