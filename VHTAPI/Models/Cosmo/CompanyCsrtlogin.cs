﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyCsrtlogin
    {
        public int CompanyId { get; set; }
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public DateTime? LastLogin { get; set; }
    }
}
