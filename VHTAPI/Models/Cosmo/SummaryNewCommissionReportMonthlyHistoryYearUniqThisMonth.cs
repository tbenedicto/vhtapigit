﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SummaryNewCommissionReportMonthlyHistoryYearUniqThisMonth
    {
        public int? Myyear { get; set; }
        public int? Mymonth { get; set; }
        public int? UniqAgents { get; set; }
    }
}
