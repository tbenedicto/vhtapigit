﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCompanyPromotion
    {
        public ZzCompanyPromotion()
        {
            CompanyCompanyPromotions = new HashSet<CompanyCompanyPromotion>();
        }

        public int CompanyPromotionId { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<CompanyCompanyPromotion> CompanyCompanyPromotions { get; set; }
    }
}
