﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class IssueAction
    {
        public int IssueActionId { get; set; }
        public int IssueId { get; set; }
        public int IssueActionTypeId { get; set; }
        public string Note { get; set; } = null!;
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public Guid Rowguid { get; set; }
        public Guid Rowguid10 { get; set; }
    }
}
