﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyCompanyUpsell
    {
        public int CompanyId { get; set; }
        public int CompanyUpsellId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual Company Company { get; set; } = null!;
        public virtual ZzCompanyUpsell CompanyUpsell { get; set; } = null!;
    }
}
