﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CollaboratorAgent
    {
        public int CollaboratorAgentId { get; set; }
        public int CollaboratorId { get; set; }
        public int AgentId { get; set; }
        public int? CollaboratorAgentStatusId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
