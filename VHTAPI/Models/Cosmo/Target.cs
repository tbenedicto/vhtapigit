﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Target
    {
        public double? Year { get; set; }
        public double? Month { get; set; }
        public string? TargetType { get; set; }
        public double? Target1 { get; set; }
    }
}
