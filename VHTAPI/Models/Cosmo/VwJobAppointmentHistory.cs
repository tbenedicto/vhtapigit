﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwJobAppointmentHistory
    {
        public int JobId { get; set; }
        public string EventAction { get; set; } = null!;
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int AppointmentTypeId { get; set; }
        public string AppointmentType { get; set; } = null!;
        public DateTime ExpiresDateTime { get; set; }
        public DateTime EventDate { get; set; }
        public string AddedBy { get; set; } = null!;
    }
}
