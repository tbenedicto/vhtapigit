﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class YouTubeAudit
    {
        public int ChannelId { get; set; }
        public string SourceApp { get; set; } = null!;
        public int SourceItemId { get; set; }
        public int NumUploads { get; set; }
        public DateTime? LastUploaded { get; set; }
        public int CurrentStatus { get; set; }
        public string Yttitle { get; set; } = null!;
        public string Ytdescription { get; set; } = null!;
        public string YtkeyWords { get; set; } = null!;
        public string SourceVideoPath { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public byte[] ObjectVersion { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public Guid Rowguid17 { get; set; }

        public virtual YouTubeChannel Channel { get; set; } = null!;
    }
}
