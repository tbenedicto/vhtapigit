﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContentServerCorrect
    {
        public int ListingId { get; set; }
        public int CurrentContentServerId { get; set; }
        public int RealContentServerId { get; set; }
    }
}
