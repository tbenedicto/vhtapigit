﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Pwreset
    {
        public Guid SecurityKey { get; set; }
        public int ContactId { get; set; }
        public DateTime DateExpires { get; set; }
        public Guid Rowguid { get; set; }
    }
}
