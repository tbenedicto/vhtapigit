﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VizziFailedPostingLog
    {
        public int Id { get; set; }
        public int? VizziinternalTourId { get; set; }
        public int? ListingId { get; set; }
        public int? JobId { get; set; }
        public DateTime? AddedTime { get; set; }
        public string? FailedReason { get; set; }
    }
}
