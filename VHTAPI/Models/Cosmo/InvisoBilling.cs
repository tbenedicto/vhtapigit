﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoBilling
    {
        public double? InvisoListingId { get; set; }
        public string? InvisoOrderId { get; set; }
        public double? ListingType { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public double? PropertyZipCode { get; set; }
        public string? Bedrooms { get; set; }
        public string? Baths { get; set; }
        public double? AgentId { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? ListingAgentEmail { get; set; }
        public string? ListingAgentCellPhone { get; set; }
        public string? LockBoxCode { get; set; }
        public string? BrokerageName { get; set; }
        public string? BrokerageAgentId { get; set; }
        public string? BrokerageOfficeId { get; set; }
        public string? BrokerageOfficeAddress1 { get; set; }
        public string? BrokerageOfficeAddress2 { get; set; }
        public string? BrokerageOfficeCity { get; set; }
        public string? BrokerageOfficeState { get; set; }
        public string? BrokerageOfficeZipCode { get; set; }
        public double? Product1 { get; set; }
        public decimal? Product1Price { get; set; }
        public string? Product1Comment { get; set; }
        public double? Product2 { get; set; }
        public decimal? Product2Price { get; set; }
        public string? Product2Comment { get; set; }
        public double? Product3 { get; set; }
        public decimal? Product3Price { get; set; }
        public string? Product3Comment { get; set; }
        public double? Product4 { get; set; }
        public decimal? Product4Price { get; set; }
        public string? Product4Comment { get; set; }
        public string? Product5 { get; set; }
        public string? Product5Price { get; set; }
        public string? Product5Comment { get; set; }
        public string? Product6 { get; set; }
        public string? Product6Price { get; set; }
        public string? Product6Comment { get; set; }
        public string? Product7 { get; set; }
        public string? Product7Price { get; set; }
        public string? Product7Comment { get; set; }
        public string? Longitude { get; set; }
        public string? Latitude { get; set; }
        public string? ListingAgentSecondaryPhone { get; set; }
        public DateTime? CompletedOrderDate { get; set; }
        public DateTime? OrderDate { get; set; }
        public string? PhotographerId { get; set; }
        public string? PhotoUploadDate { get; set; }
    }
}
