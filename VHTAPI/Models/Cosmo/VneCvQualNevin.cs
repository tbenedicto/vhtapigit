﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VneCvQualNevin
    {
        public string? Firstname { get; set; }
        public string? Lastname { get; set; }
        public string? Email { get; set; }
        public bool? Photo { get; set; }
        public bool? Floorplan { get; set; }
        public bool? Aeriel { get; set; }
        public bool? Fullmotion { get; set; }
        public bool? Agtinterview { get; set; }
        public bool? _3d { get; set; }
        public bool? Twilight { get; set; }
        public string? Notes { get; set; }
        public bool? Gold { get; set; }
    }
}
