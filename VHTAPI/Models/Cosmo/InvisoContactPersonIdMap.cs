﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoContactPersonIdMap
    {
        public int? Contactid { get; set; }
        public int? PersonId { get; set; }
        public bool? AlreadyExisted { get; set; }
    }
}
