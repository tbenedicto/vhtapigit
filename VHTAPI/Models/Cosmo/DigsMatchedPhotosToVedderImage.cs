﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DigsMatchedPhotosToVedderImage
    {
        public string FoundImageUrl { get; set; } = null!;
        public int? NumbInMatch { get; set; }
    }
}
