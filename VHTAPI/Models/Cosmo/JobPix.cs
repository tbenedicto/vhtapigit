﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobPix
    {
        public int JobId { get; set; }
        public int PixAgentId { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
