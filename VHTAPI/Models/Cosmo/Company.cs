﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Company
    {
        public Company()
        {
            BrokerageIdmatches = new HashSet<BrokerageIdmatch>();
            BrokerageIps = new HashSet<BrokerageIp>();
            CompanyCompanyPromotions = new HashSet<CompanyCompanyPromotion>();
            CompanyCompanyQuirks = new HashSet<CompanyCompanyQuirk>();
            CompanyCompanyUpsells = new HashSet<CompanyCompanyUpsell>();
            CompanyGroupCompanies = new HashSet<CompanyGroupCompany>();
        }

        public int CompanyId { get; set; }
        public int? CompanyCategoryId { get; set; }
        public string? CompanyName { get; set; }
        public string? DisplayAs { get; set; }
        public int? CompanyStatusId { get; set; }
        public DateTime? CompanyStatusDate { get; set; }
        public int? CompanyStatusReasonId { get; set; }
        public string? CompanyStatusReasonNote { get; set; }
        public int? SalespersonId { get; set; }
        public int? G3codeId { get; set; }
        public int? ParentCompanyId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public bool CoreCustomerInd { get; set; }
        public bool TopLevelReportingInd { get; set; }
        public string? CompanyRefId { get; set; }
        public string? CorporateOfficeId { get; set; }
        public int TemplateIdVhtgatewayWelcomeEmail { get; set; }
        public int TemplateIdVhtgatewayPasswordResetEmail { get; set; }
        public int TemplateIdCvappointmentEmail { get; set; }
        public int TemplateIdConfirmationEmail { get; set; }
        public int TemplateIdStudioCompleteEmail { get; set; }
        public int TemplateIdUnpaidBalanceEmail { get; set; }
        public int TemplateIdIncorrectCreditCardEmail { get; set; }
        public int TemplateIdVhtsummaryEmail { get; set; }
        public int TemplateIdBasicInfoEmail { get; set; }
        public Guid Rowguid { get; set; }
        public bool IsStyleInherited { get; set; }
        public int ViewerStyleId { get; set; }
        public int BrochureStyleId { get; set; }
        public int? RepSalesLocal { get; set; }
        public int? RepSalesRegional { get; set; }
        public int? RepSalesNational { get; set; }
        public int? RepCustomerService { get; set; }
        public int NumOffices { get; set; }
        public DateTime NumOfficesLastUpdatedDate { get; set; }
        public string NumOfficesLastUpdatedBy { get; set; } = null!;
        public int NumAgents { get; set; }
        public DateTime NumAgentsLastUpdatedDate { get; set; }
        public string NumAgentsLastUpdatedBy { get; set; } = null!;
        public string LrebrokerId { get; set; } = null!;
        public int NumListingsPerYear { get; set; }
        public DateTime NumListingsPerYearLastUpdatedDate { get; set; }
        public string NumListingsPerYearLastUpdatedBy { get; set; } = null!;
        public bool HidePricingInCvinterface { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public int? CompanyTypeId { get; set; }
        public int? MmchannelId { get; set; }
        public int? SchedulingProgramId { get; set; }
        public string? SchedulingInstructions { get; set; }
        public string? OrderConfirmationInstructions { get; set; }
        public string? OrderCompleteInstructions { get; set; }
        public bool EnableXpressdocs { get; set; }
        public bool HasUploadedToMm { get; set; }
        public string BrokerLogoFileName { get; set; } = null!;
        public int? AssignedCvid { get; set; }
        public int? CommissionProgramId { get; set; }
        public DateTime? CvcommissionVerifiedDate { get; set; }
        public string? CvcommissionVerifiedBy { get; set; }
        public DateTime? CvcommissionStartDate { get; set; }
        public DateTime? CvcommissionEndDate { get; set; }
        public int? CvpresentationAtendees { get; set; }
        public string? CvweeklyMeetDayTime { get; set; }
        public DateTime? LastPresentationDate { get; set; }
        public string? MyVhtcustomCsstemplate { get; set; }
        public string? SingleSignOnReturnUrl { get; set; }
        public bool? AutoCreateVideo { get; set; }
        public int? SalesTerritoryId { get; set; }
        public int? RepPhotographer { get; set; }
        public string? RateSheet { get; set; }
        public int? ContactVipflagId { get; set; }
        public int? ContactTlcflagId { get; set; }
        public string? WaterMarkUrl1 { get; set; }
        public bool? UseWaterMark1 { get; set; }
        public bool? WaterMarkPhotosFromFeed1 { get; set; }
        public bool? WaterMarkPhotosFromMyVht1 { get; set; }
        public bool? WaterMark3rdPartyPhotos1 { get; set; }
        public byte? WaterMarkSizePercentage1 { get; set; }
        public bool? PositionLowerLeft1 { get; set; }
        public bool? PositionLowerRight1 { get; set; }
        public bool? PositionUpperRight1 { get; set; }
        public bool? PositionUpperLeft1 { get; set; }
        public string? WaterMarkUrl2 { get; set; }
        public bool? UseWaterMark2 { get; set; }
        public bool? WaterMarkPhotosFromFeed2 { get; set; }
        public bool? WaterMarkPhotosFromMyVht2 { get; set; }
        public bool? WaterMark3rdPartyPhotos2 { get; set; }
        public byte? WaterMarkSizePercentage2 { get; set; }
        public bool? PositionLowerLeft2 { get; set; }
        public bool? PositionLowerRight2 { get; set; }
        public bool? PositionUpperRight2 { get; set; }
        public bool? PositionUpperLeft2 { get; set; }
        public string? WaterMarkUrl1Oasis { get; set; }
        public bool? UseWaterMark1Oasis { get; set; }
        public bool? WaterMarkPhotosFromFeed1Oasis { get; set; }
        public bool? WaterMarkPhotosFromMyVht1Oasis { get; set; }
        public bool? WaterMark3rdPartyPhotos1Oasis { get; set; }
        public byte? WaterMarkSizePercentage1Oasis { get; set; }
        public bool? PositionLowerLeft1Oasis { get; set; }
        public bool? PositionLowerRight1Oasis { get; set; }
        public bool? PositionUpperRight1Oasis { get; set; }
        public string? WaterMarkUrl2Oasis { get; set; }
        public bool? UseWaterMark2Oasis { get; set; }
        public bool? WaterMarkPhotosFromFeed2Oasis { get; set; }
        public bool? WaterMarkPhotosFromMyVht2Oasis { get; set; }
        public bool? WaterMark3rdPartyPhotos2Oasis { get; set; }
        public byte? WaterMarkSizePercentage2Oasis { get; set; }
        public bool? PositionLowerLeft2Oasis { get; set; }
        public bool? PositionLowerRight2Oasis { get; set; }
        public bool? PositionUpperRight2Oasis { get; set; }
        public bool? PositionUpperLeft1Oasis { get; set; }
        public bool? PositionUpperLeft2Oasis { get; set; }
        public bool? WatermarkNonBrandedMyVht1 { get; set; }
        public bool? WatermarkNonBrandedMyVht2 { get; set; }
        public bool? WatermarkNonBrandedOasis1 { get; set; }
        public bool? WatermarkNonBrandedOasis2 { get; set; }
        public bool? IsMarketDominator { get; set; }
        public bool? OptOutLegDistWaterMark { get; set; }
        public bool? IsMyVhtFullServicePlus { get; set; }
        public bool? HideNonbrandedLinksMyVht { get; set; }
        public bool? HideNonbrandedLinksOasis { get; set; }
        public bool? HideNonbrandedLinksMyVhtLinks { get; set; }
        public bool? OptOutGlobalSettings { get; set; }
        public string? Territory { get; set; }
        public bool? OptOutPrintVht1 { get; set; }
        public bool? OptOutPrintVht2 { get; set; }
        public bool? OptOutPrintOasis1 { get; set; }
        public bool? OptOutPrintOasis2 { get; set; }
        public bool? DisconnectFromParentSettings { get; set; }
        public bool DisableVideoSyndication { get; set; }
        public bool? EnableCmykDownloadOasis { get; set; }
        public int? CompanysubsidiaryId { get; set; }
        public int? MmscompanyId { get; set; }
        public int? MmsofficeId { get; set; }
        public bool DetachParentReceiptEmailSettings { get; set; }
        public bool EnableReceiptEmailAutomation { get; set; }
        public bool EnableProofing { get; set; }
        public string? MarketingApprovedCompanyNameC { get; set; }
        public int? RepSalesAdm { get; set; }
        public int? RepSalesAde { get; set; }
        public bool? IsDefaultListingIndent2Vhtid { get; set; }
        public int? VhtrelationshipId { get; set; }
        public int? TerritoryId { get; set; }
        public int? OfficeTypeId { get; set; }
        public int? RepSalesNcs { get; set; }
        public int? RepSalesSe { get; set; }
        public bool? Ispix { get; set; }
        public string? MarketingApprovedCompanyName { get; set; }
        public bool? CcnotRequiredForCustPay { get; set; }
        public bool? IsAvaEnabled { get; set; }

        public virtual SchedulingProgram? SchedulingProgram { get; set; }
        public virtual ICollection<BrokerageIdmatch> BrokerageIdmatches { get; set; }
        public virtual ICollection<BrokerageIp> BrokerageIps { get; set; }
        public virtual ICollection<CompanyCompanyPromotion> CompanyCompanyPromotions { get; set; }
        public virtual ICollection<CompanyCompanyQuirk> CompanyCompanyQuirks { get; set; }
        public virtual ICollection<CompanyCompanyUpsell> CompanyCompanyUpsells { get; set; }
        public virtual ICollection<CompanyGroupCompany> CompanyGroupCompanies { get; set; }
    }
}
