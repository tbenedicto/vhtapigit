﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VhtCopyrightedImagesThruQ420
    {
        public string? Filename { get; set; }
        public string? Dvd { get; set; }
        public double? ListingId { get; set; }
        public double? RoomCodeId { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public double? PropertyZip { get; set; }
        public string? Spreadsheet { get; set; }
        public double? PrimMlsid { get; set; }
        public string? Mlsid { get; set; }
        public string? MlsName { get; set; }
        public string? MlsidSecond { get; set; }
        public string? MlsNameSecond { get; set; }
    }
}
