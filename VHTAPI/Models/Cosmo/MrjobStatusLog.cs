﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MrjobStatusLog
    {
        public int JobStatusLogId { get; set; }
        public int MrstatusDaemonId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual MrstatusDaemon MrstatusDaemon { get; set; } = null!;
    }
}
