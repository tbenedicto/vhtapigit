﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProofingOrderSummary
    {
        public int ProofingOrderSummaryId { get; set; }
        public int ContactId { get; set; }
        public int ListingId { get; set; }
        public DateTime DateSubmitted { get; set; }
        public DateTime? EmailSentOn { get; set; }
    }
}
