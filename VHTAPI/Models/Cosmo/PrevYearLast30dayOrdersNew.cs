﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PrevYearLast30dayOrdersNew
    {
        public DateTime? OrderDate { get; set; }
        public int? OfOrders { get; set; }
        public decimal? Revenue { get; set; }
        public int? PcOfOrders { get; set; }
        public decimal? PcRevenues { get; set; }
        public int? FloorPlanOfOrders { get; set; }
        public decimal? FloorPlanRevenue { get; set; }
        public int? SumOfFloorPlan { get; set; }
        public int? PremOfOrders { get; set; }
        public decimal? PremRevenue { get; set; }
        public int? SumOfPrem { get; set; }
        public DateTime? LoadDate { get; set; }
    }
}
