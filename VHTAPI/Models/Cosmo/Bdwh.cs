﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Bdwh
    {
        public int? Month { get; set; }
        public int? Year { get; set; }
        public double? BussinessDayOfTheMonth { get; set; }
        public int? HolidaysThisMonth { get; set; }
    }
}
