﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CorrespondenceTemplateMap
    {
        public int CorrespondenceTemplateMapId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public int CorrespondenceTemplateId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual CorrespondenceTemplate CorrespondenceTemplate { get; set; } = null!;
    }
}
