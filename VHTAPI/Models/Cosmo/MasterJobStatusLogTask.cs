﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MasterJobStatusLogTask
    {
        public int MasterJobStatusLogTaskId { get; set; }
        public int JobStatusId { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public bool AllowNotComplete { get; set; }
        public string? CompletedBy { get; set; }
        public DateTime? CompletedDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
