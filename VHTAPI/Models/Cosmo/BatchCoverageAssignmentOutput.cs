﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BatchCoverageAssignmentOutput
    {
        public long? CountJobid { get; set; }
        public int? Jobid { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZipCode { get; set; }
        public string Coverage { get; set; } = null!;
        public double? Distance { get; set; }
        public int? PhotographerId { get; set; }
        public string? PhotographerName { get; set; }
        public string? PhotographerType { get; set; }
    }
}
