﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingDistributionEventType
    {
        public ListingDistributionEventType()
        {
            ListingDistributionEvents = new HashSet<ListingDistributionEvent>();
        }

        public int EventTypeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<ListingDistributionEvent> ListingDistributionEvents { get; set; }
    }
}
