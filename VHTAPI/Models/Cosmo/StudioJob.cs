﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class StudioJob
    {
        public int StudioJobId { get; set; }
        public string Source { get; set; } = null!;
        public int ListingId { get; set; }
        public Guid JobId { get; set; }
        public string? AddedByUser { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
