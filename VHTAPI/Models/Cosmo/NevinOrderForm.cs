﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NevinOrderForm
    {
        public string? Company { get; set; }
        public int? Id { get; set; }
    }
}
