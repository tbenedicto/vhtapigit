﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzContentType
    {
        public int ContentTypeId { get; set; }
        public string ContentTypeDescr { get; set; } = null!;
        public int? ContentClassId { get; set; }
        public string Dir { get; set; } = null!;
        public string SearchDisplay { get; set; } = null!;
        public string? UploadProcessingScript { get; set; }
        public int Priority { get; set; }
        public int? SortId { get; set; }
        public bool? AllowsViewersInd { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public byte[]? TimpStampField { get; set; }
        public int? Xref01 { get; set; }
        public string? Xref02 { get; set; }
        public string? Xref03 { get; set; }
        public Guid Rowguid { get; set; }
        public string? CustomerDescr { get; set; }
    }
}
