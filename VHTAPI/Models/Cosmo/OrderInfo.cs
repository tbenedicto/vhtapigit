﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderInfo
    {
        public int OrderInfoId { get; set; }
        public int JobId { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string CompanyName { get; set; } = null!;
        public string OfficeName { get; set; } = null!;
        public string OfficeAddress { get; set; } = null!;
        public string OfficeCity { get; set; } = null!;
        public string OfficeState { get; set; } = null!;
        public string OfficeZipCode { get; set; } = null!;
        public int PhoneType { get; set; }
        public string Phone { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
