﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobFlag
    {
        public int JobFlagId { get; set; }
        public int JobId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public bool FlaggedInd { get; set; }
        public DateTime? FollowupDate { get; set; }
        public string FollowupNote { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual Job Job { get; set; } = null!;
    }
}
