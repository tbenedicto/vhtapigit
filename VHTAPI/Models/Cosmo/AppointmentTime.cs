﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AppointmentTime
    {
        public int AppointmentTimeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
