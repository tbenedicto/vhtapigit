﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzManualReplication
    {
        public int ManualReplicationId { get; set; }
        public string? TableName { get; set; }
        public string? TableKey { get; set; }
        public string? Comment { get; set; }
        public bool? ManualReplStatus { get; set; }
    }
}
