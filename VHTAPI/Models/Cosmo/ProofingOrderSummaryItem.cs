﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProofingOrderSummaryItem
    {
        public int ProofingOrderSummaryItemId { get; set; }
        public int ProofingOrderSummaryId { get; set; }
        public int ImageId { get; set; }
        public decimal Price { get; set; }
        public string? ThumbUrl { get; set; }
        public string? ImageCaption { get; set; }
    }
}
