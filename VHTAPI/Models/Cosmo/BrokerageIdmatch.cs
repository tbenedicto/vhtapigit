﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BrokerageIdmatch
    {
        public int ContactId { get; set; }
        public int BrokerageCompanyId { get; set; }
        public string AgentKey { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual Company BrokerageCompany { get; set; } = null!;
        public virtual Contact Contact { get; set; } = null!;
    }
}
