﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MediaExportListing
    {
        public int ListingId { get; set; }
        public string? MlsId { get; set; }
        public string? CompanyName { get; set; }
        public string? ListingAddress { get; set; }
        public int ContactId { get; set; }
        public int? MmsloginId { get; set; }
        public string? ContactEmail { get; set; }
        public int MediaTypeId { get; set; }
        public string? ImageUrl { get; set; }
        public bool DeleteExistingImgs { get; set; }
        public DateTime? AddedDate { get; set; }
        public int SortId { get; set; }
        public bool AddScreenPlay { get; set; }
        public bool CreateListingIfNotExists { get; set; }
        public bool? AddVirtualTourUrl { get; set; }
        public string? VirtualTourUrl { get; set; }
        public string? ContactEmailList { get; set; }
    }
}
