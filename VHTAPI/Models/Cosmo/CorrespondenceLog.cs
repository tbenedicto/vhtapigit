﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CorrespondenceLog
    {
        public int RowId { get; set; }
        public int TemplateId { get; set; }
        public int RevisionId { get; set; }
        public string? ParamsXml { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public int ContactId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
