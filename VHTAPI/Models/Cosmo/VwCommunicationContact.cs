﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCommunicationContact
    {
        public int CommunicationId { get; set; }
        public string? FkparentSource { get; set; }
        public int? FkparentId { get; set; }
        public int CommunicationCategoryId { get; set; }
        public int? RelatedCommunicationId { get; set; }
        public string? TextValue { get; set; }
        public string? TextValueExtended { get; set; }
        public bool? PropogateToContactInd { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
