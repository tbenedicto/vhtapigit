﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProductRegion
    {
        public int ProductRegionId { get; set; }
        public string Descr { get; set; } = null!;
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
        public string? StateId { get; set; }
    }
}
