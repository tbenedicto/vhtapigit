﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Listing
    {
        public Listing()
        {
            ListingDistributionEvents = new HashSet<ListingDistributionEvent>();
        }

        public int ListingId { get; set; }
        public int ContentServerId { get; set; }
        public int G3codeId { get; set; }
        public int? PropertyListPrice { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Development { get; set; }
        public string? DrivingDirections { get; set; }
        public int? AgentId { get; set; }
        public string? RelistString { get; set; }
        public string? County { get; set; }
        public string? HomeownerName { get; set; }
        public string? HomeownerEmail { get; set; }
        public string? Beds { get; set; }
        public string? Baths { get; set; }
        public bool? GoodListingInd { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public int ListingTypeId { get; set; }
        public int AddressStyleId { get; set; }
        public Guid Rowguid { get; set; }
        public byte[] RowTimeStamp { get; set; } = null!;
        public int ListingPriorityId { get; set; }
        public int ListingStatusId { get; set; }
        public bool ShowInViewMyListings { get; set; }
        public bool FlagForOpenHouse { get; set; }
        public bool IsStyleInherited { get; set; }
        public int ViewerStyleId { get; set; }
        public int BrochureStyleId { get; set; }
        public string? RdcorderId { get; set; }
        public bool IsContentPartial { get; set; }
        public bool HasUploadedToMm { get; set; }
        public string? BrokerPropertyDetailPageUrl { get; set; }
        public DateTime? DateLastSeenInFeed { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? HalfBaths { get; set; }
        public string? GoogleMapListingUrl { get; set; }
        public int? RefMmslistingId { get; set; }
        public int? ContactIdAgent2 { get; set; }
        public int? ContactIdAgent3 { get; set; }
        public int? ContactIdAgent4 { get; set; }
        public string? LockBox { get; set; }
        public int ListingCategoryId { get; set; }
        public DateTime? ListingDate { get; set; }
        public int ListingDataStatusId { get; set; }
        public DateTime? DateLastUpdatedByLdfeed { get; set; }

        public virtual ICollection<ListingDistributionEvent> ListingDistributionEvents { get; set; }
    }
}
