﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablOrdersByAddedDate
    {
        public int Listingid { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public DateTime? JobAddedDate { get; set; }
        public string? ParentCompany { get; set; }
        public string? CompanyName { get; set; }
        public string? Territory { get; set; }
        public string? Region { get; set; }
        public string? AgentName { get; set; }
        public string Type { get; set; } = null!;
        public string? Date { get; set; }
    }
}
