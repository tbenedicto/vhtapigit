﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwRet
    {
        public int ListingId { get; set; }
        public string? Beds { get; set; }
        public string? Baths { get; set; }
        public int? PropertyListPrice { get; set; }
        public decimal? NumBeds { get; set; }
        public decimal? NumBaths { get; set; }
        public decimal? ListPrice { get; set; }
    }
}
