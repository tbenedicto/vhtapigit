﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DistributionTarget
    {
        public DistributionTarget()
        {
            DistributionAssignments = new HashSet<DistributionAssignment>();
            Timeslots = new HashSet<Timeslot>();
        }

        public int DistributionTargetId { get; set; }
        public string TargetName { get; set; } = null!;
        public int TimeFrameId { get; set; }
        public bool IsReassignable { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public byte[] Tsvalue { get; set; } = null!;

        public virtual DistributionTargetTimeFrame TimeFrame { get; set; } = null!;
        public virtual ICollection<DistributionAssignment> DistributionAssignments { get; set; }
        public virtual ICollection<Timeslot> Timeslots { get; set; }
    }
}
