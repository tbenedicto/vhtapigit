﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AuditRoofstockLoadListing
    {
        public int MasterListingId { get; set; }
        public int? MasterJobid { get; set; }
        public string? Clientid { get; set; }
        public DateTime? LoadDateTimedatetime { get; set; }
    }
}
