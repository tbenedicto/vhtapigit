﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobMileageHistory
    {
        public int JobMileageHistoryId { get; set; }
        public int JobId { get; set; }
        public int MileageQuantity { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public string? ApplicationName { get; set; }
        public int? PreviousMileageQuantity { get; set; }
    }
}
