﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobTblCompanyContact
    {
        public int? ParentCompanyId { get; set; }
        public int? CompanyId { get; set; }
        public int ContactStatusId { get; set; }
        public int ContactId { get; set; }
        public int? MmsloginId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Descr { get; set; }
        public string? Identification { get; set; }
        public string? CleanedId { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
    }
}
