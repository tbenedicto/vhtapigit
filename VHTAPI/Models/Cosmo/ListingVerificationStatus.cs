﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingVerificationStatus
    {
        public ListingVerificationStatus()
        {
            OrderImportItems = new HashSet<OrderImportItem>();
        }

        public int ListingVerificationStatusId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<OrderImportItem> OrderImportItems { get; set; }
    }
}
