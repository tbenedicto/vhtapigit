﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllProductRule
    {
        public int ProductRuleId { get; set; }
        public int ProductId { get; set; }
        public int? ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int? Cvid { get; set; }
        public string? StateId { get; set; }
        public int? ContactGroupId { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? CustomerPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? Cvbase { get; set; }
        public decimal? Cvbonus { get; set; }
        public int SortId { get; set; }
        public string? RuleConditionType { get; set; }
        public string? ProductRegion { get; set; }
    }
}
