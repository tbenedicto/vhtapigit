﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DigsMatchedPhotosToVedderImageSource2
    {
        public int SearchedImageId { get; set; }
        public string FoundImageUrl { get; set; } = null!;
        public int? NumbInMatch { get; set; }
    }
}
