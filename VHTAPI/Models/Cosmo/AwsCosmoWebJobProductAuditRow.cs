﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebJobProductAuditRow
    {
        public int RowId { get; set; }
        public int JobProductAuditHeaderId { get; set; }
        public int JobProductId { get; set; }
        public int JobId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal AgentPay { get; set; }
        public decimal CorporatePay { get; set; }
        public decimal Cvpay { get; set; }
        public decimal Cvupsell { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public int CvupsellQuantity { get; set; }
        public Guid Rowguid { get; set; }
    }
}
