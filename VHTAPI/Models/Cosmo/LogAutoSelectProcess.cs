﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LogAutoSelectProcess
    {
        public int LogId { get; set; }
        public decimal BatchId { get; set; }
        public int ListingId { get; set; }
        public int JobId { get; set; }
        public int ContactId { get; set; }
        public int CompanyId { get; set; }
        public int ParentCompanyId { get; set; }
        public int AutoSelectRuleId { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Processed { get; set; }
        public DateTime? DateLastProcessed { get; set; }
        public string? ProcessMessage { get; set; }
    }
}
