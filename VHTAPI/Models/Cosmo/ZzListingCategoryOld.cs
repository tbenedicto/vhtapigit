﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzListingCategoryOld
    {
        public int ListingCategoryId { get; set; }
        public int CustomerId { get; set; }
        public string TourCategoryDescr { get; set; } = null!;
        public int SortId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public byte[]? TimpStampField { get; set; }
        public int? Xref01 { get; set; }
        public string? Xref02 { get; set; }
        public string? Xref03 { get; set; }
        public Guid Rowguid { get; set; }
    }
}
