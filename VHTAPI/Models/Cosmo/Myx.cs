﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Myx
    {
        public double? ProductId { get; set; }
        public string? MlsId { get; set; }
        public string MlsName { get; set; } = null!;
        public int? ListingDate { get; set; }
        public int? ListingPrice { get; set; }
        public int? ListingType { get; set; }
        public string? Line1 { get; set; }
        public int? Line2 { get; set; }
        public string? City { get; set; }
        public string? StateCode { get; set; }
        public double? Zip { get; set; }
        public int? Bedrooms { get; set; }
        public int? FullBaths { get; set; }
        public int? HalfBaths { get; set; }
        public int ListingAgent { get; set; }
        public int? Description { get; set; }
        public string? NotesToPhotographer { get; set; }
    }
}
