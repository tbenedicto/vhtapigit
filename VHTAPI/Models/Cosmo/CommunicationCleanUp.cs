﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CommunicationCleanUp
    {
        public int PhoneId { get; set; }
        public string OriginalValue { get; set; } = null!;
        public bool Formatted { get; set; }
        public string? NationalFormat { get; set; }
        public string? InternationalFormat { get; set; }
        public string? CarrierName { get; set; }
        public string? CarrierType { get; set; }
        public string? Verdict { get; set; }
    }
}
