﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablJobsWCv
    {
        public int JobId { get; set; }
        public string? VhtjobId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? TerritoryDescr { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? DisplayAs { get; set; }
        public string? InternalName { get; set; }
    }
}
