﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyKeyword
    {
        public int CompanyKeywordId { get; set; }
        public int ParentCompanyId { get; set; }
        public int KeywordTypeId { get; set; }
        public string? Keyword { get; set; }
        public int? KeywordConditionTypeId { get; set; }
        public string? ConditionValue { get; set; }
        public int? KeywordConditionValueDataTypeId { get; set; }
        public int? SortId { get; set; }
        public string? TextToSearchFor { get; set; }
        public string? TextToReplaceWith { get; set; }
        public string? Notes { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public string? PrependText { get; set; }
        public string? AppendText { get; set; }
        public Guid Rowguid { get; set; }
    }
}
