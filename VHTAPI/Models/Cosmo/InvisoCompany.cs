﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoCompany
    {
        public string? ChainId { get; set; }
        public string? ChainName { get; set; }
        public string? RegionId { get; set; }
        public string? RegionName { get; set; }
        public string? CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public string? LegalName { get; set; }
        public string? CustomerAddressStreet { get; set; }
        public string? CustomerAddressPostalArea { get; set; }
        public string? CustomerAddressZipCode { get; set; }
        public string? PersonId { get; set; }
        public string? PersonName { get; set; }
        public string? Email { get; set; }
        public int? CosmoParentId { get; set; }
        public int? CosmoCompanyId { get; set; }
        public int? CosmoContactid { get; set; }
        public bool? Ignore { get; set; }
        public string? FirstOrderId { get; set; }
        public string? FirstOrderCreatedAt { get; set; }
        public DateTime? Loaddate { get; set; }
    }
}
