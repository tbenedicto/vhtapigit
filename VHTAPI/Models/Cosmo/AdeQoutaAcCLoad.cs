﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdeQoutaAcCLoad
    {
        public decimal? AcceleratorC { get; set; }
        public string? AdeC { get; set; }
        public string? ConnectionReceivedId { get; set; }
        public string? ConnectionSentId { get; set; }
        public string? CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Id { get; set; } = null!;
        public string IsDeleted { get; set; } = null!;
        public string? LastModifiedById { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public DateTime? LastReferencedDate { get; set; }
        public DateTime? LastViewedDate { get; set; }
        public decimal? MonthC { get; set; }
        public string? Name { get; set; }
        public string OwnerId { get; set; } = null!;
        public decimal? QuotaC { get; set; }
        public DateTime SystemModstamp { get; set; }
    }
}
