﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DigsMatchedPhotosToVedderDvdSourceImage
    {
        public string? Dvd { get; set; }
        public string SourceImageUrl { get; set; } = null!;
        public string FoundImageUrl { get; set; } = null!;
        public int? NumbInMatch { get; set; }
    }
}
