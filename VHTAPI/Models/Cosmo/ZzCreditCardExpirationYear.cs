﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCreditCardExpirationYear
    {
        public int CreditCardExpirationYearId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
