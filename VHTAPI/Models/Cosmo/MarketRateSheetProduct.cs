﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MarketRateSheetProduct
    {
        public int MarketRateSheetProductId { get; set; }
        public int CategoryId { get; set; }
        public int ProductId { get; set; }
        public int RateSheet { get; set; }
        public int? SortId { get; set; }
        public string? MarketingProdName { get; set; }
        public int? MarketRateSheetPackageId { get; set; }
        public bool UseRateCustomText { get; set; }
        public string? RateCustomText { get; set; }
        public bool UseAsAdditionalInfo { get; set; }
    }
}
