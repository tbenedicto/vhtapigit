﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LicompanyRule
    {
        public LicompanyRule()
        {
            LicompanyRuleDetails = new HashSet<LicompanyRuleDetail>();
        }

        public int ParentCompanyId { get; set; }
        public bool OverridesStateRule { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<LicompanyRuleDetail> LicompanyRuleDetails { get; set; }
    }
}
