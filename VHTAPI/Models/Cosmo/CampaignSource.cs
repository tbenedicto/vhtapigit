﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CampaignSource
    {
        public int CampaignSourceId { get; set; }
        public string Description { get; set; } = null!;
    }
}
