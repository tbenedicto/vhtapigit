﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CubicasaProductGroup
    {
        public int CubicasaProductGroupId { get; set; }
        public int ProductId { get; set; }
        public int ProductGroupId { get; set; }
        public int? SizeDownTo { get; set; }
        public int? SizeUpTo { get; set; }
    }
}
