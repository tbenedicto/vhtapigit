﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CommissionProgramAuditHistory
    {
        public int CommissionProgramAuditHistoryId { get; set; }
        public int CompanyId { get; set; }
        public int AssignedCvid { get; set; }
        public int CommissionProgramId { get; set; }
        public DateTime CvcommissionVerifiedDate { get; set; }
        public string CvcommissionVerifiedBy { get; set; } = null!;
        public DateTime CvcommissionStartDate { get; set; }
        public DateTime CvcommissionEndDate { get; set; }
        public int? CvpresentationAtendees { get; set; }
        public string? CvweeklyMeetDayTime { get; set; }
        public Guid Rowguid { get; set; }
    }
}
