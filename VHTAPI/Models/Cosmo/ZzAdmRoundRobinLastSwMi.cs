﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzAdmRoundRobinLastSwMi
    {
        public int RoundRobinId { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
