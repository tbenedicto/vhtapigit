﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactCpixPhotographersLoaded
    {
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? CompanyNameDba { get; set; }
        public decimal? PhotographersPaid2017 { get; set; }
        public string? NewContract { get; set; }
        public string? OldContract { get; set; }
        public string? Copyright { get; set; }
        public string? MissingContract { get; set; }
        public string? PhotogNumber { get; set; }
        public string? HaloId { get; set; }
        public double? PayrollId { get; set; }
        public string? HireDate { get; set; }
        public string? LastJobShotDate { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public string? Phone { get; set; }
        public string? Cell { get; set; }
        public string? Email { get; set; }
        public string? Email2 { get; set; }
        public string? Text { get; set; }
        public string? F23 { get; set; }
        public int? Contactid { get; set; }
        public string? ContactCode { get; set; }
    }
}
