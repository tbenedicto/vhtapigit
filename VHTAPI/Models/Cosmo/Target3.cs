﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Target3
    {
        public double? Month { get; set; }
        public string? Description { get; set; }
        public double? Target { get; set; }
        public string? F4 { get; set; }
        public string? F5 { get; set; }
        public string? F6 { get; set; }
        public string? F7 { get; set; }
        public string? F8 { get; set; }
        public string? F9 { get; set; }
        public string? F10 { get; set; }
        public string? F11 { get; set; }
        public string? F12 { get; set; }
        public string? F13 { get; set; }
        public string? F14 { get; set; }
        public string? F15 { get; set; }
        public string? F16 { get; set; }
    }
}
