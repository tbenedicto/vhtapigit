﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MeasureCompZip
    {
        public string Zip { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
