﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PrismClients21
    {
        public string? Name { get; set; }
        public string? MlsLogIn { get; set; }
        public string? MlsPassword { get; set; }
        public string? Agency { get; set; }
        public string? Email { get; set; }
        public string? SecondEmail { get; set; }
        public string? ThirdEmail { get; set; }
        public string? FourthEmail { get; set; }
        public string? Mobile { get; set; }
        public string? Phone { get; set; }
        public string? Fax { get; set; }
        public string? SpecialRequirements { get; set; }
        public string? CommunityPurchased { get; set; }
        public string? SmSites { get; set; }
        public string? TourPlatform { get; set; }
        public string? CardCvv1 { get; set; }
        public string? CardExp1 { get; set; }
        public string? TourLogIn { get; set; }
        public string? TourPassword { get; set; }
        public string? CreditCardOnFile { get; set; }
        public double? OfProjects { get; set; }
        public string? AddProject { get; set; }
        public double? CardCvc { get; set; }
        public DateTime? CardExp { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public double? IdInQuickBooks { get; set; }
        public string? LastModifiedBy { get; set; }
        public string? Projects { get; set; }
        public double? RecordId { get; set; }
        public string? RecordOwner { get; set; }
        public string? SmLogIn { get; set; }
        public string? SmPassword { get; set; }
        public string? Title { get; set; }
        public string? YoutubeLogIn { get; set; }
        public string? YoutubePassword { get; set; }
    }
}
