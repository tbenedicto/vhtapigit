﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwTablCancellationReport
    {
        public int JobId { get; set; }
        public int ListingId { get; set; }
        public int JobStatusId { get; set; }
        public DateTime JobStatusDate { get; set; }
        public int JobStatusReasonId { get; set; }
        public string JobStatusNote { get; set; } = null!;
        public int JobPriorityId { get; set; }
        public string? VhtjobId { get; set; }
        public int? VideoEditorId { get; set; }
        public int? VideographerContactId { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public int JobSourceId { get; set; }
        public int? JobCancellationReasonId { get; set; }
        public int Expr1 { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public int Expr3 { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public DateTime? Expr4 { get; set; }
        public string? Expr5 { get; set; }
        public DateTime? Expr6 { get; set; }
        public string? Expr7 { get; set; }
        public DateTime? Expr8 { get; set; }
        public string? Expr9 { get; set; }
        public int ListingTypeId { get; set; }
        public int AddressStyleId { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? GoogleMapListingUrl { get; set; }
        public int? RefMmslistingId { get; set; }
        public int? ContactIdAgent2 { get; set; }
        public int? ContactIdAgent3 { get; set; }
        public int? ContactIdAgent4 { get; set; }
        public int Expr118 { get; set; }
        public int? Expr119 { get; set; }
        public int Expr120 { get; set; }
        public int Expr122 { get; set; }
        public string? Expr123 { get; set; }
        public string? Expr126 { get; set; }
        public string? Expr148 { get; set; }
        public string? Expr149 { get; set; }
        public int? Expr22 { get; set; }
        public int? Expr23 { get; set; }
        public bool Expr228 { get; set; }
        public bool Expr229 { get; set; }
        public bool Expr230 { get; set; }
        public int Expr24 { get; set; }
        public int? CompanyCategoryId { get; set; }
        public string? CompanyName { get; set; }
        public string? Expr25 { get; set; }
        public int? CompanyStatusId { get; set; }
        public DateTime? CompanyStatusDate { get; set; }
        public int? CompanyStatusReasonId { get; set; }
        public string? CompanyStatusReasonNote { get; set; }
        public int? Expr26 { get; set; }
        public int? Expr27 { get; set; }
        public int? ParentCompanyId { get; set; }
        public DateTime? Expr28 { get; set; }
        public string? Expr29 { get; set; }
        public DateTime? Expr30 { get; set; }
        public string? Expr31 { get; set; }
        public DateTime? Expr32 { get; set; }
        public string? Expr33 { get; set; }
        public bool CoreCustomerInd { get; set; }
        public bool TopLevelReportingInd { get; set; }
        public string? CompanyRefId { get; set; }
        public string? CorporateOfficeId { get; set; }
        public int TemplateIdStudioCompleteEmail { get; set; }
        public int TemplateIdUnpaidBalanceEmail { get; set; }
        public int TemplateIdIncorrectCreditCardEmail { get; set; }
        public int TemplateIdVhtsummaryEmail { get; set; }
        public int TemplateIdBasicInfoEmail { get; set; }
        public Guid Expr34 { get; set; }
        public bool Expr35 { get; set; }
        public int Expr36 { get; set; }
        public int Expr37 { get; set; }
        public int? Expr38 { get; set; }
        public int? Expr39 { get; set; }
        public int? Expr40 { get; set; }
        public int? Expr41 { get; set; }
        public int NumOffices { get; set; }
        public DateTime NumOfficesLastUpdatedDate { get; set; }
        public string NumOfficesLastUpdatedBy { get; set; } = null!;
        public int NumAgents { get; set; }
        public DateTime NumAgentsLastUpdatedDate { get; set; }
        public string NumAgentsLastUpdatedBy { get; set; } = null!;
        public string NumListingsPerYearLastUpdatedBy { get; set; } = null!;
        public int? CompanyTypeId { get; set; }
        public int ProductRegionId { get; set; }
        public string Zip { get; set; } = null!;
        public DateTime? Expr100 { get; set; }
        public string? Expr101 { get; set; }
        public DateTime? Expr102 { get; set; }
        public string? Expr103 { get; set; }
        public DateTime? PcAddedDate { get; set; }
        public string? NrtEmail { get; set; }
        public int ContactSourceId { get; set; }
        public byte[] TimeStampField { get; set; } = null!;
        public string? LastName { get; set; }
        public string? MiddleInitial { get; set; }
        public string? MiddleName { get; set; }
    }
}
