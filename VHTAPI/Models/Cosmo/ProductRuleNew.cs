﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProductRuleNew
    {
        public int ProductRuleId { get; set; }
        public int ProductId { get; set; }
        public int RuleConditionTypeId { get; set; }
        public int? ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int? Cvid { get; set; }
        public int? ProductRegionId { get; set; }
        public string? StateId { get; set; }
        public int? ContactGroupId { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? CustomerPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? Cvbase { get; set; }
        public decimal? Cvbonus { get; set; }
        public string? ScreenshotUrl { get; set; }
        public string? SampleUrl { get; set; }
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
        public int? ProofPhotoCredit { get; set; }
        public decimal? ProofAmountCredit { get; set; }
        public decimal? ProofPercentCredit { get; set; }
        public decimal? CustomerRateAdj { get; set; }
        public decimal? CorporateRateAdj { get; set; }
        public decimal? CvrateAdj { get; set; }

        public virtual Product Product { get; set; } = null!;
        public virtual ZzRuleConditionType RuleConditionType { get; set; } = null!;
    }
}
