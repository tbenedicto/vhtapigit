﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageWorksVideoChannelType
    {
        public int ImageWorksVideoChannelTypeId { get; set; }
        public string? Description { get; set; }
        public Guid Rowguid { get; set; }
    }
}
