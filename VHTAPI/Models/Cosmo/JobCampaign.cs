﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobCampaign
    {
        public int JobId { get; set; }
        public int? CampaignCodeId { get; set; }
        public int? CampaignMediumId { get; set; }
        public int? CampaignSourceId { get; set; }
        public DateTime? DateLoaded { get; set; }
        public Guid? CampaignGuid { get; set; }
        public string? CampaignTerm { get; set; }
        public string? CampaignContent { get; set; }
    }
}
