﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzIssueResolution
    {
        public int IssueResolutionId { get; set; }
        public int IssueResolutionCategoryId { get; set; }
        public int SortId { get; set; }
        public bool IsActive { get; set; }
        public string Descr { get; set; } = null!;
        public bool IsCommentRequired { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ZzIssueResolutionCategory IssueResolutionCategory { get; set; } = null!;
    }
}
