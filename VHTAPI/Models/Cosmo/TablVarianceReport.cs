﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablVarianceReport
    {
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompanyName { get; set; }
        public string? AccountType { get; set; }
        public string LocalSalesRep { get; set; } = null!;
        public string? AreaSalesRep { get; set; }
        public string? AccountManager { get; set; }
        public string Csrep { get; set; } = null!;
        public string? Market { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public int? TheYear { get; set; }
        public int? TheMonth { get; set; }
        public int? NumAgents { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public double? TotalRevenue { get; set; }
        public string? Territory { get; set; }
        public string CompanyType { get; set; } = null!;
    }
}
