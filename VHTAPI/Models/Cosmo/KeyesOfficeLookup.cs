﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class KeyesOfficeLookup
    {
        public string? ClientOfficeId { get; set; }
        public double? VhtOfficeId { get; set; }
    }
}
