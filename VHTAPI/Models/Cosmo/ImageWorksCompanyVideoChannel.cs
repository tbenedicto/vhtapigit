﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageWorksCompanyVideoChannel
    {
        public int VideoChannelId { get; set; }
        public int ParentCompanyId { get; set; }
        public int ImageWorksVideoChannelTypeId { get; set; }
        public string VideoChannelName { get; set; } = null!;
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
