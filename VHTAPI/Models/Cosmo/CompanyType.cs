﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyType
    {
        public int CompanyTypeId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public Guid Rowguid4 { get; set; }
    }
}
