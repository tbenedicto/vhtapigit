﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LogListingMove
    {
        public int RowId { get; set; }
        public int ListingId { get; set; }
        public int? FromContentServerId { get; set; }
        public int? ToContentServerId { get; set; }
        public DateTime AddedDate { get; set; }
        public bool? Moved { get; set; }
        public string? StatusMessage { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
