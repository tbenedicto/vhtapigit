﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RegionMarketSheet
    {
        public int RegionId { get; set; }
        public int? RateSheetId { get; set; }
    }
}
