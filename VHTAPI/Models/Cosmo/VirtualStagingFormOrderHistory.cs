﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VirtualStagingFormOrderHistory
    {
        public Guid OrderHistoryId { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string PropertyIdaddress { get; set; } = null!;
        public string? MessagesNotes { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
