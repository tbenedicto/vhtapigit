﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwG3RateSheet
    {
        public string G3code { get; set; } = null!;
        public string OrderFormUrl { get; set; } = null!;
        public string? Ratesheeturl { get; set; }
        public int Companyid { get; set; }
        public string? Displayas { get; set; }
    }
}
