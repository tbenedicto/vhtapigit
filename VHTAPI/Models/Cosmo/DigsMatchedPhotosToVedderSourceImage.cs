﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DigsMatchedPhotosToVedderSourceImage
    {
        public string SourceImageUrl { get; set; } = null!;
        public string FoundImageUrl { get; set; } = null!;
        public int? NumbInMatch { get; set; }
    }
}
