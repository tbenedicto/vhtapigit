﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class EsoftJobRoomCode
    {
        public int EsoftJobRoomCodeId { get; set; }
        public int JobId { get; set; }
        public int RoomCodeId { get; set; }
        public bool Proofing { get; set; }
        public bool Signature { get; set; }
        public bool Premium { get; set; }
        public Guid Rowguid { get; set; }
        public bool? Drone { get; set; }
    }
}
