﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyApiWatermark
    {
        public int CompanyId { get; set; }
        public string? WaterMarkUrl1 { get; set; }
        public bool? UseWaterMark1 { get; set; }
        public bool? WaterMarkPhotosFromFeed1 { get; set; }
        public bool? WaterMarkPhotosFromMyVht1 { get; set; }
        public bool? WaterMark3rdPartyPhotos1 { get; set; }
        public bool? OptOutPrint1 { get; set; }
        public byte? WaterMarkSizePercentage1 { get; set; }
        public bool? PositionLowerLeft1 { get; set; }
        public bool? PositionLowerRight1 { get; set; }
        public bool? PositionUpperRight1 { get; set; }
        public bool? PositionUpperLeft1 { get; set; }
        public string? WaterMarkUrl2 { get; set; }
        public bool? UseWaterMark2 { get; set; }
        public bool? WaterMarkPhotosFromFeed2 { get; set; }
        public bool? WaterMarkPhotosFromMyVht2 { get; set; }
        public bool? WaterMark3rdPartyPhotos2 { get; set; }
        public bool? OptOutPrint2 { get; set; }
        public byte? WaterMarkSizePercentage2 { get; set; }
        public bool? PositionLowerLeft2 { get; set; }
        public bool? PositionLowerRight2 { get; set; }
        public bool? PositionUpperRight2 { get; set; }
        public bool? PositionUpperLeft2 { get; set; }
    }
}
