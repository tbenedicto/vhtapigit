﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class FacebookUser
    {
        public long UserId { get; set; }
        public int? ContactId { get; set; }
        public int? CollaboratorId { get; set; }
        public string? Email { get; set; }
        public Guid Rowguid { get; set; }
    }
}
