﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PassAlllastyear
    {
        public decimal? AgentPay { get; set; }
        public decimal? CvPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal CvmileagePaid { get; set; }
        public decimal Cvmisc { get; set; }
        public int? JobId { get; set; }
        public DateTime? StatusCompleteCompletedOn { get; set; }
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? ParentCompanyName { get; set; }
        public string? VideographerLastName { get; set; }
        public string? VideographerFirstName { get; set; }
        public string? AdeAccountRep { get; set; }
        public string? RegionalSales { get; set; }
        public int? AgentContactId { get; set; }
        public string? Videographer { get; set; }
        public DateTime? ParentAddedDate { get; set; }
        public DateTime? ParentStartedDate { get; set; }
        public int? MaxFloor { get; set; }
        public int? MaxPrem { get; set; }
        public string? PreferredVideographerLastName { get; set; }
        public string? PreferredVideographerFirstName { get; set; }
        public string? Territory { get; set; }
        public int? MileageQuantity { get; set; }
        public decimal? MileageRate { get; set; }
        public decimal? Cvmiscellaneous { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
