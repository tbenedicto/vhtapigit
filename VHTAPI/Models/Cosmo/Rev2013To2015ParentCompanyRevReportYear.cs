﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Rev2013To2015ParentCompanyRevReportYear
    {
        public int? Year { get; set; }
        public string? ParentCompany { get; set; }
        public decimal? Sold { get; set; }
    }
}
