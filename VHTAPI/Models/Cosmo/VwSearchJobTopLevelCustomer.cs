﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwSearchJobTopLevelCustomer
    {
        public int JobId { get; set; }
        public int? CompanyId { get; set; }
    }
}
