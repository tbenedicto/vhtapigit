﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VhtGoWithAproRateMb
    {
        public int RateId { get; set; }
        public int? MarketId { get; set; }
        public int? Silver { get; set; }
        public int? SilverPerPhoto { get; set; }
        public int? Silver10 { get; set; }
        public int? Silver15 { get; set; }
        public int? Silver25 { get; set; }
        public int? Gold { get; set; }
        public int? GoldPerPhoto { get; set; }
        public int? Gold10 { get; set; }
        public int? Gold15 { get; set; }
        public int? Gold25 { get; set; }
        public int? Platinum { get; set; }
        public int? PlatinumPerPhoto { get; set; }
        public int? Platinum10 { get; set; }
        public int? Platinum15 { get; set; }
        public int? Platinum25 { get; set; }
        public int? Platinum4 { get; set; }
        public int? Platinum6 { get; set; }
        public int? Platinum8 { get; set; }
    }
}
