﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvzipcodePinCavo
    {
        public string? Zipcode { get; set; }
        public string? ContactCode { get; set; }
        public int? Cvid { get; set; }
    }
}
