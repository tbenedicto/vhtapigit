﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoBilling530FixContactsNotchanged
    {
        public string? Displayas { get; set; }
        public string? Contactcode { get; set; }
        public string? BadAgent { get; set; }
        public string? BadAgentCode { get; set; }
        public int? FkparentId { get; set; }
        public int? CosmoContactid { get; set; }
        public string? VhtjobId { get; set; }
        public int MasterJobid { get; set; }
        public int? InvisoTourId { get; set; }
        public int Masterlistingid { get; set; }
    }
}
