﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoOrderedProduct
    {
        public string? OrderComponentId { get; set; }
        public string? OrderContainerId { get; set; }
        public string? OrderId { get; set; }
        public string? Type { get; set; }
        public string? Quantity { get; set; }
        public string? OrderComponentCreatedAt { get; set; }
        public string? OrderComponentDeliveredAt { get; set; }
        public string? ItemNo { get; set; }
    }
}
