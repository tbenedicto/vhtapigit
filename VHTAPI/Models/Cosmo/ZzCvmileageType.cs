﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCvmileageType
    {
        public ZzCvmileageType()
        {
            GeneralRules = new HashSet<GeneralRule>();
        }

        public int CvmileageTypeId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<GeneralRule> GeneralRules { get; set; }
    }
}
