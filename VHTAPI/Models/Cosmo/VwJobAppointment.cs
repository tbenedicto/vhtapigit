﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwJobAppointment
    {
        public int? JobId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int AppointmentTypeId { get; set; }
        public string AppointmentType { get; set; } = null!;
        public DateTime? ExpiresDateTime { get; set; }
    }
}
