﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AddressAddressUse
    {
        public int AddressAddressUseId { get; set; }
        public int AddressId { get; set; }
        public int AddressUseId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
