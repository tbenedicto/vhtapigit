﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebTourRoomCode
    {
        public int TourRoomCodeId { get; set; }
        public int TourId { get; set; }
        public int RoomCodeId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public byte[]? TimpStampField { get; set; }
        public int? Xref01 { get; set; }
        public string? Xref02 { get; set; }
        public string? Xref03 { get; set; }
        public Guid Rowguid { get; set; }
        public bool ShouldShowOnTour { get; set; }
        public bool ShouldDistribute { get; set; }
        public int CustomSortId { get; set; }
        public bool UseCustomSceneName { get; set; }
        public string CustomSceneName { get; set; } = null!;
        public string SceneDescription { get; set; } = null!;
        public string FileNameBase { get; set; } = null!;
        public bool IsThirdPartyImage { get; set; }
        public int UploadedByContactId { get; set; }
        public int FeedId { get; set; }
        public bool? IsPrimaryExteriorImage { get; set; }
        public bool? IsProofPurchase { get; set; }
        public int? Jobid { get; set; }
        public int? ProofJobId { get; set; }
        public int? PhotoTypeId { get; set; }
        public bool? IsVirtualStaged { get; set; }
    }
}
