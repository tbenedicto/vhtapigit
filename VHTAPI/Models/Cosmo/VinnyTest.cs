﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VinnyTest
    {
        public string? ClientStatus { get; set; }
        public string? Pc { get; set; }
        public string? Express { get; set; }
        public DateTime? Tlc { get; set; }
        public string? VhtjobId { get; set; }
        public DateTime? JobDate { get; set; }
        public string? ClientLastName { get; set; }
        public string? ClientFirstName { get; set; }
        public string? ClientPhone { get; set; }
        public string? Company { get; set; }
        public string? PropertyAddress { get; set; }
        public string? Addr2 { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public string? PropertyId { get; set; }
        public string? Development { get; set; }
        public double? Vhtid { get; set; }
        public double? ListPrice { get; set; }
        public string? ImageSpecialist { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public DateTime? MediaArrivalDate { get; set; }
        public DateTime? FieldCompleteDate { get; set; }
        public DateTime? PostingSitesCompleteDate { get; set; }
        public string? CdRomCompleteDate { get; set; }
        public DateTime? VideoCompleteDate { get; set; }
        public DateTime? StudioCompleteEMailDate { get; set; }
        public DateTime? StudioCompleteDate { get; set; }
        public DateTime? AccountingCompleteDate { get; set; }
        public DateTime? JobCompleteDate { get; set; }
        public string? Priority { get; set; }
        public string? Stage { get; set; }
        public string? StatusCode { get; set; }
        public string? CvLastName { get; set; }
        public string? CvFirstName { get; set; }
        public string? JobSource { get; set; }
        public double? ClientPay { get; set; }
        public double? CorpPay { get; set; }
        public double? TotalRate { get; set; }
        public double? DistProd { get; set; }
        public string? Url { get; set; }
        public string? JobCreator { get; set; }
        public DateTime? CommissionDate { get; set; }
        public string? AccountRep { get; set; }
        public string? RegionalSales { get; set; }
        public string? CustomerServiceRep { get; set; }
        public DateTime? CompanyAdded { get; set; }
        public DateTime? CompanyStart { get; set; }
        public double? Floorplan { get; set; }
        public double? Premium { get; set; }
        public string? PrefCvLastName { get; set; }
        public string? PrefCvFirstName { get; set; }
        public DateTime? ProjectedNew { get; set; }
        public DateTime? NewRenew1stOrder { get; set; }
        public string? ContactGroup { get; set; }
        public string? Alert { get; set; }
    }
}
