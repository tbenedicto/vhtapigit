﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SearchedImage
    {
        public int SearchedImageId { get; set; }
        public string ImageUrl { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime LastSearchDate { get; set; }
        public string LastSearchBy { get; set; } = null!;
        public int SearchCount { get; set; }
        public Guid Rowguid { get; set; }
        public int ListingId { get; set; }
    }
}
