﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProductBillingMonthlyOutputJt
    {
        public string? CorporateOfficeId { get; set; }
        public string? Company { get; set; }
        public int? CompanyId { get; set; }
        public string? ParentCompany { get; set; }
        public int? Parentcompanyid { get; set; }
        public DateTime? CompanyStudioCompleteDate { get; set; }
        public string? ClientLastName { get; set; }
        public string? ClientFirstName { get; set; }
        public string? PropertyMlsId { get; set; }
        public string? DevelopmentPropertyAddress { get; set; }
        public string? Addr2 { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? ProductNameExternal { get; set; }
        public int? Quantity { get; set; }
        public decimal? TotalJobRate { get; set; }
        public decimal? ClientPay { get; set; }
        public decimal? CorporatePay { get; set; }
    }
}
