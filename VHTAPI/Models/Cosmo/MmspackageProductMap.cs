﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MmspackageProductMap
    {
        public int CosmoPackageId { get; set; }
        public int CosmoProductId { get; set; }
        public int MmspackageProductId { get; set; }
        public int MmsproductId { get; set; }
        public int? MmsmediaTypeId { get; set; }
        public bool Active { get; set; }
        public DateTime AddedDate { get; set; }
        public int? MmssubProductId { get; set; }
    }
}
