﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebTour
    {
        public int TourId { get; set; }
        public int TourStatusId { get; set; }
        public int ListingId { get; set; }
        public int ContentTypeId { get; set; }
        public string VhtjobId { get; set; } = null!;
        public string? ExternalUrl { get; set; }
        public bool? ArEmb { get; set; }
        public bool? Stillp { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public byte[]? TimpStampField { get; set; }
        public int? Xref01 { get; set; }
        public string? Xref02 { get; set; }
        public string? Xref03 { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public Guid Rowguid { get; set; }
        public bool ShouldShowOnTour { get; set; }
        public bool ShouldDistribute { get; set; }
        public bool UseCustomSceneSorting { get; set; }
        public int DpgstyleId { get; set; }
        public string? ExternalUrlmac { get; set; }
        public string? VideoUrlmp4 { get; set; }
        public string? VideoUrlmp4overlay { get; set; }
        public int? RoomCodeIdForPrimaryExterior { get; set; }
        public bool IsThirdParty { get; set; }
        public string? TeaserVideoUrl { get; set; }
    }
}
