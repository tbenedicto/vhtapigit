﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TempToDropObjects2
    {
        public int ObjectId { get; set; }
        public string Name { get; set; } = null!;
        public string? Type { get; set; }
    }
}
