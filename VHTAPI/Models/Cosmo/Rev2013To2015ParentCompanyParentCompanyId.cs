﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Rev2013To2015ParentCompanyParentCompanyId
    {
        public int? CompanyId { get; set; }
        public string? ParentCompany { get; set; }
        public decimal? Sold { get; set; }
        public decimal? Fees { get; set; }
        public int? PhotoCount { get; set; }
    }
}
