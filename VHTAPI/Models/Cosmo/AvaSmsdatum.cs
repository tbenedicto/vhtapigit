﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AvaSmsdatum
    {
        public int AvaSmsdataId { get; set; }
        public string? MsgSid { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string? FromNumber { get; set; }
        public int? JobId { get; set; }
        public string? Message { get; set; }
        public string? ThreadId { get; set; }
        public string? ToNumber { get; set; }
        public Guid Rowguid { get; set; }
    }
}
