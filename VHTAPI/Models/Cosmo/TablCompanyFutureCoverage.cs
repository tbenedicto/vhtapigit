﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablCompanyFutureCoverage
    {
        public double? MilesFromCv { get; set; }
        public string ToZipCode { get; set; } = null!;
        public string Ziptype { get; set; } = null!;
        public decimal ZipLat { get; set; }
        public decimal ZipLong { get; set; }
        public string Source { get; set; } = null!;
        public string? Zip { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
