﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MmsFirstOrderV2
    {
        public int MmsloginId { get; set; }
        public string? OfficeAddresses { get; set; }
        public string? OfficeCities { get; set; }
        public string? OfficeStates { get; set; }
        public string? OfficeZips { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Year { get; set; }
        public string? Time { get; set; }
        public DateTime? FirstOrderDate { get; set; }
    }
}
