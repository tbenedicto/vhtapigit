﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzPhotoType
    {
        public int PhotoTypeId { get; set; }
        public string Description { get; set; } = null!;
    }
}
