﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CslaViewCommunicationHeader
    {
        public int CommunicationId { get; set; }
        public string? FkparentSource { get; set; }
        public int? FkparentId { get; set; }
        public string CommunicationCategory { get; set; } = null!;
        public string? TextValue { get; set; }
        public string? TextValueExtended { get; set; }
        public bool? PropogateToContactInd { get; set; }
    }
}
