﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactCampaign
    {
        public int ContactCampaignId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Company { get; set; }
        public string? Phone { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public int? CampaignCodeId { get; set; }
        public int? CampaignMediumId { get; set; }
        public int? CampaignSourceId { get; set; }
        public DateTime? DateLoaded { get; set; }
        public Guid? CampaignGuid { get; set; }
        public string? CampaignTerm { get; set; }
        public string? CampaignContent { get; set; }
    }
}
