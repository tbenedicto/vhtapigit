﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessManualType
    {
        public ProcessManualType()
        {
            ProcessManuals = new HashSet<ProcessManual>();
        }

        public int ProcessManualTypeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<ProcessManual> ProcessManuals { get; set; }
    }
}
