﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ApiKeyCompany
    {
        public int ApiKeyCompanyId { get; set; }
        public int ApiKeyId { get; set; }
        public int CompanyId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
