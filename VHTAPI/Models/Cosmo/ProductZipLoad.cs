﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProductZipLoad
    {
        public double? ZipCode { get; set; }
        public string? Territory { get; set; }
        public int? ProductRegionId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
