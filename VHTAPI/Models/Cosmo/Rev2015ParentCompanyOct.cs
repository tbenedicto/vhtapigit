﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Rev2015ParentCompanyOct
    {
        public int? Year { get; set; }
        public int? Month { get; set; }
        public string? ParentCompany { get; set; }
        public decimal? Sold { get; set; }
    }
}
