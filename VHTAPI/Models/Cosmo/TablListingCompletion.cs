﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablListingCompletion
    {
        public int Jobid { get; set; }
        public int ListingId { get; set; }
        public int JobstatusReasonId { get; set; }
        public string? VhtjobId { get; set; }
        public int JobSourceId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int JobStatusId { get; set; }
        public string JobStatus { get; set; } = null!;
        public string? JobSource { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Development { get; set; }
        public int? AgentId { get; set; }
        public int ListingTypeId { get; set; }
        public string? ListingType { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? DisplayAs { get; set; }
        public int? CompanyId { get; set; }
        public string? TopLevelCompany { get; set; }
        public string TopLevelParentCompany { get; set; } = null!;
        public string? CompanyName { get; set; }
        public DateTime? StatusCompleteCompletedOn { get; set; }
    }
}
