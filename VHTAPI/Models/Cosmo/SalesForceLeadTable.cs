﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SalesForceLeadTable
    {
        public string Id { get; set; } = null!;
        public string? CampaignContentC { get; set; }
        public string? CampaignGuidC { get; set; }
        public string? CampaignMediumC { get; set; }
        public string? CampaignNameC { get; set; }
        public string? CampaignSourceC { get; set; }
        public string? CampaignTermC { get; set; }
        public string? Company { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? HasOptedOutOfEmail { get; set; }
        public string? IsConverted { get; set; }
        public string? IsDeleted { get; set; }
        public string? IsUnreadByOwner { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string? LastName { get; set; }
        public string? Name { get; set; }
        public string? OwnerId { get; set; }
        public string? Status { get; set; }
        public DateTime? SystemModstamp { get; set; }
    }
}
