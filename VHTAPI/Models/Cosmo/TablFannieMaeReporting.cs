﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablFannieMaeReporting
    {
        public int JobId { get; set; }
        public string? VhtjobId { get; set; }
        public DateTime JobStatusDate { get; set; }
        public decimal MileageRate { get; set; }
        public int MileageQuantity { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public decimal? AppointmentDateTotalRevenue { get; set; }
        public decimal? InfieldTotalRevenue { get; set; }
        public DateTime? Shootduedate { get; set; }
        public DateTime? Deliveryduedate { get; set; }
        public string? CompanyName { get; set; }
        public string? DisplayAs { get; set; }
        public string? AcceptDecline { get; set; }
        public DateTime? StatusCompleteCompletedOn { get; set; }
        public DateTime? StudioComplete { get; set; }
        public double? DaysToStudioCompletion { get; set; }
        public int? CatJobid { get; set; }
        public int? CatStdStill { get; set; }
        public int? CatPremStill { get; set; }
        public int? CatStd360 { get; set; }
        public int? CatClear360 { get; set; }
        public int? CatDup { get; set; }
        public int? CatPrints { get; set; }
        public int? CatVideo { get; set; }
        public int? CatVodservices { get; set; }
        public int? CatVhttourCustom { get; set; }
        public int? CatVhttourMn { get; set; }
        public int? CatAlbum { get; set; }
        public int? CatFloorplan { get; set; }
        public int? CatSilver { get; set; }
        public int? CatGold { get; set; }
        public int? CatPlatinum { get; set; }
        public int? CatAerial { get; set; }
        public int? Cat3d { get; set; }
        public int? CatDigitalEnhancement { get; set; }
        public int? CatBlueSky { get; set; }
        public int? CatPrepaid { get; set; }
        public int? _25Photographs { get; set; }
        public int? _25Photographs7Day { get; set; }
        public int? VirtualStagingTwilight1Photograph { get; set; }
        public int? Discount { get; set; }
        public int? VhtSignatureSeriesPhotography25 { get; set; }
        public int? VirtualStaging1Photograph { get; set; }
    }
}
