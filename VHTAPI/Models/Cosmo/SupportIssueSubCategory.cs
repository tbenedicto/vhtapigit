﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SupportIssueSubCategory
    {
        public int IssueSubCategoryId { get; set; }
        public int CategoryId { get; set; }
        public string Description { get; set; } = null!;
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
