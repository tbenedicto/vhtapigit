﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AgmsCreditCard
    {
        public double? Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? TransactionType { get; set; }
        public string? PaymentType { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public double? Amount { get; set; }
        public string? OrderDescription { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusMsg { get; set; }
        public string? Company { get; set; }
        public string? Address1 { get; set; }
        public string? Address2 { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? Country { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Fax { get; set; }
        public string? SafeId { get; set; }
        public string? Ccnumber { get; set; }
        public string? CcexpDate { get; set; }
        public string? CheckName { get; set; }
        public string? CheckAba { get; set; }
        public string? CheckAccount { get; set; }
        public string? AccountHolderType { get; set; }
        public string? AccountType { get; set; }
        public string? SecCode { get; set; }
        public string? AuthCode { get; set; }
        public string? AvsCode { get; set; }
        public string? AvsMsg { get; set; }
        public string? Cvv2Code { get; set; }
        public string? Cvv2Msg { get; set; }
        public string? OrderId { get; set; }
        public string? SettlementDate { get; set; }
        public string? SafeAction { get; set; }
        public string? Website { get; set; }
        public string? ShippingFirstName { get; set; }
        public string? ShippingLastName { get; set; }
        public string? ShippingCompany { get; set; }
        public string? ShippingAddress1 { get; set; }
        public string? ShippingAddress2 { get; set; }
        public string? ShippingCity { get; set; }
        public string? ShippingState { get; set; }
        public string? ShippingZip { get; set; }
        public string? ShippingCountry { get; set; }
        public string? ShippingEmail { get; set; }
        public string? ShippingPhone { get; set; }
        public string? ShippingFax { get; set; }
        public string? TransactionId { get; set; }
        public string? TrackingNumber { get; set; }
        public string? ShippingCarrier { get; set; }
        public string? CustomField1 { get; set; }
        public string? CustomField2 { get; set; }
        public string? CustomField3 { get; set; }
        public string? CustomField4 { get; set; }
        public string? CustomField5 { get; set; }
        public string? CustomField6 { get; set; }
        public string? CustomField7 { get; set; }
        public string? CustomField8 { get; set; }
        public string? CustomField9 { get; set; }
        public string? CustomField10 { get; set; }
        public string? SafeId1 { get; set; }
        public string? GetiStatus { get; set; }
        public string? GetiDepositDate { get; set; }
        public string? GetiReturnMessage { get; set; }
        public string? GetiReturnDetails { get; set; }
        public string? Ipaddress { get; set; }
        public string? UserAgent { get; set; }
        public string? CreateUser { get; set; }
        public string? InvoiceId { get; set; }
        public string? Cccompany { get; set; }
    }
}
