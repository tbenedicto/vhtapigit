﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablIssueTrackerOrder
    {
        public int Jobid { get; set; }
        public int ListingId { get; set; }
        public int JobstatusReasonId { get; set; }
        public string? VhtjobId { get; set; }
        public int JobSourceId { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? Jobstatusdate { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Development { get; set; }
        public int? AgentId { get; set; }
        public int ListingTypeId { get; set; }
        public string? ListingType { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? DisplayAs { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompany { get; set; }
        public string? JobStatus { get; set; }
    }
}
