﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactPrepaidProductBalanceBkup
    {
        public int ContactPrepaidProductBalanceId { get; set; }
        public int ContactId { get; set; }
        public int ProductId { get; set; }
        public int PrepaidBalance { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? LastUsedDate { get; set; }
    }
}
