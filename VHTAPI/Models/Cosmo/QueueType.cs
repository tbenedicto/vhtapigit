﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class QueueType
    {
        public int QueueTypeId { get; set; }
        public string Description { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
