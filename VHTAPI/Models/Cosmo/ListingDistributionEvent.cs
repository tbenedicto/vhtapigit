﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingDistributionEvent
    {
        public int EventId { get; set; }
        public int ProcessRunId { get; set; }
        public int EventTypeId { get; set; }
        public int ListingId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ListingDistributionEventType EventType { get; set; } = null!;
        public virtual Listing Listing { get; set; } = null!;
        public virtual ProcessRun ProcessRun { get; set; } = null!;
    }
}
