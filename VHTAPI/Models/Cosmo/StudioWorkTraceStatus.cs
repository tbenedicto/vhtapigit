﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class StudioWorkTraceStatus
    {
        public int Id { get; set; }
        public string StatusName { get; set; } = null!;
    }
}
