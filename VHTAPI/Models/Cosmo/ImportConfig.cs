﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportConfig
    {
        public int ImportConfigId { get; set; }
        public string ImportConfigName { get; set; } = null!;
        public int CosmoCompanyId { get; set; }
        public string FileDirectory { get; set; } = null!;
        public string FeedFileExt { get; set; } = null!;
        public int G3codeId { get; set; }
        public bool ContainsDraftMlsNumbers { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public bool OptionalMlsId { get; set; }
        public bool AutoAssign { get; set; }
    }
}
