﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzJobChangeReason
    {
        public int JobChangeReasonId { get; set; }
        public int JobChangeCategoryId { get; set; }
        public int SortId { get; set; }
        public bool IsActive { get; set; }
        public string Descr { get; set; } = null!;
        public bool IsCommentRequired { get; set; }
        public Guid Rowguid { get; set; }
    }
}
