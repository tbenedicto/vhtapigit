﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderKey
    {
        public int OrderKeyId { get; set; }
        public int ContactId { get; set; }
        public Guid Rowguid { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
