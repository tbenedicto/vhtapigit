﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablSalesDailyRevenue
    {
        public string? TargetType { get; set; }
        public decimal? Actual { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public double? Target { get; set; }
        public double? BusinessDays { get; set; }
        public double? BussinessDayOfTheMonth { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}
