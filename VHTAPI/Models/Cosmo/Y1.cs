﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Y1
    {
        public long? Row1 { get; set; }
        public string? Textvalue { get; set; }
        public int? Fkparentid { get; set; }
    }
}
