﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Zzmonth
    {
        public int? MonthNo { get; set; }
        public string? Descr { get; set; }
        public string? Descr1 { get; set; }
    }
}
