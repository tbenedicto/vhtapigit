﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCompanyContactSearch4
    {
        public int CompanyId { get; set; }
        public string? CompanyDisplayAs { get; set; }
        public int? CompanyCategoryId { get; set; }
        public string? CompanyName { get; set; }
        public int? CompanyStatusId { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public string? ContactDisplayAs { get; set; }
        public string? CompanyCommunicationText { get; set; }
    }
}
