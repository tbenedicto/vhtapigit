﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SpecialistCommissionsAllEcp
    {
        public int? ParentCompanyId { get; set; }
        public string? Parentcompanyname { get; set; }
        public int? Companyid { get; set; }
        public string? Companyname { get; set; }
        public int? FirstJobId { get; set; }
        public string? JobAddedBy { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public int? CompletedYear { get; set; }
        public int? CompletedMonth { get; set; }
        public DateTime? MinStatusProductionCompletedOn { get; set; }
        public decimal? Totalrev { get; set; }
        public decimal? InfieldTotalRevenue { get; set; }
        public int Contactid { get; set; }
        public DateTime? JobAddedDate { get; set; }
        public DateTime? Appointmentdatetime { get; set; }
        public int? ListingId { get; set; }
        public string? AgentName { get; set; }
    }
}
