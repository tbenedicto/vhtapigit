﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwImportCompanyAddress
    {
        public string? CompanyName { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public int CompanyId { get; set; }
    }
}
