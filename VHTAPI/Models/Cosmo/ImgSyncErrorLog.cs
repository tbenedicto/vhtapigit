﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImgSyncErrorLog
    {
        public int ImgSyncErrorLogId { get; set; }
        public DateTime ErrorTimeStamp { get; set; }
        public string MessageData { get; set; } = null!;
        public string ErrorText { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
