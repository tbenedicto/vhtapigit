﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzrevenueShare
    {
        public int RevenueShareId { get; set; }
        public int TopCompanyId { get; set; }
        public int ProductId { get; set; }
        public string AgentOrCorp { get; set; } = null!;
        public string FlatOrPercent { get; set; } = null!;
        public double DiscountAmount { get; set; }
    }
}
