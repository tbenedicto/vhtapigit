﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoItemTypeMapping
    {
        public string? Type { get; set; }
        public string? ItemNo { get; set; }
        public string? Description { get; set; }
    }
}
