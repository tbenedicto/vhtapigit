﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AuditCpixListing
    {
        public int MasterListingId { get; set; }
        public int? MasterJobid { get; set; }
        public int? CpixTourId { get; set; }
        public DateTime? WelcomeEmailDate { get; set; }
        public DateTime? OrderEmailDate { get; set; }
        public string? LoadType { get; set; }
        public string? BadId { get; set; }
    }
}
