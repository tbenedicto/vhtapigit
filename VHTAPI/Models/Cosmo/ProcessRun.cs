﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessRun
    {
        public ProcessRun()
        {
            ListingDistributionEvents = new HashSet<ListingDistributionEvent>();
            ProcessLogs = new HashSet<ProcessLog>();
        }

        public int ProcessRunId { get; set; }
        public int ProcessId { get; set; }
        public int ProcessRunStatusId { get; set; }
        public int ProcessRunCodeId { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public bool FlaggedForReview { get; set; }
        public int HighestProcessLogTypeId { get; set; }
        public int ProcessScheduleId { get; set; }
        public int? ErrResolutionTypeId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ProcessLogType HighestProcessLogType { get; set; } = null!;
        public virtual Process Process { get; set; } = null!;
        public virtual ProcessRunCode ProcessRunCode { get; set; } = null!;
        public virtual ProcessRunStatus ProcessRunStatus { get; set; } = null!;
        public virtual ICollection<ListingDistributionEvent> ListingDistributionEvents { get; set; }
        public virtual ICollection<ProcessLog> ProcessLogs { get; set; }
    }
}
