﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzJobCancellationReason
    {
        public int JobCancellationReasonId { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
        public string? Olddesc { get; set; }
        public short? ShowInFocus { get; set; }
        public bool IsActive { get; set; }
    }
}
