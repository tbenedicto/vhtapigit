﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class FloorPlanOrderRoom
    {
        public int FloorPlanOrderRoomId { get; set; }
        public int FloorPlanOrderId { get; set; }
        public string RoomName { get; set; } = null!;
        public string Width { get; set; } = null!;
        public string Length { get; set; } = null!;
    }
}
