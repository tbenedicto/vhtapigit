﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportProductRules20190129AccountsNyc
    {
        public string? ProductRuleId { get; set; }
        public double? ProductId { get; set; }
        public string? ProductInternalName { get; set; }
        public double? RuleConditionTypeId { get; set; }
        public string? Type { get; set; }
        public string? ContactId { get; set; }
        public string? ContactName { get; set; }
        public double? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? OfficeType { get; set; }
        public string? VhtRelationship { get; set; }
        public string? RegionalSales { get; set; }
        public string? Cvid { get; set; }
        public string? Cvname { get; set; }
        public string? ProductRegionId { get; set; }
        public string? Market { get; set; }
        public string? StateId { get; set; }
        public string? StateName { get; set; }
        public string? ContactGroupId { get; set; }
        public string? ContactGroupName { get; set; }
        public string? RetailPrice { get; set; }
        public double? CustomerPay { get; set; }
        public double? CustomerRateAdj { get; set; }
        public double? CorporatePay { get; set; }
        public double? CorporateRateAdj { get; set; }
        public string? Cvbase { get; set; }
        public string? CvrateAdj { get; set; }
        public string? Cvbonus { get; set; }
        public string? ProofPhotoCredit { get; set; }
        public string? ProofAmountCredit { get; set; }
        public string? ProofPercentCredit { get; set; }
        public string? ScreenshotUrl { get; set; }
        public string? SampleUrl { get; set; }
    }
}
