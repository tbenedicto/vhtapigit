﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NormalizedAddress
    {
        public int NormalizedAddressId { get; set; }
        public int ListingId { get; set; }
        public bool Validated { get; set; }
        public bool Parsable { get; set; }
        public string? ErrorCode { get; set; }
        public string? ReportedBy { get; set; }
        public string? AddressCombined { get; set; }
        public string? UnitCombined { get; set; }
        public string? StreetNumber { get; set; }
        public string? StreetPreDir { get; set; }
        public string? StreetName { get; set; }
        public string? StreetSuffix { get; set; }
        public string? StreetPostDir { get; set; }
        public string? UnitType { get; set; }
        public string? UnitNumber { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? Zip4 { get; set; }
        public string? CountyName { get; set; }
        public string? CountyCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid RowGuid { get; set; }
        public DateTime? Touchdate { get; set; }
    }
}
