﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ApiLogListingsByAgentEmail
    {
        public int ApiLogListingsByAgentEmailId { get; set; }
        public string AgentEmail { get; set; } = null!;
        public int PageNumber { get; set; }
        public DateTime DateCalled { get; set; }
    }
}
