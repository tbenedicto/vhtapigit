﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobProduct
    {
        public int JobProductId { get; set; }
        public int JobId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal AgentPay { get; set; }
        public decimal CorporatePay { get; set; }
        public decimal Cvpay { get; set; }
        public decimal Cvupsell { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
        public int CvupsellQuantity { get; set; }
        public int JobProductStatusId { get; set; }
        public string JobProductStatusNote { get; set; } = null!;
        public DateTime? StatusDate { get; set; }
        public int? MmsorderTransactionId { get; set; }
        public string? MmstransactionNotes { get; set; }
        public int? BillingContactid { get; set; }
        public decimal RevenueShare { get; set; }
        public decimal AgentRateAdj { get; set; }
        public decimal AgentDiscount { get; set; }
        public decimal AgentPromoDiscount { get; set; }
        public int? AgentDiscountCodeId { get; set; }
        public int? AgentPromoCodeId { get; set; }
        public decimal CorporateRateAdj { get; set; }
        public decimal CorporateDiscount { get; set; }
        public decimal CorporatePromoDiscount { get; set; }
        public int? CorporateDiscountCodeId { get; set; }
        public int? CorporatePromoCodeId { get; set; }
        public decimal CvrateAdj { get; set; }
        public decimal Cvdiscount { get; set; }
        public int? CvdiscountCodeId { get; set; }
        public int? ProofPhotoCredit { get; set; }
        public decimal? ProofAmountCredit { get; set; }
        public decimal? ProofPercentCredit { get; set; }
        public int? ProofPhotoCreditAvailable { get; set; }
        public decimal OrigAgentPay { get; set; }
        public decimal OrigCorporatePay { get; set; }
        public decimal OrigCvpay { get; set; }
        public int? PackageId { get; set; }
        public int? JobPackageSeq { get; set; }
    }
}
