﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobInFieldFmtrigger
    {
        public int JobInFieldFmtriggerId { get; set; }
        public int JobId { get; set; }
        public int JobStatusId { get; set; }
        public int? OldJobStatusId { get; set; }
        public int SlaveJobId { get; set; }
        public int SlaveJobSourceId { get; set; }
        public int SlaveListingId { get; set; }
        public int SlaveAgentId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
