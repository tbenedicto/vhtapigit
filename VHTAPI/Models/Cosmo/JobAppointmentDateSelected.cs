﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobAppointmentDateSelected
    {
        public int JobId { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
    }
}
