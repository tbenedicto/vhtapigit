﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Filelist2010New
    {
        public int? Listing { get; set; }
        public string? RoomCode { get; set; }
    }
}
