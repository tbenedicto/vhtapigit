﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Jobspay
    {
        public int? Listingid { get; set; }
        public decimal? AgentPay { get; set; }
        public decimal? CvPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? TotalPrice { get; set; }
    }
}
