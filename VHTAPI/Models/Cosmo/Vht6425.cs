﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Vht6425
    {
        public double? OfficeNumber { get; set; }
        public string? OfficeName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? PreferredFirstName { get; set; }
        public string? Email { get; set; }
        public double? LeadRouterId { get; set; }
        public string? AgentMlsnumber { get; set; }
        public double? Phone { get; set; }
        public double? Cellphone { get; set; }
    }
}
