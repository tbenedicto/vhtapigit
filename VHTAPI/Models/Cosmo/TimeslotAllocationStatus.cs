﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TimeslotAllocationStatus
    {
        public TimeslotAllocationStatus()
        {
            TimeslotAllocations = new HashSet<TimeslotAllocation>();
        }

        public int StatusId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public byte[] Tsvalue { get; set; } = null!;

        public virtual ICollection<TimeslotAllocation> TimeslotAllocations { get; set; }
    }
}
