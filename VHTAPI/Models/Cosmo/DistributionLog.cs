﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DistributionLog
    {
        public int RowId { get; set; }
        public int ListingId { get; set; }
        public int DistributionPartnerId { get; set; }
        public int DistributionEventTypeId { get; set; }
        public DateTime? EventDate { get; set; }
        public string? EventUser { get; set; }
    }
}
