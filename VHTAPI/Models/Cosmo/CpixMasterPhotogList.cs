﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CpixMasterPhotogList
    {
        public string? PhotographerIdentifier { get; set; }
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? CompanyNameDba { get; set; }
        public string? PhotographersPaid2017 { get; set; }
        public string? NewContract { get; set; }
        public string? OldContract { get; set; }
        public string? Copyright { get; set; }
        public string? MissingContract { get; set; }
        public double? ProbableMatches { get; set; }
        public double? PhotogNumber { get; set; }
        public string? HaloId { get; set; }
        public double? PayrollId { get; set; }
        public string? HireDate { get; set; }
        public string? LastJobShotDate { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public string? Phone { get; set; }
        public string? Cell { get; set; }
        public string? Email { get; set; }
    }
}
