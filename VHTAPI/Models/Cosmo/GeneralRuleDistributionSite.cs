﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GeneralRuleDistributionSite
    {
        public int GeneralRuleId { get; set; }
        public int DistributionPointId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ZzDistributionPoint DistributionPoint { get; set; } = null!;
        public virtual GeneralRule GeneralRule { get; set; } = null!;
    }
}
