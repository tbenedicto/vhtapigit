﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CosmoTerritorySummaryCLoad
    {
        public string? TerritoryC { get; set; }
        public string? MonthC { get; set; }
        public int? NewRenewAgentsC { get; set; }
        public decimal? NewRevC { get; set; }
        public decimal? TrailingRevC { get; set; }
        public decimal? PcRevMonthC { get; set; }
        public decimal? TotalResidentialRevenueC { get; set; }
        public DateTime CreatedDate { get; set; }
        public string IsDeleted { get; set; } = null!;
        public DateTime LastModifiedDate { get; set; }
        public string SystemModstamp { get; set; } = null!;
        public int? AverageOrderResidentalC { get; set; }
        public string? Error { get; set; }
        public string? Id { get; set; }
        public string? Name { get; set; }
    }
}
