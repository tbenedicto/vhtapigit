﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingIdentifierHistory2
    {
        public int ListingIdentifierHistoryId { get; set; }
        public int ListingId { get; set; }
        public string Id { get; set; } = null!;
        public int ListingIdentifierTypeId { get; set; }
        public int ListingIdentifierHistoryActionId { get; set; }
        public DateTime ActionDate { get; set; }
        public Guid Rowguid { get; set; }
        public string? ActionBy { get; set; }
    }
}
