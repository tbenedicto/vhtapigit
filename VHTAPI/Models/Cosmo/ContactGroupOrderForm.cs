﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactGroupOrderForm
    {
        public int ContactGroupId { get; set; }
        public string? OrderFormPath { get; set; }
        public bool ShowSgp { get; set; }
        public Guid GuidUrl { get; set; }
        public int ContactGroupOrderFormId { get; set; }
        public bool RedirectEnabled { get; set; }
        public bool UseCustomRateSheet { get; set; }
        public string? CustomRateSheetUrl { get; set; }
        public int ContactName { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public int ContactEmail { get; set; }
        public string? AgentEmail { get; set; }
        public int ContactCompanyName { get; set; }
        public string? AgentCompanyName { get; set; }
        public int ContactOfficeAddress { get; set; }
        public string? AgentOfficeAddress { get; set; }
        public int ContactOfficeCity { get; set; }
        public string? AgentOfficeCity { get; set; }
        public int ContactOfficeStateZip { get; set; }
        public string? AgentOfficeState { get; set; }
        public string? AgentOfficeZip { get; set; }
        public int ContactPhoneNumber { get; set; }
        public string? AgentPhone1 { get; set; }
        public string? AgentPhone2 { get; set; }
        public string? AgentPhone3 { get; set; }
        public bool CommercialLayout { get; set; }
        public int AgentPhoneType { get; set; }
        public int ContactGroupOrderFormShowRateTypeId { get; set; }
        public string? Description { get; set; }
        public bool SchedCallPrefCv { get; set; }
        public bool SchedCallAnyCv { get; set; }
        public bool SchedEnterTime { get; set; }
    }
}
