﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactPivot
    {
        public int ContactId { get; set; }
        public string ContactCode { get; set; } = null!;
        public string DisplayAs { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string MainPhone { get; set; } = null!;
        public string MainPhoneExt { get; set; } = null!;
        public string CellPhone { get; set; } = null!;
        public string CellPhoneExt { get; set; } = null!;
        public string DirectPhone { get; set; } = null!;
        public string DirectPhoneExt { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
