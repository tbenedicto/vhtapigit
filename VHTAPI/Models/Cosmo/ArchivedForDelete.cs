﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ArchivedForDelete
    {
        public int Listingid { get; set; }
        public int? Contentserverid { get; set; }
        public string? G3code { get; set; }
        public int? Newcontentserver { get; set; }
    }
}
