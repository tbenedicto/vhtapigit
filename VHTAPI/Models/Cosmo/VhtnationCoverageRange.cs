﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VhtnationCoverageRange
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? ApplicationRating { get; set; }
        public string? ApplicationStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? LastActivity { get; set; }
        public DateTime? LastModified { get; set; }
        public string? Email { get; set; }
        public string? City { get; set; }
        public int? CvZipPostalCode { get; set; }
        public string? CityName { get; set; }
        public string? StateName { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? MilesFromCv { get; set; }
        public int? Zipcode { get; set; }
        public string? Ziptype { get; set; }
        public double? LatitudeZipLat { get; set; }
        public double? LongitudeZipLong { get; set; }
    }
}
