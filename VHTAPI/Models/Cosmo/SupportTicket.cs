﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SupportTicket
    {
        public int SupportTicketId { get; set; }
        public int IssueSubCategoryId { get; set; }
        public string Comment { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Lastname { get; set; } = null!;
        public string Firstname { get; set; } = null!;
        public int TicketStatusId { get; set; }
        public int TicketSourceId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
        public string? Note { get; set; }
    }
}
