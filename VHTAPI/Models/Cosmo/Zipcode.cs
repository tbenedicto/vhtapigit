﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Zipcode
    {
        public int ZipcodeId { get; set; }
        public string Zipcode1 { get; set; } = null!;
        public string Ziptype { get; set; } = null!;
        public string CityName { get; set; } = null!;
        public string CityType { get; set; } = null!;
        public string StateName { get; set; } = null!;
        public string StateAbbr { get; set; } = null!;
        public string AreaCode { get; set; } = null!;
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public Guid Rowguid { get; set; }
        public int? TimeZone { get; set; }
        public string? Dst { get; set; }
    }
}
