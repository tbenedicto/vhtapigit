﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCompanyCategory
    {
        public int? Companyid { get; set; }
        public int? Silver { get; set; }
        public int? Gold { get; set; }
        public int? Platinum { get; set; }
        public int? Drone { get; set; }
        public int? X3d { get; set; }
        public int? Floorplan { get; set; }
    }
}
