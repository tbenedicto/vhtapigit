﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CloneListingBad
    {
        public int MasterListingId { get; set; }
        public int MasterJobid { get; set; }
        public int SlaveListingId { get; set; }
        public int SlaveJobid { get; set; }
        public DateTime? WelcomeEmailDate { get; set; }
        public DateTime? OrderEmailDate { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsCloned { get; set; }
        public bool? IsDisconnected { get; set; }
        public string? ReoId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
