﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwProofingDetail
    {
        public int ListingId { get; set; }
        public string? Vhtjobid { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public int ContactId { get; set; }
        public string AgentName { get; set; } = null!;
        public string ContactEmail { get; set; } = null!;
        public string? ContactCode { get; set; }
        public int ParentCompanyid { get; set; }
        public string? ParentCompanyname { get; set; }
        public int Companyid { get; set; }
        public string? CompanyName { get; set; }
        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public string ProductType { get; set; } = null!;
        public bool? IsSilver { get; set; }
        public bool? IsGold { get; set; }
        public bool? IsPlatinum { get; set; }
        public int PhotographerContactId { get; set; }
        public string PhotographerName { get; set; } = null!;
        public int ContentTypeId { get; set; }
        public int TourRoomCodeId { get; set; }
        public string? PhotoType { get; set; }
        public int RoomCodeId { get; set; }
        public string CustomSceneName { get; set; } = null!;
        public bool? IsProof { get; set; }
        public int SortId { get; set; }
        public string Caption { get; set; } = null!;
        public string? RoomGroup { get; set; }
        public int? TourRoomCodeJobId { get; set; }
        public int? ProofJobId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? Vhtrelationship { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string? ContactBusinessType { get; set; }
        public string? Territory { get; set; }
        public string? Region { get; set; }
    }
}
