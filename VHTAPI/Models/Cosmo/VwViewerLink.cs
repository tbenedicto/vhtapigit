﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwViewerLink
    {
        public int G3codeId { get; set; }
        public int CustomViewerLinkId { get; set; }
        public string CustomViewerValue { get; set; } = null!;
        public string G3code { get; set; } = null!;
        public string CustomLink { get; set; } = null!;
    }
}
