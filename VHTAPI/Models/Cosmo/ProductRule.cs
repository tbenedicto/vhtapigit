﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProductRule
    {
        public int ProductRuleId { get; set; }
        public int ProductId { get; set; }
        public int ProductRuleTypeId { get; set; }
        public int? CompanyId { get; set; }
        public int? Cvid { get; set; }
        public string? Descr { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? AgentPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? Cvpay { get; set; }
        public Guid Rowguid { get; set; }
    }
}
