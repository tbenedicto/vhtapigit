﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvconfirmedAppointmentTrigger
    {
        public int CvconfirmedAppointmentTriggerId { get; set; }
        public int JobId { get; set; }
        public int ContactId { get; set; }
        public bool JobHasFloorplan { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public int? OldContactId { get; set; }
        public DateTime? OldStartDateTime { get; set; }
        public DateTime? OldEndDateTime { get; set; }
        public DateTime? EventDate { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public string? JobProductIds { get; set; }
    }
}
