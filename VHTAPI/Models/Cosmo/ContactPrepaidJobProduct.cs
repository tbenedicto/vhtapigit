﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactPrepaidJobProduct
    {
        public int ContactPrepaidJobProductId { get; set; }
        public int ContactId { get; set; }
        public int JobProductId { get; set; }
        public int JobId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual Contact Contact { get; set; } = null!;
        public virtual Job Job { get; set; } = null!;
        public virtual Product Product { get; set; } = null!;
    }
}
