﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PrismAgent
    {
        public int? CosmoCompanyid { get; set; }
        public int? Contactid { get; set; }
        public string? PersonName { get; set; }
        public string? Email { get; set; }
        public string? SecondEmail { get; set; }
        public string? Mobile { get; set; }
        public string? Agency { get; set; }
        public string? SfCompanyId { get; set; }
        public string? ContactSource { get; set; }
        public string? Street { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
    }
}
