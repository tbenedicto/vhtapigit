﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablUniqClient
    {
        public int? Agentid { get; set; }
        public string? Agent { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompany { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public string? RegionName { get; set; }
        public string? Territory { get; set; }
    }
}
