﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MarketRateSheet
    {
        public int RateSheet { get; set; }
        public bool ShowSgp { get; set; }
        public bool UseCustomRateSheet { get; set; }
        public string? CustomRateSheetUrl { get; set; }
        public bool SchedCallPrefCv { get; set; }
        public bool SchedCallAnyCv { get; set; }
        public bool SchedEnterTime { get; set; }
    }
}
