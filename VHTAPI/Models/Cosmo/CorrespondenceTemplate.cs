﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CorrespondenceTemplate
    {
        public CorrespondenceTemplate()
        {
            CorrespondenceTemplateMaps = new HashSet<CorrespondenceTemplateMap>();
        }

        public int CorrespondenceTemplateId { get; set; }
        public int TemplateTypeId { get; set; }
        public string Descr { get; set; } = null!;
        public string Filename { get; set; } = null!;
        public string Subject { get; set; } = null!;
        public bool IsDefault { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<CorrespondenceTemplateMap> CorrespondenceTemplateMaps { get; set; }
    }
}
