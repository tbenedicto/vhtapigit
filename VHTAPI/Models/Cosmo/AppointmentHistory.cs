﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AppointmentHistory
    {
        public int AppointmentHistoryId { get; set; }
        public int AppointmentId { get; set; }
        public int ContactId { get; set; }
        public int JobId { get; set; }
        public int AppointmentTypeId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime ExpiresDateTime { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public DateTime EventDate { get; set; }
        public string EventAction { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
