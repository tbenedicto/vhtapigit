﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DigsMatchedPhotosToVedderDvdSourceImageListingR
    {
        public string? Listingid { get; set; }
        public string? Roomcodeid { get; set; }
        public string? Dvd { get; set; }
        public string SourceImageUrl { get; set; } = null!;
        public string FoundImageUrl { get; set; } = null!;
        public int? NumbInMatch { get; set; }
    }
}
