﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCreditCardExpirationMonth
    {
        public int CreditCardExpirationMonthId { get; set; }
        public string? Descr { get; set; }
        public Guid Rowguid { get; set; }
    }
}
