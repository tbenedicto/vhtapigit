﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AmberPriorityList
    {
        public string? AccountName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public double? MappedContactCosmoId { get; set; }
        public string? Title { get; set; }
        public string? SalesTerritory { get; set; }
        public string? AccountDevelopmentExecutive { get; set; }
        public string? AreaSalesMarketLead { get; set; }
        public string? AgentStatus { get; set; }
        public string? Classification { get; set; }
        public string? MailingStreet { get; set; }
        public string? MailingCity { get; set; }
        public string? MailingStateProvince { get; set; }
        public string? MailingZipPostalCode { get; set; }
        public string? MailingCountry { get; set; }
        public string? Phone { get; set; }
        public string? Mobile { get; set; }
        public string? Fax { get; set; }
        public string? AccountOwner { get; set; }
        public string? MappedCompanyCosmoId { get; set; }
        public string? ContactId { get; set; }
    }
}
