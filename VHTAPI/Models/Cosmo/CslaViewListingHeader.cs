﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CslaViewListingHeader
    {
        public int ListingId { get; set; }
        public int JobId { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string PropertyAddressLine2 { get; set; } = null!;
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public DateTime? AddedDate { get; set; }
    }
}
