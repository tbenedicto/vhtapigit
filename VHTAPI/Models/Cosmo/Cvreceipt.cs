﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Cvreceipt
    {
        public int CvreceiptId { get; set; }
        public int JobId { get; set; }
        public string ReceiptsFolder { get; set; } = null!;
        public DateTime DateSubmitted { get; set; }
        public Guid Rowguid { get; set; }
    }
}
