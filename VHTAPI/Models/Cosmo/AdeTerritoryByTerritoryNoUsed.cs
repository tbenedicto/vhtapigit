﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdeTerritoryByTerritoryNoUsed
    {
        public string? Territory { get; set; }
        public int? RepSalesNational { get; set; }
    }
}
