﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AuditInvisoListingBilling
    {
        public int MasterListingId { get; set; }
        public int MasterJobid { get; set; }
        public int? InvisoTourId { get; set; }
        public string? InvisoOrderId { get; set; }
        public DateTime? Loaddate { get; set; }
        public int? CosmoContactid { get; set; }
    }
}
