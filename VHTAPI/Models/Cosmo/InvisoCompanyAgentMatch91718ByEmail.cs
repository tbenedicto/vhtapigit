﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoCompanyAgentMatch91718ByEmail
    {
        public int? CosmoContactid { get; set; }
        public string? ChainId { get; set; }
        public string? ChainName { get; set; }
        public string? RegionId { get; set; }
        public string? RegionName { get; set; }
        public string? CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public string? LegalName { get; set; }
        public string? CustomerAddressStreet { get; set; }
        public string? CustomerAddressPostalArea { get; set; }
        public string? CustomerAddressZipCode { get; set; }
        public string? PersonId { get; set; }
        public string? PersonName { get; set; }
        public string? Email { get; set; }
        public string? FirstOrderId { get; set; }
        public string? FirstOrderCreatedAt { get; set; }
    }
}
