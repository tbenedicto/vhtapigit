﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCreditCardType
    {
        public int CreditCardTypeId { get; set; }
        public string? Descr { get; set; }
        public string? SortId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
