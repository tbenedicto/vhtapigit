﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DbwebCosmoWebZzG3code
    {
        public int G3codeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public int ContentServerId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
        public string OrderFormUrl { get; set; } = null!;
        public bool? IsNewViewer { get; set; }
        public bool? IsImageWork { get; set; }
        public bool? EnableNewViewer { get; set; }
        public string? RateSheetUrl { get; set; }
        public bool? ViewerCreate { get; set; }
        public bool? IsViewerCreated { get; set; }
        public string? ViewerStatusFlag { get; set; }
        public string CorpPayOrderFormUrl { get; set; } = null!;
        public string? BrokerLogoUrl { get; set; }
    }
}
