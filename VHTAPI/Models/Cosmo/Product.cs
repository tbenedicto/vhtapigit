﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Product
    {
        public Product()
        {
            ContactPrepaidJobProductTransactions = new HashSet<ContactPrepaidJobProductTransaction>();
            ContactPrepaidJobProducts = new HashSet<ContactPrepaidJobProduct>();
            ContactPrepaidProductBalances = new HashSet<ContactPrepaidProductBalance>();
            GeneralRuleAvailableProducts = new HashSet<GeneralRuleAvailableProduct>();
            GeneralRuleDefaultProducts = new HashSet<GeneralRuleDefaultProduct>();
            GeneralRulePreferredProducts = new HashSet<GeneralRulePreferredProduct>();
            ProductRuleNews = new HashSet<ProductRuleNew>();
        }

        public int ProductId { get; set; }
        public int ProductCategoryId { get; set; }
        public string ProductCode { get; set; } = null!;
        public string ProductName { get; set; } = null!;
        public bool IsPackage { get; set; }
        public string? BillingDescription { get; set; }
        public string? VhtjobCode { get; set; }
        public int ProductStatusId { get; set; }
        public int? ProductStatusReasonId { get; set; }
        public int? ProductStatusReasonNote { get; set; }
        public int ProductCompanyModeId { get; set; }
        public string? XmlruleSet { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public int? SortId { get; set; }
        public Guid Rowguid { get; set; }
        public bool? CatStdStill { get; set; }
        public bool? CatPremStill { get; set; }
        public bool? CatStd360 { get; set; }
        public bool? CatClear360 { get; set; }
        public bool? CatDup { get; set; }
        public bool? CatPrints { get; set; }
        public bool? CatVideo { get; set; }
        public string? InternalDescription { get; set; }
        public string? ExternalDescription { get; set; }
        public string? InternalName { get; set; }
        public string? ExternalName { get; set; }
        public int Cvpoints { get; set; }
        public bool CatVodservices { get; set; }
        public bool CatVhttourCustom { get; set; }
        public bool CatVhttourMn { get; set; }
        public bool CatAlbum { get; set; }
        public bool CatFloorplan { get; set; }
        public int? ProductGroupId { get; set; }
        public bool CatSilver { get; set; }
        public bool CatGold { get; set; }
        public bool CatPlatinum { get; set; }
        public bool CatAerial { get; set; }
        public bool Cat3d { get; set; }
        public int? ProductQuantity { get; set; }
        public int? PremiumProductQuantity { get; set; }
        public int? Silver { get; set; }
        public int? Gold { get; set; }
        public int? Platinum { get; set; }
        public bool? IsNonCommissionFee { get; set; }
        public bool CatPix { get; set; }
        public bool CatPrepaid { get; set; }
        public bool CatBlueSky { get; set; }
        public bool CatDigitalEnhancement { get; set; }
        public int? SessionImagesCount { get; set; }
        public bool CatFullMotion { get; set; }
        public bool CatElevated { get; set; }
        public bool CatZillow { get; set; }
        public bool IsExpress { get; set; }
        public bool CatFloorplanCc { get; set; }
        public bool CatAerialVideo { get; set; }
        public bool CatIfp { get; set; }
        public bool CatDroneElitePhotos { get; set; }
        public bool CatFmv2 { get; set; }

        public virtual ICollection<ContactPrepaidJobProductTransaction> ContactPrepaidJobProductTransactions { get; set; }
        public virtual ICollection<ContactPrepaidJobProduct> ContactPrepaidJobProducts { get; set; }
        public virtual ICollection<ContactPrepaidProductBalance> ContactPrepaidProductBalances { get; set; }
        public virtual ICollection<GeneralRuleAvailableProduct> GeneralRuleAvailableProducts { get; set; }
        public virtual ICollection<GeneralRuleDefaultProduct> GeneralRuleDefaultProducts { get; set; }
        public virtual ICollection<GeneralRulePreferredProduct> GeneralRulePreferredProducts { get; set; }
        public virtual ICollection<ProductRuleNew> ProductRuleNews { get; set; }
    }
}
