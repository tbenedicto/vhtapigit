﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablOrderCompareToCancel
    {
        public int Jobid { get; set; }
        public int ListingId { get; set; }
        public int JobstatusReasonId { get; set; }
        public string? VhtjobId { get; set; }
        public int JobSourceId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? JobStatus { get; set; }
        public DateTime? JobStatusDate { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Development { get; set; }
        public int? AgentId { get; set; }
        public int ListingTypeId { get; set; }
        public string? ListingType { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? DisplayAs { get; set; }
        public string? OrderCompanyName { get; set; }
        public string? OrderParentCompany { get; set; }
        public int? CancelJobId { get; set; }
        public int? CancelListingId { get; set; }
        public string? JobStatus1 { get; set; }
        public int? CancelJobstatusreasonId { get; set; }
        public int? Jobpriorityid { get; set; }
        public string? CancelVhtjobId { get; set; }
        public int? CancelJobSourceId { get; set; }
        public DateTime? CancelAddedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? JobCancellationReasonId { get; set; }
        public string JobCancellationReason { get; set; } = null!;
        public int? SortId { get; set; }
        public string? Descr { get; set; }
        public string? Propertyaddressline1 { get; set; }
        public string? Propertyaddressline2 { get; set; }
        public string? CancelPropertyCity { get; set; }
        public string? CancelPropertyState { get; set; }
        public string? CancelPropertyZip { get; set; }
        public string? CancelDevelopment { get; set; }
        public int? CancelAgentId { get; set; }
        public string? CancelDisplayAs { get; set; }
        public int? CancelListingTypeId { get; set; }
        public int? Addressstyleid { get; set; }
        public int? Contactid { get; set; }
        public int? Companyid { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? CancelCompanyName { get; set; }
        public string? CancelParentCompanyName { get; set; }
        public int? Contactstatusid { get; set; }
        public DateTime? Contactstatusdate { get; set; }
        public string? Zip { get; set; }
    }
}
