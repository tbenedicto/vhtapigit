﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CpixOrdersFinalallLoaded
    {
        public string? TourId { get; set; }
        public string? ProductId { get; set; }
        public string? MlsId { get; set; }
        public string? MlsName { get; set; }
        public string? ListingDate { get; set; }
        public string? ListingPrice { get; set; }
        public string? ListingType { get; set; }
        public string? Line1 { get; set; }
        public string? Line2 { get; set; }
        public string? City { get; set; }
        public string? StateCode { get; set; }
        public string? Zip { get; set; }
        public string? Bedrooms { get; set; }
        public string? FullBaths { get; set; }
        public string? HalfBaths { get; set; }
        public string? ListingAgent { get; set; }
        public string? Description { get; set; }
        public string? NotesToPhotographer { get; set; }
        public string? PhotographerId { get; set; }
        public string? CompletedOrderDate { get; set; }
        public string? PhotographerPay { get; set; }
        public string? AgentPay { get; set; }
        public string? CorporatePay { get; set; }
        public string? ContactName { get; set; }
        public string? ContactPhone { get; set; }
        public string? ContactNotes { get; set; }
        public string? SchduleTime { get; set; }
        public string? ContactLog { get; set; }
        public int? CosmoProductid { get; set; }
        public int? CosmoPhotogId { get; set; }
        public int? CosmoContactid { get; set; }
        public int? Loadstatusid { get; set; }
        public int? MissingAgentId { get; set; }
        public int? MissingPhotographerId { get; set; }
    }
}
