﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VizziagentToCosmomap
    {
        public string? Email { get; set; }
        public double? ContactId { get; set; }
    }
}
