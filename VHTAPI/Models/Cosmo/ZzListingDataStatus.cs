﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzListingDataStatus
    {
        public int ListingDataStatusId { get; set; }
        public string? StatusName { get; set; }
        public string? StatusDefinition { get; set; }
    }
}
