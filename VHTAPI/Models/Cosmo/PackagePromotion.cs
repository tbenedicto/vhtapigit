﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PackagePromotion
    {
        public int PackageId { get; set; }
        public string? PackagePromotionName { get; set; }
        public decimal? PackageOverRideDiscount { get; set; }
        public int? PackageOverRideImageQty { get; set; }
        public DateTime? PackagePromotionStartdate { get; set; }
        public DateTime? PackagePromotionEnddate { get; set; }
    }
}
