﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablSdryesterdayTotal
    {
        public string? TargetType { get; set; }
        public decimal? Actual { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
