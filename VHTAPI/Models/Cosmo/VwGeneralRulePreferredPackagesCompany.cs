﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwGeneralRulePreferredPackagesCompany
    {
        public string Title { get; set; } = null!;
        public int PackageId { get; set; }
        public string? InternalName { get; set; }
        public int? CompanyId { get; set; }
        public string? Company { get; set; }
        public int GeneralRuleId { get; set; }
        public string? Descr { get; set; }
    }
}
