﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class FloorPlanOrder
    {
        public int FloorPlanOrderId { get; set; }
        public int CmpcontactId { get; set; }
        public string? Cmpemail { get; set; }
        public int JobId { get; set; }
        public string ListingAddress { get; set; } = null!;
        public string AgentName { get; set; } = null!;
        public string? AdditionalNotes { get; set; }
        public string? DrawingsFolder { get; set; }
        public DateTime DateSubmitted { get; set; }
        public DateTime? EmailSentOn { get; set; }
        public bool? ManualSend { get; set; }
        public string? BatchId { get; set; }
        public int? Retries { get; set; }
        public bool? RetriesNotificationSent { get; set; }
    }
}
