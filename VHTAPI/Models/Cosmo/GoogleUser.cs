﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GoogleUser
    {
        public string UserId { get; set; } = null!;
        public int? ContactId { get; set; }
        public int? CollaboratorId { get; set; }
        public string? Email { get; set; }
    }
}
