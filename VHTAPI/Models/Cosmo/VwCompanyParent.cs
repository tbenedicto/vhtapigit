﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCompanyParent
    {
        public int CompanyId { get; set; }
        public int? TopParentCompanyId { get; set; }
    }
}
