﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderFormCategory
    {
        public int OrderFormCategoryId { get; set; }
        public int CategoryId { get; set; }
        public int RateSheet { get; set; }
        public string CategoryLabel { get; set; } = null!;
        public string OptionLabel { get; set; } = null!;
        public string AfterLabelContent { get; set; } = null!;
        public string BeforeLabelContent { get; set; } = null!;
        public string? OverrideSelection { get; set; }
        public Guid Rowguid { get; set; }
        public bool ProductMoreInfoEnable { get; set; }
        public string ProductMoreInfoLabel { get; set; } = null!;
        public string ProductMoreInfoLink { get; set; } = null!;
        public bool ProductMoreInfoHtmlEnable { get; set; }
        public string ProductMoreInfoHtml { get; set; } = null!;
    }
}
