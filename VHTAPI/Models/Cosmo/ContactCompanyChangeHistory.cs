﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactCompanyChangeHistory
    {
        public int ContactCompanyChangeHistoryId { get; set; }
        public int ContactId { get; set; }
        public int FromCompanyId { get; set; }
        public int ToCompanyId { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
