﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TicketTag
    {
        public int TicketTagId { get; set; }
        public string Description { get; set; } = null!;
    }
}
