﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MrstatusDaemon
    {
        public MrstatusDaemon()
        {
            Mrcomments = new HashSet<Mrcomment>();
            MrjobStatusLogs = new HashSet<MrjobStatusLog>();
        }

        public int MrstatusDaemonId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int RunStatusId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ZzRunStatus RunStatus { get; set; } = null!;
        public virtual ICollection<Mrcomment> Mrcomments { get; set; }
        public virtual ICollection<MrjobStatusLog> MrjobStatusLogs { get; set; }
    }
}
