﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingImportSetting
    {
        public int ListingId { get; set; }
        public int? FeedIdListingData { get; set; }
        public bool IsLinkActiveListingData { get; set; }
        public DateTime? DataLastUpdatedDate { get; set; }
        public int? FeedIdPhotos { get; set; }
        public bool IsLinkActivePhotos { get; set; }
        public DateTime? PhotosLastUpdatedDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
