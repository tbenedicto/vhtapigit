﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContactFirstJobDate
    {
        public int? ContactId { get; set; }
        public DateTime? FirstJobDate { get; set; }
    }
}
