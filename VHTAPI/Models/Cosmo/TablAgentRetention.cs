﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablAgentRetention
    {
        public int? ThisYear { get; set; }
        public int? ThisMonth { get; set; }
        public DateTime? FirstOfMonth { get; set; }
        public int ContactId { get; set; }
        public string? AgentName { get; set; }
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public DateTime? CompanyAddedDate { get; set; }
        public int? ParentCompId { get; set; }
        public string? ParentCompName { get; set; }
        public string? State { get; set; }
        public string? Territory { get; set; }
        public string? Ade { get; set; }
        public int? ListingsThisMonth { get; set; }
        public int? ListingLifetime { get; set; }
        public decimal? RevenueLifetime { get; set; }
        public int? JobsOver50Lifetime { get; set; }
        public int? ListingsCurrent6Month { get; set; }
        public int? ListingsRolling12Month { get; set; }
    }
}
