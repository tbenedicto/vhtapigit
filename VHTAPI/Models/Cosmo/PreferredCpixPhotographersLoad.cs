﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PreferredCpixPhotographersLoad
    {
        public double? Id { get; set; }
        public string? IdType { get; set; }
        public double? PhotographerId { get; set; }
        public string? ActivePhotographerName { get; set; }
        public string? Client { get; set; }
        public string? Vhtidentifier { get; set; }
        public string? IdType1 { get; set; }
        public string? PhotographerId1 { get; set; }
        public string? ActivePhotographerName1 { get; set; }
        public string? Client1 { get; set; }
        public string? PhotographerId2 { get; set; }
        public string? Descr { get; set; }
        public string? AgentId { get; set; }
        public int? Contactid { get; set; }
        public int? CvContactid { get; set; }
        public string? LoadStatus { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? Addedby { get; set; }
    }
}
