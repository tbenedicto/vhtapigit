﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PrismOffice
    {
        public string? CompanyName { get; set; }
        public string? StreetAddress { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public string? F6 { get; set; }
        public string? F7 { get; set; }
        public string? F8 { get; set; }
        public string? F9 { get; set; }
        public string? F10 { get; set; }
        public string? F11 { get; set; }
        public string? F12 { get; set; }
        public string? F13 { get; set; }
        public string? F14 { get; set; }
        public string? F15 { get; set; }
        public int? CosmoCompanyId { get; set; }
    }
}
