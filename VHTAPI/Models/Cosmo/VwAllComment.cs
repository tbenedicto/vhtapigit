﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllComment
    {
        public int CommentId { get; set; }
        public int FkparentId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public string? CommentCategory { get; set; }
        public string CommentSubject { get; set; } = null!;
        public string? CommentText { get; set; }
        public DateTime? CommentAddedDate { get; set; }
        public string? CommentRecipientType { get; set; }
        public bool IsCommentUnread { get; set; }
        public bool IsCommentAutomated { get; set; }
        public int CommentCategoryId { get; set; }
    }
}
