﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Rev2013To2016ParentCompanyCommercial
    {
        public string? CompanyName { get; set; }
        public decimal? Sold { get; set; }
    }
}
