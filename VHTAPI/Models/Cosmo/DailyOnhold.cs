﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DailyOnhold
    {
        public int JobId { get; set; }
        public int ListingId { get; set; }
        public string Flag { get; set; } = null!;
        public string? VhtjobId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentFirstname { get; set; }
        public string? CompanyName { get; set; }
        public string? PropertyAddress { get; set; }
        public string? PropertyAddress2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Mls { get; set; }
        public string? Mls2 { get; set; }
        public string? Development { get; set; }
        public int? ListPrice { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public DateTime? MediaArrivalDate { get; set; }
        public DateTime? DateFieldCompleted { get; set; }
        public DateTime? DatePostingSiteCompleted { get; set; }
        public DateTime? DateCdromcompleted { get; set; }
        public DateTime? DateProductionCompleteEmailSent { get; set; }
        public DateTime? DateProductionCompleted { get; set; }
        public DateTime? DateAccountingCompleted { get; set; }
        public DateTime? DateClosed { get; set; }
        public string? Priority { get; set; }
        public string? Status { get; set; }
        public string? StatusReason { get; set; }
        public string? VideographerLastName { get; set; }
        public string? VideographerFirstName { get; set; }
        public string? OrderSource { get; set; }
        public string IsContentPartial { get; set; } = null!;
        public string? StillFlatUrl { get; set; }
        public string? VirtualUrl { get; set; }
        public string? VirtualUrladdt { get; set; }
        public string? JobCreator { get; set; }
        public decimal? AgentPay { get; set; }
        public decimal? CorpPay { get; set; }
        public decimal? TotalPrice { get; set; }
        public string? ImageSpecialist { get; set; }
        public string? AgentPhone { get; set; }
        public int? NumDistinctProducts { get; set; }
        public string? FlaggedInd { get; set; }
        public DateTime? FlaggedDate { get; set; }
        public string? FlaggedBy { get; set; }
        public DateTime? FlagFollowupDate { get; set; }
        public DateTime? DateVideoCompleted { get; set; }
        public string FlagCustomerFirstOrder { get; set; } = null!;
        public string? ContactClientStatus { get; set; }
        public string? ContactVipflag { get; set; }
        public string? ContactTlcflag { get; set; }
        public DateTime? CommStartDate { get; set; }
        public string? AdeAccountRep { get; set; }
        public string? RegionalSales { get; set; }
        public string? CustomerServiceRep { get; set; }
        public DateTime? ParentAddedDate { get; set; }
        public DateTime? ParentStartDate { get; set; }
        public int? MaxFloor { get; set; }
        public int? MaxPrem { get; set; }
        public DateTime? LoadDate { get; set; }
        public string? PreferredVideographerFirstName { get; set; }
        public string? PreferredVideographerLastName { get; set; }
        public string? ProjectedNew { get; set; }
        public DateTime? NewRenew1stOrderdate { get; set; }
        public string? Days { get; set; }
        public string? Contactgroup { get; set; }
        public string? HasExpress { get; set; }
    }
}
