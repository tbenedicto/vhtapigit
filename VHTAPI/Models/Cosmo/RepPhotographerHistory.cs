﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RepPhotographerHistory
    {
        public int? ContactId { get; set; }
        public DateTime _1stStartdate { get; set; }
        public int? CompanyId { get; set; }
        public int? RepPhotographer { get; set; }
        public DateTime? SalesStartDate { get; set; }
        public int? ParentCompanyId { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public DateTime? Addeddate { get; set; }
    }
}
