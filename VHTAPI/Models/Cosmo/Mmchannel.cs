﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Mmchannel
    {
        public int MmchannelId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public bool IsActive { get; set; }
        public string MmchannelUserName { get; set; } = null!;
        public string MmchannelPassword { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
