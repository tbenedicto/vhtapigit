﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdeDailyPullDatum
    {
        public string? Ade { get; set; }
        public string? Territory { get; set; }
        public string? RegionalSales { get; set; }
        public DateTime? CosmoAddedDate { get; set; }
        public string? ContactHistoryType { get; set; }
        public decimal? RevenueLifetime { get; set; }
        public string? ContactCode { get; set; }
        public int ContactId { get; set; }
        public string? AgentName { get; set; }
        public int? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? Tourlink { get; set; }
        public string? ParentCompanyName { get; set; }
        public string? AgentStatusDescr { get; set; }
        public int? CompanyCountNew { get; set; }
        public int? CompanyCount2ndOrderDate { get; set; }
        public int? CompanyCountRenew { get; set; }
        public DateTime _1stStartdate { get; set; }
        public int? Months { get; set; }
        public string? Year { get; set; }
        public DateTime? NewAgentAddedDate { get; set; }
        public string? CompanyType { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public string? LocalSales { get; set; }
        public int? Jobover5012Month { get; set; }
        public DateTime? _2ndOrderDate { get; set; }
        public int? Listingid { get; set; }
        public string? HowContactus { get; set; }
        public string? AddedBy { get; set; }
        public string AccountType { get; set; } = null!;
        public string? State { get; set; }
        public int? CompletedOnJobid { get; set; }
        public string? CertificateNumber { get; set; }
        public decimal? AgentPay { get; set; }
        public decimal? CorporatePay { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? CvPay { get; set; }
    }
}
