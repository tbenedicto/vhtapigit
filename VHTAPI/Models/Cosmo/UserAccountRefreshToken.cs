﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class UserAccountRefreshToken
    {
        public int RefreshTokenId { get; set; }
        public int UserAccountId { get; set; }
        public string RefreshToken { get; set; } = null!;
        public Guid ClientId { get; set; }
        public DateTime ExpiryDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
