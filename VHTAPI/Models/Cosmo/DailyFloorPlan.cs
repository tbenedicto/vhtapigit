﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DailyFloorPlan
    {
        public int JobId { get; set; }
        public int ListingId { get; set; }
        public string Flag { get; set; } = null!;
        public string? VhtjobId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentFirstname { get; set; }
        public string? CompanyName { get; set; }
        public string? PropertyAddress { get; set; }
        public string? PropertyAddress2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public DateTime? MediaArrivalDate { get; set; }
        public DateTime? DateFieldCompleted { get; set; }
        public DateTime? DateClosed { get; set; }
        public string? Priority { get; set; }
        public string? Status { get; set; }
        public string? StatusReason { get; set; }
        public string? StillFlatUrl { get; set; }
        public string? VirtualUrl { get; set; }
        public string? VirtualUrladdt { get; set; }
        public string? JobCreator { get; set; }
        public string? AdeAccountRep { get; set; }
        public string? RegionalSales { get; set; }
        public string? CustomerServiceRep { get; set; }
        public DateTime? ParentAddedDate { get; set; }
        public DateTime? ParentStartDate { get; set; }
        public int? MaxFloor { get; set; }
        public int? MaxPrem { get; set; }
        public DateTime? LoadDate { get; set; }
        public DateTime? Loaddate2 { get; set; }
        public string? FloorStageStatus { get; set; }
        public DateTime? DateProductionCompleteEmailSent { get; set; }
    }
}
