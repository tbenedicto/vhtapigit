﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablCvcoverageByRadiusTb
    {
        public double? MilesFromCv { get; set; }
        public string ToZipCode { get; set; } = null!;
        public string Ziptype { get; set; } = null!;
        public decimal ZipLat { get; set; }
        public decimal ZipLong { get; set; }
        public string? AddressLine1 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? Cvname { get; set; }
        public string? PhotographerNetwork { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
