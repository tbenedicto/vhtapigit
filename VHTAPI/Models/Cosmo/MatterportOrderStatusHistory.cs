﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MatterportOrderStatusHistory
    {
        public int MatterportOrderStatusHistoryId { get; set; }
        public int JobId { get; set; }
        public string? MatterportId { get; set; }
        public int MatterportOrderProcessStatusId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
