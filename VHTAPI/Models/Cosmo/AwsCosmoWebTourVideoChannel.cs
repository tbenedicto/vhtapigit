﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebTourVideoChannel
    {
        public int TourId { get; set; }
        public int VideoChannelId { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
