﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageWorksCompany
    {
        public int ParentCompanyId { get; set; }
        public int CompanyStatusId { get; set; }
        public bool IsVideoClient { get; set; }
        public bool IsPropertyPagesClient { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowAddedBy { get; set; }
        public bool IsImageWorksMyVhtactivated { get; set; }
        public Guid Rowguid { get; set; }
        public int MaxPhotosInVideo { get; set; }
        public string? OverlayTemplate { get; set; }
        public bool OptimizeUserUploadedImages { get; set; }
        public string? CloudQueueName { get; set; }
        public bool? SendAutomatedNewListingEmail { get; set; }
        public bool? DistributeWithoutPropertyDetailPageUrl { get; set; }
        public bool? DisplayOfficeUrlinVideoSiteDesc { get; set; }
    }
}
