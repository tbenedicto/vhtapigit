﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AutoSelectRoomGroupList
    {
        public int RowId { get; set; }
        public int RoomGroupListId { get; set; }
        public string RoomGroupName { get; set; } = null!;
    }
}
