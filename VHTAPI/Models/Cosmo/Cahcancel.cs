﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Cahcancel
    {
        public string? PropertyUnitId { get; set; }
        public string? PropertyStatus { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zipcode { get; set; }
        public string? County { get; set; }
        public string? Lockbox { get; set; }
        public string? AcquisitionCompany { get; set; }
        public string? PropertyManager { get; set; }
        public string? Portfolio { get; set; }
        public string? UnitStatus { get; set; }
        public string? ResidentStatus { get; set; }
    }
}
