﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GothamOffice
    {
        public int? RepSalesNational { get; set; }
        public string? Ade { get; set; }
        public int? Mmsofficeid { get; set; }
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? DisplayAs { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? Parentcompanyname { get; set; }
        public string? Territory { get; set; }
        public string? Commercial { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
    }
}
