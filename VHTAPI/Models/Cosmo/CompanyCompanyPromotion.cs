﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyCompanyPromotion
    {
        public int CompanyId { get; set; }
        public int CompanyPromotionId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual Company Company { get; set; } = null!;
        public virtual ZzCompanyPromotion CompanyPromotion { get; set; } = null!;
    }
}
