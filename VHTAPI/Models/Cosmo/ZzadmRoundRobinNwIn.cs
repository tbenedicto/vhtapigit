﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzadmRoundRobinNwIn
    {
        public int RoundRobinId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SalesPersonId { get; set; }
    }
}
