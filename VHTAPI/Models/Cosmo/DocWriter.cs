﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DocWriter
    {
        public int DocWriterId { get; set; }
        public int SortId { get; set; }
        public string ClassName { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
