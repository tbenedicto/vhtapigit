﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkupAgentLoad20190910FullCsv
    {
        public double? Companyid { get; set; }
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? DirectPhone { get; set; }
        public string? Extension { get; set; }
        public string? Mobile { get; set; }
        public string? Home { get; set; }
        public string? Fax { get; set; }
        public string? Email { get; set; }
        public string? PersonalWebsite { get; set; }
        public int? ContactStatusId { get; set; }
        public string? SecondaryEmail { get; set; }
    }
}
