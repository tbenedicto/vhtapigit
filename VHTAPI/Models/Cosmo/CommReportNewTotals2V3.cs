﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CommReportNewTotals2V3
    {
        public int? Year { get; set; }
        public int? Month { get; set; }
        public string? Territory { get; set; }
        public string? StateId { get; set; }
        public decimal? TotalRev { get; set; }
        public int? TotalOrders { get; set; }
        public int? TotalClients { get; set; }
        public decimal? Pcrev { get; set; }
        public decimal? TrailingYtdNewRevenue { get; set; }
        public decimal? NewRev { get; set; }
        public int? NewOrders { get; set; }
        public int? NewClients { get; set; }
        public decimal? RenewRev { get; set; }
        public int? ReNewOrders { get; set; }
        public int? ReNewClients { get; set; }
        public decimal? TotNewRenewRev { get; set; }
        public int? TotNewRenewOrders { get; set; }
        public int? TotNewRenewClients { get; set; }
        public decimal? TrailingYtdNewRenewRevenue { get; set; }
        public decimal? Trailing11monthRevNewRenew { get; set; }
        public decimal? CarryOverTrailingRevNewRenew { get; set; }
        public int? UniqAgents { get; set; }
    }
}
