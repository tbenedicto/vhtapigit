﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobChange
    {
        public int JobChangeId { get; set; }
        public int JobId { get; set; }
        public string Comment { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
