﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MarketTargetsProjection
    {
        public string? Market { get; set; }
        public int? Target { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string? Type { get; set; }
    }
}
