﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingOption
    {
        public int RowId { get; set; }
        public bool IsSystemDefault { get; set; }
        public bool IsInherited { get; set; }
        public int CompanyId { get; set; }
        public int ContactId { get; set; }
        public int ListingId { get; set; }
        public bool ShowCompanyLogo { get; set; }
        public bool ShowCustomerPhoto { get; set; }
        public bool ShowCompanyName { get; set; }
        public bool ShowCustomerName { get; set; }
        public bool ShowCustomerPhone { get; set; }
        public bool ShowContactMe { get; set; }
        public bool ShowViewWebsite { get; set; }
        public bool ShowPrintableBrochure { get; set; }
        public bool ShowImageWorks { get; set; }
        public bool ShowEnlargeVirtualTour { get; set; }
        public bool ShowViewMyListings { get; set; }
        public bool ShowFaq { get; set; }
        public bool ShowMapToProperty { get; set; }
        public bool ShowSendToAfriend { get; set; }
        public bool ShowDownloadableTour { get; set; }
        public bool ShowInViewMyListings { get; set; }
        public Guid Rowguid { get; set; }
        public bool ShowSchoolInfo { get; set; }
        public bool ShowMortgageCalculator { get; set; }
        public bool ShowScheduleShowing { get; set; }
        public bool ShowPropertyDetails { get; set; }
    }
}
