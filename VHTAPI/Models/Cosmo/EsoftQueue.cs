﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class EsoftQueue
    {
        public int EsoftQueueId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int JobId { get; set; }
        public string VhtjobId { get; set; } = null!;
        public int ListingId { get; set; }
        public string PropertyState { get; set; } = null!;
        public string ProcessingState { get; set; } = null!;
        public string? EsoftPremiumBatchId { get; set; }
        public string? EsoftSignatureBatchId { get; set; }
        public bool Result { get; set; }
        public int PremiumPhotosCount { get; set; }
        public int SignaturePhotosCount { get; set; }
        public int? TotalPhotosReceivedFromEsoft { get; set; }
        public int ProofsSignaturePhotosCount { get; set; }
        public int ProofsPremiumPhotosCount { get; set; }
        public bool? IsCpixStillsDone { get; set; }
        public bool? IsCpixPanoDone { get; set; }
        public string? Esoft360BatchId { get; set; }
        public bool? OnlyContainOnePhotoType { get; set; }
        public bool? ContainsSilver { get; set; }
        public bool? ContainsGold { get; set; }
        public bool? ContainsPlatinum { get; set; }
        public string? EsoftVirtualStagingBatchId { get; set; }
        public string? EsoftVirtualTwilightBatchId { get; set; }
        public string? ReferenceNumber { get; set; }
        public string? EsoftPlatinumBatchId { get; set; }
        public bool? ContainsDrone { get; set; }
    }
}
