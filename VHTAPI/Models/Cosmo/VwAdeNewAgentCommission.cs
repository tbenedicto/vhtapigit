﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAdeNewAgentCommission
    {
        public DateTime _1stStartdate { get; set; }
        public string? AgentName { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompanyName { get; set; }
        public string? AccountManager { get; set; }
        public int ContactId { get; set; }
        public DateTime? NewAgentAdddedDate { get; set; }
        public int? ContactClientStatusId { get; set; }
        public int? CompanyId { get; set; }
        public int? RepSalesNational { get; set; }
        public DateTime SalesStartDate { get; set; }
        public int? ParentCompanyId { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public int? Jobover5012Month { get; set; }
        public DateTime? _2ndOrderDate { get; set; }
        public string? CompanyType { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public DateTime? CosmoAddedDate { get; set; }
        public string? ContactHistoryType { get; set; }
        public string? Territory { get; set; }
        public string? Tourlink { get; set; }
    }
}
