﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SpWho5Output
    {
        public short SessionId { get; set; }
        public string Status { get; set; } = null!;
        public short? BlkBy { get; set; }
        public string? WaitType { get; set; }
        public string WaitResource { get; set; } = null!;
        public int? WaitM { get; set; }
        public int CpuTime { get; set; }
        public long LogicalReads { get; set; }
        public long Reads { get; set; }
        public long Writes { get; set; }
        public int? ElapsM { get; set; }
        public string? StatementText { get; set; }
        public string? CommandText { get; set; }
        public string Command { get; set; } = null!;
        public string LoginName { get; set; } = null!;
        public string? HostName { get; set; }
        public string? ProgramName { get; set; }
        public DateTime? LastRequestEndTime { get; set; }
        public DateTime LoginTime { get; set; }
        public int OpenTransactionCount { get; set; }
        public DateTime? SpWho5Time { get; set; }
    }
}
