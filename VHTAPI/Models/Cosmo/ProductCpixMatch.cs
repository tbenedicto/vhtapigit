﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProductCpixMatch
    {
        public int? CosmoProductId { get; set; }
        public int? PixProductId { get; set; }
    }
}
