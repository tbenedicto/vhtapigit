﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NewContactTerritoryHistoryInvisoLoad
    {
        public int? ContactId { get; set; }
        public string _1stStartdate { get; set; } = null!;
        public int ContactClientStatusId { get; set; }
        public int? CompanyId { get; set; }
        public int? RepSalesNational { get; set; }
        public string SalesStartDate { get; set; } = null!;
        public int? ParentCompanyId { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public int Jobover5012Month { get; set; }
        public DateTime Addeddate { get; set; }
        public DateTime? _2ndOrderDate { get; set; }
        public string ContactHistoryType { get; set; } = null!;
        public DateTime? Completedon1sttimeCompany { get; set; }
        public int? Listingid { get; set; }
        public Guid? Rowguid { get; set; }
        public int CompanysubsidiaryId { get; set; }
        public int? CompletedOnJobid { get; set; }
        public int? _2ndOrderJobid { get; set; }
        public int VhtrelationshipId { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
    }
}
