﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderLogExtraDatum
    {
        public long OrderLogId { get; set; }
        public string? Office { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? PhotographyLocation { get; set; }
        public bool? BillCconFile { get; set; }
        public string? FreeFormOrderInfo { get; set; }
    }
}
