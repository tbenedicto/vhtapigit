﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class KeyesVhtAgentRosterImport
    {
        public string? Wwid { get; set; }
        public double? PersonId { get; set; }
        public string? MetroId { get; set; }
        public string? OfficeId { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentMiddleName { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentPreferredName { get; set; }
        public string? AgentPhoneNumber { get; set; }
        public string? AgentFaxNumber { get; set; }
        public string? AgentEmail { get; set; }
        public string? AgentTitle { get; set; }
        public string? AgentSalesType { get; set; }
        public string? AgentPhoto { get; set; }
        public string? PreviewsFlag { get; set; }
        public string? OfficeName { get; set; }
        public string? OfficeAddress { get; set; }
        public string? OfficeCity { get; set; }
        public string? OfficeState { get; set; }
        public double? OfficeZip { get; set; }
        public string? AgentWebsiteUrl { get; set; }
        public string? Ssoguid { get; set; }
        public DateTime? DateEntered { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
