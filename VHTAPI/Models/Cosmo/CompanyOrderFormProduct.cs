﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyOrderFormProduct
    {
        public int CompanyOrderFormProductId { get; set; }
        public int CompanyOrderFormId { get; set; }
        public int ProductId { get; set; }
        public int OrderFormProductCategoryId { get; set; }
        public int? SortId { get; set; }
        public string? MarketingProdName { get; set; }
        public int CompanyOrderFormShowRateTypeId { get; set; }
        public string? RateCustomText { get; set; }
        public int? CompanyOrderFormPackageId { get; set; }
        public bool UseRateCustomText { get; set; }
        public bool UseAsAdditionalInfo { get; set; }
        public bool UseWithQuantity { get; set; }
        public string? ProductInfoContent { get; set; }
    }
}
