﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzMusicGenre
    {
        public int MusicGenreId { get; set; }
        public int SortId { get; set; }
        public string? Descr { get; set; }
        public Guid Rowguid { get; set; }
    }
}
