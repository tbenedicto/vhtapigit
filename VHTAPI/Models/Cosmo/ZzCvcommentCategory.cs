﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCvcommentCategory
    {
        public int CvcommentCategoryId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public int CommentRecipientId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
