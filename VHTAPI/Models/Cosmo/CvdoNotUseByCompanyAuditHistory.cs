﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvdoNotUseByCompanyAuditHistory
    {
        public int CvdoNotUseByCompanyAuditHistoryId { get; set; }
        public string AuditType { get; set; } = null!;
        public DateTime AuditDate { get; set; }
        public string AuditBy { get; set; } = null!;
        public string Reason { get; set; } = null!;
        public int VideographerContactId { get; set; }
        public Guid RowId { get; set; }
        public int CompanyId { get; set; }
    }
}
