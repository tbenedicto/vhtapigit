﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwTablIssueTracker
    {
        public int IssueId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public int IssueCategoryId { get; set; }
        public string? Companyname { get; set; }
        public string? Parentcompanyname { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string SubCategoryDescr { get; set; } = null!;
        public string StatusDescr { get; set; } = null!;
        public string SourceDescr { get; set; } = null!;
        public DateTime IssueStatusDate { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? Descr { get; set; }
    }
}
