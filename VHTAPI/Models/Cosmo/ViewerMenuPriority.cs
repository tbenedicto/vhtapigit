﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ViewerMenuPriority
    {
        public int PriorityId { get; set; }
        public int G3codeId { get; set; }
        public int PriorityOrder { get; set; }
        public int ViewerMenuId { get; set; }
    }
}
