﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class LogMediaExportToMm
    {
        public int LogId { get; set; }
        public decimal ExportBatchId { get; set; }
        public int ListingId { get; set; }
        public int MmsloginId { get; set; }
        public bool DeleteExistingImgs { get; set; }
        public string? FileUrl { get; set; }
        public bool Successful { get; set; }
        public string? Result { get; set; }
        public DateTime DateAdded { get; set; }
        public int? JobId { get; set; }
        public bool? SendAsSelected { get; set; }
    }
}
