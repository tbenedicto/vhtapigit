﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzPromoCode
    {
        public int PromoCodeId { get; set; }
        public string PromoCode { get; set; } = null!;
        public string Descr { get; set; } = null!;
        public bool IsActive { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
