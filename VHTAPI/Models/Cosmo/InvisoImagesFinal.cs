﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class InvisoImagesFinal
    {
        public string OrderContainerId { get; set; } = null!;
        public string OrderId { get; set; } = null!;
        public string OrderComponentId { get; set; } = null!;
        public string? AssetId { get; set; }
        public string? AssetFilename { get; set; }
        public string? AssetLabels { get; set; }
        public string? AssetContentType { get; set; }
        public string? AssetUrl { get; set; }
        public string? AssetCreatedAt { get; set; }
        public string? AssetState { get; set; }
        public string Index { get; set; } = null!;
        public DateTime? Addeddate { get; set; }
        public bool? HasProcessed { get; set; }
        public string? ProcessStage { get; set; }
    }
}
