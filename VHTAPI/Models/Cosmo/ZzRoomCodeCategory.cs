﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzRoomCodeCategory
    {
        public int RoomcodeCategoryId { get; set; }
        public string RoomcodeCategoryName { get; set; } = null!;
        public int SortId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
