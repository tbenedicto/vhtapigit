﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class WorkItemEvent
    {
        public int WorkItemEventId { get; set; }
        public string WorkItemNumber { get; set; } = null!;
        public DateTime EventDate { get; set; }
        public int EventActionId { get; set; }
        public string? Remarks { get; set; }
        public string? Info { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ZzEventAction EventAction { get; set; } = null!;
    }
}
