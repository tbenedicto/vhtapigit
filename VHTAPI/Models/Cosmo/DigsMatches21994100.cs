﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class DigsMatches21994100
    {
        public string? Dvd { get; set; }
        public string? Filename { get; set; }
        public string? VhtImageActualImage { get; set; }
        public string? ZillowImageActualImage { get; set; }
        public string? ZilPath { get; set; }
        public double? ListingId { get; set; }
        public double? Roomcode { get; set; }
    }
}
