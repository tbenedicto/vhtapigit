﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessZzDistPoint
    {
        public int ProcessId { get; set; }
        public int DistributionPointId { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime? DateRemoved { get; set; }
        public Guid Rowguid { get; set; }
    }
}
