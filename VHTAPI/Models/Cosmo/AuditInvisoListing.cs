﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AuditInvisoListing
    {
        public int MasterListingId { get; set; }
        public int MasterJobid { get; set; }
        public int? InvisoTourId { get; set; }
    }
}
