﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablCvAddressCoveredZip
    {
        public int Contactid { get; set; }
        public string? ContactStatusReasonNote { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? DisplayAs { get; set; }
        public string? CvAddress { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
        public string CvCoveredZip { get; set; } = null!;
        public string? Status { get; set; }
        public string? Reason { get; set; }
    }
}
