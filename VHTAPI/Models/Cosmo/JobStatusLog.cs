﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobStatusLog
    {
        public int JobStatusLogId { get; set; }
        public int JobId { get; set; }
        public DateTime JobStatusDate { get; set; }
        public string UpdatedBy { get; set; } = null!;
        public int JobStatusId { get; set; }
        public int JobStatusReasonId { get; set; }
        public string JobStatusNote { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public DateTime? FollowupDate { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
    }
}
