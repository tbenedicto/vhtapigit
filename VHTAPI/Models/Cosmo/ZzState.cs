﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzState
    {
        /// <summary>
        /// 2-Letter State/Territory Code
        /// </summary>
        public string StateId { get; set; } = null!;
        /// <summary>
        /// Full Name of theState/Territory Code
        /// </summary>
        public string? StateName { get; set; }
        /// <summary>
        /// SortID
        /// </summary>
        public int SortId { get; set; }
        /// <summary>
        /// Date the row was inserted
        /// </summary>
        public DateTime? AddedDate { get; set; }
        /// <summary>
        /// Who inserted the row
        /// </summary>
        public string? AddedBy { get; set; }
        /// <summary>
        /// Date the row was updated last
        /// </summary>
        public DateTime? ModifiedDate { get; set; }
        /// <summary>
        /// Who updated the row last
        /// </summary>
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
