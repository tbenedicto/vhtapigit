﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AvaSmsthread
    {
        public int AvaSmsthreadId { get; set; }
        public string? ThreadId { get; set; }
        public string? AgentPhone { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? JobId { get; set; }
        public string? Status { get; set; }
        public string? ApptDateSelected { get; set; }
        public string? ApptTimeSelected { get; set; }
        public Guid Rowguid { get; set; }
    }
}
