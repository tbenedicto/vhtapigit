﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CheckMediaSent3monthsBack
    {
        public int CompanyId { get; set; }
        public DateTime? TaskMediaReceivedCompletedOn { get; set; }
    }
}
