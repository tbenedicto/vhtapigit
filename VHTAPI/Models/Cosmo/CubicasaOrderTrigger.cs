﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CubicasaOrderTrigger
    {
        public int CubicasaOrderTriggerId { get; set; }
        public int JobId { get; set; }
        public int CubicasaOrderTriggerReasonId { get; set; }
        public DateTime TriggerEventDate { get; set; }
        public string? CubicasaOrderId { get; set; }
        public int? OldCvId { get; set; }
        public int? NewCvId { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
