﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobCvreAssign
    {
        public int? JobId { get; set; }
        public int? ListingId { get; set; }
        public int? JobStatusId { get; set; }
        public DateTime? JobStatusDate { get; set; }
        public int? JobStatusReasonId { get; set; }
        public string? JobStatusNote { get; set; }
        public int? JobPriorityId { get; set; }
        public string? VhtjobId { get; set; }
        public int? BillToMethodId { get; set; }
        public DateTime? DigdueDate { get; set; }
        public int? DigeditorId { get; set; }
        public int? DigstationId { get; set; }
        public int? DignumVirtualKeysUsed { get; set; }
        public string? DigstillUrl { get; set; }
        public string? DigvirtualUrl { get; set; }
        public DateTime? DigdateCdvhtdiskCompleted { get; set; }
        public int? VideditorId { get; set; }
        public int? VidstationId { get; set; }
        public string? Vidurl { get; set; }
        public DateTime? VidmediaArrivalDate { get; set; }
        public DateTime? ViddueDate { get; set; }
        public int? VidlinkMadeById { get; set; }
        public DateTime? VidpostDateLinkSent { get; set; }
        public string? VidlinkSentTo { get; set; }
        public int? CloserId { get; set; }
        public int? VideoEditorId { get; set; }
        public int? VideographerContactId { get; set; }
        public int? ContactCreditCardId { get; set; }
        public decimal? MileageRate { get; set; }
        public int? MileageQuantity { get; set; }
        public int? PreviousJobStatusId { get; set; }
        public int? PreviousJobPriorityId { get; set; }
        public string? CdlabelDupNum { get; set; }
        public string? CdlabelInfo { get; set; }
        public string? CertificateNumber { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public int? JobSourceId { get; set; }
        public string? DigvirtualUrladditional { get; set; }
        public string? Cvinstructions { get; set; }
        public int? CdromlabelStyleId { get; set; }
        public decimal? Cvmiscellaneous { get; set; }
        public DateTime? FollowupDate { get; set; }
        public string? FollowupNote { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public string? CustomerJobId { get; set; }
        public Guid? Rowguid { get; set; }
        public bool? FlaggedInd { get; set; }
        public DateTime? FlaggedDate { get; set; }
        public string? FlaggedBy { get; set; }
        public DateTime? FlagFollowupDate { get; set; }
        public int? ScriptwriterId { get; set; }
        public int? VoiceoverId { get; set; }
        public string? ScriptApprovalNotes { get; set; }
        public int? MusicId { get; set; }
        public bool? FlagCustomerFirstOrder { get; set; }
        public DateTime? ReassignedVideographerDate { get; set; }
        public string? CampaignCode { get; set; }
        public Guid? CampaignGuid { get; set; }
        public string? CampaignSource { get; set; }
        public int? CampaignSourceId { get; set; }
        public int? CampaignId { get; set; }
        public int? MmsorderId { get; set; }
        public int? MmsmediaTypeId { get; set; }
        public string? Ddltype { get; set; }
        public DateTime? AuditGetdate { get; set; }
        public int JobCvreassignId { get; set; }
    }
}
