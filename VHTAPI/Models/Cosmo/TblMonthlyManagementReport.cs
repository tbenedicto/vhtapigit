﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TblMonthlyManagementReport
    {
        public DateTime? MonthOfManagementReport { get; set; }
        public int? VideoListing { get; set; }
        public int? PhotoFullServ { get; set; }
        public int? PhotoIw { get; set; }
        public int? PhotoProcessedIw { get; set; }
        public int? ActiveListings { get; set; }
        public int? VideoIw { get; set; }
        public int? VideoIwnewAgent { get; set; }
    }
}
