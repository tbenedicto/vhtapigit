﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SecurityGroupMember
    {
        public int SecurityGroupId { get; set; }
        public int SecurityUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual SecurityGroup SecurityGroup { get; set; } = null!;
        public virtual SecurityUser SecurityUser { get; set; } = null!;
    }
}
