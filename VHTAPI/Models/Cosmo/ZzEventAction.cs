﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzEventAction
    {
        public ZzEventAction()
        {
            WorkItemEvents = new HashSet<WorkItemEvent>();
        }

        public int EventActionId { get; set; }
        public string Descr { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<WorkItemEvent> WorkItemEvents { get; set; }
    }
}
