﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AllRevByResourceId
    {
        public double? OrderContainerId { get; set; }
        public double? BookedResourceId { get; set; }
        public double? TotalRev { get; set; }
    }
}
