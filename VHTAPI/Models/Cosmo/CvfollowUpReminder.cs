﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvfollowUpReminder
    {
        public int CvfollowUpReminderId { get; set; }
        public int JobId { get; set; }
        public DateTime FollowUpDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
