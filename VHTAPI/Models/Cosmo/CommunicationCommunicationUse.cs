﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CommunicationCommunicationUse
    {
        public int RowId { get; set; }
        public int CommunicationId { get; set; }
        public int CommunicationUseId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public Guid Rowguid { get; set; }
    }
}
