﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkJbSheet1
    {
        public string? PropertiesCorporate { get; set; }
        public string? F2 { get; set; }
        public string? F3 { get; set; }
        public string? AmerivestRealtyCorporate { get; set; }
        public string? F5 { get; set; }
        public string? F6 { get; set; }
        public string? CompanyName { get; set; }
    }
}
