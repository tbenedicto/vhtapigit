﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzPromoCodeDetail
    {
        public int PromoCodeDetailId { get; set; }
        public int PromoCodeId { get; set; }
        public int Category { get; set; }
        public decimal Amount { get; set; }
        public Guid Rowguid { get; set; }
    }
}
