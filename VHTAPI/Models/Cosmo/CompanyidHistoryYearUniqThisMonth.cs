﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyidHistoryYearUniqThisMonth
    {
        public int? Myyear { get; set; }
        public int? Mymonth { get; set; }
        public int? UniqAgents { get; set; }
        public int? Companyid { get; set; }
    }
}
