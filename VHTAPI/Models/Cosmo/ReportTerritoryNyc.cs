﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ReportTerritoryNyc
    {
        public double? ZipCode { get; set; }
        public string? Territory { get; set; }
        public int? RepSalesNational { get; set; }
        public string? Zip { get; set; }
    }
}
