﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TempActiveObject
    {
        public string Name { get; set; } = null!;
        public int ObjectId { get; set; }
        public string? TypeDesc { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Database { get; set; } = null!;
        public DateTime? LastExecutionTime { get; set; }
        public DateTime? CachedTime { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
