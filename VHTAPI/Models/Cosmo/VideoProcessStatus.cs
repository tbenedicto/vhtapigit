﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VideoProcessStatus
    {
        public int JobId { get; set; }
        public int EsoftPostingProcessStatusId { get; set; }
        public string? EsoftReference { get; set; }
        public string? EsoftBatchId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public string? Note { get; set; }
    }
}
