﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NewContactTerritoryHistoryMnb
    {
        public int ContactId { get; set; }
        public DateTime _1stStartdate { get; set; }
        public int? ContactClientStatusId { get; set; }
        public int? CompanyId { get; set; }
        public int? RepSalesNational { get; set; }
        public DateTime SalesStartDate { get; set; }
        public int? ParentCompanyId { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public int? Jobover5012Month { get; set; }
        public DateTime? Addeddate { get; set; }
        public DateTime? _2ndOrderDate { get; set; }
        public string? ContactHistoryType { get; set; }
    }
}
