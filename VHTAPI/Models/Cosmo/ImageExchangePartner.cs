﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageExchangePartner
    {
        public Guid PartnerId { get; set; }
        public string PartnerName { get; set; } = null!;
        public string PasswordHash { get; set; } = null!;
        public string PasswordSalt { get; set; } = null!;
        public bool IsActive { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public string ModifiedBy { get; set; } = null!;
        public byte[]? TimeStampField { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
