﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablVhtselectCoverageRange
    {
        public double? MilesFromCv { get; set; }
        public string Zipcode { get; set; } = null!;
        public decimal ZipLat { get; set; }
        public decimal ZipLong { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? ApplicationRating { get; set; }
        public string? ApplicationStatus { get; set; }
        public string? LastActivity { get; set; }
        public DateTime? LastModified { get; set; }
        public string? Email { get; set; }
        public string? City { get; set; }
        public string? StateProvince { get; set; }
        public string? Cvzip { get; set; }
        public DateTime? CreateDate { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
