﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwListingG3codeUpdate
    {
        public int ListingId { get; set; }
        public int G3codeId { get; set; }
        public int? RealG3codeId { get; set; }
    }
}
