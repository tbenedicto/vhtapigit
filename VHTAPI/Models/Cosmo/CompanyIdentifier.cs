﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyIdentifier
    {
        public int CompanyIdentifierId { get; set; }
        public int CompanyId { get; set; }
        public int CompanyIdentifierTypeId { get; set; }
        public string Descr { get; set; } = null!;
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public Guid Rowguid { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
    }
}
