﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AlphaNumber
    {
        public int RowId { get; set; }
        public string TheValue { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
