﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AdmCompanyMap
    {
        public string? AccountName { get; set; }
        public string? CompanyId { get; set; }
        public double? OfOffices { get; set; }
        public double? OfAgents { get; set; }
        public double? OfAgentCustomersPast12Months { get; set; }
        public double? GrossAgentUse { get; set; }
        public double? YtdNewAgents { get; set; }
        public double? YtdTotalAgentsNewRenew { get; set; }
        public double? YtdTotalRevNewRenew { get; set; }
        public double? YtdTrailingRev { get; set; }
        public double? YtdPcRev { get; set; }
        public double? YtdRev { get; set; }
        public double? Rolling12MonthRev { get; set; }
        public DateTime? DateOfLastRateIncrease { get; set; }
        public string? BillingAddressLine1 { get; set; }
        public string? BillingStateProvince { get; set; }
        public DateTime? LastActivity { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string? Adm { get; set; }
        public double? SalesId { get; set; }
    }
}
