﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GothamMonthlyClientRevenue
    {
        public string? TimePeriod { get; set; }
        public string? Company { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public int MmsloginId { get; set; }
        public int? ContactId { get; set; }
        public int? OfficeId { get; set; }
        public string? OfficeAddress { get; set; }
        public string? OfficeCity { get; set; }
        public string? OfficeState { get; set; }
        public string? OfficeZip { get; set; }
        public string? BusinessPhone { get; set; }
        public string? EmailAddress { get; set; }
        public decimal? AmountSpent { get; set; }
        public int? NumListings { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
    }
}
