﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class HullUserevent
    {
        public string Id { get; set; } = null!;
        public string UserId { get; set; } = null!;
        public string? UserExternalId { get; set; }
        public string? UserEmail { get; set; }
        public string? UserAnonymousIds { get; set; }
        public string? Name { get; set; }
        public string? Source { get; set; }
        public string? EventType { get; set; }
        public DateTimeOffset? Timestamp { get; set; }
        public string? IpAddress { get; set; }
        public string? SessionId { get; set; }
        public string? ConnectorId { get; set; }
        public string? ConnectorName { get; set; }
        public string? Properties { get; set; }
        public string? Context { get; set; }
        public DateTimeOffset SyncCreatedAt { get; set; }
        public DateTimeOffset SyncUpdatedAt { get; set; }
        public DateTimeOffset? SyncDeletedAt { get; set; }
    }
}
