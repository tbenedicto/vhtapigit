﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BusinessDayOfTheMonth
    {
        public int? Month { get; set; }
        public int? Year { get; set; }
        public double? BussinessDayOfTheMonth { get; set; }
    }
}
