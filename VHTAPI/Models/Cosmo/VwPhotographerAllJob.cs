﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwPhotographerAllJob
    {
        public int ContactId { get; set; }
        public string? ContactCode { get; set; }
        public string? ContactName { get; set; }
        public string? ContactCategory { get; set; }
        public int? CompanyId { get; set; }
        public int ListingId { get; set; }
        public DateTime? ListingAddedDate { get; set; }
        public string? ListingStatus { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public int JobId { get; set; }
        public string? VhtjobId { get; set; }
        public string? JobStatus { get; set; }
        public string? JobStatusReason { get; set; }
        public int MileageQuantity { get; set; }
        public decimal MileageRate { get; set; }
        public decimal CvmiscPay { get; set; }
        public double? CvbasePay { get; set; }
        public double? CvtotalPay { get; set; }
        public DateTime? JobAddedDate { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public DateTime? TaskMediaReceivedCompletedOn { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public DateTime? StatusCompleteCompletedOn { get; set; }
        public DateTime? StatusAccountingCompletedOn { get; set; }
    }
}
