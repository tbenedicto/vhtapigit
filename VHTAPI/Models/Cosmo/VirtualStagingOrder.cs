﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VirtualStagingOrder
    {
        public int VirtualStagingOrderId { get; set; }
        public int ListingId { get; set; }
        public int JobId { get; set; }
        public string VhtjobId { get; set; } = null!;
        public string? VirtualStagingNote { get; set; }
        public string? VirtualTwilightNote { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; } = null!;
    }
}
