﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportDreamtownAgent
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? ExchangeEmail { get; set; }
        public string? CellPhone { get; set; }
        public double? MlsAgentId { get; set; }
        public string? DreamTownOffice { get; set; }
        public double? VhtOfficeCode { get; set; }
        public string? EmailToCc { get; set; }
        public double? EmailMatch { get; set; }
        public double? LastMatch { get; set; }
        public int? CosmoContactId { get; set; }
        public string? MatchedBy { get; set; }
    }
}
