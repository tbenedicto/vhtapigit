﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablAllResidentialOrdersAppt
    {
        public int? CompanyId { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? Companyname { get; set; }
        public string? Parentcompanyname { get; set; }
        public string CompanyType { get; set; } = null!;
        public string? AgentName { get; set; }
        public int JobId { get; set; }
        public int? CompanysubsidiaryId { get; set; }
        public string? ContactCode { get; set; }
        public string? VhtjobId { get; set; }
        public int JobStatusId { get; set; }
        public Guid? CampaignGuid { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string? ContactBusinessType { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? JobAddedBy { get; set; }
        public DateTime? JobAddedDate { get; set; }
        public string? Adm { get; set; }
        public DateTime? _1stStartdate { get; set; }
        public string? ContactHistoryType { get; set; }
        public string? PastAde { get; set; }
        public string? Ade { get; set; }
        public int NumOffices { get; set; }
        public int NumAgents { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public string Territory { get; set; } = null!;
        public DateTime? StatusProductionCompletedOn { get; set; }
        public decimal? Totalrev { get; set; }
        public int ListingId { get; set; }
        public int ContentServerId { get; set; }
        public int G3codeId { get; set; }
        public int? PropertyListPrice { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public string? Development { get; set; }
        public string? DrivingDirections { get; set; }
        public int? AgentId { get; set; }
        public string? RelistString { get; set; }
        public string? County { get; set; }
        public string? HomeownerName { get; set; }
        public string? HomeownerEmail { get; set; }
        public string? Beds { get; set; }
        public string? Baths { get; set; }
        public bool? GoodListingInd { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? RowModifiedDate { get; set; }
        public string? RowModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public int ListingTypeId { get; set; }
        public int AddressStyleId { get; set; }
        public Guid Rowguid { get; set; }
        public byte[] RowTimeStamp { get; set; } = null!;
        public int ListingPriorityId { get; set; }
        public int ListingStatusId { get; set; }
        public bool ShowInViewMyListings { get; set; }
        public bool FlagForOpenHouse { get; set; }
        public bool IsStyleInherited { get; set; }
        public int ViewerStyleId { get; set; }
        public int BrochureStyleId { get; set; }
        public string? RdcorderId { get; set; }
        public bool IsContentPartial { get; set; }
        public bool HasUploadedToMm { get; set; }
        public string? BrokerPropertyDetailPageUrl { get; set; }
        public DateTime? DateLastSeenInFeed { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? HalfBaths { get; set; }
        public string? GoogleMapListingUrl { get; set; }
        public int? RefMmslistingId { get; set; }
        public int? ContactIdAgent2 { get; set; }
        public int? ContactIdAgent3 { get; set; }
        public int? ContactIdAgent4 { get; set; }
        public string Market { get; set; } = null!;
        public int? OrdererPlacedToAppt { get; set; }
        public int? ApptToCompletion { get; set; }
        public int? OrdererPlacedToCompletion { get; set; }
        public string? RegionName { get; set; }
        public int Contactid { get; set; }
        public int? CatJobid { get; set; }
        public int? CatStdStill { get; set; }
        public int? CatPremStill { get; set; }
        public int? CatStd360 { get; set; }
        public int? CatClear360 { get; set; }
        public int? CatDup { get; set; }
        public int? CatPrints { get; set; }
        public int? CatVideo { get; set; }
        public int? CatVodservices { get; set; }
        public int? CatVhttourCustom { get; set; }
        public int? CatVhttourMn { get; set; }
        public int? CatAlbum { get; set; }
        public int? CatFloorplan { get; set; }
        public int? CatSilver { get; set; }
        public int? CatGold { get; set; }
        public int? CatPlatinum { get; set; }
        public int? CatAerial { get; set; }
        public int? Cat3d { get; set; }
        public int? CatDigitalEnhancement { get; set; }
        public int? CatBlueSky { get; set; }
        public int? CatPrepaid { get; set; }
    }
}
