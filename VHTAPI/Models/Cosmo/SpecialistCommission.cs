﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SpecialistCommission
    {
        public int? ParentCompanyId { get; set; }
        public string? Parentcompanyname { get; set; }
        public string? JobAddedBy { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public int? CompletedYear { get; set; }
        public int? CompletedMonth { get; set; }
        public DateTime? MinCompletedOn { get; set; }
        public int? FirstJobId { get; set; }
        public DateTime? MinStatusProductionCompletedOn { get; set; }
        public decimal? TotalrevForMonth { get; set; }
        public int Contactid { get; set; }
        public DateTime? JobAddedDate { get; set; }
        public DateTime? Appointmentdatetime { get; set; }
    }
}
