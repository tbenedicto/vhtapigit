﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Doc
    {
        public Doc()
        {
            CompanyDocStylePrefs = new HashSet<CompanyDocStylePref>();
        }

        public int DocId { get; set; }
        public string Title { get; set; } = null!;
        public int DocCategoryId { get; set; }
        public int DefaultDocWriterId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<CompanyDocStylePref> CompanyDocStylePrefs { get; set; }
    }
}
