﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RuleStore
    {
        public int RowId { get; set; }
        public byte[] GeneralRules { get; set; } = null!;
        public byte[] ProductRules { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public byte[] RowVersion { get; set; } = null!;
    }
}
