﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactUpdateFinal
    {
        public string? AccountAreaSalesMarketLeadC { get; set; }
        public string? AccountClassificationFC { get; set; }
        public string? AccountDevelopmentExecutiveC { get; set; }
        public string? AccountId { get; set; }
        public string? AdminNoteC { get; set; }
        public decimal? AgentIdC { get; set; }
        public DateTime? AgentLastOrderDateC { get; set; }
        public string? AgentStatusC { get; set; }
        public string? AssistantName { get; set; }
        public string? AssistantPhone { get; set; }
        public decimal? AvgOrderSpendC { get; set; }
        public DateTime? Birthdate { get; set; }
        public string? BusinessTypeC { get; set; }
        public string? CalendarSchedulerC { get; set; }
        public string? CameraTypeC { get; set; }
        public string? CbiC { get; set; }
        public string? CcOnOrderCompleteEmailsC { get; set; }
        public string? ClassificationC { get; set; }
        public string? ClientStatusC { get; set; }
        public string? CompanyAddressC { get; set; }
        public string? CompanyTypeCC { get; set; }
        public string? ConnectionReceivedId { get; set; }
        public string? ConnectionSentId { get; set; }
        public string? ContactCommentsC { get; set; }
        public decimal? CosmoCompanyIdC { get; set; }
        public string? CosmoContactCodeC { get; set; }
        public string? CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public string? Ctct21OptedOutByCtctC { get; set; }
        public string? CurrentPhotoVtProviderCC { get; set; }
        public string? CvCoverageMarketC { get; set; }
        public string? CvOnCommissionPlanC { get; set; }
        public string? CvSalesLeadC { get; set; }
        public string? CvStatusC { get; set; }
        public string? CvovC { get; set; }
        public string? Department { get; set; }
        public string? Description { get; set; }
        public string? DoTheyHaveAProjectorC { get; set; }
        public string? DoTheyHaveVhtVestC { get; set; }
        public string? Email { get; set; }
        public string? EmailSecondaryEmailC { get; set; }
        public DateTime? EmailBouncedDate { get; set; }
        public string? EmailBouncedReason { get; set; }
        public string? Fax { get; set; }
        public string? FirstName { get; set; }
        public string? FlashTypeC { get; set; }
        public string? FloorPlanCertifiedC { get; set; }
        public string HasOptedOutOfEmail { get; set; } = null!;
        public string? HighValueStatusOrTlcStatusC { get; set; }
        public string? HomePhone { get; set; }
        public string? HowManyVhtTableSkirtsDoTheyC { get; set; }
        public string Id { get; set; } = null!;
        public DateTime? InSituEcmInSituStartDateC { get; set; }
        public string? IsPremiumClientC { get; set; }
        public string IsDeleted { get; set; } = null!;
        public string IsEmailBounced { get; set; } = null!;
        public string? JigsawContactId { get; set; }
        public string? JobCountFor2012C { get; set; }
        public decimal? JobOver5012MonthC { get; set; }
        public decimal? JobOver50LifetimeC { get; set; }
        public decimal? JobOver50PriorYearC { get; set; }
        public decimal? JobOver50YearToDateC { get; set; }
        public string? JobTotalFor2012C { get; set; }
        public string? LastOrderDateC { get; set; }
        public DateTime? LastActivityDate { get; set; }
        public DateTime? LastCompletedOrder360scenesC { get; set; }
        public DateTime? LastCompletedOrderAdditionalPremC { get; set; }
        public DateTime? LastCompletedOrderAdditionalSignatureC { get; set; }
        public DateTime? LastCompletedOrderBsuC { get; set; }
        public DateTime? LastCompletedOrderCancellationFeeC { get; set; }
        public DateTime? LastCompletedOrderClearshotC { get; set; }
        public DateTime? LastCompletedOrderComboC { get; set; }
        public DateTime? LastCompletedOrderCustomTourCC { get; set; }
        public DateTime? LastCompletedOrderFpReconC { get; set; }
        public DateTime? LastCompletedOrderIfpC { get; set; }
        public DateTime? LastCompletedOrderMusicNarrationC { get; set; }
        public DateTime? LastCompletedOrderNightPhotographyC { get; set; }
        public DateTime? LastCompletedOrderPremPhotosC { get; set; }
        public DateTime? LastCompletedOrderSignaturePhotosC { get; set; }
        public DateTime? LastCompletedOrderVideoC { get; set; }
        public DateTime? LastCompletedOrderVirtualStagingC { get; set; }
        public DateTime? LastCurequestDate { get; set; }
        public DateTime? LastCuupdateDate { get; set; }
        public string? LastModifiedById { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastName { get; set; } = null!;
        public DateTime? LastReferencedDate { get; set; }
        public DateTime? LastViewedDate { get; set; }
        public string? LeadSource { get; set; }
        public string? LensTypeC { get; set; }
        public string? ListSold2012C { get; set; }
        public decimal? Listing12MonthC { get; set; }
        public string? ListingCountC { get; set; }
        public decimal? ListingLifetimeC { get; set; }
        public decimal? ListingPriorYearC { get; set; }
        public decimal? ListingYearToDateC { get; set; }
        public string? MailingCity { get; set; }
        public string? MailingCountry { get; set; }
        public decimal? MailingLatitude { get; set; }
        public decimal? MailingLongitude { get; set; }
        public string? MailingPostalCode { get; set; }
        public string? MailingState { get; set; }
        public string? MailingStreet { get; set; }
        public string? MappedContactCosmoIdC { get; set; }
        public string? MasterRecordId { get; set; }
        public string? MlsAgentIdC { get; set; }
        public string? MobilePhone { get; set; }
        public string Name { get; set; } = null!;
        public string? NotesC { get; set; }
        public string? OnlineOrderFormC { get; set; }
        public string? OrbitsTourLinkFC { get; set; }
        public string? OtherCity { get; set; }
        public string? OtherCountry { get; set; }
        public decimal? OtherLatitude { get; set; }
        public decimal? OtherLongitude { get; set; }
        public string? OtherPhone { get; set; }
        public string? OtherPostalCode { get; set; }
        public string? OtherState { get; set; }
        public string? OtherStreet { get; set; }
        public string OwnerId { get; set; } = null!;
        public string? ParentAccountDevelopmentExecutiveC { get; set; }
        public string? Phone { get; set; }
        public string? PhoneTypeC { get; set; }
        public string? PhotographerRepContactC { get; set; }
        public string? PhotoUrl { get; set; }
        public string? PreferredCv2C { get; set; }
        public string? PreferredCv3C { get; set; }
        public string? PreferredCvC { get; set; }
        public string? PremiumCertifiedC { get; set; }
        public string? ReferredByConC { get; set; }
        public string? RegionalC { get; set; }
        public string? ReportsToId { get; set; }
        public decimal? Revenue12MonthC { get; set; }
        public decimal? RevenueInLast6MonthsC { get; set; }
        public decimal? RevenueLifetimeC { get; set; }
        public decimal? RevenuePriorYearC { get; set; }
        public decimal? RevenueYearToDateC { get; set; }
        public string? SalesTerritoryC { get; set; }
        public string? Salutation { get; set; }
        public string? SignatureC { get; set; }
        public DateTime? StartDateForPhotographerRepContactC { get; set; }
        public DateTime SystemModstamp { get; set; }
        public string? Title { get; set; }
        public string? Top500BrokerC { get; set; }
        public string? VhtRelationshipC { get; set; }
        public string? X2013RtInvdSalesC { get; set; }
        public string? X2013RtInvdVolC { get; set; }
        public string? X360CameraC { get; set; }
        public string? X360SC { get; set; }
        public string? Error { get; set; }
    }
}
