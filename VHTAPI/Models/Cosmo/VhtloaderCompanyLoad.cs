﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VhtloaderCompanyLoad
    {
        public double? ParentCompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? Street { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public double? Zip { get; set; }
        public int? NumOfOffices { get; set; }
        public int? NumOfagents { get; set; }
        public string? Companyphone { get; set; }
        public int? NumListingsPerYear { get; set; }
        public string? CorporateId { get; set; }
        public string? Mlsid { get; set; }
    }
}
