﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GeneralRule
    {
        public GeneralRule()
        {
            GeneralRuleAvailableProducts = new HashSet<GeneralRuleAvailableProduct>();
            GeneralRuleDefaultProducts = new HashSet<GeneralRuleDefaultProduct>();
            GeneralRuleDistributionSites = new HashSet<GeneralRuleDistributionSite>();
            GeneralRulePreferredProducts = new HashSet<GeneralRulePreferredProduct>();
        }

        public int GeneralRuleId { get; set; }
        public int RuleConditionTypeId { get; set; }
        public int? ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int? Cvid { get; set; }
        public int? ProductRegionId { get; set; }
        public string? StateId { get; set; }
        public int? ContactGroupId { get; set; }
        public int CvmileageTypeId { get; set; }
        public decimal? CvmileageAmount { get; set; }
        public int AvailableProductsOpId { get; set; }
        public int PreferredProductsOpId { get; set; }
        public int DefaultProductsOpId { get; set; }
        public int DistributionSitesOpId { get; set; }
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
        public int AvailablePackagesOpId { get; set; }
        public int PreferredPackagesOpId { get; set; }
        public int DefaultPackagesOpId { get; set; }

        public virtual ZzGeneralRuleOp AvailableProductsOp { get; set; } = null!;
        public virtual ZzCvmileageType CvmileageType { get; set; } = null!;
        public virtual ZzGeneralRuleOp DefaultProductsOp { get; set; } = null!;
        public virtual ZzGeneralRuleOp DistributionSitesOp { get; set; } = null!;
        public virtual ZzGeneralRuleOp PreferredProductsOp { get; set; } = null!;
        public virtual ZzRuleConditionType RuleConditionType { get; set; } = null!;
        public virtual ICollection<GeneralRuleAvailableProduct> GeneralRuleAvailableProducts { get; set; }
        public virtual ICollection<GeneralRuleDefaultProduct> GeneralRuleDefaultProducts { get; set; }
        public virtual ICollection<GeneralRuleDistributionSite> GeneralRuleDistributionSites { get; set; }
        public virtual ICollection<GeneralRulePreferredProduct> GeneralRulePreferredProducts { get; set; }
    }
}
