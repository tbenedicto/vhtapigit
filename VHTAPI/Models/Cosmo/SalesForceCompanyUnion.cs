﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SalesForceCompanyUnion
    {
        public int Companyid { get; set; }
        public string? CompanyName { get; set; }
        public int IsParentCompany { get; set; }
    }
}
