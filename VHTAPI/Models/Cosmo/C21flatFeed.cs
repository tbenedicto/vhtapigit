﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class C21flatFeed
    {
        public string? OfficeGuid { get; set; }
        public string? ListingGuid { get; set; }
        public string? Brand { get; set; }
        public string? ListingKey { get; set; }
        public string? MlsId { get; set; }
        public string? ListingId { get; set; }
        public string? ListingStatus { get; set; }
        public string? Address1 { get; set; }
        public string? Address2 { get; set; }
        public string? Address3 { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? StateIsoCode { get; set; }
        public string? StandardStateName { get; set; }
        public string? Country { get; set; }
        public string? CountryIsoCode { get; set; }
        public string? StandardCountryName { get; set; }
        public string? PostalCode { get; set; }
        public string? PropertyName { get; set; }
        public string? PropertyType { get; set; }
        public string? PropertySubType { get; set; }
        public string? SourcePropertyType { get; set; }
        public string? PropertyUse { get; set; }
        public string? ListingDate { get; set; }
        public string? ExpirationDate { get; set; }
        public string? ListPrice { get; set; }
        public string? CurrencyCode { get; set; }
        public string? CurrencyName { get; set; }
        public string? Bedrooms { get; set; }
        public string? FullBath { get; set; }
        public string? HalfBath { get; set; }
        public string? ThreeQuarterBath { get; set; }
        public string? BuildingArea { get; set; }
        public string? Taxes { get; set; }
        public string? AdditionalMls { get; set; }
        public string? NeighborhoodName { get; set; }
        public string? LocationName { get; set; }
        public string? ForeclosureFlag { get; set; }
        public string? Shortsale { get; set; }
        public string? ShowProperty { get; set; }
        public string? ShowAddress { get; set; }
        public string? ShowListPrice { get; set; }
        public string? CallToShowTheProperty { get; set; }
        public string? PriceUponRequest { get; set; }
        public string? ForAuction { get; set; }
        public string? LastUpdateDate { get; set; }
        public string? TotalAcres { get; set; }
        public string? RenovatedYear { get; set; }
        public string? TotalRooms { get; set; }
        public string? ParkingPlaces { get; set; }
        public string? ListingType { get; set; }
        public string? YearBuilt { get; set; }
        public string? TaxYear { get; set; }
        public string? SourceListingUrl { get; set; }
        public string? OriginalListPrice { get; set; }
        public string? QuarterBath { get; set; }
        public string? LotDimension { get; set; }
        public string? DevelopmentId { get; set; }
        public string? AppraisalNumber { get; set; }
        public string? NewConstructionFlag { get; set; }
        public string? BuildingAreaUom { get; set; }
        public string? DaysOnMarket { get; set; }
        public string? LevelNumber { get; set; }
        public string? LastSoldDate { get; set; }
        public string? TaxRoll { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
        public string? ZoomLevel { get; set; }
        public string? AlternateListPrice { get; set; }
        public string? AlternateCurrencyCode { get; set; }
        public string? AlternateCurrencyName { get; set; }
        public string? Area { get; set; }
        public string? BuildingAreaInText { get; set; }
        public string? LotSize { get; set; }
        public string? LotSizeUom { get; set; }
        public string? LotSizeInText { get; set; }
        public string? RentalFrequencyName { get; set; }
        public string? EstimatedCloseDate { get; set; }
    }
}
