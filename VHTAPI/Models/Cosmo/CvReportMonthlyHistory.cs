﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CvReportMonthlyHistory
    {
        public int? PropertyStateCount { get; set; }
        public string? VhtjobId { get; set; }
        public string? Company { get; set; }
        public int CompanyId { get; set; }
        public DateTime? ProductionCompleteDate { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentFirstName { get; set; }
        public string? CorporateAgentId { get; set; }
        public string? CorporateOfficeId { get; set; }
        public string? Mlsid { get; set; }
        public string? Development { get; set; }
        public string? Teritory { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? InternalName { get; set; }
        public int Quantity { get; set; }
        public int CvupsellQuantity { get; set; }
        public decimal? TotalJobPrice { get; set; }
        public decimal AgentPay { get; set; }
        public decimal CorporatePay { get; set; }
        public string? CvlastName { get; set; }
        public string? CvfirstName { get; set; }
        public decimal Cvpay { get; set; }
        public decimal Cvupsell { get; set; }
        public decimal MileageRate { get; set; }
        public int MileageQuantity { get; set; }
        public decimal? CvmileageReimbursement { get; set; }
        public decimal Cvmiscellaneous { get; set; }
        public decimal? CvtotalPay { get; set; }
        public bool FlagCustomerFirstOrder { get; set; }
        public int ListingId { get; set; }
        public string? ContactClientStatus { get; set; }
        public string? ContactVipflag { get; set; }
        public string? ContactTlcflag { get; set; }
        public DateTime? CommStartDate { get; set; }
        public string? AdeAccountRep { get; set; }
        public int? Month { get; set; }
        public string? Year { get; set; }
    }
}
