﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ListingDistributionEventStartdate
    {
        public int EventId { get; set; }
        public int ProcessRunId { get; set; }
        public int EventTypeId { get; set; }
        public int ListingId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
