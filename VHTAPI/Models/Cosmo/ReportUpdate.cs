﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ReportUpdate
    {
        public int RowId { get; set; }
        public DateTime LastUpdated { get; set; }
        public Guid Rowguid { get; set; }
    }
}
