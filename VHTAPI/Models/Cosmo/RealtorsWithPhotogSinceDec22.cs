﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RealtorsWithPhotogSinceDec22
    {
        public string? Id { get; set; }
        public string? Agency { get; set; }
        public string? Mlsid { get; set; }
        public string? CompanyName { get; set; }
        public string? CompanyId { get; set; }
        public string? OfficeName { get; set; }
        public string? OfficeId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? Email { get; set; }
        public string? Username { get; set; }
        public string? Cell { get; set; }
        public string? Phone { get; set; }
        public string? Website { get; set; }
        public string? Type { get; set; }
        public string? DateSignedUp { get; set; }
        public string? DateLastOrdered { get; set; }
        public string? Column21 { get; set; }
    }
}
