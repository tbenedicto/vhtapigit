﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Prodbillingjuly
    {
        public double? JobId { get; set; }
        public double? Flag { get; set; }
        public string? VhtjobId { get; set; }
        public string? Company { get; set; }
        public string? CompanyAddress { get; set; }
        public DateTime? ProductionCompleteDate { get; set; }
        public string? AgentLastName { get; set; }
        public string? AgentFirstName { get; set; }
        public string? CorporateAgentId { get; set; }
        public string? CorporateOfficeId { get; set; }
        public double? Mlsid { get; set; }
        public string? Development { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? ProductName { get; set; }
        public double? Quantity { get; set; }
        public double? TotalJobPrice { get; set; }
        public double? AgentPay { get; set; }
        public double? CorporatePay { get; set; }
        public string? CvlastName { get; set; }
        public string? CvfirstName { get; set; }
        public double? Cvpay { get; set; }
        public double? Cvupsell { get; set; }
        public double? MileageRate { get; set; }
        public double? MileageQuantity { get; set; }
        public double? CvmileageReimbursement { get; set; }
        public double? Cvmiscellaneous { get; set; }
        public double? CvtotalPay { get; set; }
        public double? ListingId { get; set; }
        public string? CertificateNumber { get; set; }
        public string? FirstOrderAlert { get; set; }
        public double? AgentMlsid { get; set; }
        public string? ListingCategory { get; set; }
        public double? AgentRateAdj { get; set; }
        public double? AgentDiscount { get; set; }
        public string? AgentDiscountCode { get; set; }
        public double? AgentPromoDiscount { get; set; }
        public string? AgentPromoCode { get; set; }
        public double? CorporateRateAdj { get; set; }
        public double? CorporateDiscount { get; set; }
        public string? CorporateDiscountCode { get; set; }
        public double? CorporatePromoDiscount { get; set; }
        public string? CorporatePromoCode { get; set; }
        public double? CvrateAdj { get; set; }
        public double? Cvdiscount { get; set; }
        public string? CvdiscountCode { get; set; }
        public double? ProofPhotoCredit { get; set; }
        public double? ProofAmountCredit { get; set; }
        public double? ProofPercentCredit { get; set; }
        public double? ProofPhotoCreditAvailable { get; set; }
        public double? OrigAgentPay { get; set; }
        public double? OrigCorporatePay { get; set; }
        public double? OrigCvpay { get; set; }
        public double? PackageId { get; set; }
        public double? JobPackageSeq { get; set; }
    }
}
