﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwAllJobProduct
    {
        public int JobId { get; set; }
        public int JobProductId { get; set; }
        public int ProductId { get; set; }
        public int JobProductQuantity { get; set; }
        public decimal ProductTotalAgentPrice { get; set; }
        public decimal ProductTotalCorpPrice { get; set; }
        public decimal? ProductTotalPrice { get; set; }
        public decimal ProductPhotographerBasePay { get; set; }
        public decimal? ProductPhotographerUpsellPay { get; set; }
        public string? JobProductStatus { get; set; }
        public string? MmstransactionNotes { get; set; }
        public DateTime? JobProductAddedDate { get; set; }
    }
}
