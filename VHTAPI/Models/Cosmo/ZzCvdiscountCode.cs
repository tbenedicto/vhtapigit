﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCvdiscountCode
    {
        public int CvdiscountCodeId { get; set; }
        public string CvdiscountCode { get; set; } = null!;
        public string Descr { get; set; } = null!;
        public bool IsActive { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
