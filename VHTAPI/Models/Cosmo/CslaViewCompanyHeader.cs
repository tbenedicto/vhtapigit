﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CslaViewCompanyHeader
    {
        public int CompanyId { get; set; }
        public string? DisplayAs { get; set; }
        public string? Address { get; set; }
        public string? CorporateOfficeId { get; set; }
    }
}
