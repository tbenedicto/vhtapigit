﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessRunCode
    {
        public ProcessRunCode()
        {
            ProcessRuns = new HashSet<ProcessRun>();
        }

        public int ProcessRunCodeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<ProcessRun> ProcessRuns { get; set; }
    }
}
