﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BrokerageMultipleIp
    {
        public string Ipaddress { get; set; } = null!;
        public int BrokerageCompanyId { get; set; }
    }
}
