﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContactPhone
    {
        public int? ContactId { get; set; }
        public int? CommunicationId { get; set; }
    }
}
