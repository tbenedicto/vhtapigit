﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GzSheet2
    {
        public double? _1 { get; set; }
        public double? ListingId { get; set; }
        public double? SearchedImageId { get; set; }
        public string? RoomCode { get; set; }
    }
}
