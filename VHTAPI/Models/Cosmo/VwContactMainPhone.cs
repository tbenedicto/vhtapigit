﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwContactMainPhone
    {
        public int? FkparentId { get; set; }
        public int? CommunicationId { get; set; }
    }
}
