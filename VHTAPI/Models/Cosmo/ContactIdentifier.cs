﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactIdentifier
    {
        public int ContactIdentifierId { get; set; }
        public int ContactId { get; set; }
        public int ContactIdentifierTypeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual Contact Contact { get; set; } = null!;
        public virtual ContactIdentifierType ContactIdentifierType { get; set; } = null!;
    }
}
