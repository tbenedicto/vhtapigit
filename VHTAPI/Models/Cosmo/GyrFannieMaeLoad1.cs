﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GyrFannieMaeLoad1
    {
        public string? Color { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZipCode { get; set; }
        public string? Bedrooms { get; set; }
        public string? Baths { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public string? ListingAgentEmail { get; set; }
        public string? ListingAgentCellPhone { get; set; }
        public string? LockBoxCode { get; set; }
        public string? BrokerageName { get; set; }
        public string? BrokerageAgentId { get; set; }
        public string? BrokerageOfficeId { get; set; }
        public string? BrokerageOfficeAddress1 { get; set; }
        public string? BrokerageOfficeAddress2 { get; set; }
        public string? BrokerageOfficeCity { get; set; }
        public string? BrokerageOfficeState { get; set; }
        public string? BrokerageOfficeZipCode { get; set; }
        public string? ReoId { get; set; }
        public string? ListingType { get; set; }
        public string? MlsId { get; set; }
        public string? Product1 { get; set; }
        public string? Product2 { get; set; }
        public string? Product3 { get; set; }
        public string? Product4 { get; set; }
        public string? Product5 { get; set; }
        public string? Product6 { get; set; }
        public string? Product7 { get; set; }
        public int? MasterVhtid { get; set; }
        public int? MasterJobid { get; set; }
        public int? SlaveVhtid { get; set; }
        public int? SlaveJobid { get; set; }
        public string? MasterVhtjobId { get; set; }
        public string? SlaveVhtjobId { get; set; }
        public DateTime Loaddatetime { get; set; }
        public string? LatitudeCoordinate { get; set; }
        public string? LongitudeCoordinate { get; set; }
    }
}
