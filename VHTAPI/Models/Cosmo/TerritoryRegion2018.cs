﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TerritoryRegion2018
    {
        public string? State { get; set; }
        public string? StateName { get; set; }
        public string? RegionName { get; set; }
        public int? RepSalesRegion { get; set; }
        public string? RepSalesRegionTitle { get; set; }
    }
}
