﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BusinessDay
    {
        public int Month { get; set; }
        public double? BusinessDays { get; set; }
        public int Year { get; set; }
    }
}
