﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Version
    {
        public int VersionId { get; set; }
        public bool IsActive { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Revision { get; set; }
        public Guid Rowguid { get; set; }
    }
}
