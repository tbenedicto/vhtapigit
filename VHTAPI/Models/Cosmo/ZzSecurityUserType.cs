﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzSecurityUserType
    {
        public ZzSecurityUserType()
        {
            SecurityUsers = new HashSet<SecurityUser>();
        }

        public int SecurityUserTypeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<SecurityUser> SecurityUsers { get; set; }
    }
}
