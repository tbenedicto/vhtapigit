﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AvaEvent
    {
        public int AvaEventId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public int AvaEventTypeId { get; set; }
        public string? Info { get; set; }
        public bool IsFailed { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
