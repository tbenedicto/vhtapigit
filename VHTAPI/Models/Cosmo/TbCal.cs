﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TbCal
    {
        public DateTime? DateDay { get; set; }
        public bool? Holiday { get; set; }
        public string? DayName { get; set; }
        public string? HolidayName { get; set; }
    }
}
