﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class QueueJobProcessor
    {
        public int QueueId { get; set; }
        public int JobId { get; set; }
        public int MmsorderId { get; set; }
        public int MmsmediaTypeId { get; set; }
        public int ListingId { get; set; }
        public int? VideographerContactId { get; set; }
        public int JobStatusId { get; set; }
        public int ProductId { get; set; }
        public bool SendAsSelected { get; set; }
        public bool DeleteExistingImgs { get; set; }
        public DateTime DateAdded { get; set; }
        public bool Processing { get; set; }
        public bool Processed { get; set; }
        public DateTime? DateLastProcessed { get; set; }
        public string? ProcessMessage { get; set; }
        public DateTime? DateJobInField { get; set; }
        public int? ContentTypeId { get; set; }
        public DateTime? DateJobLastProdComplete { get; set; }
    }
}
