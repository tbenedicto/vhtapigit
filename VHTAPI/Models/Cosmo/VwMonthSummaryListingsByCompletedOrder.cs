﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwMonthSummaryListingsByCompletedOrder
    {
        public DateTime? MonthCompleted { get; set; }
        public long? UniqueListings { get; set; }
    }
}
