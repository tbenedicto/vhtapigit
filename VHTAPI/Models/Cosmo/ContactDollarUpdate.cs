﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactDollarUpdate
    {
        public int ContactId { get; set; }
        public decimal DollarTotal { get; set; }
    }
}
