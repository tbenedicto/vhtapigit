﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessLogArchive
    {
        public int ProcessLogId { get; set; }
        public int ProcessRunId { get; set; }
        public int ProcessLogTypeId { get; set; }
        public DateTime LogDate { get; set; }
        public string Message { get; set; } = null!;
        public string? MessageExtended { get; set; }
    }
}
