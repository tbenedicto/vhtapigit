﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebListingDistributionTrigger
    {
        public int ListingDistributionTriggerId { get; set; }
        public int ListingId { get; set; }
        public DateTime TriggerDate { get; set; }
        public string InitiatedBy { get; set; } = null!;
        public int DistributionTriggerReasonId { get; set; }
        public Guid Rowguid { get; set; }
        public Guid Rowguid7 { get; set; }
    }
}
