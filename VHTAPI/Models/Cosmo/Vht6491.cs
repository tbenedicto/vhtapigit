﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Vht6491
    {
        public string? MlsId { get; set; }
        public string? MlsName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? EmailAddress { get; set; }
        public string? PhoneNumber { get; set; }
        public string? OfficeName { get; set; }
        public double? OfficeId { get; set; }
        public string? Website { get; set; }
        public string? PortraitUrl { get; set; }
    }
}
