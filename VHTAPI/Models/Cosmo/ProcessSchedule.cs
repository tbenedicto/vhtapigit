﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessSchedule
    {
        public int ProcessScheduleId { get; set; }
        public int ProcessId { get; set; }
        public string ScheduleFormula { get; set; } = null!;
        public int ProcessScheduleDateOptionTypeId { get; set; }
        public string ProcessOptions { get; set; } = null!;
        public string OverrideOptions { get; set; } = null!;
        public DateTime? NextRunDate { get; set; }
        public bool IsEnabled { get; set; }
        public byte[] ObjectVersion { get; set; } = null!;
        public bool IsRunOnce { get; set; }
        public Guid Rowguid { get; set; }

        public virtual Process Process { get; set; } = null!;
        public virtual ProcessScheduleDateOptionType ProcessScheduleDateOptionType { get; set; } = null!;
    }
}
