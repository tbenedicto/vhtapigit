﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablYear2yearTwoMonthOrder
    {
        public string? DayThisYear { get; set; }
        public string? DateThisYear { get; set; }
        public string? DayLastYear { get; set; }
        public string? DateLastYear { get; set; }
        public int ListingsWithResidentialPhotographyThisYear { get; set; }
        public int? ListingsWithResidentialPhotographyLastYear { get; set; }
        public int? ListingsWithResidentialPhotographyComparison { get; set; }
        public string? ListingsWithResidentialPhotographyGrowth { get; set; }
        public int ListingsWithResidentialFloorPlansThisYear { get; set; }
        public int? ListingsWithResidentialFloorPlansLastYear { get; set; }
        public int? ListingsWithResidentialFloorPlansComparison { get; set; }
        public string? ListingsWithResidentialFloorPlansGrowth { get; set; }
        public int EcpListingsThisYear { get; set; }
        public int? EcpListingsLastYear { get; set; }
        public int? EcpListingsComparison { get; set; }
        public string? EcpListingsGrowth { get; set; }
        public int? TotalListingsThisYear { get; set; }
        public int? TotalListingsLastYear { get; set; }
        public int? TotalComparison { get; set; }
        public string? TotalGrowth { get; set; }
    }
}
