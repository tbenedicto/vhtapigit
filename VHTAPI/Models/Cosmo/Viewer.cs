﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Viewer
    {
        public int ViewerId { get; set; }
        public int SortId { get; set; }
        public string ViewerName { get; set; } = null!;
        public string FilePath { get; set; } = null!;
        public bool IsCustomViewer { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public byte[] RowVersion { get; set; } = null!;
        public Guid RowGuidId { get; set; }
    }
}
