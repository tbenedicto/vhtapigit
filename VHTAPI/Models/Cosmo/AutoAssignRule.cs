﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AutoAssignRule
    {
        public int AutoAssignRuleId { get; set; }
        public int AutoAssignRuleTypeId { get; set; }
        public string FkparentId { get; set; } = null!;
        public bool? AssignCv { get; set; }
        public int? MoveJobStatusId { get; set; }
        public int SortId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public int? MoveStatusIssueSubCatId { get; set; }
        public bool? DisableAutomation { get; set; }
    }
}
