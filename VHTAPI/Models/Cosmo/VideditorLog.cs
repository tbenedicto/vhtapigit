﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VideditorLog
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int VideditorId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public string AppName { get; set; } = null!;
    }
}
