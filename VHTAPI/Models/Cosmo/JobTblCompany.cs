﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobTblCompany
    {
        public int ParentCompanyId { get; set; }
        public int? OfficeCompanyId { get; set; }
        public string? Brokerage { get; set; }
        public string? Office { get; set; }
        public string? PhoneNumber { get; set; }
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string? City { get; set; }
        public string? StateId { get; set; }
        public string? Zip { get; set; }
    }
}
