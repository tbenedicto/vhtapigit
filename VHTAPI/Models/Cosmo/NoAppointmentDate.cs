﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class NoAppointmentDate
    {
        public DateTime? AppointmentDate { get; set; }
        public int? OfOrders { get; set; }
        public decimal? Revenue { get; set; }
        public int? PcOfOrders { get; set; }
        public decimal? PcRevenues { get; set; }
        public int? FloorPlanOfOrders { get; set; }
        public decimal? FloorPlanRevenue { get; set; }
        public int? SumOfFloorPlan { get; set; }
        public int? PremOfOrders { get; set; }
        public decimal? PremRevenue { get; set; }
        public int? SumOfPrem { get; set; }
        public DateTime? Loaddate { get; set; }
        public int? CommercialOrder { get; set; }
        public decimal? CommercialRevenue { get; set; }
        public int? ProsHighValueOrder { get; set; }
        public decimal? ProsHighValueRevenue { get; set; }
        public int? DuganOrder { get; set; }
        public decimal? DuganRevenue { get; set; }
        public int? SchmidtOrder { get; set; }
        public decimal? SchmidtRevenue { get; set; }
        public int? McDermottOrder { get; set; }
        public decimal? McDermottRevenue { get; set; }
        public DateTime? Loaddate2 { get; set; }
        public int? NoneOrder { get; set; }
        public decimal? NoneRevenue { get; set; }
        public int? StudioOrder { get; set; }
        public decimal? StudioRevenue { get; set; }
        public int? StudioDuganOrder { get; set; }
        public decimal? StudioDuganRevenue { get; set; }
        public int? StudioProsHighValueOrder { get; set; }
        public decimal? StudioProsHighValueRevenue { get; set; }
        public int? StudioCommercialOrder { get; set; }
        public decimal? StudioCommercialRevenue { get; set; }
        public int? StudioSchmidtOrder { get; set; }
        public decimal? StudioSchmidtRevenue { get; set; }
        public int? StudioMcDermottOrder { get; set; }
        public decimal? StudioMcDermottRevenue { get; set; }
        public int? StudioNoneOrder { get; set; }
        public decimal? StudioNoneRevenue { get; set; }
    }
}
