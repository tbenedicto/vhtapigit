﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class BkupCheckdupMl
    {
        public string Mlsid { get; set; } = null!;
        public string? TourDisplayEmail { get; set; }
        public int? CompanyId { get; set; }
        public int? ParentCompanyId { get; set; }
    }
}
