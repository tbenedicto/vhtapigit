﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TblDbmaintVhtopSupportC21imageworksBillingReportCount
    {
        public DateTime? LoadDate { get; set; }
        public int? MyTotal { get; set; }
        public int? MyTotalEtlErrors { get; set; }
        public int? MyTotalJobenginefailure { get; set; }
        public int? MyTotalSyndbErrors { get; set; }
        public int? MyTotalYoutubePostErrors { get; set; }
        public decimal? PercentAllComplete { get; set; }
        public string? LoadType { get; set; }
        public decimal? PercentAfterJobEngine { get; set; }
        public decimal? PercentAferSyndb { get; set; }
        public int? MyLostListings { get; set; }
        public decimal? PecentAfterEtl { get; set; }
        public int? MyLostAfterEtl { get; set; }
        public int? MyLostAterJobEngine { get; set; }
        public int? MyLostAferSynDb { get; set; }
    }
}
