﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CommReportNew
    {
        public int CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompany { get; set; }
        public string? RegionalSalesRep { get; set; }
        public string? AreaSalesRep { get; set; }
        public string? Ade { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public decimal? TotalRev { get; set; }
        public int? TotalOrders { get; set; }
        public int? TotalClients { get; set; }
        public decimal ImageWorksRev { get; set; }
        public int ImageWorksOrders { get; set; }
        public int ImageWorksClients { get; set; }
        public decimal TrailingYtdNewRevenue { get; set; }
        public int TrailingYtdNewOrders { get; set; }
        public int TrailingYtdNewClients { get; set; }
        public decimal NewRev { get; set; }
        public int NewOrders { get; set; }
        public int NewClients { get; set; }
        public decimal YtdRev { get; set; }
        public int Ytdorders { get; set; }
        public int YtdClients { get; set; }
        public decimal YtdRevLessFees { get; set; }
        public decimal FeesRev { get; set; }
        public int FeesOrders { get; set; }
        public int FeesClients { get; set; }
        public decimal Pcrev { get; set; }
        public int Pcorders { get; set; }
        public int Pcclients { get; set; }
        public decimal TrailingYtdRenewRevenue { get; set; }
        public int TrailingYtdRenewOrders { get; set; }
        public int TrailingYtdRenewClients { get; set; }
        public decimal RenewRev { get; set; }
        public int RenewOrders { get; set; }
        public int RenewClients { get; set; }
        public decimal MktDomintorRev { get; set; }
        public int MktDomintorOrders { get; set; }
        public int MktDomintorClients { get; set; }
        public decimal CommercialRev { get; set; }
        public int CommercialOrders { get; set; }
        public int CommercialClients { get; set; }
        public decimal NoFeeOver50Rev { get; set; }
        public int NoFeeOver50Orders { get; set; }
        public int NoFeeOver50Clients { get; set; }
        public decimal? ExistingResidentalRev { get; set; }
        public int? ExistingResidentalOrders { get; set; }
        public int? ExistingResidentalClients { get; set; }
        public string? StateId { get; set; }
        public decimal? TotNewRenewRev { get; set; }
        public int? TotNewRenewOrders { get; set; }
        public int? TotNewRenewClients { get; set; }
        public decimal? Trailing11monthRevNewRenew { get; set; }
        public int? Trailing11monthOrdersNewRenew { get; set; }
        public int? Trailing11monthClientsNewRenew { get; set; }
        public decimal? TrailingYtdNewRenewRevenue { get; set; }
        public decimal? CarryOverTrailingRevNewRenew { get; set; }
        public int TrailingYtdNewRenewOrders { get; set; }
        public int TrailingYtdNewRenewClients { get; set; }
        public string? Territory { get; set; }
        public int? UniqAgents { get; set; }
        public int? UniqAgentsYtd { get; set; }
        public string? RegionName { get; set; }
        public string? RegionRep { get; set; }
        public string? Admrep { get; set; }
        public string? Aderep { get; set; }
        public string? Vhtrelationship { get; set; }
        public DateTime? SalesAccountStartDate { get; set; }
        public decimal CpixthisMonthRevenue { get; set; }
        public decimal CpixthisMonthOrders { get; set; }
        public int CpixclientThisMonth { get; set; }
        public int? CustomerTotNewRenewClients { get; set; }
        public int? ProspectTotNewRenewClients { get; set; }
        public int? CustomerAndProspectTotNewRenewClients { get; set; }
        public decimal? CustomerTotalRevenue { get; set; }
        public decimal? ProspectTotalRevenue { get; set; }
        public decimal? CustomerAndProspectTotalRevenue { get; set; }
        public int? PixNewrenewClients { get; set; }
        public int NewClientWonThisYear { get; set; }
        public decimal RevenueWonThisYear { get; set; }
    }
}
