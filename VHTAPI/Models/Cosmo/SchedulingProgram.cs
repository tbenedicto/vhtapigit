﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SchedulingProgram
    {
        public SchedulingProgram()
        {
            Companies = new HashSet<Company>();
        }

        public int SchedulingProgramId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
    }
}
