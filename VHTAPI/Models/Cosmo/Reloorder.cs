﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Reloorder
    {
        public int ReloorderId { get; set; }
        public DateTime AddedDate { get; set; }
        public int OrderStatusId { get; set; }
        public int TourType { get; set; }
        public string Ipaddress { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string CompanyName { get; set; } = null!;
        public string AddressLine1 { get; set; } = null!;
        public string AddressLine2 { get; set; } = null!;
        public string City { get; set; } = null!;
        public string State { get; set; } = null!;
        public string Zip { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string PropertyAddressLine1 { get; set; } = null!;
        public string PropertyAddressLine2 { get; set; } = null!;
        public string PropertyCity { get; set; } = null!;
        public string PropertyState { get; set; } = null!;
        public string PropertyZip { get; set; } = null!;
        public decimal PropertyNumBeds { get; set; }
        public decimal PropertyNumBaths { get; set; }
        public decimal PropertyNumGarage { get; set; }
        public int PropertySqFt { get; set; }
        public string Mlsnumber { get; set; } = null!;
        public string Mlsname { get; set; } = null!;
        public int ListingPrice { get; set; }
        public string PropertyNotes { get; set; } = null!;
        public string OrderNotes { get; set; } = null!;
        public string? ImageDir { get; set; }
        public bool AltContactInd { get; set; }
        public string AltContactFirstName { get; set; } = null!;
        public string AltContactLastName { get; set; } = null!;
        public string AltContactPhone { get; set; } = null!;
        public string AltContactEmail { get; set; } = null!;
        public string Website { get; set; } = null!;
        public string PhotographerName { get; set; } = null!;
        public string PhotographerContact { get; set; } = null!;
        public int PaymentMethodId { get; set; }
        public string DigitalSignature { get; set; } = null!;
    }
}
