﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzadmRoundRobinLgWi
    {
        public int RoundRobinId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SalesPersonId { get; set; }
    }
}
