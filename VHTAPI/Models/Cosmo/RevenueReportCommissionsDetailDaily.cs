﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class RevenueReportCommissionsDetailDaily
    {
        public int? CompanyId { get; set; }
        public int ContactId { get; set; }
        public string? AgentName { get; set; }
        public string? ContactCode { get; set; }
        public string? CompanyName { get; set; }
        public string? ParentCompany { get; set; }
        public string? RegionalSalesRep { get; set; }
        public string? AreaSalesRep { get; set; }
        public string? AccountManager { get; set; }
        public string? PhotographerRep { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public decimal? TotalRev { get; set; }
        public int? TotalOrders { get; set; }
        public int? TotalClients { get; set; }
        public decimal? OldRev { get; set; }
        public int? OldOrders { get; set; }
        public int? OldClients { get; set; }
        public decimal TrailingRev { get; set; }
        public int TrailingOrders { get; set; }
        public int TrailingClients { get; set; }
        public decimal NewRev { get; set; }
        public int NewOrders { get; set; }
        public int NewClients { get; set; }
        public decimal? YtdRev { get; set; }
        public decimal ReNewRev { get; set; }
        public int ReNewOrders { get; set; }
        public int ReNewClients { get; set; }
        public decimal NewAndReNewRev { get; set; }
        public int NewAndReNewOrders { get; set; }
        public int NewAndReNewClients { get; set; }
        public decimal Viprev { get; set; }
        public int Viporders { get; set; }
        public int Vipclients { get; set; }
    }
}
