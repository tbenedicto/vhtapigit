﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MnbContactGroupOrderForm
    {
        public int ContactGroupOrderFormId { get; set; }
        public int ContactGroupId { get; set; }
        public string? OrderFormPath { get; set; }
        public bool ShowSgp { get; set; }
        public Guid GuidUrl { get; set; }
        public Guid Rowguid { get; set; }
    }
}
