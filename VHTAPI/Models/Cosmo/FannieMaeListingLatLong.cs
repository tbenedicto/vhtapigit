﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class FannieMaeListingLatLong
    {
        public int FmlistingLatLongId { get; set; }
        public int MasterListingId { get; set; }
        public string LatitudeCoordinate { get; set; } = null!;
        public string LongitudeCoordinate { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
