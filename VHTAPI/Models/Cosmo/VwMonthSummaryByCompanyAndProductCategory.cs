﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwMonthSummaryByCompanyAndProductCategory
    {
        public DateTime? MonthCompleted { get; set; }
        public string Category { get; set; } = null!;
        public string CategoryGrouping { get; set; } = null!;
        public int? CompanyId { get; set; }
        public string? CompanyFullName { get; set; }
        public int? TopLevelCompanyId { get; set; }
        public string? TopLevelCompanyFullName { get; set; }
        public string? Territory { get; set; }
        public string? TerritoryState { get; set; }
        public string? CompanyCategory { get; set; }
        public int? NumberOfJobs { get; set; }
        public int? NumberOfUniqueListings { get; set; }
        public int? NumberOfUniqueAgents { get; set; }
        public decimal? TotalPrice { get; set; }
    }
}
