﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzEmployee
    {
        public int EmployeeId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? DisplayAs { get; set; }
        public string? NetworkUserName { get; set; }
        public bool ActiveInd { get; set; }
        public Guid Rowguid { get; set; }
    }
}
