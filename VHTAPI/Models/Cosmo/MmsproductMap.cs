﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MmsproductMap
    {
        public int MmsproductId { get; set; }
        public int? MmsmediaTypeId { get; set; }
        public int ProductId { get; set; }
    }
}
