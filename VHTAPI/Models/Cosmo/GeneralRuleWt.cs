﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class GeneralRuleWt
    {
        public int? GeneralRuleId { get; set; }
        public int? ProductId { get; set; }
    }
}
