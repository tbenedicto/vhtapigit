﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CommissionProgram
    {
        public int CommissionProgramId { get; set; }
        public string? Descr { get; set; }
        public decimal Rate { get; set; }
        public int MonthsInSchedule { get; set; }
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
