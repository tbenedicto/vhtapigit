﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class MmsFirstOrder
    {
        public int MmsloginId { get; set; }
        public int? MmsofficeId { get; set; }
        public int MmscompanyId { get; set; }
        public string? Agent { get; set; }
        public string? CompanyName { get; set; }
        public string? OfficeAddresses { get; set; }
        public string? OfficeCities { get; set; }
        public string? OfficeStates { get; set; }
        public string? OfficeZips { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Year { get; set; }
        public string? Time { get; set; }
        public DateTime? FirstOrderDate { get; set; }
    }
}
