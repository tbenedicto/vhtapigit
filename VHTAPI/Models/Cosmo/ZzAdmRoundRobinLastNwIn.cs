﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzAdmRoundRobinLastNwIn
    {
        public int RoundRobinId { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
