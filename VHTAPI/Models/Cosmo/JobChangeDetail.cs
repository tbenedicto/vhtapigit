﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class JobChangeDetail
    {
        public int JobChangeDetailId { get; set; }
        public int JobChangeId { get; set; }
        public int JobChangeReasonId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
