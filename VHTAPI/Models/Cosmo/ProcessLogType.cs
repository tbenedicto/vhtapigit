﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ProcessLogType
    {
        public ProcessLogType()
        {
            ProcessLogs = new HashSet<ProcessLog>();
            ProcessRuns = new HashSet<ProcessRun>();
        }

        public int ProcessLogTypeId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public bool ShouldFlagForReview { get; set; }
        public string ColorName { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<ProcessLog> ProcessLogs { get; set; }
        public virtual ICollection<ProcessRun> ProcessRuns { get; set; }
    }
}
