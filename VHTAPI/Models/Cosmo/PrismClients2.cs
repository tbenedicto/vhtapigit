﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class PrismClients2
    {
        public string? Name { get; set; }
        public string? MlsLogIn { get; set; }
        public string? MlsPassword { get; set; }
        public string? Agency { get; set; }
        public string? Email { get; set; }
        public string? SecondEmail { get; set; }
        public string? ThirdEmail { get; set; }
        public string? FourthEmail { get; set; }
        public string? Mobile { get; set; }
        public string? Phone { get; set; }
        public string? Fax { get; set; }
        public string? SpecialRequirements { get; set; }
        public string? CommunityPurchased { get; set; }
        public string? SmSites { get; set; }
        public string? TourPlatform { get; set; }
        public string? CardCvv2 { get; set; }
        public string? CardExp2 { get; set; }
        public string? TourLogIn { get; set; }
        public string? TourPassword { get; set; }
        public string? CreditCardOnFile { get; set; }
        public string? OfProjects { get; set; }
        public string? AddProject { get; set; }
        public string? CardCvc { get; set; }
        public string? CardExp { get; set; }
        public string? DateCreated { get; set; }
        public string? DateModified { get; set; }
        public string? IdInQuickBooks { get; set; }
        public string? LastModifiedBy { get; set; }
        public string? Projects { get; set; }
        public string? RecordId { get; set; }
        public string? RecordOwner { get; set; }
        public string? SmLogIn { get; set; }
        public string? SmPassword { get; set; }
        public string? Title { get; set; }
        public string? YoutubeLogIn { get; set; }
        public string? YoutubePassword { get; set; }
    }
}
