﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CreditCard
    {
        public int CreditCardId { get; set; }
        public int ContactId { get; set; }
        public int CreditCardStatusId { get; set; }
        public string? NameOnCard { get; set; }
        public int? CreditCardTypeId { get; set; }
        public string? CreditCardNumber { get; set; }
        public int? ExpirationMonthId { get; set; }
        public int? ExpirationYearId { get; set; }
        public string? BillingAddress1 { get; set; }
        public string? BillingAddress2 { get; set; }
        public string? BillingCity { get; set; }
        public string? BillingState { get; set; }
        public string? BillingZip { get; set; }
        public bool IsDefault { get; set; }
        public string? Last4Nums { get; set; }
        public Guid Rowguid { get; set; }
        public string? Cvncode { get; set; }
        public string? TransId { get; set; }
        public string? Safeid { get; set; }
        public int? AuthStatus { get; set; }
        public string? AuthMessage { get; set; }
        public string? AuthCode { get; set; }
        public DateTime? AddedDate { get; set; }
        public bool? DeletedFromSafe { get; set; }
        public string? DeletedStatus { get; set; }
        public bool SkipSafeupdate { get; set; }

        public virtual Contact Contact { get; set; } = null!;
    }
}
