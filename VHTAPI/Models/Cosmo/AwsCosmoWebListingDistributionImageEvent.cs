﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebListingDistributionImageEvent
    {
        public int ImageEventId { get; set; }
        public int ProcessRunId { get; set; }
        public int ProcessId { get; set; }
        public int TourRoomCodeId { get; set; }
        public string Id { get; set; } = null!;
        public int ListingId { get; set; }
        public int ProcessScheduleDateOptionTypeId { get; set; }
        public DateTime ImageFileDateCreated { get; set; }
        public DateTime? ImageFileDateModified { get; set; }
        public Guid Rowguid { get; set; }
        public Guid Rowguid11 { get; set; }
    }
}
