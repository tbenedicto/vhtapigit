﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class TablSdrBusinessDayYtd
    {
        public int? Month { get; set; }
        public int? Year { get; set; }
        public double? BusinessDaysThisMonth { get; set; }
        public double? BusinessDaysPassed { get; set; }
        public double? BusinessDaysThisYear { get; set; }
        public double? TotalRevYtdtarget { get; set; }
        public double? Projections { get; set; }
    }
}
