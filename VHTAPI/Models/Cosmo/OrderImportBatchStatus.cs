﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderImportBatchStatus
    {
        public OrderImportBatchStatus()
        {
            OrderImportBatches = new HashSet<OrderImportBatch>();
        }

        public int OrderImportBatchStatusId { get; set; }
        public int SortId { get; set; }
        public string Descr { get; set; } = null!;
        public Guid Rowguid { get; set; }

        public virtual ICollection<OrderImportBatch> OrderImportBatches { get; set; }
    }
}
