﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ContactProjectedOrder
    {
        public int Contactid { get; set; }
        public int Listingid { get; set; }
        public string? _1stStartdate { get; set; }
        public int? ProjectedJobid { get; set; }
        public string ContactHistoryType { get; set; } = null!;
        public string? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? StatusProductionCompletedOn { get; set; }
        public string? JobSource { get; set; }
        public string? Stage { get; set; }
        public decimal? TotalRev { get; set; }
    }
}
