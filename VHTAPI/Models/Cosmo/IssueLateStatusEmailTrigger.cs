﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class IssueLateStatusEmailTrigger
    {
        public int IssueLateStatusEmailTriggerId { get; set; }
        public int IssueId { get; set; }
        public string FkParentSource { get; set; } = null!;
        public int FkParentId { get; set; }
        public int? OldIssueStatusId { get; set; }
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
    }
}
