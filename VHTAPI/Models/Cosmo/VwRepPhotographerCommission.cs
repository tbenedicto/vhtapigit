﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwRepPhotographerCommission
    {
        public int? RepPhotographer { get; set; }
        public string? RepName { get; set; }
        public string? CompanyCategoryType { get; set; }
        public string? Name { get; set; }
        public int PhotographerCompanyType { get; set; }
        public string CommissionType { get; set; } = null!;
        public double? Month1 { get; set; }
        public double? Month212 { get; set; }
        public int? ContactId { get; set; }
        public int? CompanyId { get; set; }
        public int? CompanyCategoryId { get; set; }
        public string? CompanyName { get; set; }
        public string? DisplayAs { get; set; }
        public int? ParentCompanyId { get; set; }
    }
}
