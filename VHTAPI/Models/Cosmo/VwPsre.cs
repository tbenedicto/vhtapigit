﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwPsre
    {
        public string Mlsid { get; set; } = null!;
        public string Phone { get; set; } = null!;
        public string Website { get; set; } = null!;
        public int? ContactId { get; set; }
        public string? ContactCode { get; set; }
        public string? TourDisplayPhone { get; set; }
        public string? TourDisplayWebsite { get; set; }
    }
}
