﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImageExchangeLog
    {
        public int LogId { get; set; }
        public Guid PartnerId { get; set; }
        public int ListingId { get; set; }
        public int ImageId { get; set; }
        public string Vhtemail { get; set; } = null!;
        public int CustomerId { get; set; }
        public string BrokerId { get; set; } = null!;
        public string PartnerUserId { get; set; } = null!;
        public DateTime LogDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
