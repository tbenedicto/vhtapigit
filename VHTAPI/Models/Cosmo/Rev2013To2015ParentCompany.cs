﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class Rev2013To2015ParentCompany
    {
        public string? CompanyName { get; set; }
        public decimal? Sold { get; set; }
        public int? Companyid { get; set; }
        public int? CountCompany { get; set; }
    }
}
