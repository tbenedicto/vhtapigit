﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class VwCirlePixOrderCompletedPriorMonthMb
    {
        public int ListingId { get; set; }
        public int? CompanyId { get; set; }
        public int? ParentCompanyId { get; set; }
        public string? Companyname { get; set; }
        public int ContactId { get; set; }
        public string? PreloadedTourId { get; set; }
        public string? AgentName { get; set; }
        public int? CompanysubsidiaryId { get; set; }
        public string? ContactCode { get; set; }
        public string? AgentFirstName { get; set; }
        public string? AgentLastName { get; set; }
        public decimal? TotalrevByListing { get; set; }
        public int ContentServerId { get; set; }
        public int G3codeId { get; set; }
        public int? PropertyListPrice { get; set; }
        public string? PropertyAddressLine1 { get; set; }
        public string? PropertyAddressLine2 { get; set; }
        public string? PropertyCity { get; set; }
        public string? PropertyState { get; set; }
        public string? PropertyZip { get; set; }
        public int? AgentId { get; set; }
        public string? RelistString { get; set; }
        public string? County { get; set; }
        public string? HomeownerName { get; set; }
        public string? HomeownerEmail { get; set; }
        public string? Beds { get; set; }
        public string? Baths { get; set; }
        public bool? GoodListingInd { get; set; }
        public string Market { get; set; } = null!;
        public string? Vhtrelationship { get; set; }
    }
}
