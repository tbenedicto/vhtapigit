﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ImportPackageRules20190418Cv
    {
        public string? PackageRuleId { get; set; }
        public double? PackageId { get; set; }
        public string? PackageInternalName { get; set; }
        public double? RuleConditionTypeId { get; set; }
        public string? PackageRuleProductDataId { get; set; }
        public double? PackageProductId { get; set; }
        public double? ProductId { get; set; }
        public string? ProductInternalName { get; set; }
        public string? Type { get; set; }
        public string? ContactId { get; set; }
        public string? ContactName { get; set; }
        public string? CompanyId { get; set; }
        public string? CompanyName { get; set; }
        public string? OfficeType { get; set; }
        public string? VhtRelationship { get; set; }
        public string? RegionalSales { get; set; }
        public double? Cvid { get; set; }
        public string? Cvname { get; set; }
        public string? ProductRegionId { get; set; }
        public string? Market { get; set; }
        public string? StateId { get; set; }
        public string? StateName { get; set; }
        public string? ContactGroupId { get; set; }
        public string? ContactGroupName { get; set; }
        public string? RetailPrice { get; set; }
        public string? CustomerPay { get; set; }
        public string? CustomerRateAdj { get; set; }
        public string? CorporatePay { get; set; }
        public string? CorporateRateAdj { get; set; }
        public double? Cvbase { get; set; }
        public double? CvrateAdj { get; set; }
        public double? Cvbonus { get; set; }
        public string? ProofPhotoCredit { get; set; }
        public string? ProofAmountCredit { get; set; }
        public string? ProofPercentCredit { get; set; }
        public string? ScreenshotUrl { get; set; }
        public string? SampleUrl { get; set; }
        public string? F38 { get; set; }
    }
}
