﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CompanyOrderFormCategory
    {
        public int CompanyOrderFormCategoryId { get; set; }
        public int CategoryId { get; set; }
        public int CompanyId { get; set; }
        public string CategoryLabel { get; set; } = null!;
        public string OptionLabel { get; set; } = null!;
        public string AfterLabelContent { get; set; } = null!;
        public string BeforeLabelContent { get; set; } = null!;
        public string? OverrideSelection { get; set; }
        public Guid Rowguid { get; set; }
        public int CompanyOrderFormId { get; set; }
        public bool ProductMoreInfoEnable { get; set; }
        public string ProductMoreInfoLabel { get; set; } = null!;
        public string ProductMoreInfoLink { get; set; } = null!;
        public bool HasCustomSeries { get; set; }
        public string? SeriesLabel { get; set; }
        public string? SeriesContent { get; set; }
        public bool HasCustomMoreInfo { get; set; }
        public string? MoreInfoLabel { get; set; }
        public string? MoreInfoContent { get; set; }
    }
}
