﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class ZzCompanyUpsell
    {
        public ZzCompanyUpsell()
        {
            CompanyCompanyUpsells = new HashSet<CompanyCompanyUpsell>();
        }

        public int CompanyUpsellId { get; set; }
        public string Descr { get; set; } = null!;
        public int SortId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual ICollection<CompanyCompanyUpsell> CompanyCompanyUpsells { get; set; }
    }
}
