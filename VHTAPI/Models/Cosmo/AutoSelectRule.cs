﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AutoSelectRule
    {
        public int AutoSelectRuleId { get; set; }
        public string TargetTable { get; set; } = null!;
        public int TargetId { get; set; }
        public int AutoSelectRoomGroupListId1 { get; set; }
        public int? AutoSelectRoomGroupListId2 { get; set; }
        public int? SelectionQuantity { get; set; }
        public int? NumHoursAfterStudioComplete { get; set; }
        public bool OptedOut { get; set; }
        public bool Active { get; set; }
        public DateTime AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
    }
}
