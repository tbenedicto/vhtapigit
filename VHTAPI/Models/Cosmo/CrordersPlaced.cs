﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class CrordersPlaced
    {
        public int RowId { get; set; }
        public int? ProductRegionId { get; set; }
        public DateTime? WeekDate { get; set; }
        public Guid Rowguid { get; set; }
    }
}
