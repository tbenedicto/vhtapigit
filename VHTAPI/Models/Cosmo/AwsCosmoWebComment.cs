﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebComment
    {
        public int CommentId { get; set; }
        public string FkparentSource { get; set; } = null!;
        public int FkparentId { get; set; }
        public int CommentCategoryId { get; set; }
        public string Subject { get; set; } = null!;
        public string? CommentText { get; set; }
        public DateTime? AddedDate { get; set; }
        public string? AddedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string? ModifiedBy { get; set; }
        public bool PropogateToJob { get; set; }
        public string? SortField { get; set; }
        public Guid Rowguid { get; set; }
        public int RecipientTypeId { get; set; }
        public int RecipientId { get; set; }
        public bool IsUnread { get; set; }
        public bool IsPublic { get; set; }
        public bool IsAutomated { get; set; }
    }
}
