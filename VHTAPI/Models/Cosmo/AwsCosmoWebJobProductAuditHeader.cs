﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class AwsCosmoWebJobProductAuditHeader
    {
        public int JobProductAuditHeaderId { get; set; }
        public DateTime? AuditDate { get; set; }
        public int? JobId { get; set; }
        public int? JobStatusId { get; set; }
        public Guid Rowguid { get; set; }
    }
}
