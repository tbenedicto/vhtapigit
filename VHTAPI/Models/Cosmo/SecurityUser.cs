﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class SecurityUser
    {
        public SecurityUser()
        {
            SecurityGroupMembers = new HashSet<SecurityGroupMember>();
        }

        public int SecurityUserId { get; set; }
        public bool ActiveInd { get; set; }
        public bool SystemInd { get; set; }
        public int SecurityUserTypeId { get; set; }
        public string Username { get; set; } = null!;
        public string PasswordHash { get; set; } = null!;
        public DateTime AddedDate { get; set; }
        public string AddedBy { get; set; } = null!;
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; } = null!;
        public Guid Rowguid { get; set; }
        public int? ContactId { get; set; }

        public virtual ZzSecurityUserType SecurityUserType { get; set; } = null!;
        public virtual ICollection<SecurityGroupMember> SecurityGroupMembers { get; set; }
    }
}
