﻿using System;
using System.Collections.Generic;

namespace VHTAPI.Models.Cosmo
{
    public partial class OrderImportItem
    {
        public int OrderImportItemId { get; set; }
        public int OrderImportBatchId { get; set; }
        public bool DoNotImportInd { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string CorporateContactId { get; set; } = null!;
        public string CompanyName { get; set; } = null!;
        public string MainPhone { get; set; } = null!;
        public string MainPhoneExtension { get; set; } = null!;
        public string CellPhone { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string PropertyAddressLine1 { get; set; } = null!;
        public string PropertyAddressLine2 { get; set; } = null!;
        public string City { get; set; } = null!;
        public string State { get; set; } = null!;
        public string Zip { get; set; } = null!;
        public string Development { get; set; } = null!;
        public string Beds { get; set; } = null!;
        public string Baths { get; set; } = null!;
        public int ListingPrice { get; set; }
        public int ListingTypeId { get; set; }
        public int PropertyIdtype { get; set; }
        public string PropertyId { get; set; } = null!;
        public string CustomerJobId { get; set; } = null!;
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string Notes { get; set; } = null!;
        public int CustomerVerificationStatusId { get; set; }
        public int AttachToCustomerId { get; set; }
        public int AttachNewCustomerToCompanyId { get; set; }
        public int ListingVerificationStatusId { get; set; }
        public int AttachToListingId { get; set; }
        public Guid Rowguid { get; set; }

        public virtual CustomerVerificationStatus CustomerVerificationStatus { get; set; } = null!;
        public virtual ListingVerificationStatus ListingVerificationStatus { get; set; } = null!;
    }
}
