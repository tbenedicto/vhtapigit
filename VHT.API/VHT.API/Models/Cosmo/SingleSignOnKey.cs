﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class SingleSignOnKey
    {
        [Key]
        public Guid SignOnKey{ get; set; }
        public string BrokerageIPAddress { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
