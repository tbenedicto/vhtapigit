﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class Company
    {
        [Key]
        public int CompanyID { get; set; }
        public int CompanyCategoryID { get; set; }

        // TODO: add more Company fields
    }
}
