﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Controllers.Cosmo
{
    public class Comment
    {
        public int CommentID { get; set; }
        public string Subject { get; set;}
        public string CommentText { get; set; }

        // TODO: add more Comment fields
    }
}
