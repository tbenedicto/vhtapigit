﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class PackageProducts
    {
        [Key]
        public int PackageProductID { get; set; }
        public int PackageID { get; set; }
        public int ProductID { get; set; }
        // TODO: add more fields
    }
}
