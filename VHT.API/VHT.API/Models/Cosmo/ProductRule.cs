﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Controllers.Cosmo
{
    public class ProductRule
    {
        [Key]
        public int ProductRuleID { get; set; }
        public int ProductID { get; set; }

        // TODO: add more fields from ProductRule table
    }
}
