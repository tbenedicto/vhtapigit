﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class BrokerageIDMatch
    {
        [Key]
        public string AgentKey { get; set; }
        public int ContactID { get; set; }
        public int BrokerageCompanyID { get; set; }
        public Guid RowGuid { get; set; }

    }
}
