﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class Contact
    {
        [Key]
        public int ContactID { get; set; }
        public int CompanyID { get; set; }

        // TODO: add more fields from Contact table
    }
}
