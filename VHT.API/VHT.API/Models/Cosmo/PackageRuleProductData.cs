﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class PackageRuleProductData
    {
        [Key]
        public int PackageRuleProductDataID { get; set; }
        public int PackageRuleID { get; set; }
        public int PackageProductID { get; set; }
        // TODO: add more fields
    }
}
