﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        public string InternalDescription { get; set; }

        // TODO: add more fiels from Product table
    }
}
