﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Controllers.Cosmo
{
    public class PackageRule
    {
        [Key]
        public int PackageRuleID { get; set; }
        public int PackageID { get; set; }

        // TODO: add more fields from PackageRule table
    }
}
