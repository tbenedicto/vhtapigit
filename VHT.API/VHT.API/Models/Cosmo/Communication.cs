﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class Communication
    {
        [Key]
        public int CommunicationID { get; set; }
        public string FKParentSource { get; set; }
        // TODO: add more fields from Communication table
    }
}
