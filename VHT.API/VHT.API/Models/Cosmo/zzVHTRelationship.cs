﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class zzVHTRelationship
    {
        [Key]
        public int VHTRelationshipID { get; set; }
        public int SortID { get; set; }
        // TODO: add more fields
    }
}
