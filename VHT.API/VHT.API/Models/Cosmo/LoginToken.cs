﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class LoginToken
    {
        [Key]
        public Guid LoginTokenID { get; set; }
        public string ASPSessionID { get; set; }
        public string UserAgent { get; set; }
        // TODO: add more fields from LoginToken table
    }
}
