﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VHT.API.Models.Cosmo;
using VHT.API.Models;
using VHT.API.Controllers.Cosmo;

namespace VHT.API.Models
{
    public class CosmoDbContext : DbContext
    {
        public CosmoDbContext(DbContextOptions<CosmoDbContext> options): base (options)
        {
            
        }

        public DbSet<BrokerageMultipleIP> BrokerageMultipleIP { get; set; }

        public DbSet<SingleSignOnKey> SingleSignOnKey { get; set; }

        public DbSet<BrokerageIDMatch> BrokerageIDMatch { get; set; }

        public DbSet<BrokerageIP> BrokerageIP { get; set; }

        public DbSet<Contact> Contact { get; set; }

        public DbSet<Company> Company { get; set; }

        public DbSet<Communication> Communication { get; set; }

        public DbSet<LoginToken> LoginToken { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<Package> Package { get; set; }

        public DbSet<ImportConfig> ImportConfig { get; set; }

        public DbSet<Listing> Listing { get; set; }

        public DbSet<PackageRule> PackageRule { get; set; }

        public DbSet<ProductRule> ProductRule { get; set; }

        public DbSet<VHT.API.Controllers.Cosmo.Comment> Comment { get; set; }

        public DbSet<VHT.API.Models.Cosmo.PackageProducts> PackageProducts { get; set; }

        public DbSet<VHT.API.Models.Cosmo.PackageRuleProductData> PackageRuleProductData { get; set; }

        public DbSet<VHT.API.Models.Cosmo.zzOfficeType> zzOfficeType { get; set; }

        public DbSet<VHT.API.Models.Cosmo.zzState> zzState { get; set; }

        public DbSet<VHT.API.Models.Cosmo.zzVHTRelationship> zzVHTRelationship { get; set; }

    }
}
