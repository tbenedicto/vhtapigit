﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class ImportConfig
    {
        public int ImportConfigID { get; set; }
        public string ImportConfigName { get; set; }

        // TODO: add more fields frm ImportConfig table

    }
}
