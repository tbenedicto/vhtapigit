﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class Package
    {
        [Key]
        public int PackageID { get; set; }
        public string PackageName { get; set; }

        // TODO: add more fields from Package table
    }
}
