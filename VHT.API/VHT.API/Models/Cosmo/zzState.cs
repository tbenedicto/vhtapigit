﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class zzState
    {
        [Key]
        public string StateID { get; set; }
        public string StateName { get; set; }
        //TODO: add more fields
    }
}
