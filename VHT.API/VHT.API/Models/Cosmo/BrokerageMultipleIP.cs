﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class BrokerageMultipleIP
    {
        [Key]
        public string IPAddress { get; set; }
        public int BrokerageCompanyID { get; set; }
    }
}
