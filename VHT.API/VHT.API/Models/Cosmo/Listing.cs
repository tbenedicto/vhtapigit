﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VHT.API.Models.Cosmo
{
    public class Listing
    {
        [Key]
        public int ListingID { get; set; }
        public int ContentServerID { get; set; }

        // TODO: add more fields from Listing table
    }
}
