﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class BrokerageIDMatchController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public BrokerageIDMatchController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/BrokerageIDMatch
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BrokerageIDMatch>>> GetBrokerageIDMatch()
        {
            return await _context.BrokerageIDMatch.ToListAsync();
        }

        // GET: api/BrokerageIDMatch/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BrokerageIDMatch>> GetBrokerageIDMatch(string id)
        {
            var brokerageIDMatch = await _context.BrokerageIDMatch.FindAsync(id);

            if (brokerageIDMatch == null)
            {
                return NotFound();
            }

            return brokerageIDMatch;
        }

        // PUT: api/BrokerageIDMatch/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrokerageIDMatch(string id, BrokerageIDMatch brokerageIDMatch)
        {
            if (id != brokerageIDMatch.AgentKey)
            {
                return BadRequest();
            }

            _context.Entry(brokerageIDMatch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrokerageIDMatchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BrokerageIDMatch
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BrokerageIDMatch>> PostBrokerageIDMatch(BrokerageIDMatch brokerageIDMatch)
        {
            _context.BrokerageIDMatch.Add(brokerageIDMatch);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BrokerageIDMatchExists(brokerageIDMatch.AgentKey))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetBrokerageIDMatch", new { id = brokerageIDMatch.AgentKey }, brokerageIDMatch);
        }

        // DELETE: api/BrokerageIDMatch/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrokerageIDMatch(string id)
        {
            var brokerageIDMatch = await _context.BrokerageIDMatch.FindAsync(id);
            if (brokerageIDMatch == null)
            {
                return NotFound();
            }

            _context.BrokerageIDMatch.Remove(brokerageIDMatch);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BrokerageIDMatchExists(string id)
        {
            return _context.BrokerageIDMatch.Any(e => e.AgentKey == id);
        }
    }
}
