﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class ProductRuleController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public ProductRuleController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/ProductRule
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductRule>>> GetProductRule()
        {
            return await _context.ProductRule.ToListAsync();
        }

        // GET: api/ProductRule/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductRule>> GetProductRule(int id)
        {
            var productRule = await _context.ProductRule.FindAsync(id);

            if (productRule == null)
            {
                return NotFound();
            }

            return productRule;
        }

        // PUT: api/ProductRule/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductRule(int id, ProductRule productRule)
        {
            if (id != productRule.ProductRuleID)
            {
                return BadRequest();
            }

            _context.Entry(productRule).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductRuleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductRule
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ProductRule>> PostProductRule(ProductRule productRule)
        {
            _context.ProductRule.Add(productRule);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductRule", new { id = productRule.ProductRuleID }, productRule);
        }

        // DELETE: api/ProductRule/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductRule(int id)
        {
            var productRule = await _context.ProductRule.FindAsync(id);
            if (productRule == null)
            {
                return NotFound();
            }

            _context.ProductRule.Remove(productRule);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductRuleExists(int id)
        {
            return _context.ProductRule.Any(e => e.ProductRuleID == id);
        }
    }
}
