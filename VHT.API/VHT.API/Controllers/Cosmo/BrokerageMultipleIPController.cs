﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class BrokerageMultipleIPController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public BrokerageMultipleIPController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/BrokerageMultipleIPs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BrokerageMultipleIP>>> GetBrokerageMultipleIP()
        {
            return await _context.BrokerageMultipleIP.ToListAsync();
        }

        // GET: api/BrokerageMultipleIPs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BrokerageMultipleIP>> GetBrokerageMultipleIP(string id)
        {
            var brokerageMultipleIP = await _context.BrokerageMultipleIP.FindAsync(id);

            if (brokerageMultipleIP == null)
            {
                return NotFound();
            }

            return brokerageMultipleIP;
        }

        // PUT: api/BrokerageMultipleIPs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrokerageMultipleIP(string id, BrokerageMultipleIP brokerageMultipleIP)
        {
            if (id != brokerageMultipleIP.IPAddress)
            {
                return BadRequest();
            }

            _context.Entry(brokerageMultipleIP).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrokerageMultipleIPExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BrokerageMultipleIPs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BrokerageMultipleIP>> PostBrokerageMultipleIP(BrokerageMultipleIP brokerageMultipleIP)
        {
            _context.BrokerageMultipleIP.Add(brokerageMultipleIP);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BrokerageMultipleIPExists(brokerageMultipleIP.IPAddress))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetBrokerageMultipleIP", new { id = brokerageMultipleIP.IPAddress }, brokerageMultipleIP);
        }

        // DELETE: api/BrokerageMultipleIPs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrokerageMultipleIP(string id)
        {
            var brokerageMultipleIP = await _context.BrokerageMultipleIP.FindAsync(id);
            if (brokerageMultipleIP == null)
            {
                return NotFound();
            }

            _context.BrokerageMultipleIP.Remove(brokerageMultipleIP);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BrokerageMultipleIPExists(string id)
        {
            return _context.BrokerageMultipleIP.Any(e => e.IPAddress == id);
        }
    }
}
