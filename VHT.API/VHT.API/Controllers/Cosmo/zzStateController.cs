﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class zzStateController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public zzStateController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/zzState
        [HttpGet]
        public async Task<ActionResult<IEnumerable<zzState>>> GetzzState()
        {
            return await _context.zzState.ToListAsync();
        }

        // GET: api/zzState/5
        [HttpGet("{id}")]
        public async Task<ActionResult<zzState>> GetzzState(string id)
        {
            var zzState = await _context.zzState.FindAsync(id);

            if (zzState == null)
            {
                return NotFound();
            }

            return zzState;
        }

        // PUT: api/zzState/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutzzState(string id, zzState zzState)
        {
            if (id != zzState.StateID)
            {
                return BadRequest();
            }

            _context.Entry(zzState).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!zzStateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/zzState
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<zzState>> PostzzState(zzState zzState)
        {
            _context.zzState.Add(zzState);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (zzStateExists(zzState.StateID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetzzState", new { id = zzState.StateID }, zzState);
        }

        // DELETE: api/zzState/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletezzState(string id)
        {
            var zzState = await _context.zzState.FindAsync(id);
            if (zzState == null)
            {
                return NotFound();
            }

            _context.zzState.Remove(zzState);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool zzStateExists(string id)
        {
            return _context.zzState.Any(e => e.StateID == id);
        }
    }
}
