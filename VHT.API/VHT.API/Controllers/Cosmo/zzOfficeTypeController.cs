﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class zzOfficeTypeController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public zzOfficeTypeController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/zzOfficeType
        [HttpGet]
        public async Task<ActionResult<IEnumerable<zzOfficeType>>> GetzzOfficeType()
        {
            return await _context.zzOfficeType.ToListAsync();
        }

        // GET: api/zzOfficeType/5
        [HttpGet("{id}")]
        public async Task<ActionResult<zzOfficeType>> GetzzOfficeType(int id)
        {
            var zzOfficeType = await _context.zzOfficeType.FindAsync(id);

            if (zzOfficeType == null)
            {
                return NotFound();
            }

            return zzOfficeType;
        }

        // PUT: api/zzOfficeType/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutzzOfficeType(int id, zzOfficeType zzOfficeType)
        {
            if (id != zzOfficeType.OfficeTypeID)
            {
                return BadRequest();
            }

            _context.Entry(zzOfficeType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!zzOfficeTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/zzOfficeType
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<zzOfficeType>> PostzzOfficeType(zzOfficeType zzOfficeType)
        {
            _context.zzOfficeType.Add(zzOfficeType);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetzzOfficeType", new { id = zzOfficeType.OfficeTypeID }, zzOfficeType);
        }

        // DELETE: api/zzOfficeType/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletezzOfficeType(int id)
        {
            var zzOfficeType = await _context.zzOfficeType.FindAsync(id);
            if (zzOfficeType == null)
            {
                return NotFound();
            }

            _context.zzOfficeType.Remove(zzOfficeType);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool zzOfficeTypeExists(int id)
        {
            return _context.zzOfficeType.Any(e => e.OfficeTypeID == id);
        }
    }
}
