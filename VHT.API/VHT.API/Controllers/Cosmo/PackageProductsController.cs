﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class PackageProductsController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public PackageProductsController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/PackageProducts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PackageProducts>>> GetPackageProducts()
        {
            return await _context.PackageProducts.ToListAsync();
        }

        // GET: api/PackageProducts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PackageProducts>> GetPackageProducts(int id)
        {
            var packageProducts = await _context.PackageProducts.FindAsync(id);

            if (packageProducts == null)
            {
                return NotFound();
            }

            return packageProducts;
        }

        // PUT: api/PackageProducts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPackageProducts(int id, PackageProducts packageProducts)
        {
            if (id != packageProducts.PackageProductID)
            {
                return BadRequest();
            }

            _context.Entry(packageProducts).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PackageProductsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PackageProducts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PackageProducts>> PostPackageProducts(PackageProducts packageProducts)
        {
            _context.PackageProducts.Add(packageProducts);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPackageProducts", new { id = packageProducts.PackageProductID }, packageProducts);
        }

        // DELETE: api/PackageProducts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePackageProducts(int id)
        {
            var packageProducts = await _context.PackageProducts.FindAsync(id);
            if (packageProducts == null)
            {
                return NotFound();
            }

            _context.PackageProducts.Remove(packageProducts);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PackageProductsExists(int id)
        {
            return _context.PackageProducts.Any(e => e.PackageProductID == id);
        }
    }
}
