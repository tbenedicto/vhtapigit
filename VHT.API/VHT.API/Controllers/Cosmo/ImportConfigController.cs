﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class ImportConfigController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public ImportConfigController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/ImportConfig
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ImportConfig>>> GetImportConfig()
        {
            return await _context.ImportConfig.ToListAsync();
        }

        // GET: api/ImportConfig/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ImportConfig>> GetImportConfig(int id)
        {
            var importConfig = await _context.ImportConfig.FindAsync(id);

            if (importConfig == null)
            {
                return NotFound();
            }

            return importConfig;
        }

        // PUT: api/ImportConfig/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutImportConfig(int id, ImportConfig importConfig)
        {
            if (id != importConfig.ImportConfigID)
            {
                return BadRequest();
            }

            _context.Entry(importConfig).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImportConfigExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ImportConfig
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ImportConfig>> PostImportConfig(ImportConfig importConfig)
        {
            _context.ImportConfig.Add(importConfig);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetImportConfig", new { id = importConfig.ImportConfigID }, importConfig);
        }

        // DELETE: api/ImportConfig/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteImportConfig(int id)
        {
            var importConfig = await _context.ImportConfig.FindAsync(id);
            if (importConfig == null)
            {
                return NotFound();
            }

            _context.ImportConfig.Remove(importConfig);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ImportConfigExists(int id)
        {
            return _context.ImportConfig.Any(e => e.ImportConfigID == id);
        }
    }
}
