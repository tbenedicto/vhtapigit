﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class PackageRuleProductDataController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public PackageRuleProductDataController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/PackageRuleProductData
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PackageRuleProductData>>> GetPackageRuleProductData()
        {
            return await _context.PackageRuleProductData.ToListAsync();
        }

        // GET: api/PackageRuleProductData/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PackageRuleProductData>> GetPackageRuleProductData(int id)
        {
            var packageRuleProductData = await _context.PackageRuleProductData.FindAsync(id);

            if (packageRuleProductData == null)
            {
                return NotFound();
            }

            return packageRuleProductData;
        }

        // PUT: api/PackageRuleProductData/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPackageRuleProductData(int id, PackageRuleProductData packageRuleProductData)
        {
            if (id != packageRuleProductData.PackageRuleProductDataID)
            {
                return BadRequest();
            }

            _context.Entry(packageRuleProductData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PackageRuleProductDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PackageRuleProductData
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PackageRuleProductData>> PostPackageRuleProductData(PackageRuleProductData packageRuleProductData)
        {
            _context.PackageRuleProductData.Add(packageRuleProductData);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPackageRuleProductData", new { id = packageRuleProductData.PackageRuleProductDataID }, packageRuleProductData);
        }

        // DELETE: api/PackageRuleProductData/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePackageRuleProductData(int id)
        {
            var packageRuleProductData = await _context.PackageRuleProductData.FindAsync(id);
            if (packageRuleProductData == null)
            {
                return NotFound();
            }

            _context.PackageRuleProductData.Remove(packageRuleProductData);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PackageRuleProductDataExists(int id)
        {
            return _context.PackageRuleProductData.Any(e => e.PackageRuleProductDataID == id);
        }
    }
}
