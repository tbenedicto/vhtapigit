﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class BrokerageIPController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public BrokerageIPController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/BrokerageIP
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BrokerageIP>>> GetBrokerageIP()
        {
            return await _context.BrokerageIP.ToListAsync();
        }

        // GET: api/BrokerageIP/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BrokerageIP>> GetBrokerageIP(string id)
        {
            var brokerageIP = await _context.BrokerageIP.FindAsync(id);

            if (brokerageIP == null)
            {
                return NotFound();
            }

            return brokerageIP;
        }

        // PUT: api/BrokerageIP/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBrokerageIP(string id, BrokerageIP brokerageIP)
        {
            if (id != brokerageIP.IPAddress)
            {
                return BadRequest();
            }

            _context.Entry(brokerageIP).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrokerageIPExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BrokerageIP
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BrokerageIP>> PostBrokerageIP(BrokerageIP brokerageIP)
        {
            _context.BrokerageIP.Add(brokerageIP);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BrokerageIPExists(brokerageIP.IPAddress))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetBrokerageIP", new { id = brokerageIP.IPAddress }, brokerageIP);
        }

        // DELETE: api/BrokerageIP/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBrokerageIP(string id)
        {
            var brokerageIP = await _context.BrokerageIP.FindAsync(id);
            if (brokerageIP == null)
            {
                return NotFound();
            }

            _context.BrokerageIP.Remove(brokerageIP);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BrokerageIPExists(string id)
        {
            return _context.BrokerageIP.Any(e => e.IPAddress == id);
        }
    }
}
