﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class SingleSignOnKeyController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public SingleSignOnKeyController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/SingleSignOnKeys
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SingleSignOnKey>>> GetSingleSignOnKey()
        {
            return await _context.SingleSignOnKey.ToListAsync();
        }

        // GET: api/SingleSignOnKeys/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SingleSignOnKey>> GetSingleSignOnKey(Guid id)
        {
            var singleSignOnKey = await _context.SingleSignOnKey.FindAsync(id);

            if (singleSignOnKey == null)
            {
                return NotFound();
            }

            return singleSignOnKey;
        }

        // PUT: api/SingleSignOnKeys/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSingleSignOnKey(Guid id, SingleSignOnKey singleSignOnKey)
        {
            if (id != singleSignOnKey.SignOnKey)
            {
                return BadRequest();
            }

            _context.Entry(singleSignOnKey).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SingleSignOnKeyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SingleSignOnKeys
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SingleSignOnKey>> PostSingleSignOnKey(SingleSignOnKey singleSignOnKey)
        {
            _context.SingleSignOnKey.Add(singleSignOnKey);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSingleSignOnKey", new { id = singleSignOnKey.SignOnKey }, singleSignOnKey);
        }

        // DELETE: api/SingleSignOnKeys/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSingleSignOnKey(Guid id)
        {
            var singleSignOnKey = await _context.SingleSignOnKey.FindAsync(id);
            if (singleSignOnKey == null)
            {
                return NotFound();
            }

            _context.SingleSignOnKey.Remove(singleSignOnKey);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool SingleSignOnKeyExists(Guid id)
        {
            return _context.SingleSignOnKey.Any(e => e.SignOnKey == id);
        }
    }
}
