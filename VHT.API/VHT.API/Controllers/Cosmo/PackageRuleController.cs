﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class PackageRuleController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public PackageRuleController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/PackageRule
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PackageRule>>> GetPackageRule()
        {
            return await _context.PackageRule.ToListAsync();
        }

        // GET: api/PackageRule/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PackageRule>> GetPackageRule(int id)
        {
            var packageRule = await _context.PackageRule.FindAsync(id);

            if (packageRule == null)
            {
                return NotFound();
            }

            return packageRule;
        }

        // PUT: api/PackageRule/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPackageRule(int id, PackageRule packageRule)
        {
            if (id != packageRule.PackageRuleID)
            {
                return BadRequest();
            }

            _context.Entry(packageRule).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PackageRuleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PackageRule
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PackageRule>> PostPackageRule(PackageRule packageRule)
        {
            _context.PackageRule.Add(packageRule);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPackageRule", new { id = packageRule.PackageRuleID }, packageRule);
        }

        // DELETE: api/PackageRule/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePackageRule(int id)
        {
            var packageRule = await _context.PackageRule.FindAsync(id);
            if (packageRule == null)
            {
                return NotFound();
            }

            _context.PackageRule.Remove(packageRule);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PackageRuleExists(int id)
        {
            return _context.PackageRule.Any(e => e.PackageRuleID == id);
        }
    }
}
