﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class CommunicationController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public CommunicationController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/Communication
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Communication>>> GetCommunication()
        {
            return await _context.Communication.ToListAsync();
        }

        // GET: api/Communication/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Communication>> GetCommunication(int id)
        {
            var communication = await _context.Communication.FindAsync(id);

            if (communication == null)
            {
                return NotFound();
            }

            return communication;
        }

        // PUT: api/Communication/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCommunication(int id, Communication communication)
        {
            if (id != communication.CommunicationID)
            {
                return BadRequest();
            }

            _context.Entry(communication).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommunicationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Communication
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Communication>> PostCommunication(Communication communication)
        {
            _context.Communication.Add(communication);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCommunication", new { id = communication.CommunicationID }, communication);
        }

        // DELETE: api/Communication/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCommunication(int id)
        {
            var communication = await _context.Communication.FindAsync(id);
            if (communication == null)
            {
                return NotFound();
            }

            _context.Communication.Remove(communication);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CommunicationExists(int id)
        {
            return _context.Communication.Any(e => e.CommunicationID == id);
        }
    }
}
