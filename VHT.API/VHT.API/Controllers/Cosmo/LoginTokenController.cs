﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class LoginTokenController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public LoginTokenController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/LoginToken
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LoginToken>>> GetLoginToken()
        {
            return await _context.LoginToken.ToListAsync();
        }

        // GET: api/LoginToken/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LoginToken>> GetLoginToken(Guid id)
        {
            var loginToken = await _context.LoginToken.FindAsync(id);

            if (loginToken == null)
            {
                return NotFound();
            }

            return loginToken;
        }

        // PUT: api/LoginToken/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLoginToken(Guid id, LoginToken loginToken)
        {
            if (id != loginToken.LoginTokenID)
            {
                return BadRequest();
            }

            _context.Entry(loginToken).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoginTokenExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LoginToken
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LoginToken>> PostLoginToken(LoginToken loginToken)
        {
            _context.LoginToken.Add(loginToken);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLoginToken", new { id = loginToken.LoginTokenID }, loginToken);
        }

        // DELETE: api/LoginToken/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLoginToken(Guid id)
        {
            var loginToken = await _context.LoginToken.FindAsync(id);
            if (loginToken == null)
            {
                return NotFound();
            }

            _context.LoginToken.Remove(loginToken);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LoginTokenExists(Guid id)
        {
            return _context.LoginToken.Any(e => e.LoginTokenID == id);
        }
    }
}
