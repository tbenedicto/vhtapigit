﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VHT.API.Models;
using VHT.API.Models.Cosmo;

namespace VHT.API.Controllers.Cosmo
{
    [Route("api/cosmo/[controller]")]
    [ApiController]
    public class zzVHTRelationshipController : ControllerBase
    {
        private readonly CosmoDbContext _context;

        public zzVHTRelationshipController(CosmoDbContext context)
        {
            _context = context;
        }

        // GET: api/zzVHTRelationship
        [HttpGet]
        public async Task<ActionResult<IEnumerable<zzVHTRelationship>>> GetzzVHTRelationship()
        {
            return await _context.zzVHTRelationship.ToListAsync();
        }

        // GET: api/zzVHTRelationship/5
        [HttpGet("{id}")]
        public async Task<ActionResult<zzVHTRelationship>> GetzzVHTRelationship(int id)
        {
            var zzVHTRelationship = await _context.zzVHTRelationship.FindAsync(id);

            if (zzVHTRelationship == null)
            {
                return NotFound();
            }

            return zzVHTRelationship;
        }

        // PUT: api/zzVHTRelationship/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutzzVHTRelationship(int id, zzVHTRelationship zzVHTRelationship)
        {
            if (id != zzVHTRelationship.VHTRelationshipID)
            {
                return BadRequest();
            }

            _context.Entry(zzVHTRelationship).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!zzVHTRelationshipExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/zzVHTRelationship
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<zzVHTRelationship>> PostzzVHTRelationship(zzVHTRelationship zzVHTRelationship)
        {
            _context.zzVHTRelationship.Add(zzVHTRelationship);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetzzVHTRelationship", new { id = zzVHTRelationship.VHTRelationshipID }, zzVHTRelationship);
        }

        // DELETE: api/zzVHTRelationship/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletezzVHTRelationship(int id)
        {
            var zzVHTRelationship = await _context.zzVHTRelationship.FindAsync(id);
            if (zzVHTRelationship == null)
            {
                return NotFound();
            }

            _context.zzVHTRelationship.Remove(zzVHTRelationship);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool zzVHTRelationshipExists(int id)
        {
            return _context.zzVHTRelationship.Any(e => e.VHTRelationshipID == id);
        }
    }
}
